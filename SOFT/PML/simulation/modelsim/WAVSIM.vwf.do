vcom -work work WAVSIM.vwf.vht
vsim -voptargs=+acc -c -t 1ps -L maxii -L altera -L altera_mf -L 220model -L sgate -L altera_lnsim work.PML_vhd_vec_tst -voptargs="+acc"
add wave /*
run -all
