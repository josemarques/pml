-- Copyright (C) 2022  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 21.1.1 Build 850 06/23/2022 SJ Lite Edition"

-- DATE "10/27/2022 21:53:42"

-- 
-- Device: Altera EPM1270T144C5 Package TQFP144
-- 

-- 
-- This VHDL file should be used for Questa Intel FPGA (VHDL) only
-- 

LIBRARY IEEE;
LIBRARY MAXII;
USE IEEE.STD_LOGIC_1164.ALL;
USE MAXII.MAXII_COMPONENTS.ALL;

ENTITY 	PML IS
    PORT (
	PHSSIGN : OUT std_logic;
	FRQ0 : IN std_logic;
	FRQ1 : IN std_logic;
	PHSSIG : OUT std_logic;
	PHASE : OUT std_logic_vector(31 DOWNTO 0);
	PRECOUNT : OUT std_logic_vector(15 DOWNTO 0)
	);
END PML;

-- Design Ports Information


ARCHITECTURE structure OF PML IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_PHSSIGN : std_logic;
SIGNAL ww_FRQ0 : std_logic;
SIGNAL ww_FRQ1 : std_logic;
SIGNAL ww_PHSSIG : std_logic;
SIGNAL ww_PHASE : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_PRECOUNT : std_logic_vector(15 DOWNTO 0);
SIGNAL \FRQ0~combout\ : std_logic;
SIGNAL \FRQ1~combout\ : std_logic;
SIGNAL \inst21|TMPNEG~regout\ : std_logic;
SIGNAL \inst1|TSUM[0][30]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][29]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][24]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][19]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][14]~combout\ : std_logic;
SIGNAL \inst7|DIVD~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][0]~317\ : std_logic;
SIGNAL \inst1|PSUM[1][0]~317COUT1_433\ : std_logic;
SIGNAL \inst1|PSUM[1][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][1]~313\ : std_logic;
SIGNAL \inst1|PSUM[1][1]~313COUT1_434\ : std_logic;
SIGNAL \inst1|PSUM[1][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][2]~309\ : std_logic;
SIGNAL \inst1|PSUM[1][2]~309COUT1_435\ : std_logic;
SIGNAL \inst1|PSUM[1][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][3]~305\ : std_logic;
SIGNAL \inst1|PSUM[1][3]~305COUT1_436\ : std_logic;
SIGNAL \inst1|PSUM[1][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][4]~301\ : std_logic;
SIGNAL \inst1|PSUM[1][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][5]~297\ : std_logic;
SIGNAL \inst1|PSUM[1][5]~297COUT1_437\ : std_logic;
SIGNAL \inst1|PSUM[1][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][6]~293\ : std_logic;
SIGNAL \inst1|PSUM[1][6]~293COUT1_438\ : std_logic;
SIGNAL \inst1|PSUM[1][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][7]~289\ : std_logic;
SIGNAL \inst1|PSUM[1][7]~289COUT1_439\ : std_logic;
SIGNAL \inst1|PSUM[1][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[1][8]~285\ : std_logic;
SIGNAL \inst1|PSUM[1][8]~285COUT1_440\ : std_logic;
SIGNAL \inst1|PSUM[1][9]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][0]~319\ : std_logic;
SIGNAL \inst1|PSUM[0][0]~319COUT1_441\ : std_logic;
SIGNAL \inst1|PSUM[0][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][1]~315\ : std_logic;
SIGNAL \inst1|PSUM[0][1]~315COUT1_442\ : std_logic;
SIGNAL \inst1|PSUM[0][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][2]~311\ : std_logic;
SIGNAL \inst1|PSUM[0][2]~311COUT1_443\ : std_logic;
SIGNAL \inst1|PSUM[0][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][3]~307\ : std_logic;
SIGNAL \inst1|PSUM[0][3]~307COUT1_444\ : std_logic;
SIGNAL \inst1|PSUM[0][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][4]~303\ : std_logic;
SIGNAL \inst1|PSUM[0][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][5]~299\ : std_logic;
SIGNAL \inst1|PSUM[0][5]~299COUT1_445\ : std_logic;
SIGNAL \inst1|PSUM[0][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][6]~295\ : std_logic;
SIGNAL \inst1|PSUM[0][6]~295COUT1_446\ : std_logic;
SIGNAL \inst1|PSUM[0][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][7]~291\ : std_logic;
SIGNAL \inst1|PSUM[0][7]~291COUT1_447\ : std_logic;
SIGNAL \inst1|PSUM[0][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[0][8]~287\ : std_logic;
SIGNAL \inst1|PSUM[0][8]~287COUT1_448\ : std_logic;
SIGNAL \inst1|PSUM[0][9]~regout\ : std_logic;
SIGNAL \inst1|Add0~47\ : std_logic;
SIGNAL \inst1|Add0~47COUT1_51\ : std_logic;
SIGNAL \inst1|Add0~42\ : std_logic;
SIGNAL \inst1|Add0~42COUT1_52\ : std_logic;
SIGNAL \inst1|Add0~37\ : std_logic;
SIGNAL \inst1|Add0~37COUT1_53\ : std_logic;
SIGNAL \inst1|Add0~32\ : std_logic;
SIGNAL \inst1|Add0~32COUT1_54\ : std_logic;
SIGNAL \inst1|Add0~27\ : std_logic;
SIGNAL \inst1|Add0~22\ : std_logic;
SIGNAL \inst1|Add0~22COUT1_55\ : std_logic;
SIGNAL \inst1|Add0~17\ : std_logic;
SIGNAL \inst1|Add0~17COUT1_56\ : std_logic;
SIGNAL \inst1|Add0~12\ : std_logic;
SIGNAL \inst1|Add0~12COUT1_57\ : std_logic;
SIGNAL \inst1|Add0~7\ : std_logic;
SIGNAL \inst1|Add0~7COUT1_58\ : std_logic;
SIGNAL \inst1|Add0~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][9]~combout\ : std_logic;
SIGNAL \inst1|PSUM[2][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][0]~279\ : std_logic;
SIGNAL \inst1|PSUM[2][0]~279COUT1_425\ : std_logic;
SIGNAL \inst1|PSUM[2][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][1]~277\ : std_logic;
SIGNAL \inst1|PSUM[2][1]~277COUT1_426\ : std_logic;
SIGNAL \inst1|PSUM[2][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][2]~275\ : std_logic;
SIGNAL \inst1|PSUM[2][2]~275COUT1_427\ : std_logic;
SIGNAL \inst1|PSUM[2][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][3]~273\ : std_logic;
SIGNAL \inst1|PSUM[2][3]~273COUT1_428\ : std_logic;
SIGNAL \inst1|PSUM[2][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][4]~271\ : std_logic;
SIGNAL \inst1|PSUM[2][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][5]~269\ : std_logic;
SIGNAL \inst1|PSUM[2][5]~269COUT1_429\ : std_logic;
SIGNAL \inst1|PSUM[2][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][6]~267\ : std_logic;
SIGNAL \inst1|PSUM[2][6]~267COUT1_430\ : std_logic;
SIGNAL \inst1|PSUM[2][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][7]~265\ : std_logic;
SIGNAL \inst1|PSUM[2][7]~265COUT1_431\ : std_logic;
SIGNAL \inst1|PSUM[2][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[2][8]~263\ : std_logic;
SIGNAL \inst1|PSUM[2][8]~263COUT1_432\ : std_logic;
SIGNAL \inst1|PSUM[2][9]~regout\ : std_logic;
SIGNAL \inst1|Add0~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][4]~combout\ : std_logic;
SIGNAL \inst1|Add0~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][3]~combout\ : std_logic;
SIGNAL \inst1|Add0~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][2]~combout\ : std_logic;
SIGNAL \inst1|Add0~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][1]~combout\ : std_logic;
SIGNAL \inst1|Add0~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][0]~combout\ : std_logic;
SIGNAL \inst1|Add1~152\ : std_logic;
SIGNAL \inst1|Add1~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add1~147\ : std_logic;
SIGNAL \inst1|Add1~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add1~142\ : std_logic;
SIGNAL \inst1|Add1~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add1~137\ : std_logic;
SIGNAL \inst1|Add1~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add1~132\ : std_logic;
SIGNAL \inst1|Add0~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][8]~combout\ : std_logic;
SIGNAL \inst1|Add0~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][7]~combout\ : std_logic;
SIGNAL \inst1|Add0~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][6]~combout\ : std_logic;
SIGNAL \inst1|Add0~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[0][5]~combout\ : std_logic;
SIGNAL \inst1|Add1~127\ : std_logic;
SIGNAL \inst1|Add1~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add1~122\ : std_logic;
SIGNAL \inst1|Add1~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add1~117\ : std_logic;
SIGNAL \inst1|Add1~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add1~112\ : std_logic;
SIGNAL \inst1|Add1~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add1~107\ : std_logic;
SIGNAL \inst1|TSUM[0][13]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][12]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][11]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][10]~combout\ : std_logic;
SIGNAL \inst1|Add1~102\ : std_logic;
SIGNAL \inst1|Add1~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add1~97\ : std_logic;
SIGNAL \inst1|Add1~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add1~92\ : std_logic;
SIGNAL \inst1|Add1~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add1~87\ : std_logic;
SIGNAL \inst1|Add1~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add1~82\ : std_logic;
SIGNAL \inst1|TSUM[0][18]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][17]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][16]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][15]~combout\ : std_logic;
SIGNAL \inst1|Add1~77\ : std_logic;
SIGNAL \inst1|Add1~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add1~72\ : std_logic;
SIGNAL \inst1|Add1~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add1~67\ : std_logic;
SIGNAL \inst1|Add1~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add1~62\ : std_logic;
SIGNAL \inst1|Add1~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add1~57\ : std_logic;
SIGNAL \inst1|TSUM[0][23]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][22]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][21]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][20]~combout\ : std_logic;
SIGNAL \inst1|Add1~52\ : std_logic;
SIGNAL \inst1|Add1~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add1~47\ : std_logic;
SIGNAL \inst1|Add1~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add1~42\ : std_logic;
SIGNAL \inst1|Add1~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add1~37\ : std_logic;
SIGNAL \inst1|Add1~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add1~32\ : std_logic;
SIGNAL \inst1|TSUM[0][28]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][27]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][26]~combout\ : std_logic;
SIGNAL \inst1|TSUM[0][25]~combout\ : std_logic;
SIGNAL \inst1|Add1~27\ : std_logic;
SIGNAL \inst1|Add1~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add1~22\ : std_logic;
SIGNAL \inst1|Add1~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add1~17\ : std_logic;
SIGNAL \inst1|Add1~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add1~12\ : std_logic;
SIGNAL \inst1|Add1~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add1~7\ : std_logic;
SIGNAL \inst1|Add1~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][30]~combout\ : std_logic;
SIGNAL \inst1|Add1~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][29]~combout\ : std_logic;
SIGNAL \inst1|Add1~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][24]~combout\ : std_logic;
SIGNAL \inst1|Add1~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][19]~combout\ : std_logic;
SIGNAL \inst1|Add1~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][14]~combout\ : std_logic;
SIGNAL \inst1|Add1~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][9]~combout\ : std_logic;
SIGNAL \inst1|PSUM[3][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][0]~259\ : std_logic;
SIGNAL \inst1|PSUM[3][0]~259COUT1_417\ : std_logic;
SIGNAL \inst1|PSUM[3][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][1]~257\ : std_logic;
SIGNAL \inst1|PSUM[3][1]~257COUT1_418\ : std_logic;
SIGNAL \inst1|PSUM[3][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][2]~255\ : std_logic;
SIGNAL \inst1|PSUM[3][2]~255COUT1_419\ : std_logic;
SIGNAL \inst1|PSUM[3][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][3]~253\ : std_logic;
SIGNAL \inst1|PSUM[3][3]~253COUT1_420\ : std_logic;
SIGNAL \inst1|PSUM[3][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][4]~251\ : std_logic;
SIGNAL \inst1|PSUM[3][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][5]~249\ : std_logic;
SIGNAL \inst1|PSUM[3][5]~249COUT1_421\ : std_logic;
SIGNAL \inst1|PSUM[3][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][6]~247\ : std_logic;
SIGNAL \inst1|PSUM[3][6]~247COUT1_422\ : std_logic;
SIGNAL \inst1|PSUM[3][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][7]~245\ : std_logic;
SIGNAL \inst1|PSUM[3][7]~245COUT1_423\ : std_logic;
SIGNAL \inst1|PSUM[3][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[3][8]~243\ : std_logic;
SIGNAL \inst1|PSUM[3][8]~243COUT1_424\ : std_logic;
SIGNAL \inst1|PSUM[3][9]~regout\ : std_logic;
SIGNAL \inst1|Add1~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][4]~combout\ : std_logic;
SIGNAL \inst1|Add1~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][3]~combout\ : std_logic;
SIGNAL \inst1|Add1~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][2]~combout\ : std_logic;
SIGNAL \inst1|Add1~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][1]~combout\ : std_logic;
SIGNAL \inst1|Add1~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][0]~combout\ : std_logic;
SIGNAL \inst1|Add2~152\ : std_logic;
SIGNAL \inst1|Add2~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add2~147\ : std_logic;
SIGNAL \inst1|Add2~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add2~142\ : std_logic;
SIGNAL \inst1|Add2~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add2~137\ : std_logic;
SIGNAL \inst1|Add2~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add2~132\ : std_logic;
SIGNAL \inst1|Add1~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][8]~combout\ : std_logic;
SIGNAL \inst1|Add1~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][7]~combout\ : std_logic;
SIGNAL \inst1|Add1~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][6]~combout\ : std_logic;
SIGNAL \inst1|Add1~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][5]~combout\ : std_logic;
SIGNAL \inst1|Add2~127\ : std_logic;
SIGNAL \inst1|Add2~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add2~122\ : std_logic;
SIGNAL \inst1|Add2~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add2~117\ : std_logic;
SIGNAL \inst1|Add2~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add2~112\ : std_logic;
SIGNAL \inst1|Add2~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add2~107\ : std_logic;
SIGNAL \inst1|Add1~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][13]~combout\ : std_logic;
SIGNAL \inst1|Add1~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][12]~combout\ : std_logic;
SIGNAL \inst1|Add1~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][11]~combout\ : std_logic;
SIGNAL \inst1|Add1~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][10]~combout\ : std_logic;
SIGNAL \inst1|Add2~102\ : std_logic;
SIGNAL \inst1|Add2~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add2~97\ : std_logic;
SIGNAL \inst1|Add2~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add2~92\ : std_logic;
SIGNAL \inst1|Add2~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add2~87\ : std_logic;
SIGNAL \inst1|Add2~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add2~82\ : std_logic;
SIGNAL \inst1|Add1~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][18]~combout\ : std_logic;
SIGNAL \inst1|Add1~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][17]~combout\ : std_logic;
SIGNAL \inst1|Add1~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][16]~combout\ : std_logic;
SIGNAL \inst1|Add1~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][15]~combout\ : std_logic;
SIGNAL \inst1|Add2~77\ : std_logic;
SIGNAL \inst1|Add2~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add2~72\ : std_logic;
SIGNAL \inst1|Add2~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add2~67\ : std_logic;
SIGNAL \inst1|Add2~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add2~62\ : std_logic;
SIGNAL \inst1|Add2~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add2~57\ : std_logic;
SIGNAL \inst1|Add1~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][23]~combout\ : std_logic;
SIGNAL \inst1|Add1~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][22]~combout\ : std_logic;
SIGNAL \inst1|Add1~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][21]~combout\ : std_logic;
SIGNAL \inst1|Add1~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][20]~combout\ : std_logic;
SIGNAL \inst1|Add2~52\ : std_logic;
SIGNAL \inst1|Add2~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add2~47\ : std_logic;
SIGNAL \inst1|Add2~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add2~42\ : std_logic;
SIGNAL \inst1|Add2~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add2~37\ : std_logic;
SIGNAL \inst1|Add2~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add2~32\ : std_logic;
SIGNAL \inst1|Add1~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][28]~combout\ : std_logic;
SIGNAL \inst1|Add1~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][27]~combout\ : std_logic;
SIGNAL \inst1|Add1~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][26]~combout\ : std_logic;
SIGNAL \inst1|Add1~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[1][25]~combout\ : std_logic;
SIGNAL \inst1|Add2~27\ : std_logic;
SIGNAL \inst1|Add2~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add2~22\ : std_logic;
SIGNAL \inst1|Add2~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add2~17\ : std_logic;
SIGNAL \inst1|Add2~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add2~12\ : std_logic;
SIGNAL \inst1|Add2~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add2~7\ : std_logic;
SIGNAL \inst1|Add2~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][30]~combout\ : std_logic;
SIGNAL \inst1|Add2~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][29]~combout\ : std_logic;
SIGNAL \inst1|Add2~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][24]~combout\ : std_logic;
SIGNAL \inst1|Add2~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][19]~combout\ : std_logic;
SIGNAL \inst1|Add2~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[4][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][0]~239\ : std_logic;
SIGNAL \inst1|PSUM[4][0]~239COUT1_409\ : std_logic;
SIGNAL \inst1|PSUM[4][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][1]~237\ : std_logic;
SIGNAL \inst1|PSUM[4][1]~237COUT1_410\ : std_logic;
SIGNAL \inst1|PSUM[4][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][2]~235\ : std_logic;
SIGNAL \inst1|PSUM[4][2]~235COUT1_411\ : std_logic;
SIGNAL \inst1|PSUM[4][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][3]~233\ : std_logic;
SIGNAL \inst1|PSUM[4][3]~233COUT1_412\ : std_logic;
SIGNAL \inst1|PSUM[4][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][4]~231\ : std_logic;
SIGNAL \inst1|PSUM[4][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][5]~229\ : std_logic;
SIGNAL \inst1|PSUM[4][5]~229COUT1_413\ : std_logic;
SIGNAL \inst1|PSUM[4][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][6]~227\ : std_logic;
SIGNAL \inst1|PSUM[4][6]~227COUT1_414\ : std_logic;
SIGNAL \inst1|PSUM[4][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][7]~225\ : std_logic;
SIGNAL \inst1|PSUM[4][7]~225COUT1_415\ : std_logic;
SIGNAL \inst1|PSUM[4][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[4][8]~223\ : std_logic;
SIGNAL \inst1|PSUM[4][8]~223COUT1_416\ : std_logic;
SIGNAL \inst1|PSUM[4][9]~regout\ : std_logic;
SIGNAL \inst1|Add2~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][9]~combout\ : std_logic;
SIGNAL \inst1|Add2~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][4]~combout\ : std_logic;
SIGNAL \inst1|Add2~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][3]~combout\ : std_logic;
SIGNAL \inst1|Add2~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][2]~combout\ : std_logic;
SIGNAL \inst1|Add2~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][1]~combout\ : std_logic;
SIGNAL \inst1|Add2~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][0]~combout\ : std_logic;
SIGNAL \inst1|Add3~152\ : std_logic;
SIGNAL \inst1|Add3~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add3~147\ : std_logic;
SIGNAL \inst1|Add3~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add3~142\ : std_logic;
SIGNAL \inst1|Add3~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add3~137\ : std_logic;
SIGNAL \inst1|Add3~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add3~132\ : std_logic;
SIGNAL \inst1|Add2~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][8]~combout\ : std_logic;
SIGNAL \inst1|Add2~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][7]~combout\ : std_logic;
SIGNAL \inst1|Add2~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][6]~combout\ : std_logic;
SIGNAL \inst1|Add2~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][5]~combout\ : std_logic;
SIGNAL \inst1|Add3~127\ : std_logic;
SIGNAL \inst1|Add3~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add3~122\ : std_logic;
SIGNAL \inst1|Add3~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add3~117\ : std_logic;
SIGNAL \inst1|Add3~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add3~112\ : std_logic;
SIGNAL \inst1|Add3~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add3~107\ : std_logic;
SIGNAL \inst1|Add2~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][13]~combout\ : std_logic;
SIGNAL \inst1|Add2~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][12]~combout\ : std_logic;
SIGNAL \inst1|Add2~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][11]~combout\ : std_logic;
SIGNAL \inst1|Add2~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][10]~combout\ : std_logic;
SIGNAL \inst1|Add3~102\ : std_logic;
SIGNAL \inst1|Add3~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add3~97\ : std_logic;
SIGNAL \inst1|Add3~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add3~92\ : std_logic;
SIGNAL \inst1|Add3~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add3~87\ : std_logic;
SIGNAL \inst1|Add3~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add3~82\ : std_logic;
SIGNAL \inst1|Add2~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][18]~combout\ : std_logic;
SIGNAL \inst1|Add2~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][17]~combout\ : std_logic;
SIGNAL \inst1|Add2~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][16]~combout\ : std_logic;
SIGNAL \inst1|Add2~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][15]~combout\ : std_logic;
SIGNAL \inst1|Add3~77\ : std_logic;
SIGNAL \inst1|Add3~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add3~72\ : std_logic;
SIGNAL \inst1|Add3~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add3~67\ : std_logic;
SIGNAL \inst1|Add3~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add3~62\ : std_logic;
SIGNAL \inst1|Add3~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add3~57\ : std_logic;
SIGNAL \inst1|Add2~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][23]~combout\ : std_logic;
SIGNAL \inst1|Add2~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][22]~combout\ : std_logic;
SIGNAL \inst1|Add2~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][21]~combout\ : std_logic;
SIGNAL \inst1|Add2~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][20]~combout\ : std_logic;
SIGNAL \inst1|Add3~52\ : std_logic;
SIGNAL \inst1|Add3~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add3~47\ : std_logic;
SIGNAL \inst1|Add3~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add3~42\ : std_logic;
SIGNAL \inst1|Add3~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add3~37\ : std_logic;
SIGNAL \inst1|Add3~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add3~32\ : std_logic;
SIGNAL \inst1|Add2~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][28]~combout\ : std_logic;
SIGNAL \inst1|Add2~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][27]~combout\ : std_logic;
SIGNAL \inst1|Add2~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][26]~combout\ : std_logic;
SIGNAL \inst1|Add2~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[2][25]~combout\ : std_logic;
SIGNAL \inst1|Add3~27\ : std_logic;
SIGNAL \inst1|Add3~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add3~22\ : std_logic;
SIGNAL \inst1|Add3~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add3~17\ : std_logic;
SIGNAL \inst1|Add3~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add3~12\ : std_logic;
SIGNAL \inst1|Add3~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add3~7\ : std_logic;
SIGNAL \inst1|Add3~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][30]~combout\ : std_logic;
SIGNAL \inst1|Add3~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][29]~combout\ : std_logic;
SIGNAL \inst1|Add3~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][24]~combout\ : std_logic;
SIGNAL \inst1|Add3~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][19]~combout\ : std_logic;
SIGNAL \inst1|Add3~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[5][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][0]~219\ : std_logic;
SIGNAL \inst1|PSUM[5][0]~219COUT1_401\ : std_logic;
SIGNAL \inst1|PSUM[5][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][1]~217\ : std_logic;
SIGNAL \inst1|PSUM[5][1]~217COUT1_402\ : std_logic;
SIGNAL \inst1|PSUM[5][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][2]~215\ : std_logic;
SIGNAL \inst1|PSUM[5][2]~215COUT1_403\ : std_logic;
SIGNAL \inst1|PSUM[5][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][3]~213\ : std_logic;
SIGNAL \inst1|PSUM[5][3]~213COUT1_404\ : std_logic;
SIGNAL \inst1|PSUM[5][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][4]~211\ : std_logic;
SIGNAL \inst1|PSUM[5][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][5]~209\ : std_logic;
SIGNAL \inst1|PSUM[5][5]~209COUT1_405\ : std_logic;
SIGNAL \inst1|PSUM[5][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][6]~207\ : std_logic;
SIGNAL \inst1|PSUM[5][6]~207COUT1_406\ : std_logic;
SIGNAL \inst1|PSUM[5][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][7]~205\ : std_logic;
SIGNAL \inst1|PSUM[5][7]~205COUT1_407\ : std_logic;
SIGNAL \inst1|PSUM[5][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[5][8]~203\ : std_logic;
SIGNAL \inst1|PSUM[5][8]~203COUT1_408\ : std_logic;
SIGNAL \inst1|PSUM[5][9]~regout\ : std_logic;
SIGNAL \inst1|Add3~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][9]~combout\ : std_logic;
SIGNAL \inst1|Add3~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][4]~combout\ : std_logic;
SIGNAL \inst1|Add3~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][3]~combout\ : std_logic;
SIGNAL \inst1|Add3~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][2]~combout\ : std_logic;
SIGNAL \inst1|Add3~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][1]~combout\ : std_logic;
SIGNAL \inst1|Add3~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][0]~combout\ : std_logic;
SIGNAL \inst1|Add4~152\ : std_logic;
SIGNAL \inst1|Add4~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add4~147\ : std_logic;
SIGNAL \inst1|Add4~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add4~142\ : std_logic;
SIGNAL \inst1|Add4~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add4~137\ : std_logic;
SIGNAL \inst1|Add4~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add4~132\ : std_logic;
SIGNAL \inst1|Add3~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][8]~combout\ : std_logic;
SIGNAL \inst1|Add3~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][7]~combout\ : std_logic;
SIGNAL \inst1|Add3~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][6]~combout\ : std_logic;
SIGNAL \inst1|Add3~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][5]~combout\ : std_logic;
SIGNAL \inst1|Add4~127\ : std_logic;
SIGNAL \inst1|Add4~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add4~122\ : std_logic;
SIGNAL \inst1|Add4~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add4~117\ : std_logic;
SIGNAL \inst1|Add4~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add4~112\ : std_logic;
SIGNAL \inst1|Add4~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add4~107\ : std_logic;
SIGNAL \inst1|Add3~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][13]~combout\ : std_logic;
SIGNAL \inst1|Add3~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][12]~combout\ : std_logic;
SIGNAL \inst1|Add3~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][11]~combout\ : std_logic;
SIGNAL \inst1|Add3~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][10]~combout\ : std_logic;
SIGNAL \inst1|Add4~102\ : std_logic;
SIGNAL \inst1|Add4~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add4~97\ : std_logic;
SIGNAL \inst1|Add4~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add4~92\ : std_logic;
SIGNAL \inst1|Add4~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add4~87\ : std_logic;
SIGNAL \inst1|Add4~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add4~82\ : std_logic;
SIGNAL \inst1|Add3~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][18]~combout\ : std_logic;
SIGNAL \inst1|Add3~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][17]~combout\ : std_logic;
SIGNAL \inst1|Add3~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][16]~combout\ : std_logic;
SIGNAL \inst1|Add3~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][15]~combout\ : std_logic;
SIGNAL \inst1|Add4~77\ : std_logic;
SIGNAL \inst1|Add4~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add4~72\ : std_logic;
SIGNAL \inst1|Add4~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add4~67\ : std_logic;
SIGNAL \inst1|Add4~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add4~62\ : std_logic;
SIGNAL \inst1|Add4~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add4~57\ : std_logic;
SIGNAL \inst1|Add3~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][23]~combout\ : std_logic;
SIGNAL \inst1|Add3~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][22]~combout\ : std_logic;
SIGNAL \inst1|Add3~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][21]~combout\ : std_logic;
SIGNAL \inst1|Add3~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][20]~combout\ : std_logic;
SIGNAL \inst1|Add4~52\ : std_logic;
SIGNAL \inst1|Add4~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add4~47\ : std_logic;
SIGNAL \inst1|Add4~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add4~42\ : std_logic;
SIGNAL \inst1|Add4~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add4~37\ : std_logic;
SIGNAL \inst1|Add4~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add4~32\ : std_logic;
SIGNAL \inst1|Add3~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][28]~combout\ : std_logic;
SIGNAL \inst1|Add3~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][27]~combout\ : std_logic;
SIGNAL \inst1|Add3~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][26]~combout\ : std_logic;
SIGNAL \inst1|Add3~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[3][25]~combout\ : std_logic;
SIGNAL \inst1|Add4~27\ : std_logic;
SIGNAL \inst1|Add4~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add4~22\ : std_logic;
SIGNAL \inst1|Add4~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add4~17\ : std_logic;
SIGNAL \inst1|Add4~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add4~12\ : std_logic;
SIGNAL \inst1|Add4~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add4~7\ : std_logic;
SIGNAL \inst1|Add4~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][30]~combout\ : std_logic;
SIGNAL \inst1|Add4~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][29]~combout\ : std_logic;
SIGNAL \inst1|Add4~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][24]~combout\ : std_logic;
SIGNAL \inst1|Add4~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][19]~combout\ : std_logic;
SIGNAL \inst1|Add4~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[6][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][0]~199\ : std_logic;
SIGNAL \inst1|PSUM[6][0]~199COUT1_393\ : std_logic;
SIGNAL \inst1|PSUM[6][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][1]~197\ : std_logic;
SIGNAL \inst1|PSUM[6][1]~197COUT1_394\ : std_logic;
SIGNAL \inst1|PSUM[6][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][2]~195\ : std_logic;
SIGNAL \inst1|PSUM[6][2]~195COUT1_395\ : std_logic;
SIGNAL \inst1|PSUM[6][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][3]~193\ : std_logic;
SIGNAL \inst1|PSUM[6][3]~193COUT1_396\ : std_logic;
SIGNAL \inst1|PSUM[6][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][4]~191\ : std_logic;
SIGNAL \inst1|PSUM[6][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][5]~189\ : std_logic;
SIGNAL \inst1|PSUM[6][5]~189COUT1_397\ : std_logic;
SIGNAL \inst1|PSUM[6][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][6]~187\ : std_logic;
SIGNAL \inst1|PSUM[6][6]~187COUT1_398\ : std_logic;
SIGNAL \inst1|PSUM[6][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][7]~185\ : std_logic;
SIGNAL \inst1|PSUM[6][7]~185COUT1_399\ : std_logic;
SIGNAL \inst1|PSUM[6][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[6][8]~183\ : std_logic;
SIGNAL \inst1|PSUM[6][8]~183COUT1_400\ : std_logic;
SIGNAL \inst1|PSUM[6][9]~regout\ : std_logic;
SIGNAL \inst1|Add4~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][9]~combout\ : std_logic;
SIGNAL \inst1|Add4~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][4]~combout\ : std_logic;
SIGNAL \inst1|Add4~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][3]~combout\ : std_logic;
SIGNAL \inst1|Add4~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][2]~combout\ : std_logic;
SIGNAL \inst1|Add4~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][1]~combout\ : std_logic;
SIGNAL \inst1|Add4~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][0]~combout\ : std_logic;
SIGNAL \inst1|Add5~152\ : std_logic;
SIGNAL \inst1|Add5~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add5~147\ : std_logic;
SIGNAL \inst1|Add5~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add5~142\ : std_logic;
SIGNAL \inst1|Add5~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add5~137\ : std_logic;
SIGNAL \inst1|Add5~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add5~132\ : std_logic;
SIGNAL \inst1|Add4~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][8]~combout\ : std_logic;
SIGNAL \inst1|Add4~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][7]~combout\ : std_logic;
SIGNAL \inst1|Add4~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][6]~combout\ : std_logic;
SIGNAL \inst1|Add4~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][5]~combout\ : std_logic;
SIGNAL \inst1|Add5~127\ : std_logic;
SIGNAL \inst1|Add5~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add5~122\ : std_logic;
SIGNAL \inst1|Add5~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add5~117\ : std_logic;
SIGNAL \inst1|Add5~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add5~112\ : std_logic;
SIGNAL \inst1|Add5~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add5~107\ : std_logic;
SIGNAL \inst1|Add4~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][13]~combout\ : std_logic;
SIGNAL \inst1|Add4~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][12]~combout\ : std_logic;
SIGNAL \inst1|Add4~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][11]~combout\ : std_logic;
SIGNAL \inst1|Add4~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][10]~combout\ : std_logic;
SIGNAL \inst1|Add5~102\ : std_logic;
SIGNAL \inst1|Add5~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add5~97\ : std_logic;
SIGNAL \inst1|Add5~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add5~92\ : std_logic;
SIGNAL \inst1|Add5~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add5~87\ : std_logic;
SIGNAL \inst1|Add5~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add5~82\ : std_logic;
SIGNAL \inst1|Add4~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][18]~combout\ : std_logic;
SIGNAL \inst1|Add4~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][17]~combout\ : std_logic;
SIGNAL \inst1|Add4~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][16]~combout\ : std_logic;
SIGNAL \inst1|Add4~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][15]~combout\ : std_logic;
SIGNAL \inst1|Add5~77\ : std_logic;
SIGNAL \inst1|Add5~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add5~72\ : std_logic;
SIGNAL \inst1|Add5~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add5~67\ : std_logic;
SIGNAL \inst1|Add5~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add5~62\ : std_logic;
SIGNAL \inst1|Add5~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add5~57\ : std_logic;
SIGNAL \inst1|Add4~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][23]~combout\ : std_logic;
SIGNAL \inst1|Add4~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][22]~combout\ : std_logic;
SIGNAL \inst1|Add4~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][21]~combout\ : std_logic;
SIGNAL \inst1|Add4~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][20]~combout\ : std_logic;
SIGNAL \inst1|Add5~52\ : std_logic;
SIGNAL \inst1|Add5~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add5~47\ : std_logic;
SIGNAL \inst1|Add5~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add5~42\ : std_logic;
SIGNAL \inst1|Add5~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add5~37\ : std_logic;
SIGNAL \inst1|Add5~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add5~32\ : std_logic;
SIGNAL \inst1|Add4~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][28]~combout\ : std_logic;
SIGNAL \inst1|Add4~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][27]~combout\ : std_logic;
SIGNAL \inst1|Add4~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][26]~combout\ : std_logic;
SIGNAL \inst1|Add4~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[4][25]~combout\ : std_logic;
SIGNAL \inst1|Add5~27\ : std_logic;
SIGNAL \inst1|Add5~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add5~22\ : std_logic;
SIGNAL \inst1|Add5~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add5~17\ : std_logic;
SIGNAL \inst1|Add5~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add5~12\ : std_logic;
SIGNAL \inst1|Add5~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add5~7\ : std_logic;
SIGNAL \inst1|Add5~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][30]~combout\ : std_logic;
SIGNAL \inst1|Add5~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][29]~combout\ : std_logic;
SIGNAL \inst1|Add5~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][24]~combout\ : std_logic;
SIGNAL \inst1|Add5~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][19]~combout\ : std_logic;
SIGNAL \inst1|Add5~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][14]~combout\ : std_logic;
SIGNAL \inst1|Add5~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][9]~combout\ : std_logic;
SIGNAL \inst1|PSUM[7][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][0]~179\ : std_logic;
SIGNAL \inst1|PSUM[7][0]~179COUT1_385\ : std_logic;
SIGNAL \inst1|PSUM[7][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][1]~177\ : std_logic;
SIGNAL \inst1|PSUM[7][1]~177COUT1_386\ : std_logic;
SIGNAL \inst1|PSUM[7][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][2]~175\ : std_logic;
SIGNAL \inst1|PSUM[7][2]~175COUT1_387\ : std_logic;
SIGNAL \inst1|PSUM[7][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][3]~173\ : std_logic;
SIGNAL \inst1|PSUM[7][3]~173COUT1_388\ : std_logic;
SIGNAL \inst1|PSUM[7][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][4]~171\ : std_logic;
SIGNAL \inst1|PSUM[7][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][5]~169\ : std_logic;
SIGNAL \inst1|PSUM[7][5]~169COUT1_389\ : std_logic;
SIGNAL \inst1|PSUM[7][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][6]~167\ : std_logic;
SIGNAL \inst1|PSUM[7][6]~167COUT1_390\ : std_logic;
SIGNAL \inst1|PSUM[7][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][7]~165\ : std_logic;
SIGNAL \inst1|PSUM[7][7]~165COUT1_391\ : std_logic;
SIGNAL \inst1|PSUM[7][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[7][8]~163\ : std_logic;
SIGNAL \inst1|PSUM[7][8]~163COUT1_392\ : std_logic;
SIGNAL \inst1|PSUM[7][9]~regout\ : std_logic;
SIGNAL \inst1|Add5~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][4]~combout\ : std_logic;
SIGNAL \inst1|Add5~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][3]~combout\ : std_logic;
SIGNAL \inst1|Add5~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][2]~combout\ : std_logic;
SIGNAL \inst1|Add5~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][1]~combout\ : std_logic;
SIGNAL \inst1|Add5~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][0]~combout\ : std_logic;
SIGNAL \inst1|Add6~152\ : std_logic;
SIGNAL \inst1|Add6~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add6~147\ : std_logic;
SIGNAL \inst1|Add6~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add6~142\ : std_logic;
SIGNAL \inst1|Add6~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add6~137\ : std_logic;
SIGNAL \inst1|Add6~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add6~132\ : std_logic;
SIGNAL \inst1|Add5~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][8]~combout\ : std_logic;
SIGNAL \inst1|Add5~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][7]~combout\ : std_logic;
SIGNAL \inst1|Add5~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][6]~combout\ : std_logic;
SIGNAL \inst1|Add5~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][5]~combout\ : std_logic;
SIGNAL \inst1|Add6~127\ : std_logic;
SIGNAL \inst1|Add6~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add6~122\ : std_logic;
SIGNAL \inst1|Add6~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add6~117\ : std_logic;
SIGNAL \inst1|Add6~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add6~112\ : std_logic;
SIGNAL \inst1|Add6~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add6~107\ : std_logic;
SIGNAL \inst1|Add5~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][13]~combout\ : std_logic;
SIGNAL \inst1|Add5~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][12]~combout\ : std_logic;
SIGNAL \inst1|Add5~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][11]~combout\ : std_logic;
SIGNAL \inst1|Add5~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][10]~combout\ : std_logic;
SIGNAL \inst1|Add6~102\ : std_logic;
SIGNAL \inst1|Add6~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add6~97\ : std_logic;
SIGNAL \inst1|Add6~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add6~92\ : std_logic;
SIGNAL \inst1|Add6~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add6~87\ : std_logic;
SIGNAL \inst1|Add6~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add6~82\ : std_logic;
SIGNAL \inst1|Add5~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][18]~combout\ : std_logic;
SIGNAL \inst1|Add5~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][17]~combout\ : std_logic;
SIGNAL \inst1|Add5~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][16]~combout\ : std_logic;
SIGNAL \inst1|Add5~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][15]~combout\ : std_logic;
SIGNAL \inst1|Add6~77\ : std_logic;
SIGNAL \inst1|Add6~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add6~72\ : std_logic;
SIGNAL \inst1|Add6~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add6~67\ : std_logic;
SIGNAL \inst1|Add6~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add6~62\ : std_logic;
SIGNAL \inst1|Add6~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add6~57\ : std_logic;
SIGNAL \inst1|Add5~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][23]~combout\ : std_logic;
SIGNAL \inst1|Add5~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][22]~combout\ : std_logic;
SIGNAL \inst1|Add5~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][21]~combout\ : std_logic;
SIGNAL \inst1|Add5~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][20]~combout\ : std_logic;
SIGNAL \inst1|Add6~52\ : std_logic;
SIGNAL \inst1|Add6~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add6~47\ : std_logic;
SIGNAL \inst1|Add6~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add6~42\ : std_logic;
SIGNAL \inst1|Add6~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add6~37\ : std_logic;
SIGNAL \inst1|Add6~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add6~32\ : std_logic;
SIGNAL \inst1|Add5~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][28]~combout\ : std_logic;
SIGNAL \inst1|Add5~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][27]~combout\ : std_logic;
SIGNAL \inst1|Add5~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][26]~combout\ : std_logic;
SIGNAL \inst1|Add5~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[5][25]~combout\ : std_logic;
SIGNAL \inst1|Add6~27\ : std_logic;
SIGNAL \inst1|Add6~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add6~22\ : std_logic;
SIGNAL \inst1|Add6~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add6~17\ : std_logic;
SIGNAL \inst1|Add6~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add6~12\ : std_logic;
SIGNAL \inst1|Add6~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add6~7\ : std_logic;
SIGNAL \inst1|Add6~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][30]~combout\ : std_logic;
SIGNAL \inst1|Add6~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][29]~combout\ : std_logic;
SIGNAL \inst1|Add6~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][24]~combout\ : std_logic;
SIGNAL \inst1|Add6~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][19]~combout\ : std_logic;
SIGNAL \inst1|Add6~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[8][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][0]~159\ : std_logic;
SIGNAL \inst1|PSUM[8][0]~159COUT1_377\ : std_logic;
SIGNAL \inst1|PSUM[8][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][1]~157\ : std_logic;
SIGNAL \inst1|PSUM[8][1]~157COUT1_378\ : std_logic;
SIGNAL \inst1|PSUM[8][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][2]~155\ : std_logic;
SIGNAL \inst1|PSUM[8][2]~155COUT1_379\ : std_logic;
SIGNAL \inst1|PSUM[8][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][3]~153\ : std_logic;
SIGNAL \inst1|PSUM[8][3]~153COUT1_380\ : std_logic;
SIGNAL \inst1|PSUM[8][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][4]~151\ : std_logic;
SIGNAL \inst1|PSUM[8][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][5]~149\ : std_logic;
SIGNAL \inst1|PSUM[8][5]~149COUT1_381\ : std_logic;
SIGNAL \inst1|PSUM[8][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][6]~147\ : std_logic;
SIGNAL \inst1|PSUM[8][6]~147COUT1_382\ : std_logic;
SIGNAL \inst1|PSUM[8][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][7]~145\ : std_logic;
SIGNAL \inst1|PSUM[8][7]~145COUT1_383\ : std_logic;
SIGNAL \inst1|PSUM[8][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[8][8]~143\ : std_logic;
SIGNAL \inst1|PSUM[8][8]~143COUT1_384\ : std_logic;
SIGNAL \inst1|PSUM[8][9]~regout\ : std_logic;
SIGNAL \inst1|Add6~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][9]~combout\ : std_logic;
SIGNAL \inst1|Add6~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][4]~combout\ : std_logic;
SIGNAL \inst1|Add6~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][3]~combout\ : std_logic;
SIGNAL \inst1|Add6~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][2]~combout\ : std_logic;
SIGNAL \inst1|Add6~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][1]~combout\ : std_logic;
SIGNAL \inst1|Add6~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][0]~combout\ : std_logic;
SIGNAL \inst1|Add7~152\ : std_logic;
SIGNAL \inst1|Add7~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add7~147\ : std_logic;
SIGNAL \inst1|Add7~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add7~142\ : std_logic;
SIGNAL \inst1|Add7~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add7~137\ : std_logic;
SIGNAL \inst1|Add7~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add7~132\ : std_logic;
SIGNAL \inst1|Add6~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][8]~combout\ : std_logic;
SIGNAL \inst1|Add6~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][7]~combout\ : std_logic;
SIGNAL \inst1|Add6~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][6]~combout\ : std_logic;
SIGNAL \inst1|Add6~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][5]~combout\ : std_logic;
SIGNAL \inst1|Add7~127\ : std_logic;
SIGNAL \inst1|Add7~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add7~122\ : std_logic;
SIGNAL \inst1|Add7~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add7~117\ : std_logic;
SIGNAL \inst1|Add7~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add7~112\ : std_logic;
SIGNAL \inst1|Add7~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add7~107\ : std_logic;
SIGNAL \inst1|Add6~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][13]~combout\ : std_logic;
SIGNAL \inst1|Add6~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][12]~combout\ : std_logic;
SIGNAL \inst1|Add6~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][11]~combout\ : std_logic;
SIGNAL \inst1|Add6~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][10]~combout\ : std_logic;
SIGNAL \inst1|Add7~102\ : std_logic;
SIGNAL \inst1|Add7~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add7~97\ : std_logic;
SIGNAL \inst1|Add7~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add7~92\ : std_logic;
SIGNAL \inst1|Add7~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add7~87\ : std_logic;
SIGNAL \inst1|Add7~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add7~82\ : std_logic;
SIGNAL \inst1|Add6~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][18]~combout\ : std_logic;
SIGNAL \inst1|Add6~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][17]~combout\ : std_logic;
SIGNAL \inst1|Add6~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][16]~combout\ : std_logic;
SIGNAL \inst1|Add6~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][15]~combout\ : std_logic;
SIGNAL \inst1|Add7~77\ : std_logic;
SIGNAL \inst1|Add7~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add7~72\ : std_logic;
SIGNAL \inst1|Add7~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add7~67\ : std_logic;
SIGNAL \inst1|Add7~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add7~62\ : std_logic;
SIGNAL \inst1|Add7~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add7~57\ : std_logic;
SIGNAL \inst1|Add6~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][23]~combout\ : std_logic;
SIGNAL \inst1|Add6~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][22]~combout\ : std_logic;
SIGNAL \inst1|Add6~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][21]~combout\ : std_logic;
SIGNAL \inst1|Add6~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][20]~combout\ : std_logic;
SIGNAL \inst1|Add7~52\ : std_logic;
SIGNAL \inst1|Add7~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add7~47\ : std_logic;
SIGNAL \inst1|Add7~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add7~42\ : std_logic;
SIGNAL \inst1|Add7~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add7~37\ : std_logic;
SIGNAL \inst1|Add7~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add7~32\ : std_logic;
SIGNAL \inst1|Add6~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][28]~combout\ : std_logic;
SIGNAL \inst1|Add6~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][27]~combout\ : std_logic;
SIGNAL \inst1|Add6~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][26]~combout\ : std_logic;
SIGNAL \inst1|Add6~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[6][25]~combout\ : std_logic;
SIGNAL \inst1|Add7~27\ : std_logic;
SIGNAL \inst1|Add7~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add7~22\ : std_logic;
SIGNAL \inst1|Add7~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add7~17\ : std_logic;
SIGNAL \inst1|Add7~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add7~12\ : std_logic;
SIGNAL \inst1|Add7~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add7~7\ : std_logic;
SIGNAL \inst1|Add7~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][30]~combout\ : std_logic;
SIGNAL \inst1|Add7~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][29]~combout\ : std_logic;
SIGNAL \inst1|Add7~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][24]~combout\ : std_logic;
SIGNAL \inst1|Add7~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][19]~combout\ : std_logic;
SIGNAL \inst1|Add7~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[9][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][0]~139\ : std_logic;
SIGNAL \inst1|PSUM[9][0]~139COUT1_369\ : std_logic;
SIGNAL \inst1|PSUM[9][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][1]~137\ : std_logic;
SIGNAL \inst1|PSUM[9][1]~137COUT1_370\ : std_logic;
SIGNAL \inst1|PSUM[9][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][2]~135\ : std_logic;
SIGNAL \inst1|PSUM[9][2]~135COUT1_371\ : std_logic;
SIGNAL \inst1|PSUM[9][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][3]~133\ : std_logic;
SIGNAL \inst1|PSUM[9][3]~133COUT1_372\ : std_logic;
SIGNAL \inst1|PSUM[9][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][4]~131\ : std_logic;
SIGNAL \inst1|PSUM[9][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][5]~129\ : std_logic;
SIGNAL \inst1|PSUM[9][5]~129COUT1_373\ : std_logic;
SIGNAL \inst1|PSUM[9][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][6]~127\ : std_logic;
SIGNAL \inst1|PSUM[9][6]~127COUT1_374\ : std_logic;
SIGNAL \inst1|PSUM[9][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][7]~125\ : std_logic;
SIGNAL \inst1|PSUM[9][7]~125COUT1_375\ : std_logic;
SIGNAL \inst1|PSUM[9][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[9][8]~123\ : std_logic;
SIGNAL \inst1|PSUM[9][8]~123COUT1_376\ : std_logic;
SIGNAL \inst1|PSUM[9][9]~regout\ : std_logic;
SIGNAL \inst1|Add7~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][9]~combout\ : std_logic;
SIGNAL \inst1|Add7~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][4]~combout\ : std_logic;
SIGNAL \inst1|Add7~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][3]~combout\ : std_logic;
SIGNAL \inst1|Add7~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][2]~combout\ : std_logic;
SIGNAL \inst1|Add7~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][1]~combout\ : std_logic;
SIGNAL \inst1|Add7~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][0]~combout\ : std_logic;
SIGNAL \inst1|Add8~152\ : std_logic;
SIGNAL \inst1|Add8~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add8~147\ : std_logic;
SIGNAL \inst1|Add8~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add8~142\ : std_logic;
SIGNAL \inst1|Add8~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add8~137\ : std_logic;
SIGNAL \inst1|Add8~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add8~132\ : std_logic;
SIGNAL \inst1|Add7~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][8]~combout\ : std_logic;
SIGNAL \inst1|Add7~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][7]~combout\ : std_logic;
SIGNAL \inst1|Add7~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][6]~combout\ : std_logic;
SIGNAL \inst1|Add7~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][5]~combout\ : std_logic;
SIGNAL \inst1|Add8~127\ : std_logic;
SIGNAL \inst1|Add8~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add8~122\ : std_logic;
SIGNAL \inst1|Add8~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add8~117\ : std_logic;
SIGNAL \inst1|Add8~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add8~112\ : std_logic;
SIGNAL \inst1|Add8~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add8~107\ : std_logic;
SIGNAL \inst1|Add7~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][13]~combout\ : std_logic;
SIGNAL \inst1|Add7~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][12]~combout\ : std_logic;
SIGNAL \inst1|Add7~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][11]~combout\ : std_logic;
SIGNAL \inst1|Add7~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][10]~combout\ : std_logic;
SIGNAL \inst1|Add8~102\ : std_logic;
SIGNAL \inst1|Add8~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add8~97\ : std_logic;
SIGNAL \inst1|Add8~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add8~92\ : std_logic;
SIGNAL \inst1|Add8~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add8~87\ : std_logic;
SIGNAL \inst1|Add8~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add8~82\ : std_logic;
SIGNAL \inst1|Add7~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][18]~combout\ : std_logic;
SIGNAL \inst1|Add7~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][17]~combout\ : std_logic;
SIGNAL \inst1|Add7~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][16]~combout\ : std_logic;
SIGNAL \inst1|Add7~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][15]~combout\ : std_logic;
SIGNAL \inst1|Add8~77\ : std_logic;
SIGNAL \inst1|Add8~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add8~72\ : std_logic;
SIGNAL \inst1|Add8~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add8~67\ : std_logic;
SIGNAL \inst1|Add8~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add8~62\ : std_logic;
SIGNAL \inst1|Add8~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add8~57\ : std_logic;
SIGNAL \inst1|Add7~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][23]~combout\ : std_logic;
SIGNAL \inst1|Add7~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][22]~combout\ : std_logic;
SIGNAL \inst1|Add7~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][21]~combout\ : std_logic;
SIGNAL \inst1|Add7~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][20]~combout\ : std_logic;
SIGNAL \inst1|Add8~52\ : std_logic;
SIGNAL \inst1|Add8~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add8~47\ : std_logic;
SIGNAL \inst1|Add8~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add8~42\ : std_logic;
SIGNAL \inst1|Add8~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add8~37\ : std_logic;
SIGNAL \inst1|Add8~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add8~32\ : std_logic;
SIGNAL \inst1|Add7~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][28]~combout\ : std_logic;
SIGNAL \inst1|Add7~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][27]~combout\ : std_logic;
SIGNAL \inst1|Add7~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][26]~combout\ : std_logic;
SIGNAL \inst1|Add7~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[7][25]~combout\ : std_logic;
SIGNAL \inst1|Add8~27\ : std_logic;
SIGNAL \inst1|Add8~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add8~22\ : std_logic;
SIGNAL \inst1|Add8~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add8~17\ : std_logic;
SIGNAL \inst1|Add8~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add8~12\ : std_logic;
SIGNAL \inst1|Add8~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add8~7\ : std_logic;
SIGNAL \inst1|Add8~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][30]~combout\ : std_logic;
SIGNAL \inst1|Add8~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][29]~combout\ : std_logic;
SIGNAL \inst1|Add8~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][24]~combout\ : std_logic;
SIGNAL \inst1|Add8~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][19]~combout\ : std_logic;
SIGNAL \inst1|Add8~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][14]~combout\ : std_logic;
SIGNAL \inst1|Add8~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][9]~combout\ : std_logic;
SIGNAL \inst1|PSUM[10][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][0]~119\ : std_logic;
SIGNAL \inst1|PSUM[10][0]~119COUT1_361\ : std_logic;
SIGNAL \inst1|PSUM[10][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][1]~117\ : std_logic;
SIGNAL \inst1|PSUM[10][1]~117COUT1_362\ : std_logic;
SIGNAL \inst1|PSUM[10][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][2]~115\ : std_logic;
SIGNAL \inst1|PSUM[10][2]~115COUT1_363\ : std_logic;
SIGNAL \inst1|PSUM[10][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][3]~113\ : std_logic;
SIGNAL \inst1|PSUM[10][3]~113COUT1_364\ : std_logic;
SIGNAL \inst1|PSUM[10][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][4]~111\ : std_logic;
SIGNAL \inst1|PSUM[10][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][5]~109\ : std_logic;
SIGNAL \inst1|PSUM[10][5]~109COUT1_365\ : std_logic;
SIGNAL \inst1|PSUM[10][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][6]~107\ : std_logic;
SIGNAL \inst1|PSUM[10][6]~107COUT1_366\ : std_logic;
SIGNAL \inst1|PSUM[10][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][7]~105\ : std_logic;
SIGNAL \inst1|PSUM[10][7]~105COUT1_367\ : std_logic;
SIGNAL \inst1|PSUM[10][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[10][8]~103\ : std_logic;
SIGNAL \inst1|PSUM[10][8]~103COUT1_368\ : std_logic;
SIGNAL \inst1|PSUM[10][9]~regout\ : std_logic;
SIGNAL \inst1|Add8~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][4]~combout\ : std_logic;
SIGNAL \inst1|Add8~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][3]~combout\ : std_logic;
SIGNAL \inst1|Add8~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][2]~combout\ : std_logic;
SIGNAL \inst1|Add8~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][1]~combout\ : std_logic;
SIGNAL \inst1|Add8~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][0]~combout\ : std_logic;
SIGNAL \inst1|Add9~152\ : std_logic;
SIGNAL \inst1|Add9~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add9~147\ : std_logic;
SIGNAL \inst1|Add9~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add9~142\ : std_logic;
SIGNAL \inst1|Add9~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add9~137\ : std_logic;
SIGNAL \inst1|Add9~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add9~132\ : std_logic;
SIGNAL \inst1|Add8~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][8]~combout\ : std_logic;
SIGNAL \inst1|Add8~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][7]~combout\ : std_logic;
SIGNAL \inst1|Add8~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][6]~combout\ : std_logic;
SIGNAL \inst1|Add8~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][5]~combout\ : std_logic;
SIGNAL \inst1|Add9~127\ : std_logic;
SIGNAL \inst1|Add9~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add9~122\ : std_logic;
SIGNAL \inst1|Add9~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add9~117\ : std_logic;
SIGNAL \inst1|Add9~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add9~112\ : std_logic;
SIGNAL \inst1|Add9~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add9~107\ : std_logic;
SIGNAL \inst1|Add8~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][13]~combout\ : std_logic;
SIGNAL \inst1|Add8~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][12]~combout\ : std_logic;
SIGNAL \inst1|Add8~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][11]~combout\ : std_logic;
SIGNAL \inst1|Add8~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][10]~combout\ : std_logic;
SIGNAL \inst1|Add9~102\ : std_logic;
SIGNAL \inst1|Add9~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add9~97\ : std_logic;
SIGNAL \inst1|Add9~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add9~92\ : std_logic;
SIGNAL \inst1|Add9~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add9~87\ : std_logic;
SIGNAL \inst1|Add9~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add9~82\ : std_logic;
SIGNAL \inst1|Add8~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][18]~combout\ : std_logic;
SIGNAL \inst1|Add8~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][17]~combout\ : std_logic;
SIGNAL \inst1|Add8~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][16]~combout\ : std_logic;
SIGNAL \inst1|Add8~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][15]~combout\ : std_logic;
SIGNAL \inst1|Add9~77\ : std_logic;
SIGNAL \inst1|Add9~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add9~72\ : std_logic;
SIGNAL \inst1|Add9~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add9~67\ : std_logic;
SIGNAL \inst1|Add9~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add9~62\ : std_logic;
SIGNAL \inst1|Add9~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add9~57\ : std_logic;
SIGNAL \inst1|Add8~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][23]~combout\ : std_logic;
SIGNAL \inst1|Add8~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][22]~combout\ : std_logic;
SIGNAL \inst1|Add8~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][21]~combout\ : std_logic;
SIGNAL \inst1|Add8~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][20]~combout\ : std_logic;
SIGNAL \inst1|Add9~52\ : std_logic;
SIGNAL \inst1|Add9~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add9~47\ : std_logic;
SIGNAL \inst1|Add9~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add9~42\ : std_logic;
SIGNAL \inst1|Add9~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add9~37\ : std_logic;
SIGNAL \inst1|Add9~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add9~32\ : std_logic;
SIGNAL \inst1|Add8~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][28]~combout\ : std_logic;
SIGNAL \inst1|Add8~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][27]~combout\ : std_logic;
SIGNAL \inst1|Add8~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][26]~combout\ : std_logic;
SIGNAL \inst1|Add8~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[8][25]~combout\ : std_logic;
SIGNAL \inst1|Add9~27\ : std_logic;
SIGNAL \inst1|Add9~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add9~22\ : std_logic;
SIGNAL \inst1|Add9~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add9~17\ : std_logic;
SIGNAL \inst1|Add9~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add9~12\ : std_logic;
SIGNAL \inst1|Add9~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add9~7\ : std_logic;
SIGNAL \inst1|Add9~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][30]~combout\ : std_logic;
SIGNAL \inst1|Add9~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][29]~combout\ : std_logic;
SIGNAL \inst1|Add9~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][24]~combout\ : std_logic;
SIGNAL \inst1|Add9~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][19]~combout\ : std_logic;
SIGNAL \inst1|Add9~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][14]~combout\ : std_logic;
SIGNAL \inst1|Add9~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][9]~combout\ : std_logic;
SIGNAL \inst1|PSUM[11][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][0]~99\ : std_logic;
SIGNAL \inst1|PSUM[11][0]~99COUT1_353\ : std_logic;
SIGNAL \inst1|PSUM[11][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][1]~97\ : std_logic;
SIGNAL \inst1|PSUM[11][1]~97COUT1_354\ : std_logic;
SIGNAL \inst1|PSUM[11][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][2]~95\ : std_logic;
SIGNAL \inst1|PSUM[11][2]~95COUT1_355\ : std_logic;
SIGNAL \inst1|PSUM[11][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][3]~93\ : std_logic;
SIGNAL \inst1|PSUM[11][3]~93COUT1_356\ : std_logic;
SIGNAL \inst1|PSUM[11][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][4]~91\ : std_logic;
SIGNAL \inst1|PSUM[11][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][5]~89\ : std_logic;
SIGNAL \inst1|PSUM[11][5]~89COUT1_357\ : std_logic;
SIGNAL \inst1|PSUM[11][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][6]~87\ : std_logic;
SIGNAL \inst1|PSUM[11][6]~87COUT1_358\ : std_logic;
SIGNAL \inst1|PSUM[11][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][7]~85\ : std_logic;
SIGNAL \inst1|PSUM[11][7]~85COUT1_359\ : std_logic;
SIGNAL \inst1|PSUM[11][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[11][8]~83\ : std_logic;
SIGNAL \inst1|PSUM[11][8]~83COUT1_360\ : std_logic;
SIGNAL \inst1|PSUM[11][9]~regout\ : std_logic;
SIGNAL \inst1|Add9~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][4]~combout\ : std_logic;
SIGNAL \inst1|Add9~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][3]~combout\ : std_logic;
SIGNAL \inst1|Add9~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][2]~combout\ : std_logic;
SIGNAL \inst1|Add9~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][1]~combout\ : std_logic;
SIGNAL \inst1|Add9~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][0]~combout\ : std_logic;
SIGNAL \inst1|Add10~152\ : std_logic;
SIGNAL \inst1|Add10~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add10~147\ : std_logic;
SIGNAL \inst1|Add10~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add10~142\ : std_logic;
SIGNAL \inst1|Add10~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add10~137\ : std_logic;
SIGNAL \inst1|Add10~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add10~132\ : std_logic;
SIGNAL \inst1|Add9~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][8]~combout\ : std_logic;
SIGNAL \inst1|Add9~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][7]~combout\ : std_logic;
SIGNAL \inst1|Add9~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][6]~combout\ : std_logic;
SIGNAL \inst1|Add9~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][5]~combout\ : std_logic;
SIGNAL \inst1|Add10~127\ : std_logic;
SIGNAL \inst1|Add10~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add10~122\ : std_logic;
SIGNAL \inst1|Add10~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add10~117\ : std_logic;
SIGNAL \inst1|Add10~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add10~112\ : std_logic;
SIGNAL \inst1|Add10~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add10~107\ : std_logic;
SIGNAL \inst1|Add9~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][13]~combout\ : std_logic;
SIGNAL \inst1|Add9~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][12]~combout\ : std_logic;
SIGNAL \inst1|Add9~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][11]~combout\ : std_logic;
SIGNAL \inst1|Add9~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][10]~combout\ : std_logic;
SIGNAL \inst1|Add10~102\ : std_logic;
SIGNAL \inst1|Add10~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add10~97\ : std_logic;
SIGNAL \inst1|Add10~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add10~92\ : std_logic;
SIGNAL \inst1|Add10~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add10~87\ : std_logic;
SIGNAL \inst1|Add10~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add10~82\ : std_logic;
SIGNAL \inst1|Add9~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][18]~combout\ : std_logic;
SIGNAL \inst1|Add9~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][17]~combout\ : std_logic;
SIGNAL \inst1|Add9~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][16]~combout\ : std_logic;
SIGNAL \inst1|Add9~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][15]~combout\ : std_logic;
SIGNAL \inst1|Add10~77\ : std_logic;
SIGNAL \inst1|Add10~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add10~72\ : std_logic;
SIGNAL \inst1|Add10~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add10~67\ : std_logic;
SIGNAL \inst1|Add10~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add10~62\ : std_logic;
SIGNAL \inst1|Add10~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add10~57\ : std_logic;
SIGNAL \inst1|Add9~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][23]~combout\ : std_logic;
SIGNAL \inst1|Add9~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][22]~combout\ : std_logic;
SIGNAL \inst1|Add9~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][21]~combout\ : std_logic;
SIGNAL \inst1|Add9~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][20]~combout\ : std_logic;
SIGNAL \inst1|Add10~52\ : std_logic;
SIGNAL \inst1|Add10~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add10~47\ : std_logic;
SIGNAL \inst1|Add10~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add10~42\ : std_logic;
SIGNAL \inst1|Add10~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add10~37\ : std_logic;
SIGNAL \inst1|Add10~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add10~32\ : std_logic;
SIGNAL \inst1|Add9~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][28]~combout\ : std_logic;
SIGNAL \inst1|Add9~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][27]~combout\ : std_logic;
SIGNAL \inst1|Add9~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][26]~combout\ : std_logic;
SIGNAL \inst1|Add9~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[9][25]~combout\ : std_logic;
SIGNAL \inst1|Add10~27\ : std_logic;
SIGNAL \inst1|Add10~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add10~22\ : std_logic;
SIGNAL \inst1|Add10~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add10~17\ : std_logic;
SIGNAL \inst1|Add10~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add10~12\ : std_logic;
SIGNAL \inst1|Add10~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add10~7\ : std_logic;
SIGNAL \inst1|Add10~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][30]~combout\ : std_logic;
SIGNAL \inst1|Add10~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][29]~combout\ : std_logic;
SIGNAL \inst1|Add10~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][24]~combout\ : std_logic;
SIGNAL \inst1|Add10~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][19]~combout\ : std_logic;
SIGNAL \inst1|Add10~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][14]~combout\ : std_logic;
SIGNAL \inst1|Add10~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][9]~combout\ : std_logic;
SIGNAL \inst1|PSUM[12][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][0]~79\ : std_logic;
SIGNAL \inst1|PSUM[12][0]~79COUT1_345\ : std_logic;
SIGNAL \inst1|PSUM[12][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][1]~77\ : std_logic;
SIGNAL \inst1|PSUM[12][1]~77COUT1_346\ : std_logic;
SIGNAL \inst1|PSUM[12][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][2]~75\ : std_logic;
SIGNAL \inst1|PSUM[12][2]~75COUT1_347\ : std_logic;
SIGNAL \inst1|PSUM[12][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][3]~73\ : std_logic;
SIGNAL \inst1|PSUM[12][3]~73COUT1_348\ : std_logic;
SIGNAL \inst1|PSUM[12][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][4]~71\ : std_logic;
SIGNAL \inst1|PSUM[12][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][5]~69\ : std_logic;
SIGNAL \inst1|PSUM[12][5]~69COUT1_349\ : std_logic;
SIGNAL \inst1|PSUM[12][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][6]~67\ : std_logic;
SIGNAL \inst1|PSUM[12][6]~67COUT1_350\ : std_logic;
SIGNAL \inst1|PSUM[12][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][7]~65\ : std_logic;
SIGNAL \inst1|PSUM[12][7]~65COUT1_351\ : std_logic;
SIGNAL \inst1|PSUM[12][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[12][8]~63\ : std_logic;
SIGNAL \inst1|PSUM[12][8]~63COUT1_352\ : std_logic;
SIGNAL \inst1|PSUM[12][9]~regout\ : std_logic;
SIGNAL \inst1|Add10~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][4]~combout\ : std_logic;
SIGNAL \inst1|Add10~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][3]~combout\ : std_logic;
SIGNAL \inst1|Add10~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][2]~combout\ : std_logic;
SIGNAL \inst1|Add10~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][1]~combout\ : std_logic;
SIGNAL \inst1|Add10~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][0]~combout\ : std_logic;
SIGNAL \inst1|Add11~152\ : std_logic;
SIGNAL \inst1|Add11~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add11~147\ : std_logic;
SIGNAL \inst1|Add11~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add11~142\ : std_logic;
SIGNAL \inst1|Add11~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add11~137\ : std_logic;
SIGNAL \inst1|Add11~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add11~132\ : std_logic;
SIGNAL \inst1|Add10~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][8]~combout\ : std_logic;
SIGNAL \inst1|Add10~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][7]~combout\ : std_logic;
SIGNAL \inst1|Add10~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][6]~combout\ : std_logic;
SIGNAL \inst1|Add10~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][5]~combout\ : std_logic;
SIGNAL \inst1|Add11~127\ : std_logic;
SIGNAL \inst1|Add11~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add11~122\ : std_logic;
SIGNAL \inst1|Add11~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add11~117\ : std_logic;
SIGNAL \inst1|Add11~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add11~112\ : std_logic;
SIGNAL \inst1|Add11~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add11~107\ : std_logic;
SIGNAL \inst1|Add10~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][13]~combout\ : std_logic;
SIGNAL \inst1|Add10~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][12]~combout\ : std_logic;
SIGNAL \inst1|Add10~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][11]~combout\ : std_logic;
SIGNAL \inst1|Add10~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][10]~combout\ : std_logic;
SIGNAL \inst1|Add11~102\ : std_logic;
SIGNAL \inst1|Add11~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add11~97\ : std_logic;
SIGNAL \inst1|Add11~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add11~92\ : std_logic;
SIGNAL \inst1|Add11~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add11~87\ : std_logic;
SIGNAL \inst1|Add11~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add11~82\ : std_logic;
SIGNAL \inst1|Add10~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][18]~combout\ : std_logic;
SIGNAL \inst1|Add10~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][17]~combout\ : std_logic;
SIGNAL \inst1|Add10~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][16]~combout\ : std_logic;
SIGNAL \inst1|Add10~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][15]~combout\ : std_logic;
SIGNAL \inst1|Add11~77\ : std_logic;
SIGNAL \inst1|Add11~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add11~72\ : std_logic;
SIGNAL \inst1|Add11~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add11~67\ : std_logic;
SIGNAL \inst1|Add11~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add11~62\ : std_logic;
SIGNAL \inst1|Add11~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add11~57\ : std_logic;
SIGNAL \inst1|Add10~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][23]~combout\ : std_logic;
SIGNAL \inst1|Add10~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][22]~combout\ : std_logic;
SIGNAL \inst1|Add10~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][21]~combout\ : std_logic;
SIGNAL \inst1|Add10~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][20]~combout\ : std_logic;
SIGNAL \inst1|Add11~52\ : std_logic;
SIGNAL \inst1|Add11~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add11~47\ : std_logic;
SIGNAL \inst1|Add11~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add11~42\ : std_logic;
SIGNAL \inst1|Add11~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add11~37\ : std_logic;
SIGNAL \inst1|Add11~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add11~32\ : std_logic;
SIGNAL \inst1|Add10~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][28]~combout\ : std_logic;
SIGNAL \inst1|Add10~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][27]~combout\ : std_logic;
SIGNAL \inst1|Add10~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][26]~combout\ : std_logic;
SIGNAL \inst1|Add10~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[10][25]~combout\ : std_logic;
SIGNAL \inst1|Add11~27\ : std_logic;
SIGNAL \inst1|Add11~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add11~22\ : std_logic;
SIGNAL \inst1|Add11~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add11~17\ : std_logic;
SIGNAL \inst1|Add11~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add11~12\ : std_logic;
SIGNAL \inst1|Add11~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add11~7\ : std_logic;
SIGNAL \inst1|Add11~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][30]~combout\ : std_logic;
SIGNAL \inst1|Add11~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][29]~combout\ : std_logic;
SIGNAL \inst1|Add11~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][24]~combout\ : std_logic;
SIGNAL \inst1|Add11~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][19]~combout\ : std_logic;
SIGNAL \inst1|Add11~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[13][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][0]~59\ : std_logic;
SIGNAL \inst1|PSUM[13][0]~59COUT1_337\ : std_logic;
SIGNAL \inst1|PSUM[13][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][1]~57\ : std_logic;
SIGNAL \inst1|PSUM[13][1]~57COUT1_338\ : std_logic;
SIGNAL \inst1|PSUM[13][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][2]~55\ : std_logic;
SIGNAL \inst1|PSUM[13][2]~55COUT1_339\ : std_logic;
SIGNAL \inst1|PSUM[13][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][3]~53\ : std_logic;
SIGNAL \inst1|PSUM[13][3]~53COUT1_340\ : std_logic;
SIGNAL \inst1|PSUM[13][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][4]~51\ : std_logic;
SIGNAL \inst1|PSUM[13][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][5]~49\ : std_logic;
SIGNAL \inst1|PSUM[13][5]~49COUT1_341\ : std_logic;
SIGNAL \inst1|PSUM[13][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][6]~47\ : std_logic;
SIGNAL \inst1|PSUM[13][6]~47COUT1_342\ : std_logic;
SIGNAL \inst1|PSUM[13][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][7]~45\ : std_logic;
SIGNAL \inst1|PSUM[13][7]~45COUT1_343\ : std_logic;
SIGNAL \inst1|PSUM[13][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[13][8]~43\ : std_logic;
SIGNAL \inst1|PSUM[13][8]~43COUT1_344\ : std_logic;
SIGNAL \inst1|PSUM[13][9]~regout\ : std_logic;
SIGNAL \inst1|Add11~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][9]~combout\ : std_logic;
SIGNAL \inst1|Add11~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][4]~combout\ : std_logic;
SIGNAL \inst1|Add11~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][3]~combout\ : std_logic;
SIGNAL \inst1|Add11~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][2]~combout\ : std_logic;
SIGNAL \inst1|Add11~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][1]~combout\ : std_logic;
SIGNAL \inst1|Add11~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][0]~combout\ : std_logic;
SIGNAL \inst1|Add12~152\ : std_logic;
SIGNAL \inst1|Add12~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add12~147\ : std_logic;
SIGNAL \inst1|Add12~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add12~142\ : std_logic;
SIGNAL \inst1|Add12~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add12~137\ : std_logic;
SIGNAL \inst1|Add12~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add12~132\ : std_logic;
SIGNAL \inst1|Add11~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][8]~combout\ : std_logic;
SIGNAL \inst1|Add11~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][7]~combout\ : std_logic;
SIGNAL \inst1|Add11~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][6]~combout\ : std_logic;
SIGNAL \inst1|Add11~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][5]~combout\ : std_logic;
SIGNAL \inst1|Add12~127\ : std_logic;
SIGNAL \inst1|Add12~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add12~122\ : std_logic;
SIGNAL \inst1|Add12~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add12~117\ : std_logic;
SIGNAL \inst1|Add12~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add12~112\ : std_logic;
SIGNAL \inst1|Add12~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add12~107\ : std_logic;
SIGNAL \inst1|Add11~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][13]~combout\ : std_logic;
SIGNAL \inst1|Add11~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][12]~combout\ : std_logic;
SIGNAL \inst1|Add11~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][11]~combout\ : std_logic;
SIGNAL \inst1|Add11~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][10]~combout\ : std_logic;
SIGNAL \inst1|Add12~102\ : std_logic;
SIGNAL \inst1|Add12~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add12~97\ : std_logic;
SIGNAL \inst1|Add12~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add12~92\ : std_logic;
SIGNAL \inst1|Add12~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add12~87\ : std_logic;
SIGNAL \inst1|Add12~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add12~82\ : std_logic;
SIGNAL \inst1|Add11~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][18]~combout\ : std_logic;
SIGNAL \inst1|Add11~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][17]~combout\ : std_logic;
SIGNAL \inst1|Add11~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][16]~combout\ : std_logic;
SIGNAL \inst1|Add11~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][15]~combout\ : std_logic;
SIGNAL \inst1|Add12~77\ : std_logic;
SIGNAL \inst1|Add12~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add12~72\ : std_logic;
SIGNAL \inst1|Add12~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add12~67\ : std_logic;
SIGNAL \inst1|Add12~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add12~62\ : std_logic;
SIGNAL \inst1|Add12~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add12~57\ : std_logic;
SIGNAL \inst1|Add11~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][23]~combout\ : std_logic;
SIGNAL \inst1|Add11~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][22]~combout\ : std_logic;
SIGNAL \inst1|Add11~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][21]~combout\ : std_logic;
SIGNAL \inst1|Add11~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][20]~combout\ : std_logic;
SIGNAL \inst1|Add12~52\ : std_logic;
SIGNAL \inst1|Add12~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add12~47\ : std_logic;
SIGNAL \inst1|Add12~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add12~42\ : std_logic;
SIGNAL \inst1|Add12~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add12~37\ : std_logic;
SIGNAL \inst1|Add12~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add12~32\ : std_logic;
SIGNAL \inst1|Add11~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][28]~combout\ : std_logic;
SIGNAL \inst1|Add11~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][27]~combout\ : std_logic;
SIGNAL \inst1|Add11~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][26]~combout\ : std_logic;
SIGNAL \inst1|Add11~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[11][25]~combout\ : std_logic;
SIGNAL \inst1|Add12~27\ : std_logic;
SIGNAL \inst1|Add12~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add12~22\ : std_logic;
SIGNAL \inst1|Add12~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add12~17\ : std_logic;
SIGNAL \inst1|Add12~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add12~12\ : std_logic;
SIGNAL \inst1|Add12~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add12~7\ : std_logic;
SIGNAL \inst1|Add12~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][30]~combout\ : std_logic;
SIGNAL \inst1|Add12~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][29]~combout\ : std_logic;
SIGNAL \inst1|Add12~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][24]~combout\ : std_logic;
SIGNAL \inst1|Add12~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][19]~combout\ : std_logic;
SIGNAL \inst1|Add12~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[14][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][0]~39\ : std_logic;
SIGNAL \inst1|PSUM[14][0]~39COUT1_329\ : std_logic;
SIGNAL \inst1|PSUM[14][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][1]~37\ : std_logic;
SIGNAL \inst1|PSUM[14][1]~37COUT1_330\ : std_logic;
SIGNAL \inst1|PSUM[14][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][2]~35\ : std_logic;
SIGNAL \inst1|PSUM[14][2]~35COUT1_331\ : std_logic;
SIGNAL \inst1|PSUM[14][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][3]~33\ : std_logic;
SIGNAL \inst1|PSUM[14][3]~33COUT1_332\ : std_logic;
SIGNAL \inst1|PSUM[14][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][4]~31\ : std_logic;
SIGNAL \inst1|PSUM[14][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][5]~29\ : std_logic;
SIGNAL \inst1|PSUM[14][5]~29COUT1_333\ : std_logic;
SIGNAL \inst1|PSUM[14][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][6]~27\ : std_logic;
SIGNAL \inst1|PSUM[14][6]~27COUT1_334\ : std_logic;
SIGNAL \inst1|PSUM[14][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][7]~25\ : std_logic;
SIGNAL \inst1|PSUM[14][7]~25COUT1_335\ : std_logic;
SIGNAL \inst1|PSUM[14][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[14][8]~23\ : std_logic;
SIGNAL \inst1|PSUM[14][8]~23COUT1_336\ : std_logic;
SIGNAL \inst1|PSUM[14][9]~regout\ : std_logic;
SIGNAL \inst1|Add12~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][9]~combout\ : std_logic;
SIGNAL \inst1|Add12~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][4]~combout\ : std_logic;
SIGNAL \inst1|Add12~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][3]~combout\ : std_logic;
SIGNAL \inst1|Add12~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][2]~combout\ : std_logic;
SIGNAL \inst1|Add12~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][1]~combout\ : std_logic;
SIGNAL \inst1|Add12~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][0]~combout\ : std_logic;
SIGNAL \inst1|Add13~152\ : std_logic;
SIGNAL \inst1|Add13~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add13~147\ : std_logic;
SIGNAL \inst1|Add13~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add13~142\ : std_logic;
SIGNAL \inst1|Add13~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add13~137\ : std_logic;
SIGNAL \inst1|Add13~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add13~132\ : std_logic;
SIGNAL \inst1|Add12~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][8]~combout\ : std_logic;
SIGNAL \inst1|Add12~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][7]~combout\ : std_logic;
SIGNAL \inst1|Add12~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][6]~combout\ : std_logic;
SIGNAL \inst1|Add12~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][5]~combout\ : std_logic;
SIGNAL \inst1|Add13~127\ : std_logic;
SIGNAL \inst1|Add13~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add13~122\ : std_logic;
SIGNAL \inst1|Add13~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add13~117\ : std_logic;
SIGNAL \inst1|Add13~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add13~112\ : std_logic;
SIGNAL \inst1|Add13~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add13~107\ : std_logic;
SIGNAL \inst1|Add12~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][13]~combout\ : std_logic;
SIGNAL \inst1|Add12~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][12]~combout\ : std_logic;
SIGNAL \inst1|Add12~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][11]~combout\ : std_logic;
SIGNAL \inst1|Add12~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][10]~combout\ : std_logic;
SIGNAL \inst1|Add13~102\ : std_logic;
SIGNAL \inst1|Add13~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add13~97\ : std_logic;
SIGNAL \inst1|Add13~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add13~92\ : std_logic;
SIGNAL \inst1|Add13~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add13~87\ : std_logic;
SIGNAL \inst1|Add13~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add13~82\ : std_logic;
SIGNAL \inst1|Add12~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][18]~combout\ : std_logic;
SIGNAL \inst1|Add12~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][17]~combout\ : std_logic;
SIGNAL \inst1|Add12~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][16]~combout\ : std_logic;
SIGNAL \inst1|Add12~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][15]~combout\ : std_logic;
SIGNAL \inst1|Add13~77\ : std_logic;
SIGNAL \inst1|Add13~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add13~72\ : std_logic;
SIGNAL \inst1|Add13~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add13~67\ : std_logic;
SIGNAL \inst1|Add13~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add13~62\ : std_logic;
SIGNAL \inst1|Add13~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add13~57\ : std_logic;
SIGNAL \inst1|Add12~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][23]~combout\ : std_logic;
SIGNAL \inst1|Add12~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][22]~combout\ : std_logic;
SIGNAL \inst1|Add12~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][21]~combout\ : std_logic;
SIGNAL \inst1|Add12~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][20]~combout\ : std_logic;
SIGNAL \inst1|Add13~52\ : std_logic;
SIGNAL \inst1|Add13~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add13~47\ : std_logic;
SIGNAL \inst1|Add13~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add13~42\ : std_logic;
SIGNAL \inst1|Add13~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add13~37\ : std_logic;
SIGNAL \inst1|Add13~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add13~32\ : std_logic;
SIGNAL \inst1|Add12~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][28]~combout\ : std_logic;
SIGNAL \inst1|Add12~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][27]~combout\ : std_logic;
SIGNAL \inst1|Add12~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][26]~combout\ : std_logic;
SIGNAL \inst1|Add12~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[12][25]~combout\ : std_logic;
SIGNAL \inst1|Add13~27\ : std_logic;
SIGNAL \inst1|Add13~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add13~22\ : std_logic;
SIGNAL \inst1|Add13~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add13~17\ : std_logic;
SIGNAL \inst1|Add13~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add13~12\ : std_logic;
SIGNAL \inst1|Add13~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add13~7\ : std_logic;
SIGNAL \inst1|Add13~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][30]~combout\ : std_logic;
SIGNAL \inst1|Add13~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][29]~combout\ : std_logic;
SIGNAL \inst1|Add13~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][24]~combout\ : std_logic;
SIGNAL \inst1|Add13~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][19]~combout\ : std_logic;
SIGNAL \inst1|Add13~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][14]~combout\ : std_logic;
SIGNAL \inst1|PSUM[15][0]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][0]~19\ : std_logic;
SIGNAL \inst1|PSUM[15][0]~19COUT1_321\ : std_logic;
SIGNAL \inst1|PSUM[15][1]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][1]~17\ : std_logic;
SIGNAL \inst1|PSUM[15][1]~17COUT1_322\ : std_logic;
SIGNAL \inst1|PSUM[15][2]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][2]~15\ : std_logic;
SIGNAL \inst1|PSUM[15][2]~15COUT1_323\ : std_logic;
SIGNAL \inst1|PSUM[15][3]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][3]~13\ : std_logic;
SIGNAL \inst1|PSUM[15][3]~13COUT1_324\ : std_logic;
SIGNAL \inst1|PSUM[15][4]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][4]~11\ : std_logic;
SIGNAL \inst1|PSUM[15][5]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][5]~9\ : std_logic;
SIGNAL \inst1|PSUM[15][5]~9COUT1_325\ : std_logic;
SIGNAL \inst1|PSUM[15][6]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][6]~7\ : std_logic;
SIGNAL \inst1|PSUM[15][6]~7COUT1_326\ : std_logic;
SIGNAL \inst1|PSUM[15][7]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][7]~5\ : std_logic;
SIGNAL \inst1|PSUM[15][7]~5COUT1_327\ : std_logic;
SIGNAL \inst1|PSUM[15][8]~regout\ : std_logic;
SIGNAL \inst1|PSUM[15][8]~3\ : std_logic;
SIGNAL \inst1|PSUM[15][8]~3COUT1_328\ : std_logic;
SIGNAL \inst1|PSUM[15][9]~regout\ : std_logic;
SIGNAL \inst1|Add13~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][9]~combout\ : std_logic;
SIGNAL \inst1|Add13~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][4]~combout\ : std_logic;
SIGNAL \inst1|Add13~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][3]~combout\ : std_logic;
SIGNAL \inst1|Add13~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][2]~combout\ : std_logic;
SIGNAL \inst1|Add13~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][1]~combout\ : std_logic;
SIGNAL \inst1|Add13~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][0]~combout\ : std_logic;
SIGNAL \inst1|Add14~152\ : std_logic;
SIGNAL \inst1|Add14~152COUT1_156\ : std_logic;
SIGNAL \inst1|Add14~147\ : std_logic;
SIGNAL \inst1|Add14~147COUT1_157\ : std_logic;
SIGNAL \inst1|Add14~142\ : std_logic;
SIGNAL \inst1|Add14~142COUT1_158\ : std_logic;
SIGNAL \inst1|Add14~137\ : std_logic;
SIGNAL \inst1|Add14~137COUT1_159\ : std_logic;
SIGNAL \inst1|Add14~132\ : std_logic;
SIGNAL \inst1|Add13~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][8]~combout\ : std_logic;
SIGNAL \inst1|Add13~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][7]~combout\ : std_logic;
SIGNAL \inst1|Add13~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][6]~combout\ : std_logic;
SIGNAL \inst1|Add13~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][5]~combout\ : std_logic;
SIGNAL \inst1|Add14~127\ : std_logic;
SIGNAL \inst1|Add14~127COUT1_160\ : std_logic;
SIGNAL \inst1|Add14~122\ : std_logic;
SIGNAL \inst1|Add14~122COUT1_161\ : std_logic;
SIGNAL \inst1|Add14~117\ : std_logic;
SIGNAL \inst1|Add14~117COUT1_162\ : std_logic;
SIGNAL \inst1|Add14~112\ : std_logic;
SIGNAL \inst1|Add14~112COUT1_163\ : std_logic;
SIGNAL \inst1|Add14~107\ : std_logic;
SIGNAL \inst1|Add13~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][13]~combout\ : std_logic;
SIGNAL \inst1|Add13~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][12]~combout\ : std_logic;
SIGNAL \inst1|Add13~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][11]~combout\ : std_logic;
SIGNAL \inst1|Add13~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][10]~combout\ : std_logic;
SIGNAL \inst1|Add14~102\ : std_logic;
SIGNAL \inst1|Add14~102COUT1_164\ : std_logic;
SIGNAL \inst1|Add14~97\ : std_logic;
SIGNAL \inst1|Add14~97COUT1_165\ : std_logic;
SIGNAL \inst1|Add14~92\ : std_logic;
SIGNAL \inst1|Add14~92COUT1_166\ : std_logic;
SIGNAL \inst1|Add14~87\ : std_logic;
SIGNAL \inst1|Add14~87COUT1_167\ : std_logic;
SIGNAL \inst1|Add14~82\ : std_logic;
SIGNAL \inst1|Add13~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][18]~combout\ : std_logic;
SIGNAL \inst1|Add13~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][17]~combout\ : std_logic;
SIGNAL \inst1|Add13~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][16]~combout\ : std_logic;
SIGNAL \inst1|Add13~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][15]~combout\ : std_logic;
SIGNAL \inst1|Add14~77\ : std_logic;
SIGNAL \inst1|Add14~77COUT1_168\ : std_logic;
SIGNAL \inst1|Add14~72\ : std_logic;
SIGNAL \inst1|Add14~72COUT1_169\ : std_logic;
SIGNAL \inst1|Add14~67\ : std_logic;
SIGNAL \inst1|Add14~67COUT1_170\ : std_logic;
SIGNAL \inst1|Add14~62\ : std_logic;
SIGNAL \inst1|Add14~62COUT1_171\ : std_logic;
SIGNAL \inst1|Add14~57\ : std_logic;
SIGNAL \inst1|Add13~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][23]~combout\ : std_logic;
SIGNAL \inst1|Add13~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][22]~combout\ : std_logic;
SIGNAL \inst1|Add13~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][21]~combout\ : std_logic;
SIGNAL \inst1|Add13~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][20]~combout\ : std_logic;
SIGNAL \inst1|Add14~52\ : std_logic;
SIGNAL \inst1|Add14~52COUT1_172\ : std_logic;
SIGNAL \inst1|Add14~47\ : std_logic;
SIGNAL \inst1|Add14~47COUT1_173\ : std_logic;
SIGNAL \inst1|Add14~42\ : std_logic;
SIGNAL \inst1|Add14~42COUT1_174\ : std_logic;
SIGNAL \inst1|Add14~37\ : std_logic;
SIGNAL \inst1|Add14~37COUT1_175\ : std_logic;
SIGNAL \inst1|Add14~32\ : std_logic;
SIGNAL \inst1|Add13~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][28]~combout\ : std_logic;
SIGNAL \inst1|Add13~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][27]~combout\ : std_logic;
SIGNAL \inst1|Add13~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][26]~combout\ : std_logic;
SIGNAL \inst1|Add13~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[13][25]~combout\ : std_logic;
SIGNAL \inst1|Add14~27\ : std_logic;
SIGNAL \inst1|Add14~27COUT1_176\ : std_logic;
SIGNAL \inst1|Add14~22\ : std_logic;
SIGNAL \inst1|Add14~22COUT1_177\ : std_logic;
SIGNAL \inst1|Add14~17\ : std_logic;
SIGNAL \inst1|Add14~17COUT1_178\ : std_logic;
SIGNAL \inst1|Add14~12\ : std_logic;
SIGNAL \inst1|Add14~12COUT1_179\ : std_logic;
SIGNAL \inst1|Add14~7\ : std_logic;
SIGNAL \inst1|Add14~0_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][30]~combout\ : std_logic;
SIGNAL \inst1|Add14~5_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][29]~combout\ : std_logic;
SIGNAL \inst1|Add14~10_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][28]~combout\ : std_logic;
SIGNAL \inst1|Add14~15_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][27]~combout\ : std_logic;
SIGNAL \inst1|Add14~20_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][26]~combout\ : std_logic;
SIGNAL \inst1|Add14~25_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][25]~combout\ : std_logic;
SIGNAL \inst1|Add14~30_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][24]~combout\ : std_logic;
SIGNAL \inst1|Add14~35_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][23]~combout\ : std_logic;
SIGNAL \inst1|Add14~40_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][22]~combout\ : std_logic;
SIGNAL \inst1|Add14~45_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][21]~combout\ : std_logic;
SIGNAL \inst1|Add14~50_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][20]~combout\ : std_logic;
SIGNAL \inst1|Add14~55_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][19]~combout\ : std_logic;
SIGNAL \inst1|Add14~60_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][18]~combout\ : std_logic;
SIGNAL \inst1|Add14~65_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][17]~combout\ : std_logic;
SIGNAL \inst1|Add14~70_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][16]~combout\ : std_logic;
SIGNAL \inst1|Add14~75_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][15]~combout\ : std_logic;
SIGNAL \inst1|Add14~80_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][14]~combout\ : std_logic;
SIGNAL \inst1|Add14~85_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][13]~combout\ : std_logic;
SIGNAL \inst1|Add14~90_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][12]~combout\ : std_logic;
SIGNAL \inst1|Add14~95_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][11]~combout\ : std_logic;
SIGNAL \inst1|Add14~100_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][10]~combout\ : std_logic;
SIGNAL \inst1|Add14~105_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][9]~combout\ : std_logic;
SIGNAL \inst1|Add14~110_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][8]~combout\ : std_logic;
SIGNAL \inst1|Add14~115_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][7]~combout\ : std_logic;
SIGNAL \inst1|Add14~120_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][6]~combout\ : std_logic;
SIGNAL \inst1|Add14~125_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][5]~combout\ : std_logic;
SIGNAL \inst1|Add14~130_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][4]~combout\ : std_logic;
SIGNAL \inst1|Add14~135_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][3]~combout\ : std_logic;
SIGNAL \inst1|Add14~140_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][2]~combout\ : std_logic;
SIGNAL \inst1|Add14~145_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][1]~combout\ : std_logic;
SIGNAL \inst1|Add14~150_combout\ : std_logic;
SIGNAL \inst1|TSUM[14][0]~combout\ : std_logic;
SIGNAL \inst1|SUM\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst7|COUNT\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst6|STEMP\ : std_logic_vector(15 DOWNTO 0);

BEGIN

PHSSIGN <= ww_PHSSIGN;
ww_FRQ0 <= FRQ0;
ww_FRQ1 <= FRQ1;
PHSSIG <= ww_PHSSIG;
PHASE <= ww_PHASE;
PRECOUNT <= ww_PRECOUNT;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: PIN_18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\FRQ0~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_FRQ0,
	combout => \FRQ0~combout\);

-- Location: PIN_131,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\FRQ1~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_FRQ1,
	combout => \FRQ1~combout\);

-- Location: LC_X11_Y7_N1
\inst21|TMPNEG\ : maxii_lcell
-- Equation(s):
-- \inst21|TMPNEG~regout\ = DFFEAS((((!\FRQ1~combout\))), GLOBAL(\FRQ0~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0f0f",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~combout\,
	datac => \FRQ1~combout\,
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst21|TMPNEG~regout\);

-- Location: LC_X11_Y7_N5
\inst6|STEMP[15]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(15) = LCELL((((\FRQ1~combout\ & !\FRQ0~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \FRQ1~combout\,
	datad => \FRQ0~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(15));

-- Location: LC_X1_Y4_N0
\inst1|TSUM[0][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][30]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][30]~combout\);

-- Location: LC_X5_Y4_N9
\inst1|TSUM[0][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][29]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][29]~combout\);

-- Location: LC_X2_Y5_N2
\inst1|TSUM[0][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][24]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][24]~combout\);

-- Location: LC_X5_Y5_N2
\inst1|TSUM[0][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][19]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][19]~combout\);

-- Location: LC_X2_Y4_N3
\inst1|TSUM[0][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][14]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][14]~combout\);

-- Location: LC_X11_Y7_N0
\inst6|STEMP[14]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(14) = LCELL((((\inst6|STEMP\(15)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(15),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(14));

-- Location: LC_X11_Y7_N8
\inst6|STEMP[13]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(13) = LCELL((((\inst6|STEMP\(14)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(14),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(13));

-- Location: LC_X11_Y6_N3
\inst6|STEMP[12]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(12) = LCELL((((\inst6|STEMP\(13)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(13),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(12));

-- Location: LC_X12_Y3_N1
\inst6|STEMP[11]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(11) = LCELL((((\inst6|STEMP\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(12),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(11));

-- Location: LC_X12_Y3_N2
\inst6|STEMP[10]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(10) = LCELL((((\inst6|STEMP\(11)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(11),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(10));

-- Location: LC_X7_Y5_N2
\inst6|STEMP[9]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(9) = LCELL((((\inst6|STEMP\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(10),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(9));

-- Location: LC_X7_Y6_N3
\inst6|STEMP[8]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(8) = LCELL((((\inst6|STEMP\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(9),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(8));

-- Location: LC_X6_Y7_N7
\inst6|STEMP[7]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(7) = LCELL((((\inst6|STEMP\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(8),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(7));

-- Location: LC_X6_Y7_N5
\inst6|STEMP[6]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(6) = LCELL((((\inst6|STEMP\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(6));

-- Location: LC_X6_Y7_N0
\inst6|STEMP[5]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(5) = LCELL((((\inst6|STEMP\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(5));

-- Location: LC_X6_Y7_N1
\inst6|STEMP[4]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(4) = LCELL((((\inst6|STEMP\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(4));

-- Location: LC_X6_Y7_N2
\inst6|STEMP[3]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(3) = LCELL((((\inst6|STEMP\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(3));

-- Location: LC_X6_Y7_N3
\inst6|STEMP[2]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(2) = LCELL((((\inst6|STEMP\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(2));

-- Location: LC_X6_Y7_N4
\inst6|STEMP[1]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(1) = LCELL((((\inst6|STEMP\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(1));

-- Location: LC_X11_Y8_N3
\inst7|COUNT[2]\ : maxii_lcell
-- Equation(s):
-- \inst7|COUNT\(2) = DFFEAS(((\inst7|COUNT\(0) & (\inst7|COUNT\(1) & !\inst7|COUNT\(2))) # (!\inst7|COUNT\(0) & (!\inst7|COUNT\(1) & \inst7|COUNT\(2)))), GLOBAL(\FRQ0~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "03c0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~combout\,
	datab => \inst7|COUNT\(0),
	datac => \inst7|COUNT\(1),
	datad => \inst7|COUNT\(2),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst7|COUNT\(2));

-- Location: LC_X11_Y8_N0
\inst7|COUNT[1]\ : maxii_lcell
-- Equation(s):
-- \inst7|COUNT\(1) = DFFEAS(((!\inst7|COUNT\(2) & (\inst7|COUNT\(0) $ (\inst7|COUNT\(1))))), GLOBAL(\FRQ0~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "003c",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~combout\,
	datab => \inst7|COUNT\(0),
	datac => \inst7|COUNT\(1),
	datad => \inst7|COUNT\(2),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst7|COUNT\(1));

-- Location: LC_X11_Y8_N9
\inst7|COUNT[0]\ : maxii_lcell
-- Equation(s):
-- \inst7|COUNT\(0) = DFFEAS(((!\inst7|COUNT\(0) & ((!\inst7|COUNT\(2)) # (!\inst7|COUNT\(1))))), GLOBAL(\FRQ0~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0333",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~combout\,
	datab => \inst7|COUNT\(0),
	datac => \inst7|COUNT\(1),
	datad => \inst7|COUNT\(2),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst7|COUNT\(0));

-- Location: LC_X11_Y8_N5
\inst7|DIVD\ : maxii_lcell
-- Equation(s):
-- \inst7|DIVD~regout\ = DFFEAS(\inst7|DIVD~regout\ $ (((\inst7|COUNT\(2) & ((\inst7|COUNT\(0)) # (\inst7|COUNT\(1)))))), GLOBAL(\FRQ0~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "56aa",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \FRQ0~combout\,
	dataa => \inst7|DIVD~regout\,
	datab => \inst7|COUNT\(0),
	datac => \inst7|COUNT\(1),
	datad => \inst7|COUNT\(2),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst7|DIVD~regout\);

-- Location: LC_X1_Y7_N0
\inst1|PSUM[1][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][0]~regout\ = DFFEAS(((!\inst1|PSUM[1][0]~regout\)), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][0]~317\ = CARRY(((\inst1|PSUM[1][0]~regout\)))
-- \inst1|PSUM[1][0]~317COUT1_433\ = CARRY(((\inst1|PSUM[1][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	datab => \inst1|PSUM[1][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][0]~regout\,
	cout0 => \inst1|PSUM[1][0]~317\,
	cout1 => \inst1|PSUM[1][0]~317COUT1_433\);

-- Location: LC_X1_Y7_N1
\inst1|PSUM[1][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][1]~regout\ = DFFEAS((\inst1|PSUM[1][1]~regout\ $ ((\inst1|PSUM[1][0]~317\))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][1]~313\ = CARRY(((!\inst1|PSUM[1][0]~317\) # (!\inst1|PSUM[1][1]~regout\)))
-- \inst1|PSUM[1][1]~313COUT1_434\ = CARRY(((!\inst1|PSUM[1][0]~317COUT1_433\) # (!\inst1|PSUM[1][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	datab => \inst1|PSUM[1][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[1][0]~317\,
	cin1 => \inst1|PSUM[1][0]~317COUT1_433\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][1]~regout\,
	cout0 => \inst1|PSUM[1][1]~313\,
	cout1 => \inst1|PSUM[1][1]~313COUT1_434\);

-- Location: LC_X1_Y7_N2
\inst1|PSUM[1][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][2]~regout\ = DFFEAS((\inst1|PSUM[1][2]~regout\ $ ((!\inst1|PSUM[1][1]~313\))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][2]~309\ = CARRY(((\inst1|PSUM[1][2]~regout\ & !\inst1|PSUM[1][1]~313\)))
-- \inst1|PSUM[1][2]~309COUT1_435\ = CARRY(((\inst1|PSUM[1][2]~regout\ & !\inst1|PSUM[1][1]~313COUT1_434\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	datab => \inst1|PSUM[1][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[1][1]~313\,
	cin1 => \inst1|PSUM[1][1]~313COUT1_434\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][2]~regout\,
	cout0 => \inst1|PSUM[1][2]~309\,
	cout1 => \inst1|PSUM[1][2]~309COUT1_435\);

-- Location: LC_X1_Y7_N3
\inst1|PSUM[1][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][3]~regout\ = DFFEAS(\inst1|PSUM[1][3]~regout\ $ ((((\inst1|PSUM[1][2]~309\)))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][3]~305\ = CARRY(((!\inst1|PSUM[1][2]~309\)) # (!\inst1|PSUM[1][3]~regout\))
-- \inst1|PSUM[1][3]~305COUT1_436\ = CARRY(((!\inst1|PSUM[1][2]~309COUT1_435\)) # (!\inst1|PSUM[1][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	dataa => \inst1|PSUM[1][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[1][2]~309\,
	cin1 => \inst1|PSUM[1][2]~309COUT1_435\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][3]~regout\,
	cout0 => \inst1|PSUM[1][3]~305\,
	cout1 => \inst1|PSUM[1][3]~305COUT1_436\);

-- Location: LC_X1_Y7_N4
\inst1|PSUM[1][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][4]~regout\ = DFFEAS(\inst1|PSUM[1][4]~regout\ $ ((((!\inst1|PSUM[1][3]~305\)))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][4]~301\ = CARRY((\inst1|PSUM[1][4]~regout\ & ((!\inst1|PSUM[1][3]~305COUT1_436\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	dataa => \inst1|PSUM[1][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[1][3]~305\,
	cin1 => \inst1|PSUM[1][3]~305COUT1_436\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][4]~regout\,
	cout => \inst1|PSUM[1][4]~301\);

-- Location: LC_X1_Y7_N5
\inst1|PSUM[1][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][5]~regout\ = DFFEAS(\inst1|PSUM[1][5]~regout\ $ ((((\inst1|PSUM[1][4]~301\)))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][5]~297\ = CARRY(((!\inst1|PSUM[1][4]~301\)) # (!\inst1|PSUM[1][5]~regout\))
-- \inst1|PSUM[1][5]~297COUT1_437\ = CARRY(((!\inst1|PSUM[1][4]~301\)) # (!\inst1|PSUM[1][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	dataa => \inst1|PSUM[1][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[1][4]~301\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][5]~regout\,
	cout0 => \inst1|PSUM[1][5]~297\,
	cout1 => \inst1|PSUM[1][5]~297COUT1_437\);

-- Location: LC_X1_Y7_N6
\inst1|PSUM[1][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][6]~regout\ = DFFEAS(\inst1|PSUM[1][6]~regout\ $ ((((!(!\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][5]~297\) # (\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][5]~297COUT1_437\))))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][6]~293\ = CARRY((\inst1|PSUM[1][6]~regout\ & ((!\inst1|PSUM[1][5]~297\))))
-- \inst1|PSUM[1][6]~293COUT1_438\ = CARRY((\inst1|PSUM[1][6]~regout\ & ((!\inst1|PSUM[1][5]~297COUT1_437\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	dataa => \inst1|PSUM[1][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[1][4]~301\,
	cin0 => \inst1|PSUM[1][5]~297\,
	cin1 => \inst1|PSUM[1][5]~297COUT1_437\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][6]~regout\,
	cout0 => \inst1|PSUM[1][6]~293\,
	cout1 => \inst1|PSUM[1][6]~293COUT1_438\);

-- Location: LC_X1_Y7_N7
\inst1|PSUM[1][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][7]~regout\ = DFFEAS((\inst1|PSUM[1][7]~regout\ $ (((!\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][6]~293\) # (\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][6]~293COUT1_438\)))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][7]~289\ = CARRY(((!\inst1|PSUM[1][6]~293\) # (!\inst1|PSUM[1][7]~regout\)))
-- \inst1|PSUM[1][7]~289COUT1_439\ = CARRY(((!\inst1|PSUM[1][6]~293COUT1_438\) # (!\inst1|PSUM[1][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	datab => \inst1|PSUM[1][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[1][4]~301\,
	cin0 => \inst1|PSUM[1][6]~293\,
	cin1 => \inst1|PSUM[1][6]~293COUT1_438\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][7]~regout\,
	cout0 => \inst1|PSUM[1][7]~289\,
	cout1 => \inst1|PSUM[1][7]~289COUT1_439\);

-- Location: LC_X1_Y7_N8
\inst1|PSUM[1][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][8]~regout\ = DFFEAS(\inst1|PSUM[1][8]~regout\ $ ((((!(!\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][7]~289\) # (\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][7]~289COUT1_439\))))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[1][8]~285\ = CARRY((\inst1|PSUM[1][8]~regout\ & ((!\inst1|PSUM[1][7]~289\))))
-- \inst1|PSUM[1][8]~285COUT1_440\ = CARRY((\inst1|PSUM[1][8]~regout\ & ((!\inst1|PSUM[1][7]~289COUT1_439\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	dataa => \inst1|PSUM[1][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[1][4]~301\,
	cin0 => \inst1|PSUM[1][7]~289\,
	cin1 => \inst1|PSUM[1][7]~289COUT1_439\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][8]~regout\,
	cout0 => \inst1|PSUM[1][8]~285\,
	cout1 => \inst1|PSUM[1][8]~285COUT1_440\);

-- Location: LC_X1_Y7_N9
\inst1|PSUM[1][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[1][9]~regout\ = DFFEAS((((!\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][8]~285\) # (\inst1|PSUM[1][4]~301\ & \inst1|PSUM[1][8]~285COUT1_440\) $ (\inst1|PSUM[1][9]~regout\))), \inst6|STEMP\(1), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(1),
	datad => \inst1|PSUM[1][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[1][4]~301\,
	cin0 => \inst1|PSUM[1][8]~285\,
	cin1 => \inst1|PSUM[1][8]~285COUT1_440\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[1][9]~regout\);

-- Location: LC_X12_Y3_N4
\inst6|STEMP[0]\ : maxii_lcell
-- Equation(s):
-- \inst6|STEMP\(0) = LCELL((((\inst6|STEMP\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst6|STEMP\(0));

-- Location: LC_X1_Y9_N0
\inst1|PSUM[0][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][0]~regout\ = DFFEAS(((!\inst1|PSUM[0][0]~regout\)), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][0]~319\ = CARRY(((\inst1|PSUM[0][0]~regout\)))
-- \inst1|PSUM[0][0]~319COUT1_441\ = CARRY(((\inst1|PSUM[0][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	datab => \inst1|PSUM[0][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][0]~regout\,
	cout0 => \inst1|PSUM[0][0]~319\,
	cout1 => \inst1|PSUM[0][0]~319COUT1_441\);

-- Location: LC_X1_Y9_N1
\inst1|PSUM[0][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][1]~regout\ = DFFEAS((\inst1|PSUM[0][1]~regout\ $ ((\inst1|PSUM[0][0]~319\))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][1]~315\ = CARRY(((!\inst1|PSUM[0][0]~319\) # (!\inst1|PSUM[0][1]~regout\)))
-- \inst1|PSUM[0][1]~315COUT1_442\ = CARRY(((!\inst1|PSUM[0][0]~319COUT1_441\) # (!\inst1|PSUM[0][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	datab => \inst1|PSUM[0][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[0][0]~319\,
	cin1 => \inst1|PSUM[0][0]~319COUT1_441\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][1]~regout\,
	cout0 => \inst1|PSUM[0][1]~315\,
	cout1 => \inst1|PSUM[0][1]~315COUT1_442\);

-- Location: LC_X1_Y9_N2
\inst1|PSUM[0][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][2]~regout\ = DFFEAS((\inst1|PSUM[0][2]~regout\ $ ((!\inst1|PSUM[0][1]~315\))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][2]~311\ = CARRY(((\inst1|PSUM[0][2]~regout\ & !\inst1|PSUM[0][1]~315\)))
-- \inst1|PSUM[0][2]~311COUT1_443\ = CARRY(((\inst1|PSUM[0][2]~regout\ & !\inst1|PSUM[0][1]~315COUT1_442\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	datab => \inst1|PSUM[0][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[0][1]~315\,
	cin1 => \inst1|PSUM[0][1]~315COUT1_442\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][2]~regout\,
	cout0 => \inst1|PSUM[0][2]~311\,
	cout1 => \inst1|PSUM[0][2]~311COUT1_443\);

-- Location: LC_X1_Y9_N3
\inst1|PSUM[0][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][3]~regout\ = DFFEAS(\inst1|PSUM[0][3]~regout\ $ ((((\inst1|PSUM[0][2]~311\)))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][3]~307\ = CARRY(((!\inst1|PSUM[0][2]~311\)) # (!\inst1|PSUM[0][3]~regout\))
-- \inst1|PSUM[0][3]~307COUT1_444\ = CARRY(((!\inst1|PSUM[0][2]~311COUT1_443\)) # (!\inst1|PSUM[0][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	dataa => \inst1|PSUM[0][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[0][2]~311\,
	cin1 => \inst1|PSUM[0][2]~311COUT1_443\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][3]~regout\,
	cout0 => \inst1|PSUM[0][3]~307\,
	cout1 => \inst1|PSUM[0][3]~307COUT1_444\);

-- Location: LC_X1_Y9_N4
\inst1|PSUM[0][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][4]~regout\ = DFFEAS(\inst1|PSUM[0][4]~regout\ $ ((((!\inst1|PSUM[0][3]~307\)))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][4]~303\ = CARRY((\inst1|PSUM[0][4]~regout\ & ((!\inst1|PSUM[0][3]~307COUT1_444\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	dataa => \inst1|PSUM[0][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[0][3]~307\,
	cin1 => \inst1|PSUM[0][3]~307COUT1_444\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][4]~regout\,
	cout => \inst1|PSUM[0][4]~303\);

-- Location: LC_X1_Y9_N5
\inst1|PSUM[0][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][5]~regout\ = DFFEAS(\inst1|PSUM[0][5]~regout\ $ ((((\inst1|PSUM[0][4]~303\)))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][5]~299\ = CARRY(((!\inst1|PSUM[0][4]~303\)) # (!\inst1|PSUM[0][5]~regout\))
-- \inst1|PSUM[0][5]~299COUT1_445\ = CARRY(((!\inst1|PSUM[0][4]~303\)) # (!\inst1|PSUM[0][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	dataa => \inst1|PSUM[0][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[0][4]~303\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][5]~regout\,
	cout0 => \inst1|PSUM[0][5]~299\,
	cout1 => \inst1|PSUM[0][5]~299COUT1_445\);

-- Location: LC_X1_Y9_N6
\inst1|PSUM[0][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][6]~regout\ = DFFEAS(\inst1|PSUM[0][6]~regout\ $ ((((!(!\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][5]~299\) # (\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][5]~299COUT1_445\))))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][6]~295\ = CARRY((\inst1|PSUM[0][6]~regout\ & ((!\inst1|PSUM[0][5]~299\))))
-- \inst1|PSUM[0][6]~295COUT1_446\ = CARRY((\inst1|PSUM[0][6]~regout\ & ((!\inst1|PSUM[0][5]~299COUT1_445\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	dataa => \inst1|PSUM[0][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[0][4]~303\,
	cin0 => \inst1|PSUM[0][5]~299\,
	cin1 => \inst1|PSUM[0][5]~299COUT1_445\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][6]~regout\,
	cout0 => \inst1|PSUM[0][6]~295\,
	cout1 => \inst1|PSUM[0][6]~295COUT1_446\);

-- Location: LC_X1_Y9_N7
\inst1|PSUM[0][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][7]~regout\ = DFFEAS((\inst1|PSUM[0][7]~regout\ $ (((!\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][6]~295\) # (\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][6]~295COUT1_446\)))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][7]~291\ = CARRY(((!\inst1|PSUM[0][6]~295\) # (!\inst1|PSUM[0][7]~regout\)))
-- \inst1|PSUM[0][7]~291COUT1_447\ = CARRY(((!\inst1|PSUM[0][6]~295COUT1_446\) # (!\inst1|PSUM[0][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	datab => \inst1|PSUM[0][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[0][4]~303\,
	cin0 => \inst1|PSUM[0][6]~295\,
	cin1 => \inst1|PSUM[0][6]~295COUT1_446\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][7]~regout\,
	cout0 => \inst1|PSUM[0][7]~291\,
	cout1 => \inst1|PSUM[0][7]~291COUT1_447\);

-- Location: LC_X1_Y9_N8
\inst1|PSUM[0][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][8]~regout\ = DFFEAS(\inst1|PSUM[0][8]~regout\ $ ((((!(!\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][7]~291\) # (\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][7]~291COUT1_447\))))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[0][8]~287\ = CARRY((\inst1|PSUM[0][8]~regout\ & ((!\inst1|PSUM[0][7]~291\))))
-- \inst1|PSUM[0][8]~287COUT1_448\ = CARRY((\inst1|PSUM[0][8]~regout\ & ((!\inst1|PSUM[0][7]~291COUT1_447\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	dataa => \inst1|PSUM[0][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[0][4]~303\,
	cin0 => \inst1|PSUM[0][7]~291\,
	cin1 => \inst1|PSUM[0][7]~291COUT1_447\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][8]~regout\,
	cout0 => \inst1|PSUM[0][8]~287\,
	cout1 => \inst1|PSUM[0][8]~287COUT1_448\);

-- Location: LC_X1_Y9_N9
\inst1|PSUM[0][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[0][9]~regout\ = DFFEAS((((!\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][8]~287\) # (\inst1|PSUM[0][4]~303\ & \inst1|PSUM[0][8]~287COUT1_448\) $ (\inst1|PSUM[0][9]~regout\))), GLOBAL(\inst6|STEMP\(0)), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(0),
	datad => \inst1|PSUM[0][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[0][4]~303\,
	cin0 => \inst1|PSUM[0][8]~287\,
	cin1 => \inst1|PSUM[0][8]~287COUT1_448\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[0][9]~regout\);

-- Location: LC_X1_Y8_N0
\inst1|Add0~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~45_combout\ = \inst1|PSUM[0][0]~regout\ $ ((\inst1|PSUM[1][0]~regout\))
-- \inst1|Add0~47\ = CARRY((\inst1|PSUM[0][0]~regout\ & (\inst1|PSUM[1][0]~regout\)))
-- \inst1|Add0~47COUT1_51\ = CARRY((\inst1|PSUM[0][0]~regout\ & (\inst1|PSUM[1][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[0][0]~regout\,
	datab => \inst1|PSUM[1][0]~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~45_combout\,
	cout0 => \inst1|Add0~47\,
	cout1 => \inst1|Add0~47COUT1_51\);

-- Location: LC_X1_Y8_N1
\inst1|Add0~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~40_combout\ = \inst1|PSUM[1][1]~regout\ $ (\inst1|PSUM[0][1]~regout\ $ ((\inst1|Add0~47\)))
-- \inst1|Add0~42\ = CARRY((\inst1|PSUM[1][1]~regout\ & (!\inst1|PSUM[0][1]~regout\ & !\inst1|Add0~47\)) # (!\inst1|PSUM[1][1]~regout\ & ((!\inst1|Add0~47\) # (!\inst1|PSUM[0][1]~regout\))))
-- \inst1|Add0~42COUT1_52\ = CARRY((\inst1|PSUM[1][1]~regout\ & (!\inst1|PSUM[0][1]~regout\ & !\inst1|Add0~47COUT1_51\)) # (!\inst1|PSUM[1][1]~regout\ & ((!\inst1|Add0~47COUT1_51\) # (!\inst1|PSUM[0][1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[1][1]~regout\,
	datab => \inst1|PSUM[0][1]~regout\,
	cin0 => \inst1|Add0~47\,
	cin1 => \inst1|Add0~47COUT1_51\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~40_combout\,
	cout0 => \inst1|Add0~42\,
	cout1 => \inst1|Add0~42COUT1_52\);

-- Location: LC_X1_Y8_N2
\inst1|Add0~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~35_combout\ = \inst1|PSUM[0][2]~regout\ $ (\inst1|PSUM[1][2]~regout\ $ ((!\inst1|Add0~42\)))
-- \inst1|Add0~37\ = CARRY((\inst1|PSUM[0][2]~regout\ & ((\inst1|PSUM[1][2]~regout\) # (!\inst1|Add0~42\))) # (!\inst1|PSUM[0][2]~regout\ & (\inst1|PSUM[1][2]~regout\ & !\inst1|Add0~42\)))
-- \inst1|Add0~37COUT1_53\ = CARRY((\inst1|PSUM[0][2]~regout\ & ((\inst1|PSUM[1][2]~regout\) # (!\inst1|Add0~42COUT1_52\))) # (!\inst1|PSUM[0][2]~regout\ & (\inst1|PSUM[1][2]~regout\ & !\inst1|Add0~42COUT1_52\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[0][2]~regout\,
	datab => \inst1|PSUM[1][2]~regout\,
	cin0 => \inst1|Add0~42\,
	cin1 => \inst1|Add0~42COUT1_52\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~35_combout\,
	cout0 => \inst1|Add0~37\,
	cout1 => \inst1|Add0~37COUT1_53\);

-- Location: LC_X1_Y8_N3
\inst1|Add0~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~30_combout\ = \inst1|PSUM[1][3]~regout\ $ (\inst1|PSUM[0][3]~regout\ $ ((\inst1|Add0~37\)))
-- \inst1|Add0~32\ = CARRY((\inst1|PSUM[1][3]~regout\ & (!\inst1|PSUM[0][3]~regout\ & !\inst1|Add0~37\)) # (!\inst1|PSUM[1][3]~regout\ & ((!\inst1|Add0~37\) # (!\inst1|PSUM[0][3]~regout\))))
-- \inst1|Add0~32COUT1_54\ = CARRY((\inst1|PSUM[1][3]~regout\ & (!\inst1|PSUM[0][3]~regout\ & !\inst1|Add0~37COUT1_53\)) # (!\inst1|PSUM[1][3]~regout\ & ((!\inst1|Add0~37COUT1_53\) # (!\inst1|PSUM[0][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[1][3]~regout\,
	datab => \inst1|PSUM[0][3]~regout\,
	cin0 => \inst1|Add0~37\,
	cin1 => \inst1|Add0~37COUT1_53\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~30_combout\,
	cout0 => \inst1|Add0~32\,
	cout1 => \inst1|Add0~32COUT1_54\);

-- Location: LC_X1_Y8_N4
\inst1|Add0~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~25_combout\ = \inst1|PSUM[1][4]~regout\ $ (\inst1|PSUM[0][4]~regout\ $ ((!\inst1|Add0~32\)))
-- \inst1|Add0~27\ = CARRY((\inst1|PSUM[1][4]~regout\ & ((\inst1|PSUM[0][4]~regout\) # (!\inst1|Add0~32COUT1_54\))) # (!\inst1|PSUM[1][4]~regout\ & (\inst1|PSUM[0][4]~regout\ & !\inst1|Add0~32COUT1_54\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[1][4]~regout\,
	datab => \inst1|PSUM[0][4]~regout\,
	cin0 => \inst1|Add0~32\,
	cin1 => \inst1|Add0~32COUT1_54\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~25_combout\,
	cout => \inst1|Add0~27\);

-- Location: LC_X1_Y8_N5
\inst1|Add0~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~20_combout\ = \inst1|PSUM[0][5]~regout\ $ (\inst1|PSUM[1][5]~regout\ $ ((\inst1|Add0~27\)))
-- \inst1|Add0~22\ = CARRY((\inst1|PSUM[0][5]~regout\ & (!\inst1|PSUM[1][5]~regout\ & !\inst1|Add0~27\)) # (!\inst1|PSUM[0][5]~regout\ & ((!\inst1|Add0~27\) # (!\inst1|PSUM[1][5]~regout\))))
-- \inst1|Add0~22COUT1_55\ = CARRY((\inst1|PSUM[0][5]~regout\ & (!\inst1|PSUM[1][5]~regout\ & !\inst1|Add0~27\)) # (!\inst1|PSUM[0][5]~regout\ & ((!\inst1|Add0~27\) # (!\inst1|PSUM[1][5]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[0][5]~regout\,
	datab => \inst1|PSUM[1][5]~regout\,
	cin => \inst1|Add0~27\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~20_combout\,
	cout0 => \inst1|Add0~22\,
	cout1 => \inst1|Add0~22COUT1_55\);

-- Location: LC_X1_Y8_N6
\inst1|Add0~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~15_combout\ = \inst1|PSUM[1][6]~regout\ $ (\inst1|PSUM[0][6]~regout\ $ ((!(!\inst1|Add0~27\ & \inst1|Add0~22\) # (\inst1|Add0~27\ & \inst1|Add0~22COUT1_55\))))
-- \inst1|Add0~17\ = CARRY((\inst1|PSUM[1][6]~regout\ & ((\inst1|PSUM[0][6]~regout\) # (!\inst1|Add0~22\))) # (!\inst1|PSUM[1][6]~regout\ & (\inst1|PSUM[0][6]~regout\ & !\inst1|Add0~22\)))
-- \inst1|Add0~17COUT1_56\ = CARRY((\inst1|PSUM[1][6]~regout\ & ((\inst1|PSUM[0][6]~regout\) # (!\inst1|Add0~22COUT1_55\))) # (!\inst1|PSUM[1][6]~regout\ & (\inst1|PSUM[0][6]~regout\ & !\inst1|Add0~22COUT1_55\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[1][6]~regout\,
	datab => \inst1|PSUM[0][6]~regout\,
	cin => \inst1|Add0~27\,
	cin0 => \inst1|Add0~22\,
	cin1 => \inst1|Add0~22COUT1_55\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~15_combout\,
	cout0 => \inst1|Add0~17\,
	cout1 => \inst1|Add0~17COUT1_56\);

-- Location: LC_X1_Y8_N7
\inst1|Add0~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~10_combout\ = \inst1|PSUM[1][7]~regout\ $ (\inst1|PSUM[0][7]~regout\ $ (((!\inst1|Add0~27\ & \inst1|Add0~17\) # (\inst1|Add0~27\ & \inst1|Add0~17COUT1_56\))))
-- \inst1|Add0~12\ = CARRY((\inst1|PSUM[1][7]~regout\ & (!\inst1|PSUM[0][7]~regout\ & !\inst1|Add0~17\)) # (!\inst1|PSUM[1][7]~regout\ & ((!\inst1|Add0~17\) # (!\inst1|PSUM[0][7]~regout\))))
-- \inst1|Add0~12COUT1_57\ = CARRY((\inst1|PSUM[1][7]~regout\ & (!\inst1|PSUM[0][7]~regout\ & !\inst1|Add0~17COUT1_56\)) # (!\inst1|PSUM[1][7]~regout\ & ((!\inst1|Add0~17COUT1_56\) # (!\inst1|PSUM[0][7]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[1][7]~regout\,
	datab => \inst1|PSUM[0][7]~regout\,
	cin => \inst1|Add0~27\,
	cin0 => \inst1|Add0~17\,
	cin1 => \inst1|Add0~17COUT1_56\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~10_combout\,
	cout0 => \inst1|Add0~12\,
	cout1 => \inst1|Add0~12COUT1_57\);

-- Location: LC_X1_Y8_N8
\inst1|Add0~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~5_combout\ = \inst1|PSUM[1][8]~regout\ $ (\inst1|PSUM[0][8]~regout\ $ ((!(!\inst1|Add0~27\ & \inst1|Add0~12\) # (\inst1|Add0~27\ & \inst1|Add0~12COUT1_57\))))
-- \inst1|Add0~7\ = CARRY((\inst1|PSUM[1][8]~regout\ & ((\inst1|PSUM[0][8]~regout\) # (!\inst1|Add0~12\))) # (!\inst1|PSUM[1][8]~regout\ & (\inst1|PSUM[0][8]~regout\ & !\inst1|Add0~12\)))
-- \inst1|Add0~7COUT1_58\ = CARRY((\inst1|PSUM[1][8]~regout\ & ((\inst1|PSUM[0][8]~regout\) # (!\inst1|Add0~12COUT1_57\))) # (!\inst1|PSUM[1][8]~regout\ & (\inst1|PSUM[0][8]~regout\ & !\inst1|Add0~12COUT1_57\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[1][8]~regout\,
	datab => \inst1|PSUM[0][8]~regout\,
	cin => \inst1|Add0~27\,
	cin0 => \inst1|Add0~12\,
	cin1 => \inst1|Add0~12COUT1_57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~5_combout\,
	cout0 => \inst1|Add0~7\,
	cout1 => \inst1|Add0~7COUT1_58\);

-- Location: LC_X1_Y8_N9
\inst1|Add0~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add0~0_combout\ = \inst1|PSUM[1][9]~regout\ $ ((((!\inst1|Add0~27\ & \inst1|Add0~7\) # (\inst1|Add0~27\ & \inst1|Add0~7COUT1_58\) $ (\inst1|PSUM[0][9]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a55a",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[1][9]~regout\,
	datad => \inst1|PSUM[0][9]~regout\,
	cin => \inst1|Add0~27\,
	cin0 => \inst1|Add0~7\,
	cin1 => \inst1|Add0~7COUT1_58\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add0~0_combout\);

-- Location: LC_X1_Y6_N4
\inst1|TSUM[0][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][9]~combout\ = LCELL((((\inst1|Add0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][9]~combout\);

-- Location: LC_X6_Y4_N0
\inst1|PSUM[2][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][0]~regout\ = DFFEAS(((!\inst1|PSUM[2][0]~regout\)), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][0]~279\ = CARRY(((\inst1|PSUM[2][0]~regout\)))
-- \inst1|PSUM[2][0]~279COUT1_425\ = CARRY(((\inst1|PSUM[2][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	datab => \inst1|PSUM[2][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][0]~regout\,
	cout0 => \inst1|PSUM[2][0]~279\,
	cout1 => \inst1|PSUM[2][0]~279COUT1_425\);

-- Location: LC_X6_Y4_N1
\inst1|PSUM[2][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][1]~regout\ = DFFEAS((\inst1|PSUM[2][1]~regout\ $ ((\inst1|PSUM[2][0]~279\))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][1]~277\ = CARRY(((!\inst1|PSUM[2][0]~279\) # (!\inst1|PSUM[2][1]~regout\)))
-- \inst1|PSUM[2][1]~277COUT1_426\ = CARRY(((!\inst1|PSUM[2][0]~279COUT1_425\) # (!\inst1|PSUM[2][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	datab => \inst1|PSUM[2][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[2][0]~279\,
	cin1 => \inst1|PSUM[2][0]~279COUT1_425\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][1]~regout\,
	cout0 => \inst1|PSUM[2][1]~277\,
	cout1 => \inst1|PSUM[2][1]~277COUT1_426\);

-- Location: LC_X6_Y4_N2
\inst1|PSUM[2][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][2]~regout\ = DFFEAS((\inst1|PSUM[2][2]~regout\ $ ((!\inst1|PSUM[2][1]~277\))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][2]~275\ = CARRY(((\inst1|PSUM[2][2]~regout\ & !\inst1|PSUM[2][1]~277\)))
-- \inst1|PSUM[2][2]~275COUT1_427\ = CARRY(((\inst1|PSUM[2][2]~regout\ & !\inst1|PSUM[2][1]~277COUT1_426\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	datab => \inst1|PSUM[2][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[2][1]~277\,
	cin1 => \inst1|PSUM[2][1]~277COUT1_426\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][2]~regout\,
	cout0 => \inst1|PSUM[2][2]~275\,
	cout1 => \inst1|PSUM[2][2]~275COUT1_427\);

-- Location: LC_X6_Y4_N3
\inst1|PSUM[2][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][3]~regout\ = DFFEAS(\inst1|PSUM[2][3]~regout\ $ ((((\inst1|PSUM[2][2]~275\)))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][3]~273\ = CARRY(((!\inst1|PSUM[2][2]~275\)) # (!\inst1|PSUM[2][3]~regout\))
-- \inst1|PSUM[2][3]~273COUT1_428\ = CARRY(((!\inst1|PSUM[2][2]~275COUT1_427\)) # (!\inst1|PSUM[2][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	dataa => \inst1|PSUM[2][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[2][2]~275\,
	cin1 => \inst1|PSUM[2][2]~275COUT1_427\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][3]~regout\,
	cout0 => \inst1|PSUM[2][3]~273\,
	cout1 => \inst1|PSUM[2][3]~273COUT1_428\);

-- Location: LC_X6_Y4_N4
\inst1|PSUM[2][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][4]~regout\ = DFFEAS(\inst1|PSUM[2][4]~regout\ $ ((((!\inst1|PSUM[2][3]~273\)))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][4]~271\ = CARRY((\inst1|PSUM[2][4]~regout\ & ((!\inst1|PSUM[2][3]~273COUT1_428\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	dataa => \inst1|PSUM[2][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[2][3]~273\,
	cin1 => \inst1|PSUM[2][3]~273COUT1_428\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][4]~regout\,
	cout => \inst1|PSUM[2][4]~271\);

-- Location: LC_X6_Y4_N5
\inst1|PSUM[2][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][5]~regout\ = DFFEAS(\inst1|PSUM[2][5]~regout\ $ ((((\inst1|PSUM[2][4]~271\)))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][5]~269\ = CARRY(((!\inst1|PSUM[2][4]~271\)) # (!\inst1|PSUM[2][5]~regout\))
-- \inst1|PSUM[2][5]~269COUT1_429\ = CARRY(((!\inst1|PSUM[2][4]~271\)) # (!\inst1|PSUM[2][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	dataa => \inst1|PSUM[2][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[2][4]~271\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][5]~regout\,
	cout0 => \inst1|PSUM[2][5]~269\,
	cout1 => \inst1|PSUM[2][5]~269COUT1_429\);

-- Location: LC_X6_Y4_N6
\inst1|PSUM[2][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][6]~regout\ = DFFEAS(\inst1|PSUM[2][6]~regout\ $ ((((!(!\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][5]~269\) # (\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][5]~269COUT1_429\))))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][6]~267\ = CARRY((\inst1|PSUM[2][6]~regout\ & ((!\inst1|PSUM[2][5]~269\))))
-- \inst1|PSUM[2][6]~267COUT1_430\ = CARRY((\inst1|PSUM[2][6]~regout\ & ((!\inst1|PSUM[2][5]~269COUT1_429\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	dataa => \inst1|PSUM[2][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[2][4]~271\,
	cin0 => \inst1|PSUM[2][5]~269\,
	cin1 => \inst1|PSUM[2][5]~269COUT1_429\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][6]~regout\,
	cout0 => \inst1|PSUM[2][6]~267\,
	cout1 => \inst1|PSUM[2][6]~267COUT1_430\);

-- Location: LC_X6_Y4_N7
\inst1|PSUM[2][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][7]~regout\ = DFFEAS((\inst1|PSUM[2][7]~regout\ $ (((!\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][6]~267\) # (\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][6]~267COUT1_430\)))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][7]~265\ = CARRY(((!\inst1|PSUM[2][6]~267\) # (!\inst1|PSUM[2][7]~regout\)))
-- \inst1|PSUM[2][7]~265COUT1_431\ = CARRY(((!\inst1|PSUM[2][6]~267COUT1_430\) # (!\inst1|PSUM[2][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	datab => \inst1|PSUM[2][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[2][4]~271\,
	cin0 => \inst1|PSUM[2][6]~267\,
	cin1 => \inst1|PSUM[2][6]~267COUT1_430\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][7]~regout\,
	cout0 => \inst1|PSUM[2][7]~265\,
	cout1 => \inst1|PSUM[2][7]~265COUT1_431\);

-- Location: LC_X6_Y4_N8
\inst1|PSUM[2][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][8]~regout\ = DFFEAS(\inst1|PSUM[2][8]~regout\ $ ((((!(!\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][7]~265\) # (\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][7]~265COUT1_431\))))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[2][8]~263\ = CARRY((\inst1|PSUM[2][8]~regout\ & ((!\inst1|PSUM[2][7]~265\))))
-- \inst1|PSUM[2][8]~263COUT1_432\ = CARRY((\inst1|PSUM[2][8]~regout\ & ((!\inst1|PSUM[2][7]~265COUT1_431\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	dataa => \inst1|PSUM[2][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[2][4]~271\,
	cin0 => \inst1|PSUM[2][7]~265\,
	cin1 => \inst1|PSUM[2][7]~265COUT1_431\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][8]~regout\,
	cout0 => \inst1|PSUM[2][8]~263\,
	cout1 => \inst1|PSUM[2][8]~263COUT1_432\);

-- Location: LC_X6_Y4_N9
\inst1|PSUM[2][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[2][9]~regout\ = DFFEAS((((!\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][8]~263\) # (\inst1|PSUM[2][4]~271\ & \inst1|PSUM[2][8]~263COUT1_432\) $ (\inst1|PSUM[2][9]~regout\))), \inst6|STEMP\(2), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(2),
	datad => \inst1|PSUM[2][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[2][4]~271\,
	cin0 => \inst1|PSUM[2][8]~263\,
	cin1 => \inst1|PSUM[2][8]~263COUT1_432\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[2][9]~regout\);

-- Location: LC_X1_Y6_N0
\inst1|TSUM[0][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][4]~combout\ = LCELL((((\inst1|Add0~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add0~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][4]~combout\);

-- Location: LC_X1_Y6_N2
\inst1|TSUM[0][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][3]~combout\ = LCELL((((\inst1|Add0~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add0~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][3]~combout\);

-- Location: LC_X1_Y6_N1
\inst1|TSUM[0][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][2]~combout\ = LCELL((((\inst1|Add0~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add0~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][2]~combout\);

-- Location: LC_X1_Y6_N6
\inst1|TSUM[0][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][1]~combout\ = LCELL((((\inst1|Add0~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add0~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][1]~combout\);

-- Location: LC_X1_Y6_N7
\inst1|TSUM[0][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][0]~combout\ = LCELL((((\inst1|Add0~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add0~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][0]~combout\);

-- Location: LC_X2_Y4_N5
\inst1|Add1~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~150_combout\ = \inst1|PSUM[2][0]~regout\ $ ((\inst1|TSUM[0][0]~combout\))
-- \inst1|Add1~152\ = CARRY((\inst1|PSUM[2][0]~regout\ & (\inst1|TSUM[0][0]~combout\)))
-- \inst1|Add1~152COUT1_156\ = CARRY((\inst1|PSUM[2][0]~regout\ & (\inst1|TSUM[0][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[2][0]~regout\,
	datab => \inst1|TSUM[0][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~150_combout\,
	cout0 => \inst1|Add1~152\,
	cout1 => \inst1|Add1~152COUT1_156\);

-- Location: LC_X2_Y4_N6
\inst1|Add1~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~145_combout\ = \inst1|TSUM[0][1]~combout\ $ (\inst1|PSUM[2][1]~regout\ $ ((\inst1|Add1~152\)))
-- \inst1|Add1~147\ = CARRY((\inst1|TSUM[0][1]~combout\ & (!\inst1|PSUM[2][1]~regout\ & !\inst1|Add1~152\)) # (!\inst1|TSUM[0][1]~combout\ & ((!\inst1|Add1~152\) # (!\inst1|PSUM[2][1]~regout\))))
-- \inst1|Add1~147COUT1_157\ = CARRY((\inst1|TSUM[0][1]~combout\ & (!\inst1|PSUM[2][1]~regout\ & !\inst1|Add1~152COUT1_156\)) # (!\inst1|TSUM[0][1]~combout\ & ((!\inst1|Add1~152COUT1_156\) # (!\inst1|PSUM[2][1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][1]~combout\,
	datab => \inst1|PSUM[2][1]~regout\,
	cin0 => \inst1|Add1~152\,
	cin1 => \inst1|Add1~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~145_combout\,
	cout0 => \inst1|Add1~147\,
	cout1 => \inst1|Add1~147COUT1_157\);

-- Location: LC_X2_Y4_N7
\inst1|Add1~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~140_combout\ = \inst1|TSUM[0][2]~combout\ $ (\inst1|PSUM[2][2]~regout\ $ ((!\inst1|Add1~147\)))
-- \inst1|Add1~142\ = CARRY((\inst1|TSUM[0][2]~combout\ & ((\inst1|PSUM[2][2]~regout\) # (!\inst1|Add1~147\))) # (!\inst1|TSUM[0][2]~combout\ & (\inst1|PSUM[2][2]~regout\ & !\inst1|Add1~147\)))
-- \inst1|Add1~142COUT1_158\ = CARRY((\inst1|TSUM[0][2]~combout\ & ((\inst1|PSUM[2][2]~regout\) # (!\inst1|Add1~147COUT1_157\))) # (!\inst1|TSUM[0][2]~combout\ & (\inst1|PSUM[2][2]~regout\ & !\inst1|Add1~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][2]~combout\,
	datab => \inst1|PSUM[2][2]~regout\,
	cin0 => \inst1|Add1~147\,
	cin1 => \inst1|Add1~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~140_combout\,
	cout0 => \inst1|Add1~142\,
	cout1 => \inst1|Add1~142COUT1_158\);

-- Location: LC_X2_Y4_N8
\inst1|Add1~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~135_combout\ = \inst1|PSUM[2][3]~regout\ $ (\inst1|TSUM[0][3]~combout\ $ ((\inst1|Add1~142\)))
-- \inst1|Add1~137\ = CARRY((\inst1|PSUM[2][3]~regout\ & (!\inst1|TSUM[0][3]~combout\ & !\inst1|Add1~142\)) # (!\inst1|PSUM[2][3]~regout\ & ((!\inst1|Add1~142\) # (!\inst1|TSUM[0][3]~combout\))))
-- \inst1|Add1~137COUT1_159\ = CARRY((\inst1|PSUM[2][3]~regout\ & (!\inst1|TSUM[0][3]~combout\ & !\inst1|Add1~142COUT1_158\)) # (!\inst1|PSUM[2][3]~regout\ & ((!\inst1|Add1~142COUT1_158\) # (!\inst1|TSUM[0][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[2][3]~regout\,
	datab => \inst1|TSUM[0][3]~combout\,
	cin0 => \inst1|Add1~142\,
	cin1 => \inst1|Add1~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~135_combout\,
	cout0 => \inst1|Add1~137\,
	cout1 => \inst1|Add1~137COUT1_159\);

-- Location: LC_X2_Y4_N9
\inst1|Add1~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~130_combout\ = \inst1|PSUM[2][4]~regout\ $ (\inst1|TSUM[0][4]~combout\ $ ((!\inst1|Add1~137\)))
-- \inst1|Add1~132\ = CARRY((\inst1|PSUM[2][4]~regout\ & ((\inst1|TSUM[0][4]~combout\) # (!\inst1|Add1~137COUT1_159\))) # (!\inst1|PSUM[2][4]~regout\ & (\inst1|TSUM[0][4]~combout\ & !\inst1|Add1~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[2][4]~regout\,
	datab => \inst1|TSUM[0][4]~combout\,
	cin0 => \inst1|Add1~137\,
	cin1 => \inst1|Add1~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~130_combout\,
	cout => \inst1|Add1~132\);

-- Location: LC_X1_Y6_N8
\inst1|TSUM[0][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][8]~combout\ = LCELL((((\inst1|Add0~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add0~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][8]~combout\);

-- Location: LC_X1_Y6_N3
\inst1|TSUM[0][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][7]~combout\ = LCELL((((\inst1|Add0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][7]~combout\);

-- Location: LC_X1_Y6_N5
\inst1|TSUM[0][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][6]~combout\ = LCELL((((\inst1|Add0~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add0~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][6]~combout\);

-- Location: LC_X1_Y6_N9
\inst1|TSUM[0][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][5]~combout\ = LCELL((((\inst1|Add0~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add0~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][5]~combout\);

-- Location: LC_X3_Y4_N0
\inst1|Add1~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~125_combout\ = \inst1|PSUM[2][5]~regout\ $ (\inst1|TSUM[0][5]~combout\ $ ((\inst1|Add1~132\)))
-- \inst1|Add1~127\ = CARRY((\inst1|PSUM[2][5]~regout\ & (!\inst1|TSUM[0][5]~combout\ & !\inst1|Add1~132\)) # (!\inst1|PSUM[2][5]~regout\ & ((!\inst1|Add1~132\) # (!\inst1|TSUM[0][5]~combout\))))
-- \inst1|Add1~127COUT1_160\ = CARRY((\inst1|PSUM[2][5]~regout\ & (!\inst1|TSUM[0][5]~combout\ & !\inst1|Add1~132\)) # (!\inst1|PSUM[2][5]~regout\ & ((!\inst1|Add1~132\) # (!\inst1|TSUM[0][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[2][5]~regout\,
	datab => \inst1|TSUM[0][5]~combout\,
	cin => \inst1|Add1~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~125_combout\,
	cout0 => \inst1|Add1~127\,
	cout1 => \inst1|Add1~127COUT1_160\);

-- Location: LC_X3_Y4_N1
\inst1|Add1~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~120_combout\ = \inst1|PSUM[2][6]~regout\ $ (\inst1|TSUM[0][6]~combout\ $ ((!(!\inst1|Add1~132\ & \inst1|Add1~127\) # (\inst1|Add1~132\ & \inst1|Add1~127COUT1_160\))))
-- \inst1|Add1~122\ = CARRY((\inst1|PSUM[2][6]~regout\ & ((\inst1|TSUM[0][6]~combout\) # (!\inst1|Add1~127\))) # (!\inst1|PSUM[2][6]~regout\ & (\inst1|TSUM[0][6]~combout\ & !\inst1|Add1~127\)))
-- \inst1|Add1~122COUT1_161\ = CARRY((\inst1|PSUM[2][6]~regout\ & ((\inst1|TSUM[0][6]~combout\) # (!\inst1|Add1~127COUT1_160\))) # (!\inst1|PSUM[2][6]~regout\ & (\inst1|TSUM[0][6]~combout\ & !\inst1|Add1~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[2][6]~regout\,
	datab => \inst1|TSUM[0][6]~combout\,
	cin => \inst1|Add1~132\,
	cin0 => \inst1|Add1~127\,
	cin1 => \inst1|Add1~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~120_combout\,
	cout0 => \inst1|Add1~122\,
	cout1 => \inst1|Add1~122COUT1_161\);

-- Location: LC_X3_Y4_N2
\inst1|Add1~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~115_combout\ = \inst1|PSUM[2][7]~regout\ $ (\inst1|TSUM[0][7]~combout\ $ (((!\inst1|Add1~132\ & \inst1|Add1~122\) # (\inst1|Add1~132\ & \inst1|Add1~122COUT1_161\))))
-- \inst1|Add1~117\ = CARRY((\inst1|PSUM[2][7]~regout\ & (!\inst1|TSUM[0][7]~combout\ & !\inst1|Add1~122\)) # (!\inst1|PSUM[2][7]~regout\ & ((!\inst1|Add1~122\) # (!\inst1|TSUM[0][7]~combout\))))
-- \inst1|Add1~117COUT1_162\ = CARRY((\inst1|PSUM[2][7]~regout\ & (!\inst1|TSUM[0][7]~combout\ & !\inst1|Add1~122COUT1_161\)) # (!\inst1|PSUM[2][7]~regout\ & ((!\inst1|Add1~122COUT1_161\) # (!\inst1|TSUM[0][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[2][7]~regout\,
	datab => \inst1|TSUM[0][7]~combout\,
	cin => \inst1|Add1~132\,
	cin0 => \inst1|Add1~122\,
	cin1 => \inst1|Add1~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~115_combout\,
	cout0 => \inst1|Add1~117\,
	cout1 => \inst1|Add1~117COUT1_162\);

-- Location: LC_X3_Y4_N3
\inst1|Add1~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~110_combout\ = \inst1|PSUM[2][8]~regout\ $ (\inst1|TSUM[0][8]~combout\ $ ((!(!\inst1|Add1~132\ & \inst1|Add1~117\) # (\inst1|Add1~132\ & \inst1|Add1~117COUT1_162\))))
-- \inst1|Add1~112\ = CARRY((\inst1|PSUM[2][8]~regout\ & ((\inst1|TSUM[0][8]~combout\) # (!\inst1|Add1~117\))) # (!\inst1|PSUM[2][8]~regout\ & (\inst1|TSUM[0][8]~combout\ & !\inst1|Add1~117\)))
-- \inst1|Add1~112COUT1_163\ = CARRY((\inst1|PSUM[2][8]~regout\ & ((\inst1|TSUM[0][8]~combout\) # (!\inst1|Add1~117COUT1_162\))) # (!\inst1|PSUM[2][8]~regout\ & (\inst1|TSUM[0][8]~combout\ & !\inst1|Add1~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[2][8]~regout\,
	datab => \inst1|TSUM[0][8]~combout\,
	cin => \inst1|Add1~132\,
	cin0 => \inst1|Add1~117\,
	cin1 => \inst1|Add1~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~110_combout\,
	cout0 => \inst1|Add1~112\,
	cout1 => \inst1|Add1~112COUT1_163\);

-- Location: LC_X3_Y4_N4
\inst1|Add1~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~105_combout\ = \inst1|TSUM[0][9]~combout\ $ (\inst1|PSUM[2][9]~regout\ $ (((!\inst1|Add1~132\ & \inst1|Add1~112\) # (\inst1|Add1~132\ & \inst1|Add1~112COUT1_163\))))
-- \inst1|Add1~107\ = CARRY((\inst1|TSUM[0][9]~combout\ & (!\inst1|PSUM[2][9]~regout\ & !\inst1|Add1~112COUT1_163\)) # (!\inst1|TSUM[0][9]~combout\ & ((!\inst1|Add1~112COUT1_163\) # (!\inst1|PSUM[2][9]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][9]~combout\,
	datab => \inst1|PSUM[2][9]~regout\,
	cin => \inst1|Add1~132\,
	cin0 => \inst1|Add1~112\,
	cin1 => \inst1|Add1~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~105_combout\,
	cout => \inst1|Add1~107\);

-- Location: LC_X2_Y4_N0
\inst1|TSUM[0][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][13]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][13]~combout\);

-- Location: LC_X2_Y4_N2
\inst1|TSUM[0][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][12]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][12]~combout\);

-- Location: LC_X2_Y4_N1
\inst1|TSUM[0][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][11]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][11]~combout\);

-- Location: LC_X1_Y4_N1
\inst1|TSUM[0][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][10]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][10]~combout\);

-- Location: LC_X3_Y4_N5
\inst1|Add1~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~100_combout\ = (\inst1|TSUM[0][10]~combout\ $ ((!\inst1|Add1~107\)))
-- \inst1|Add1~102\ = CARRY(((\inst1|TSUM[0][10]~combout\ & !\inst1|Add1~107\)))
-- \inst1|Add1~102COUT1_164\ = CARRY(((\inst1|TSUM[0][10]~combout\ & !\inst1|Add1~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][10]~combout\,
	cin => \inst1|Add1~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~100_combout\,
	cout0 => \inst1|Add1~102\,
	cout1 => \inst1|Add1~102COUT1_164\);

-- Location: LC_X3_Y4_N6
\inst1|Add1~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~95_combout\ = \inst1|TSUM[0][11]~combout\ $ (((((!\inst1|Add1~107\ & \inst1|Add1~102\) # (\inst1|Add1~107\ & \inst1|Add1~102COUT1_164\)))))
-- \inst1|Add1~97\ = CARRY(((!\inst1|Add1~102\)) # (!\inst1|TSUM[0][11]~combout\))
-- \inst1|Add1~97COUT1_165\ = CARRY(((!\inst1|Add1~102COUT1_164\)) # (!\inst1|TSUM[0][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][11]~combout\,
	cin => \inst1|Add1~107\,
	cin0 => \inst1|Add1~102\,
	cin1 => \inst1|Add1~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~95_combout\,
	cout0 => \inst1|Add1~97\,
	cout1 => \inst1|Add1~97COUT1_165\);

-- Location: LC_X3_Y4_N7
\inst1|Add1~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~90_combout\ = (\inst1|TSUM[0][12]~combout\ $ ((!(!\inst1|Add1~107\ & \inst1|Add1~97\) # (\inst1|Add1~107\ & \inst1|Add1~97COUT1_165\))))
-- \inst1|Add1~92\ = CARRY(((\inst1|TSUM[0][12]~combout\ & !\inst1|Add1~97\)))
-- \inst1|Add1~92COUT1_166\ = CARRY(((\inst1|TSUM[0][12]~combout\ & !\inst1|Add1~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][12]~combout\,
	cin => \inst1|Add1~107\,
	cin0 => \inst1|Add1~97\,
	cin1 => \inst1|Add1~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~90_combout\,
	cout0 => \inst1|Add1~92\,
	cout1 => \inst1|Add1~92COUT1_166\);

-- Location: LC_X3_Y4_N8
\inst1|Add1~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~85_combout\ = \inst1|TSUM[0][13]~combout\ $ (((((!\inst1|Add1~107\ & \inst1|Add1~92\) # (\inst1|Add1~107\ & \inst1|Add1~92COUT1_166\)))))
-- \inst1|Add1~87\ = CARRY(((!\inst1|Add1~92\)) # (!\inst1|TSUM[0][13]~combout\))
-- \inst1|Add1~87COUT1_167\ = CARRY(((!\inst1|Add1~92COUT1_166\)) # (!\inst1|TSUM[0][13]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][13]~combout\,
	cin => \inst1|Add1~107\,
	cin0 => \inst1|Add1~92\,
	cin1 => \inst1|Add1~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~85_combout\,
	cout0 => \inst1|Add1~87\,
	cout1 => \inst1|Add1~87COUT1_167\);

-- Location: LC_X3_Y4_N9
\inst1|Add1~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~80_combout\ = (\inst1|TSUM[0][14]~combout\ $ ((!(!\inst1|Add1~107\ & \inst1|Add1~87\) # (\inst1|Add1~107\ & \inst1|Add1~87COUT1_167\))))
-- \inst1|Add1~82\ = CARRY(((\inst1|TSUM[0][14]~combout\ & !\inst1|Add1~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][14]~combout\,
	cin => \inst1|Add1~107\,
	cin0 => \inst1|Add1~87\,
	cin1 => \inst1|Add1~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~80_combout\,
	cout => \inst1|Add1~82\);

-- Location: LC_X1_Y4_N4
\inst1|TSUM[0][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][18]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][18]~combout\);

-- Location: LC_X4_Y5_N7
\inst1|TSUM[0][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][17]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][17]~combout\);

-- Location: LC_X1_Y4_N3
\inst1|TSUM[0][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][16]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][16]~combout\);

-- Location: LC_X2_Y5_N5
\inst1|TSUM[0][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][15]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][15]~combout\);

-- Location: LC_X4_Y4_N0
\inst1|Add1~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~75_combout\ = (\inst1|TSUM[0][15]~combout\ $ ((\inst1|Add1~82\)))
-- \inst1|Add1~77\ = CARRY(((!\inst1|Add1~82\) # (!\inst1|TSUM[0][15]~combout\)))
-- \inst1|Add1~77COUT1_168\ = CARRY(((!\inst1|Add1~82\) # (!\inst1|TSUM[0][15]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][15]~combout\,
	cin => \inst1|Add1~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~75_combout\,
	cout0 => \inst1|Add1~77\,
	cout1 => \inst1|Add1~77COUT1_168\);

-- Location: LC_X4_Y4_N1
\inst1|Add1~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~70_combout\ = (\inst1|TSUM[0][16]~combout\ $ ((!(!\inst1|Add1~82\ & \inst1|Add1~77\) # (\inst1|Add1~82\ & \inst1|Add1~77COUT1_168\))))
-- \inst1|Add1~72\ = CARRY(((\inst1|TSUM[0][16]~combout\ & !\inst1|Add1~77\)))
-- \inst1|Add1~72COUT1_169\ = CARRY(((\inst1|TSUM[0][16]~combout\ & !\inst1|Add1~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][16]~combout\,
	cin => \inst1|Add1~82\,
	cin0 => \inst1|Add1~77\,
	cin1 => \inst1|Add1~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~70_combout\,
	cout0 => \inst1|Add1~72\,
	cout1 => \inst1|Add1~72COUT1_169\);

-- Location: LC_X4_Y4_N2
\inst1|Add1~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~65_combout\ = \inst1|TSUM[0][17]~combout\ $ (((((!\inst1|Add1~82\ & \inst1|Add1~72\) # (\inst1|Add1~82\ & \inst1|Add1~72COUT1_169\)))))
-- \inst1|Add1~67\ = CARRY(((!\inst1|Add1~72\)) # (!\inst1|TSUM[0][17]~combout\))
-- \inst1|Add1~67COUT1_170\ = CARRY(((!\inst1|Add1~72COUT1_169\)) # (!\inst1|TSUM[0][17]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][17]~combout\,
	cin => \inst1|Add1~82\,
	cin0 => \inst1|Add1~72\,
	cin1 => \inst1|Add1~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~65_combout\,
	cout0 => \inst1|Add1~67\,
	cout1 => \inst1|Add1~67COUT1_170\);

-- Location: LC_X4_Y4_N3
\inst1|Add1~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~60_combout\ = (\inst1|TSUM[0][18]~combout\ $ ((!(!\inst1|Add1~82\ & \inst1|Add1~67\) # (\inst1|Add1~82\ & \inst1|Add1~67COUT1_170\))))
-- \inst1|Add1~62\ = CARRY(((\inst1|TSUM[0][18]~combout\ & !\inst1|Add1~67\)))
-- \inst1|Add1~62COUT1_171\ = CARRY(((\inst1|TSUM[0][18]~combout\ & !\inst1|Add1~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][18]~combout\,
	cin => \inst1|Add1~82\,
	cin0 => \inst1|Add1~67\,
	cin1 => \inst1|Add1~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~60_combout\,
	cout0 => \inst1|Add1~62\,
	cout1 => \inst1|Add1~62COUT1_171\);

-- Location: LC_X4_Y4_N4
\inst1|Add1~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~55_combout\ = \inst1|TSUM[0][19]~combout\ $ (((((!\inst1|Add1~82\ & \inst1|Add1~62\) # (\inst1|Add1~82\ & \inst1|Add1~62COUT1_171\)))))
-- \inst1|Add1~57\ = CARRY(((!\inst1|Add1~62COUT1_171\)) # (!\inst1|TSUM[0][19]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][19]~combout\,
	cin => \inst1|Add1~82\,
	cin0 => \inst1|Add1~62\,
	cin1 => \inst1|Add1~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~55_combout\,
	cout => \inst1|Add1~57\);

-- Location: LC_X2_Y4_N4
\inst1|TSUM[0][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][23]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][23]~combout\);

-- Location: LC_X5_Y5_N6
\inst1|TSUM[0][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][22]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][22]~combout\);

-- Location: LC_X2_Y5_N6
\inst1|TSUM[0][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][21]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][21]~combout\);

-- Location: LC_X1_Y4_N9
\inst1|TSUM[0][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][20]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][20]~combout\);

-- Location: LC_X4_Y4_N5
\inst1|Add1~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~50_combout\ = (\inst1|TSUM[0][20]~combout\ $ ((!\inst1|Add1~57\)))
-- \inst1|Add1~52\ = CARRY(((\inst1|TSUM[0][20]~combout\ & !\inst1|Add1~57\)))
-- \inst1|Add1~52COUT1_172\ = CARRY(((\inst1|TSUM[0][20]~combout\ & !\inst1|Add1~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][20]~combout\,
	cin => \inst1|Add1~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~50_combout\,
	cout0 => \inst1|Add1~52\,
	cout1 => \inst1|Add1~52COUT1_172\);

-- Location: LC_X4_Y4_N6
\inst1|Add1~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~45_combout\ = (\inst1|TSUM[0][21]~combout\ $ (((!\inst1|Add1~57\ & \inst1|Add1~52\) # (\inst1|Add1~57\ & \inst1|Add1~52COUT1_172\))))
-- \inst1|Add1~47\ = CARRY(((!\inst1|Add1~52\) # (!\inst1|TSUM[0][21]~combout\)))
-- \inst1|Add1~47COUT1_173\ = CARRY(((!\inst1|Add1~52COUT1_172\) # (!\inst1|TSUM[0][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][21]~combout\,
	cin => \inst1|Add1~57\,
	cin0 => \inst1|Add1~52\,
	cin1 => \inst1|Add1~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~45_combout\,
	cout0 => \inst1|Add1~47\,
	cout1 => \inst1|Add1~47COUT1_173\);

-- Location: LC_X4_Y4_N7
\inst1|Add1~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~40_combout\ = (\inst1|TSUM[0][22]~combout\ $ ((!(!\inst1|Add1~57\ & \inst1|Add1~47\) # (\inst1|Add1~57\ & \inst1|Add1~47COUT1_173\))))
-- \inst1|Add1~42\ = CARRY(((\inst1|TSUM[0][22]~combout\ & !\inst1|Add1~47\)))
-- \inst1|Add1~42COUT1_174\ = CARRY(((\inst1|TSUM[0][22]~combout\ & !\inst1|Add1~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][22]~combout\,
	cin => \inst1|Add1~57\,
	cin0 => \inst1|Add1~47\,
	cin1 => \inst1|Add1~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~40_combout\,
	cout0 => \inst1|Add1~42\,
	cout1 => \inst1|Add1~42COUT1_174\);

-- Location: LC_X4_Y4_N8
\inst1|Add1~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~35_combout\ = \inst1|TSUM[0][23]~combout\ $ (((((!\inst1|Add1~57\ & \inst1|Add1~42\) # (\inst1|Add1~57\ & \inst1|Add1~42COUT1_174\)))))
-- \inst1|Add1~37\ = CARRY(((!\inst1|Add1~42\)) # (!\inst1|TSUM[0][23]~combout\))
-- \inst1|Add1~37COUT1_175\ = CARRY(((!\inst1|Add1~42COUT1_174\)) # (!\inst1|TSUM[0][23]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][23]~combout\,
	cin => \inst1|Add1~57\,
	cin0 => \inst1|Add1~42\,
	cin1 => \inst1|Add1~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~35_combout\,
	cout0 => \inst1|Add1~37\,
	cout1 => \inst1|Add1~37COUT1_175\);

-- Location: LC_X4_Y4_N9
\inst1|Add1~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~30_combout\ = (\inst1|TSUM[0][24]~combout\ $ ((!(!\inst1|Add1~57\ & \inst1|Add1~37\) # (\inst1|Add1~57\ & \inst1|Add1~37COUT1_175\))))
-- \inst1|Add1~32\ = CARRY(((\inst1|TSUM[0][24]~combout\ & !\inst1|Add1~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][24]~combout\,
	cin => \inst1|Add1~57\,
	cin0 => \inst1|Add1~37\,
	cin1 => \inst1|Add1~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~30_combout\,
	cout => \inst1|Add1~32\);

-- Location: LC_X5_Y4_N6
\inst1|TSUM[0][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][28]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][28]~combout\);

-- Location: LC_X5_Y4_N7
\inst1|TSUM[0][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][27]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][27]~combout\);

-- Location: LC_X1_Y4_N8
\inst1|TSUM[0][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][26]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][26]~combout\);

-- Location: LC_X5_Y4_N8
\inst1|TSUM[0][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[0][25]~combout\ = LCELL(GND)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[0][25]~combout\);

-- Location: LC_X5_Y4_N0
\inst1|Add1~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~25_combout\ = \inst1|TSUM[0][25]~combout\ $ ((((\inst1|Add1~32\))))
-- \inst1|Add1~27\ = CARRY(((!\inst1|Add1~32\)) # (!\inst1|TSUM[0][25]~combout\))
-- \inst1|Add1~27COUT1_176\ = CARRY(((!\inst1|Add1~32\)) # (!\inst1|TSUM[0][25]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][25]~combout\,
	cin => \inst1|Add1~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~25_combout\,
	cout0 => \inst1|Add1~27\,
	cout1 => \inst1|Add1~27COUT1_176\);

-- Location: LC_X5_Y4_N1
\inst1|Add1~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~20_combout\ = (\inst1|TSUM[0][26]~combout\ $ ((!(!\inst1|Add1~32\ & \inst1|Add1~27\) # (\inst1|Add1~32\ & \inst1|Add1~27COUT1_176\))))
-- \inst1|Add1~22\ = CARRY(((\inst1|TSUM[0][26]~combout\ & !\inst1|Add1~27\)))
-- \inst1|Add1~22COUT1_177\ = CARRY(((\inst1|TSUM[0][26]~combout\ & !\inst1|Add1~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][26]~combout\,
	cin => \inst1|Add1~32\,
	cin0 => \inst1|Add1~27\,
	cin1 => \inst1|Add1~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~20_combout\,
	cout0 => \inst1|Add1~22\,
	cout1 => \inst1|Add1~22COUT1_177\);

-- Location: LC_X5_Y4_N2
\inst1|Add1~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~15_combout\ = (\inst1|TSUM[0][27]~combout\ $ (((!\inst1|Add1~32\ & \inst1|Add1~22\) # (\inst1|Add1~32\ & \inst1|Add1~22COUT1_177\))))
-- \inst1|Add1~17\ = CARRY(((!\inst1|Add1~22\) # (!\inst1|TSUM[0][27]~combout\)))
-- \inst1|Add1~17COUT1_178\ = CARRY(((!\inst1|Add1~22COUT1_177\) # (!\inst1|TSUM[0][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][27]~combout\,
	cin => \inst1|Add1~32\,
	cin0 => \inst1|Add1~22\,
	cin1 => \inst1|Add1~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~15_combout\,
	cout0 => \inst1|Add1~17\,
	cout1 => \inst1|Add1~17COUT1_178\);

-- Location: LC_X5_Y4_N3
\inst1|Add1~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~10_combout\ = \inst1|TSUM[0][28]~combout\ $ ((((!(!\inst1|Add1~32\ & \inst1|Add1~17\) # (\inst1|Add1~32\ & \inst1|Add1~17COUT1_178\)))))
-- \inst1|Add1~12\ = CARRY((\inst1|TSUM[0][28]~combout\ & ((!\inst1|Add1~17\))))
-- \inst1|Add1~12COUT1_179\ = CARRY((\inst1|TSUM[0][28]~combout\ & ((!\inst1|Add1~17COUT1_178\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[0][28]~combout\,
	cin => \inst1|Add1~32\,
	cin0 => \inst1|Add1~17\,
	cin1 => \inst1|Add1~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~10_combout\,
	cout0 => \inst1|Add1~12\,
	cout1 => \inst1|Add1~12COUT1_179\);

-- Location: LC_X5_Y4_N4
\inst1|Add1~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~5_combout\ = (\inst1|TSUM[0][29]~combout\ $ (((!\inst1|Add1~32\ & \inst1|Add1~12\) # (\inst1|Add1~32\ & \inst1|Add1~12COUT1_179\))))
-- \inst1|Add1~7\ = CARRY(((!\inst1|Add1~12COUT1_179\) # (!\inst1|TSUM[0][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[0][29]~combout\,
	cin => \inst1|Add1~32\,
	cin0 => \inst1|Add1~12\,
	cin1 => \inst1|Add1~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~5_combout\,
	cout => \inst1|Add1~7\);

-- Location: LC_X5_Y4_N5
\inst1|Add1~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add1~0_combout\ = ((\inst1|Add1~7\ $ (!\inst1|TSUM[0][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[0][30]~combout\,
	cin => \inst1|Add1~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add1~0_combout\);

-- Location: LC_X5_Y5_N8
\inst1|TSUM[1][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][30]~combout\ = LCELL((((\inst1|Add1~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][30]~combout\);

-- Location: LC_X5_Y5_N7
\inst1|TSUM[1][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][29]~combout\ = LCELL((((\inst1|Add1~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][29]~combout\);

-- Location: LC_X4_Y5_N6
\inst1|TSUM[1][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][24]~combout\ = LCELL((((\inst1|Add1~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][24]~combout\);

-- Location: LC_X4_Y5_N1
\inst1|TSUM[1][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][19]~combout\ = LCELL((((\inst1|Add1~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][19]~combout\);

-- Location: LC_X3_Y5_N8
\inst1|TSUM[1][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][14]~combout\ = LCELL((((\inst1|Add1~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][14]~combout\);

-- Location: LC_X3_Y5_N4
\inst1|TSUM[1][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][9]~combout\ = LCELL((((\inst1|Add1~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][9]~combout\);

-- Location: LC_X2_Y7_N0
\inst1|PSUM[3][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][0]~regout\ = DFFEAS(((!\inst1|PSUM[3][0]~regout\)), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][0]~259\ = CARRY(((\inst1|PSUM[3][0]~regout\)))
-- \inst1|PSUM[3][0]~259COUT1_417\ = CARRY(((\inst1|PSUM[3][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	datab => \inst1|PSUM[3][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][0]~regout\,
	cout0 => \inst1|PSUM[3][0]~259\,
	cout1 => \inst1|PSUM[3][0]~259COUT1_417\);

-- Location: LC_X2_Y7_N1
\inst1|PSUM[3][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][1]~regout\ = DFFEAS((\inst1|PSUM[3][1]~regout\ $ ((\inst1|PSUM[3][0]~259\))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][1]~257\ = CARRY(((!\inst1|PSUM[3][0]~259\) # (!\inst1|PSUM[3][1]~regout\)))
-- \inst1|PSUM[3][1]~257COUT1_418\ = CARRY(((!\inst1|PSUM[3][0]~259COUT1_417\) # (!\inst1|PSUM[3][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	datab => \inst1|PSUM[3][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[3][0]~259\,
	cin1 => \inst1|PSUM[3][0]~259COUT1_417\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][1]~regout\,
	cout0 => \inst1|PSUM[3][1]~257\,
	cout1 => \inst1|PSUM[3][1]~257COUT1_418\);

-- Location: LC_X2_Y7_N2
\inst1|PSUM[3][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][2]~regout\ = DFFEAS((\inst1|PSUM[3][2]~regout\ $ ((!\inst1|PSUM[3][1]~257\))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][2]~255\ = CARRY(((\inst1|PSUM[3][2]~regout\ & !\inst1|PSUM[3][1]~257\)))
-- \inst1|PSUM[3][2]~255COUT1_419\ = CARRY(((\inst1|PSUM[3][2]~regout\ & !\inst1|PSUM[3][1]~257COUT1_418\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	datab => \inst1|PSUM[3][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[3][1]~257\,
	cin1 => \inst1|PSUM[3][1]~257COUT1_418\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][2]~regout\,
	cout0 => \inst1|PSUM[3][2]~255\,
	cout1 => \inst1|PSUM[3][2]~255COUT1_419\);

-- Location: LC_X2_Y7_N3
\inst1|PSUM[3][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][3]~regout\ = DFFEAS(\inst1|PSUM[3][3]~regout\ $ ((((\inst1|PSUM[3][2]~255\)))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][3]~253\ = CARRY(((!\inst1|PSUM[3][2]~255\)) # (!\inst1|PSUM[3][3]~regout\))
-- \inst1|PSUM[3][3]~253COUT1_420\ = CARRY(((!\inst1|PSUM[3][2]~255COUT1_419\)) # (!\inst1|PSUM[3][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	dataa => \inst1|PSUM[3][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[3][2]~255\,
	cin1 => \inst1|PSUM[3][2]~255COUT1_419\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][3]~regout\,
	cout0 => \inst1|PSUM[3][3]~253\,
	cout1 => \inst1|PSUM[3][3]~253COUT1_420\);

-- Location: LC_X2_Y7_N4
\inst1|PSUM[3][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][4]~regout\ = DFFEAS(\inst1|PSUM[3][4]~regout\ $ ((((!\inst1|PSUM[3][3]~253\)))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][4]~251\ = CARRY((\inst1|PSUM[3][4]~regout\ & ((!\inst1|PSUM[3][3]~253COUT1_420\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	dataa => \inst1|PSUM[3][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[3][3]~253\,
	cin1 => \inst1|PSUM[3][3]~253COUT1_420\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][4]~regout\,
	cout => \inst1|PSUM[3][4]~251\);

-- Location: LC_X2_Y7_N5
\inst1|PSUM[3][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][5]~regout\ = DFFEAS(\inst1|PSUM[3][5]~regout\ $ ((((\inst1|PSUM[3][4]~251\)))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][5]~249\ = CARRY(((!\inst1|PSUM[3][4]~251\)) # (!\inst1|PSUM[3][5]~regout\))
-- \inst1|PSUM[3][5]~249COUT1_421\ = CARRY(((!\inst1|PSUM[3][4]~251\)) # (!\inst1|PSUM[3][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	dataa => \inst1|PSUM[3][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[3][4]~251\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][5]~regout\,
	cout0 => \inst1|PSUM[3][5]~249\,
	cout1 => \inst1|PSUM[3][5]~249COUT1_421\);

-- Location: LC_X2_Y7_N6
\inst1|PSUM[3][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][6]~regout\ = DFFEAS(\inst1|PSUM[3][6]~regout\ $ ((((!(!\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][5]~249\) # (\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][5]~249COUT1_421\))))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][6]~247\ = CARRY((\inst1|PSUM[3][6]~regout\ & ((!\inst1|PSUM[3][5]~249\))))
-- \inst1|PSUM[3][6]~247COUT1_422\ = CARRY((\inst1|PSUM[3][6]~regout\ & ((!\inst1|PSUM[3][5]~249COUT1_421\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	dataa => \inst1|PSUM[3][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[3][4]~251\,
	cin0 => \inst1|PSUM[3][5]~249\,
	cin1 => \inst1|PSUM[3][5]~249COUT1_421\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][6]~regout\,
	cout0 => \inst1|PSUM[3][6]~247\,
	cout1 => \inst1|PSUM[3][6]~247COUT1_422\);

-- Location: LC_X2_Y7_N7
\inst1|PSUM[3][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][7]~regout\ = DFFEAS((\inst1|PSUM[3][7]~regout\ $ (((!\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][6]~247\) # (\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][6]~247COUT1_422\)))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][7]~245\ = CARRY(((!\inst1|PSUM[3][6]~247\) # (!\inst1|PSUM[3][7]~regout\)))
-- \inst1|PSUM[3][7]~245COUT1_423\ = CARRY(((!\inst1|PSUM[3][6]~247COUT1_422\) # (!\inst1|PSUM[3][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	datab => \inst1|PSUM[3][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[3][4]~251\,
	cin0 => \inst1|PSUM[3][6]~247\,
	cin1 => \inst1|PSUM[3][6]~247COUT1_422\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][7]~regout\,
	cout0 => \inst1|PSUM[3][7]~245\,
	cout1 => \inst1|PSUM[3][7]~245COUT1_423\);

-- Location: LC_X2_Y7_N8
\inst1|PSUM[3][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][8]~regout\ = DFFEAS(\inst1|PSUM[3][8]~regout\ $ ((((!(!\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][7]~245\) # (\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][7]~245COUT1_423\))))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[3][8]~243\ = CARRY((\inst1|PSUM[3][8]~regout\ & ((!\inst1|PSUM[3][7]~245\))))
-- \inst1|PSUM[3][8]~243COUT1_424\ = CARRY((\inst1|PSUM[3][8]~regout\ & ((!\inst1|PSUM[3][7]~245COUT1_423\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	dataa => \inst1|PSUM[3][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[3][4]~251\,
	cin0 => \inst1|PSUM[3][7]~245\,
	cin1 => \inst1|PSUM[3][7]~245COUT1_423\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][8]~regout\,
	cout0 => \inst1|PSUM[3][8]~243\,
	cout1 => \inst1|PSUM[3][8]~243COUT1_424\);

-- Location: LC_X2_Y7_N9
\inst1|PSUM[3][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[3][9]~regout\ = DFFEAS((((!\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][8]~243\) # (\inst1|PSUM[3][4]~251\ & \inst1|PSUM[3][8]~243COUT1_424\) $ (\inst1|PSUM[3][9]~regout\))), \inst6|STEMP\(3), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(3),
	datad => \inst1|PSUM[3][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[3][4]~251\,
	cin0 => \inst1|PSUM[3][8]~243\,
	cin1 => \inst1|PSUM[3][8]~243COUT1_424\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[3][9]~regout\);

-- Location: LC_X2_Y5_N7
\inst1|TSUM[1][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][4]~combout\ = LCELL((((\inst1|Add1~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][4]~combout\);

-- Location: LC_X2_Y6_N0
\inst1|TSUM[1][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][3]~combout\ = LCELL((((\inst1|Add1~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][3]~combout\);

-- Location: LC_X2_Y5_N1
\inst1|TSUM[1][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][2]~combout\ = LCELL((((\inst1|Add1~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][2]~combout\);

-- Location: LC_X2_Y5_N0
\inst1|TSUM[1][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][1]~combout\ = LCELL((((\inst1|Add1~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][1]~combout\);

-- Location: LC_X2_Y6_N4
\inst1|TSUM[1][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][0]~combout\ = LCELL((((\inst1|Add1~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][0]~combout\);

-- Location: LC_X2_Y6_N5
\inst1|Add2~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~150_combout\ = \inst1|PSUM[3][0]~regout\ $ ((\inst1|TSUM[1][0]~combout\))
-- \inst1|Add2~152\ = CARRY((\inst1|PSUM[3][0]~regout\ & (\inst1|TSUM[1][0]~combout\)))
-- \inst1|Add2~152COUT1_156\ = CARRY((\inst1|PSUM[3][0]~regout\ & (\inst1|TSUM[1][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][0]~regout\,
	datab => \inst1|TSUM[1][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~150_combout\,
	cout0 => \inst1|Add2~152\,
	cout1 => \inst1|Add2~152COUT1_156\);

-- Location: LC_X2_Y6_N6
\inst1|Add2~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~145_combout\ = \inst1|PSUM[3][1]~regout\ $ (\inst1|TSUM[1][1]~combout\ $ ((\inst1|Add2~152\)))
-- \inst1|Add2~147\ = CARRY((\inst1|PSUM[3][1]~regout\ & (!\inst1|TSUM[1][1]~combout\ & !\inst1|Add2~152\)) # (!\inst1|PSUM[3][1]~regout\ & ((!\inst1|Add2~152\) # (!\inst1|TSUM[1][1]~combout\))))
-- \inst1|Add2~147COUT1_157\ = CARRY((\inst1|PSUM[3][1]~regout\ & (!\inst1|TSUM[1][1]~combout\ & !\inst1|Add2~152COUT1_156\)) # (!\inst1|PSUM[3][1]~regout\ & ((!\inst1|Add2~152COUT1_156\) # (!\inst1|TSUM[1][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][1]~regout\,
	datab => \inst1|TSUM[1][1]~combout\,
	cin0 => \inst1|Add2~152\,
	cin1 => \inst1|Add2~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~145_combout\,
	cout0 => \inst1|Add2~147\,
	cout1 => \inst1|Add2~147COUT1_157\);

-- Location: LC_X2_Y6_N7
\inst1|Add2~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~140_combout\ = \inst1|TSUM[1][2]~combout\ $ (\inst1|PSUM[3][2]~regout\ $ ((!\inst1|Add2~147\)))
-- \inst1|Add2~142\ = CARRY((\inst1|TSUM[1][2]~combout\ & ((\inst1|PSUM[3][2]~regout\) # (!\inst1|Add2~147\))) # (!\inst1|TSUM[1][2]~combout\ & (\inst1|PSUM[3][2]~regout\ & !\inst1|Add2~147\)))
-- \inst1|Add2~142COUT1_158\ = CARRY((\inst1|TSUM[1][2]~combout\ & ((\inst1|PSUM[3][2]~regout\) # (!\inst1|Add2~147COUT1_157\))) # (!\inst1|TSUM[1][2]~combout\ & (\inst1|PSUM[3][2]~regout\ & !\inst1|Add2~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[1][2]~combout\,
	datab => \inst1|PSUM[3][2]~regout\,
	cin0 => \inst1|Add2~147\,
	cin1 => \inst1|Add2~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~140_combout\,
	cout0 => \inst1|Add2~142\,
	cout1 => \inst1|Add2~142COUT1_158\);

-- Location: LC_X2_Y6_N8
\inst1|Add2~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~135_combout\ = \inst1|PSUM[3][3]~regout\ $ (\inst1|TSUM[1][3]~combout\ $ ((\inst1|Add2~142\)))
-- \inst1|Add2~137\ = CARRY((\inst1|PSUM[3][3]~regout\ & (!\inst1|TSUM[1][3]~combout\ & !\inst1|Add2~142\)) # (!\inst1|PSUM[3][3]~regout\ & ((!\inst1|Add2~142\) # (!\inst1|TSUM[1][3]~combout\))))
-- \inst1|Add2~137COUT1_159\ = CARRY((\inst1|PSUM[3][3]~regout\ & (!\inst1|TSUM[1][3]~combout\ & !\inst1|Add2~142COUT1_158\)) # (!\inst1|PSUM[3][3]~regout\ & ((!\inst1|Add2~142COUT1_158\) # (!\inst1|TSUM[1][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][3]~regout\,
	datab => \inst1|TSUM[1][3]~combout\,
	cin0 => \inst1|Add2~142\,
	cin1 => \inst1|Add2~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~135_combout\,
	cout0 => \inst1|Add2~137\,
	cout1 => \inst1|Add2~137COUT1_159\);

-- Location: LC_X2_Y6_N9
\inst1|Add2~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~130_combout\ = \inst1|PSUM[3][4]~regout\ $ (\inst1|TSUM[1][4]~combout\ $ ((!\inst1|Add2~137\)))
-- \inst1|Add2~132\ = CARRY((\inst1|PSUM[3][4]~regout\ & ((\inst1|TSUM[1][4]~combout\) # (!\inst1|Add2~137COUT1_159\))) # (!\inst1|PSUM[3][4]~regout\ & (\inst1|TSUM[1][4]~combout\ & !\inst1|Add2~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][4]~regout\,
	datab => \inst1|TSUM[1][4]~combout\,
	cin0 => \inst1|Add2~137\,
	cin1 => \inst1|Add2~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~130_combout\,
	cout => \inst1|Add2~132\);

-- Location: LC_X3_Y5_N9
\inst1|TSUM[1][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][8]~combout\ = LCELL((((\inst1|Add1~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][8]~combout\);

-- Location: LC_X3_Y5_N3
\inst1|TSUM[1][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][7]~combout\ = LCELL((((\inst1|Add1~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][7]~combout\);

-- Location: LC_X3_Y5_N7
\inst1|TSUM[1][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][6]~combout\ = LCELL((((\inst1|Add1~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][6]~combout\);

-- Location: LC_X3_Y5_N1
\inst1|TSUM[1][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][5]~combout\ = LCELL((((\inst1|Add1~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][5]~combout\);

-- Location: LC_X3_Y6_N0
\inst1|Add2~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~125_combout\ = \inst1|PSUM[3][5]~regout\ $ (\inst1|TSUM[1][5]~combout\ $ ((\inst1|Add2~132\)))
-- \inst1|Add2~127\ = CARRY((\inst1|PSUM[3][5]~regout\ & (!\inst1|TSUM[1][5]~combout\ & !\inst1|Add2~132\)) # (!\inst1|PSUM[3][5]~regout\ & ((!\inst1|Add2~132\) # (!\inst1|TSUM[1][5]~combout\))))
-- \inst1|Add2~127COUT1_160\ = CARRY((\inst1|PSUM[3][5]~regout\ & (!\inst1|TSUM[1][5]~combout\ & !\inst1|Add2~132\)) # (!\inst1|PSUM[3][5]~regout\ & ((!\inst1|Add2~132\) # (!\inst1|TSUM[1][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][5]~regout\,
	datab => \inst1|TSUM[1][5]~combout\,
	cin => \inst1|Add2~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~125_combout\,
	cout0 => \inst1|Add2~127\,
	cout1 => \inst1|Add2~127COUT1_160\);

-- Location: LC_X3_Y6_N1
\inst1|Add2~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~120_combout\ = \inst1|PSUM[3][6]~regout\ $ (\inst1|TSUM[1][6]~combout\ $ ((!(!\inst1|Add2~132\ & \inst1|Add2~127\) # (\inst1|Add2~132\ & \inst1|Add2~127COUT1_160\))))
-- \inst1|Add2~122\ = CARRY((\inst1|PSUM[3][6]~regout\ & ((\inst1|TSUM[1][6]~combout\) # (!\inst1|Add2~127\))) # (!\inst1|PSUM[3][6]~regout\ & (\inst1|TSUM[1][6]~combout\ & !\inst1|Add2~127\)))
-- \inst1|Add2~122COUT1_161\ = CARRY((\inst1|PSUM[3][6]~regout\ & ((\inst1|TSUM[1][6]~combout\) # (!\inst1|Add2~127COUT1_160\))) # (!\inst1|PSUM[3][6]~regout\ & (\inst1|TSUM[1][6]~combout\ & !\inst1|Add2~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][6]~regout\,
	datab => \inst1|TSUM[1][6]~combout\,
	cin => \inst1|Add2~132\,
	cin0 => \inst1|Add2~127\,
	cin1 => \inst1|Add2~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~120_combout\,
	cout0 => \inst1|Add2~122\,
	cout1 => \inst1|Add2~122COUT1_161\);

-- Location: LC_X3_Y6_N2
\inst1|Add2~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~115_combout\ = \inst1|PSUM[3][7]~regout\ $ (\inst1|TSUM[1][7]~combout\ $ (((!\inst1|Add2~132\ & \inst1|Add2~122\) # (\inst1|Add2~132\ & \inst1|Add2~122COUT1_161\))))
-- \inst1|Add2~117\ = CARRY((\inst1|PSUM[3][7]~regout\ & (!\inst1|TSUM[1][7]~combout\ & !\inst1|Add2~122\)) # (!\inst1|PSUM[3][7]~regout\ & ((!\inst1|Add2~122\) # (!\inst1|TSUM[1][7]~combout\))))
-- \inst1|Add2~117COUT1_162\ = CARRY((\inst1|PSUM[3][7]~regout\ & (!\inst1|TSUM[1][7]~combout\ & !\inst1|Add2~122COUT1_161\)) # (!\inst1|PSUM[3][7]~regout\ & ((!\inst1|Add2~122COUT1_161\) # (!\inst1|TSUM[1][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][7]~regout\,
	datab => \inst1|TSUM[1][7]~combout\,
	cin => \inst1|Add2~132\,
	cin0 => \inst1|Add2~122\,
	cin1 => \inst1|Add2~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~115_combout\,
	cout0 => \inst1|Add2~117\,
	cout1 => \inst1|Add2~117COUT1_162\);

-- Location: LC_X3_Y6_N3
\inst1|Add2~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~110_combout\ = \inst1|PSUM[3][8]~regout\ $ (\inst1|TSUM[1][8]~combout\ $ ((!(!\inst1|Add2~132\ & \inst1|Add2~117\) # (\inst1|Add2~132\ & \inst1|Add2~117COUT1_162\))))
-- \inst1|Add2~112\ = CARRY((\inst1|PSUM[3][8]~regout\ & ((\inst1|TSUM[1][8]~combout\) # (!\inst1|Add2~117\))) # (!\inst1|PSUM[3][8]~regout\ & (\inst1|TSUM[1][8]~combout\ & !\inst1|Add2~117\)))
-- \inst1|Add2~112COUT1_163\ = CARRY((\inst1|PSUM[3][8]~regout\ & ((\inst1|TSUM[1][8]~combout\) # (!\inst1|Add2~117COUT1_162\))) # (!\inst1|PSUM[3][8]~regout\ & (\inst1|TSUM[1][8]~combout\ & !\inst1|Add2~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[3][8]~regout\,
	datab => \inst1|TSUM[1][8]~combout\,
	cin => \inst1|Add2~132\,
	cin0 => \inst1|Add2~117\,
	cin1 => \inst1|Add2~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~110_combout\,
	cout0 => \inst1|Add2~112\,
	cout1 => \inst1|Add2~112COUT1_163\);

-- Location: LC_X3_Y6_N4
\inst1|Add2~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~105_combout\ = \inst1|TSUM[1][9]~combout\ $ (\inst1|PSUM[3][9]~regout\ $ (((!\inst1|Add2~132\ & \inst1|Add2~112\) # (\inst1|Add2~132\ & \inst1|Add2~112COUT1_163\))))
-- \inst1|Add2~107\ = CARRY((\inst1|TSUM[1][9]~combout\ & (!\inst1|PSUM[3][9]~regout\ & !\inst1|Add2~112COUT1_163\)) # (!\inst1|TSUM[1][9]~combout\ & ((!\inst1|Add2~112COUT1_163\) # (!\inst1|PSUM[3][9]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[1][9]~combout\,
	datab => \inst1|PSUM[3][9]~regout\,
	cin => \inst1|Add2~132\,
	cin0 => \inst1|Add2~112\,
	cin1 => \inst1|Add2~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~105_combout\,
	cout => \inst1|Add2~107\);

-- Location: LC_X3_Y5_N6
\inst1|TSUM[1][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][13]~combout\ = LCELL((((\inst1|Add1~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][13]~combout\);

-- Location: LC_X3_Y5_N0
\inst1|TSUM[1][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][12]~combout\ = LCELL((((\inst1|Add1~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][12]~combout\);

-- Location: LC_X3_Y5_N2
\inst1|TSUM[1][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][11]~combout\ = LCELL((((\inst1|Add1~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][11]~combout\);

-- Location: LC_X3_Y5_N5
\inst1|TSUM[1][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][10]~combout\ = LCELL((((\inst1|Add1~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][10]~combout\);

-- Location: LC_X3_Y6_N5
\inst1|Add2~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~100_combout\ = (\inst1|TSUM[1][10]~combout\ $ ((!\inst1|Add2~107\)))
-- \inst1|Add2~102\ = CARRY(((\inst1|TSUM[1][10]~combout\ & !\inst1|Add2~107\)))
-- \inst1|Add2~102COUT1_164\ = CARRY(((\inst1|TSUM[1][10]~combout\ & !\inst1|Add2~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][10]~combout\,
	cin => \inst1|Add2~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~100_combout\,
	cout0 => \inst1|Add2~102\,
	cout1 => \inst1|Add2~102COUT1_164\);

-- Location: LC_X3_Y6_N6
\inst1|Add2~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~95_combout\ = (\inst1|TSUM[1][11]~combout\ $ (((!\inst1|Add2~107\ & \inst1|Add2~102\) # (\inst1|Add2~107\ & \inst1|Add2~102COUT1_164\))))
-- \inst1|Add2~97\ = CARRY(((!\inst1|Add2~102\) # (!\inst1|TSUM[1][11]~combout\)))
-- \inst1|Add2~97COUT1_165\ = CARRY(((!\inst1|Add2~102COUT1_164\) # (!\inst1|TSUM[1][11]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][11]~combout\,
	cin => \inst1|Add2~107\,
	cin0 => \inst1|Add2~102\,
	cin1 => \inst1|Add2~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~95_combout\,
	cout0 => \inst1|Add2~97\,
	cout1 => \inst1|Add2~97COUT1_165\);

-- Location: LC_X3_Y6_N7
\inst1|Add2~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~90_combout\ = (\inst1|TSUM[1][12]~combout\ $ ((!(!\inst1|Add2~107\ & \inst1|Add2~97\) # (\inst1|Add2~107\ & \inst1|Add2~97COUT1_165\))))
-- \inst1|Add2~92\ = CARRY(((\inst1|TSUM[1][12]~combout\ & !\inst1|Add2~97\)))
-- \inst1|Add2~92COUT1_166\ = CARRY(((\inst1|TSUM[1][12]~combout\ & !\inst1|Add2~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][12]~combout\,
	cin => \inst1|Add2~107\,
	cin0 => \inst1|Add2~97\,
	cin1 => \inst1|Add2~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~90_combout\,
	cout0 => \inst1|Add2~92\,
	cout1 => \inst1|Add2~92COUT1_166\);

-- Location: LC_X3_Y6_N8
\inst1|Add2~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~85_combout\ = (\inst1|TSUM[1][13]~combout\ $ (((!\inst1|Add2~107\ & \inst1|Add2~92\) # (\inst1|Add2~107\ & \inst1|Add2~92COUT1_166\))))
-- \inst1|Add2~87\ = CARRY(((!\inst1|Add2~92\) # (!\inst1|TSUM[1][13]~combout\)))
-- \inst1|Add2~87COUT1_167\ = CARRY(((!\inst1|Add2~92COUT1_166\) # (!\inst1|TSUM[1][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][13]~combout\,
	cin => \inst1|Add2~107\,
	cin0 => \inst1|Add2~92\,
	cin1 => \inst1|Add2~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~85_combout\,
	cout0 => \inst1|Add2~87\,
	cout1 => \inst1|Add2~87COUT1_167\);

-- Location: LC_X3_Y6_N9
\inst1|Add2~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~80_combout\ = (\inst1|TSUM[1][14]~combout\ $ ((!(!\inst1|Add2~107\ & \inst1|Add2~87\) # (\inst1|Add2~107\ & \inst1|Add2~87COUT1_167\))))
-- \inst1|Add2~82\ = CARRY(((\inst1|TSUM[1][14]~combout\ & !\inst1|Add2~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][14]~combout\,
	cin => \inst1|Add2~107\,
	cin0 => \inst1|Add2~87\,
	cin1 => \inst1|Add2~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~80_combout\,
	cout => \inst1|Add2~82\);

-- Location: LC_X4_Y5_N4
\inst1|TSUM[1][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][18]~combout\ = LCELL((((\inst1|Add1~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][18]~combout\);

-- Location: LC_X4_Y5_N9
\inst1|TSUM[1][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][17]~combout\ = LCELL((((\inst1|Add1~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][17]~combout\);

-- Location: LC_X4_Y5_N3
\inst1|TSUM[1][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][16]~combout\ = LCELL((((\inst1|Add1~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][16]~combout\);

-- Location: LC_X4_Y5_N5
\inst1|TSUM[1][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][15]~combout\ = LCELL((((\inst1|Add1~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][15]~combout\);

-- Location: LC_X4_Y6_N0
\inst1|Add2~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~75_combout\ = (\inst1|TSUM[1][15]~combout\ $ ((\inst1|Add2~82\)))
-- \inst1|Add2~77\ = CARRY(((!\inst1|Add2~82\) # (!\inst1|TSUM[1][15]~combout\)))
-- \inst1|Add2~77COUT1_168\ = CARRY(((!\inst1|Add2~82\) # (!\inst1|TSUM[1][15]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][15]~combout\,
	cin => \inst1|Add2~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~75_combout\,
	cout0 => \inst1|Add2~77\,
	cout1 => \inst1|Add2~77COUT1_168\);

-- Location: LC_X4_Y6_N1
\inst1|Add2~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~70_combout\ = (\inst1|TSUM[1][16]~combout\ $ ((!(!\inst1|Add2~82\ & \inst1|Add2~77\) # (\inst1|Add2~82\ & \inst1|Add2~77COUT1_168\))))
-- \inst1|Add2~72\ = CARRY(((\inst1|TSUM[1][16]~combout\ & !\inst1|Add2~77\)))
-- \inst1|Add2~72COUT1_169\ = CARRY(((\inst1|TSUM[1][16]~combout\ & !\inst1|Add2~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][16]~combout\,
	cin => \inst1|Add2~82\,
	cin0 => \inst1|Add2~77\,
	cin1 => \inst1|Add2~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~70_combout\,
	cout0 => \inst1|Add2~72\,
	cout1 => \inst1|Add2~72COUT1_169\);

-- Location: LC_X4_Y6_N2
\inst1|Add2~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~65_combout\ = (\inst1|TSUM[1][17]~combout\ $ (((!\inst1|Add2~82\ & \inst1|Add2~72\) # (\inst1|Add2~82\ & \inst1|Add2~72COUT1_169\))))
-- \inst1|Add2~67\ = CARRY(((!\inst1|Add2~72\) # (!\inst1|TSUM[1][17]~combout\)))
-- \inst1|Add2~67COUT1_170\ = CARRY(((!\inst1|Add2~72COUT1_169\) # (!\inst1|TSUM[1][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][17]~combout\,
	cin => \inst1|Add2~82\,
	cin0 => \inst1|Add2~72\,
	cin1 => \inst1|Add2~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~65_combout\,
	cout0 => \inst1|Add2~67\,
	cout1 => \inst1|Add2~67COUT1_170\);

-- Location: LC_X4_Y6_N3
\inst1|Add2~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~60_combout\ = \inst1|TSUM[1][18]~combout\ $ ((((!(!\inst1|Add2~82\ & \inst1|Add2~67\) # (\inst1|Add2~82\ & \inst1|Add2~67COUT1_170\)))))
-- \inst1|Add2~62\ = CARRY((\inst1|TSUM[1][18]~combout\ & ((!\inst1|Add2~67\))))
-- \inst1|Add2~62COUT1_171\ = CARRY((\inst1|TSUM[1][18]~combout\ & ((!\inst1|Add2~67COUT1_170\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[1][18]~combout\,
	cin => \inst1|Add2~82\,
	cin0 => \inst1|Add2~67\,
	cin1 => \inst1|Add2~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~60_combout\,
	cout0 => \inst1|Add2~62\,
	cout1 => \inst1|Add2~62COUT1_171\);

-- Location: LC_X4_Y6_N4
\inst1|Add2~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~55_combout\ = (\inst1|TSUM[1][19]~combout\ $ (((!\inst1|Add2~82\ & \inst1|Add2~62\) # (\inst1|Add2~82\ & \inst1|Add2~62COUT1_171\))))
-- \inst1|Add2~57\ = CARRY(((!\inst1|Add2~62COUT1_171\) # (!\inst1|TSUM[1][19]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][19]~combout\,
	cin => \inst1|Add2~82\,
	cin0 => \inst1|Add2~62\,
	cin1 => \inst1|Add2~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~55_combout\,
	cout => \inst1|Add2~57\);

-- Location: LC_X2_Y5_N8
\inst1|TSUM[1][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][23]~combout\ = LCELL((((\inst1|Add1~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][23]~combout\);

-- Location: LC_X4_Y5_N0
\inst1|TSUM[1][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][22]~combout\ = LCELL((((\inst1|Add1~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][22]~combout\);

-- Location: LC_X4_Y5_N2
\inst1|TSUM[1][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][21]~combout\ = LCELL((((\inst1|Add1~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][21]~combout\);

-- Location: LC_X4_Y5_N8
\inst1|TSUM[1][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][20]~combout\ = LCELL((((\inst1|Add1~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][20]~combout\);

-- Location: LC_X4_Y6_N5
\inst1|Add2~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~50_combout\ = (\inst1|TSUM[1][20]~combout\ $ ((!\inst1|Add2~57\)))
-- \inst1|Add2~52\ = CARRY(((\inst1|TSUM[1][20]~combout\ & !\inst1|Add2~57\)))
-- \inst1|Add2~52COUT1_172\ = CARRY(((\inst1|TSUM[1][20]~combout\ & !\inst1|Add2~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][20]~combout\,
	cin => \inst1|Add2~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~50_combout\,
	cout0 => \inst1|Add2~52\,
	cout1 => \inst1|Add2~52COUT1_172\);

-- Location: LC_X4_Y6_N6
\inst1|Add2~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~45_combout\ = (\inst1|TSUM[1][21]~combout\ $ (((!\inst1|Add2~57\ & \inst1|Add2~52\) # (\inst1|Add2~57\ & \inst1|Add2~52COUT1_172\))))
-- \inst1|Add2~47\ = CARRY(((!\inst1|Add2~52\) # (!\inst1|TSUM[1][21]~combout\)))
-- \inst1|Add2~47COUT1_173\ = CARRY(((!\inst1|Add2~52COUT1_172\) # (!\inst1|TSUM[1][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][21]~combout\,
	cin => \inst1|Add2~57\,
	cin0 => \inst1|Add2~52\,
	cin1 => \inst1|Add2~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~45_combout\,
	cout0 => \inst1|Add2~47\,
	cout1 => \inst1|Add2~47COUT1_173\);

-- Location: LC_X4_Y6_N7
\inst1|Add2~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~40_combout\ = \inst1|TSUM[1][22]~combout\ $ ((((!(!\inst1|Add2~57\ & \inst1|Add2~47\) # (\inst1|Add2~57\ & \inst1|Add2~47COUT1_173\)))))
-- \inst1|Add2~42\ = CARRY((\inst1|TSUM[1][22]~combout\ & ((!\inst1|Add2~47\))))
-- \inst1|Add2~42COUT1_174\ = CARRY((\inst1|TSUM[1][22]~combout\ & ((!\inst1|Add2~47COUT1_173\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[1][22]~combout\,
	cin => \inst1|Add2~57\,
	cin0 => \inst1|Add2~47\,
	cin1 => \inst1|Add2~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~40_combout\,
	cout0 => \inst1|Add2~42\,
	cout1 => \inst1|Add2~42COUT1_174\);

-- Location: LC_X4_Y6_N8
\inst1|Add2~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~35_combout\ = (\inst1|TSUM[1][23]~combout\ $ (((!\inst1|Add2~57\ & \inst1|Add2~42\) # (\inst1|Add2~57\ & \inst1|Add2~42COUT1_174\))))
-- \inst1|Add2~37\ = CARRY(((!\inst1|Add2~42\) # (!\inst1|TSUM[1][23]~combout\)))
-- \inst1|Add2~37COUT1_175\ = CARRY(((!\inst1|Add2~42COUT1_174\) # (!\inst1|TSUM[1][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][23]~combout\,
	cin => \inst1|Add2~57\,
	cin0 => \inst1|Add2~42\,
	cin1 => \inst1|Add2~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~35_combout\,
	cout0 => \inst1|Add2~37\,
	cout1 => \inst1|Add2~37COUT1_175\);

-- Location: LC_X4_Y6_N9
\inst1|Add2~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~30_combout\ = (\inst1|TSUM[1][24]~combout\ $ ((!(!\inst1|Add2~57\ & \inst1|Add2~37\) # (\inst1|Add2~57\ & \inst1|Add2~37COUT1_175\))))
-- \inst1|Add2~32\ = CARRY(((\inst1|TSUM[1][24]~combout\ & !\inst1|Add2~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][24]~combout\,
	cin => \inst1|Add2~57\,
	cin0 => \inst1|Add2~37\,
	cin1 => \inst1|Add2~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~30_combout\,
	cout => \inst1|Add2~32\);

-- Location: LC_X5_Y5_N9
\inst1|TSUM[1][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][28]~combout\ = LCELL((((\inst1|Add1~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][28]~combout\);

-- Location: LC_X5_Y5_N1
\inst1|TSUM[1][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][27]~combout\ = LCELL((((\inst1|Add1~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][27]~combout\);

-- Location: LC_X5_Y5_N3
\inst1|TSUM[1][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][26]~combout\ = LCELL((((\inst1|Add1~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add1~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][26]~combout\);

-- Location: LC_X5_Y5_N0
\inst1|TSUM[1][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[1][25]~combout\ = LCELL((((\inst1|Add1~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add1~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[1][25]~combout\);

-- Location: LC_X5_Y6_N0
\inst1|Add2~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~25_combout\ = (\inst1|TSUM[1][25]~combout\ $ ((\inst1|Add2~32\)))
-- \inst1|Add2~27\ = CARRY(((!\inst1|Add2~32\) # (!\inst1|TSUM[1][25]~combout\)))
-- \inst1|Add2~27COUT1_176\ = CARRY(((!\inst1|Add2~32\) # (!\inst1|TSUM[1][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][25]~combout\,
	cin => \inst1|Add2~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~25_combout\,
	cout0 => \inst1|Add2~27\,
	cout1 => \inst1|Add2~27COUT1_176\);

-- Location: LC_X5_Y6_N1
\inst1|Add2~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~20_combout\ = (\inst1|TSUM[1][26]~combout\ $ ((!(!\inst1|Add2~32\ & \inst1|Add2~27\) # (\inst1|Add2~32\ & \inst1|Add2~27COUT1_176\))))
-- \inst1|Add2~22\ = CARRY(((\inst1|TSUM[1][26]~combout\ & !\inst1|Add2~27\)))
-- \inst1|Add2~22COUT1_177\ = CARRY(((\inst1|TSUM[1][26]~combout\ & !\inst1|Add2~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][26]~combout\,
	cin => \inst1|Add2~32\,
	cin0 => \inst1|Add2~27\,
	cin1 => \inst1|Add2~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~20_combout\,
	cout0 => \inst1|Add2~22\,
	cout1 => \inst1|Add2~22COUT1_177\);

-- Location: LC_X5_Y6_N2
\inst1|Add2~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~15_combout\ = \inst1|TSUM[1][27]~combout\ $ (((((!\inst1|Add2~32\ & \inst1|Add2~22\) # (\inst1|Add2~32\ & \inst1|Add2~22COUT1_177\)))))
-- \inst1|Add2~17\ = CARRY(((!\inst1|Add2~22\)) # (!\inst1|TSUM[1][27]~combout\))
-- \inst1|Add2~17COUT1_178\ = CARRY(((!\inst1|Add2~22COUT1_177\)) # (!\inst1|TSUM[1][27]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[1][27]~combout\,
	cin => \inst1|Add2~32\,
	cin0 => \inst1|Add2~22\,
	cin1 => \inst1|Add2~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~15_combout\,
	cout0 => \inst1|Add2~17\,
	cout1 => \inst1|Add2~17COUT1_178\);

-- Location: LC_X5_Y6_N3
\inst1|Add2~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~10_combout\ = (\inst1|TSUM[1][28]~combout\ $ ((!(!\inst1|Add2~32\ & \inst1|Add2~17\) # (\inst1|Add2~32\ & \inst1|Add2~17COUT1_178\))))
-- \inst1|Add2~12\ = CARRY(((\inst1|TSUM[1][28]~combout\ & !\inst1|Add2~17\)))
-- \inst1|Add2~12COUT1_179\ = CARRY(((\inst1|TSUM[1][28]~combout\ & !\inst1|Add2~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][28]~combout\,
	cin => \inst1|Add2~32\,
	cin0 => \inst1|Add2~17\,
	cin1 => \inst1|Add2~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~10_combout\,
	cout0 => \inst1|Add2~12\,
	cout1 => \inst1|Add2~12COUT1_179\);

-- Location: LC_X5_Y6_N4
\inst1|Add2~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~5_combout\ = (\inst1|TSUM[1][29]~combout\ $ (((!\inst1|Add2~32\ & \inst1|Add2~12\) # (\inst1|Add2~32\ & \inst1|Add2~12COUT1_179\))))
-- \inst1|Add2~7\ = CARRY(((!\inst1|Add2~12COUT1_179\) # (!\inst1|TSUM[1][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[1][29]~combout\,
	cin => \inst1|Add2~32\,
	cin0 => \inst1|Add2~12\,
	cin1 => \inst1|Add2~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~5_combout\,
	cout => \inst1|Add2~7\);

-- Location: LC_X5_Y6_N5
\inst1|Add2~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add2~0_combout\ = ((\inst1|Add2~7\ $ (!\inst1|TSUM[1][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[1][30]~combout\,
	cin => \inst1|Add2~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add2~0_combout\);

-- Location: LC_X5_Y5_N4
\inst1|TSUM[2][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][30]~combout\ = LCELL((((\inst1|Add2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][30]~combout\);

-- Location: LC_X5_Y6_N8
\inst1|TSUM[2][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][29]~combout\ = LCELL((((\inst1|Add2~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][29]~combout\);

-- Location: LC_X4_Y7_N9
\inst1|TSUM[2][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][24]~combout\ = LCELL((((\inst1|Add2~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][24]~combout\);

-- Location: LC_X4_Y7_N4
\inst1|TSUM[2][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][19]~combout\ = LCELL((((\inst1|Add2~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][19]~combout\);

-- Location: LC_X2_Y9_N0
\inst1|TSUM[2][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][14]~combout\ = LCELL((((\inst1|Add2~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][14]~combout\);

-- Location: LC_X5_Y7_N0
\inst1|PSUM[4][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][0]~regout\ = DFFEAS(((!\inst1|PSUM[4][0]~regout\)), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][0]~239\ = CARRY(((\inst1|PSUM[4][0]~regout\)))
-- \inst1|PSUM[4][0]~239COUT1_409\ = CARRY(((\inst1|PSUM[4][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	datab => \inst1|PSUM[4][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][0]~regout\,
	cout0 => \inst1|PSUM[4][0]~239\,
	cout1 => \inst1|PSUM[4][0]~239COUT1_409\);

-- Location: LC_X5_Y7_N1
\inst1|PSUM[4][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][1]~regout\ = DFFEAS((\inst1|PSUM[4][1]~regout\ $ ((\inst1|PSUM[4][0]~239\))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][1]~237\ = CARRY(((!\inst1|PSUM[4][0]~239\) # (!\inst1|PSUM[4][1]~regout\)))
-- \inst1|PSUM[4][1]~237COUT1_410\ = CARRY(((!\inst1|PSUM[4][0]~239COUT1_409\) # (!\inst1|PSUM[4][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	datab => \inst1|PSUM[4][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[4][0]~239\,
	cin1 => \inst1|PSUM[4][0]~239COUT1_409\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][1]~regout\,
	cout0 => \inst1|PSUM[4][1]~237\,
	cout1 => \inst1|PSUM[4][1]~237COUT1_410\);

-- Location: LC_X5_Y7_N2
\inst1|PSUM[4][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][2]~regout\ = DFFEAS((\inst1|PSUM[4][2]~regout\ $ ((!\inst1|PSUM[4][1]~237\))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][2]~235\ = CARRY(((\inst1|PSUM[4][2]~regout\ & !\inst1|PSUM[4][1]~237\)))
-- \inst1|PSUM[4][2]~235COUT1_411\ = CARRY(((\inst1|PSUM[4][2]~regout\ & !\inst1|PSUM[4][1]~237COUT1_410\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	datab => \inst1|PSUM[4][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[4][1]~237\,
	cin1 => \inst1|PSUM[4][1]~237COUT1_410\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][2]~regout\,
	cout0 => \inst1|PSUM[4][2]~235\,
	cout1 => \inst1|PSUM[4][2]~235COUT1_411\);

-- Location: LC_X5_Y7_N3
\inst1|PSUM[4][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][3]~regout\ = DFFEAS(\inst1|PSUM[4][3]~regout\ $ ((((\inst1|PSUM[4][2]~235\)))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][3]~233\ = CARRY(((!\inst1|PSUM[4][2]~235\)) # (!\inst1|PSUM[4][3]~regout\))
-- \inst1|PSUM[4][3]~233COUT1_412\ = CARRY(((!\inst1|PSUM[4][2]~235COUT1_411\)) # (!\inst1|PSUM[4][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	dataa => \inst1|PSUM[4][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[4][2]~235\,
	cin1 => \inst1|PSUM[4][2]~235COUT1_411\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][3]~regout\,
	cout0 => \inst1|PSUM[4][3]~233\,
	cout1 => \inst1|PSUM[4][3]~233COUT1_412\);

-- Location: LC_X5_Y7_N4
\inst1|PSUM[4][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][4]~regout\ = DFFEAS(\inst1|PSUM[4][4]~regout\ $ ((((!\inst1|PSUM[4][3]~233\)))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][4]~231\ = CARRY((\inst1|PSUM[4][4]~regout\ & ((!\inst1|PSUM[4][3]~233COUT1_412\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	dataa => \inst1|PSUM[4][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[4][3]~233\,
	cin1 => \inst1|PSUM[4][3]~233COUT1_412\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][4]~regout\,
	cout => \inst1|PSUM[4][4]~231\);

-- Location: LC_X5_Y7_N5
\inst1|PSUM[4][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][5]~regout\ = DFFEAS(\inst1|PSUM[4][5]~regout\ $ ((((\inst1|PSUM[4][4]~231\)))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][5]~229\ = CARRY(((!\inst1|PSUM[4][4]~231\)) # (!\inst1|PSUM[4][5]~regout\))
-- \inst1|PSUM[4][5]~229COUT1_413\ = CARRY(((!\inst1|PSUM[4][4]~231\)) # (!\inst1|PSUM[4][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	dataa => \inst1|PSUM[4][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[4][4]~231\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][5]~regout\,
	cout0 => \inst1|PSUM[4][5]~229\,
	cout1 => \inst1|PSUM[4][5]~229COUT1_413\);

-- Location: LC_X5_Y7_N6
\inst1|PSUM[4][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][6]~regout\ = DFFEAS(\inst1|PSUM[4][6]~regout\ $ ((((!(!\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][5]~229\) # (\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][5]~229COUT1_413\))))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][6]~227\ = CARRY((\inst1|PSUM[4][6]~regout\ & ((!\inst1|PSUM[4][5]~229\))))
-- \inst1|PSUM[4][6]~227COUT1_414\ = CARRY((\inst1|PSUM[4][6]~regout\ & ((!\inst1|PSUM[4][5]~229COUT1_413\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	dataa => \inst1|PSUM[4][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[4][4]~231\,
	cin0 => \inst1|PSUM[4][5]~229\,
	cin1 => \inst1|PSUM[4][5]~229COUT1_413\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][6]~regout\,
	cout0 => \inst1|PSUM[4][6]~227\,
	cout1 => \inst1|PSUM[4][6]~227COUT1_414\);

-- Location: LC_X5_Y7_N7
\inst1|PSUM[4][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][7]~regout\ = DFFEAS((\inst1|PSUM[4][7]~regout\ $ (((!\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][6]~227\) # (\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][6]~227COUT1_414\)))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][7]~225\ = CARRY(((!\inst1|PSUM[4][6]~227\) # (!\inst1|PSUM[4][7]~regout\)))
-- \inst1|PSUM[4][7]~225COUT1_415\ = CARRY(((!\inst1|PSUM[4][6]~227COUT1_414\) # (!\inst1|PSUM[4][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	datab => \inst1|PSUM[4][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[4][4]~231\,
	cin0 => \inst1|PSUM[4][6]~227\,
	cin1 => \inst1|PSUM[4][6]~227COUT1_414\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][7]~regout\,
	cout0 => \inst1|PSUM[4][7]~225\,
	cout1 => \inst1|PSUM[4][7]~225COUT1_415\);

-- Location: LC_X5_Y7_N8
\inst1|PSUM[4][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][8]~regout\ = DFFEAS(\inst1|PSUM[4][8]~regout\ $ ((((!(!\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][7]~225\) # (\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][7]~225COUT1_415\))))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[4][8]~223\ = CARRY((\inst1|PSUM[4][8]~regout\ & ((!\inst1|PSUM[4][7]~225\))))
-- \inst1|PSUM[4][8]~223COUT1_416\ = CARRY((\inst1|PSUM[4][8]~regout\ & ((!\inst1|PSUM[4][7]~225COUT1_415\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	dataa => \inst1|PSUM[4][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[4][4]~231\,
	cin0 => \inst1|PSUM[4][7]~225\,
	cin1 => \inst1|PSUM[4][7]~225COUT1_415\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][8]~regout\,
	cout0 => \inst1|PSUM[4][8]~223\,
	cout1 => \inst1|PSUM[4][8]~223COUT1_416\);

-- Location: LC_X5_Y7_N9
\inst1|PSUM[4][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[4][9]~regout\ = DFFEAS((((!\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][8]~223\) # (\inst1|PSUM[4][4]~231\ & \inst1|PSUM[4][8]~223COUT1_416\) $ (\inst1|PSUM[4][9]~regout\))), \inst6|STEMP\(4), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(4),
	datad => \inst1|PSUM[4][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[4][4]~231\,
	cin0 => \inst1|PSUM[4][8]~223\,
	cin1 => \inst1|PSUM[4][8]~223COUT1_416\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[4][9]~regout\);

-- Location: LC_X2_Y9_N9
\inst1|TSUM[2][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][9]~combout\ = LCELL((((\inst1|Add2~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][9]~combout\);

-- Location: LC_X2_Y6_N1
\inst1|TSUM[2][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][4]~combout\ = LCELL((((\inst1|Add2~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][4]~combout\);

-- Location: LC_X2_Y8_N2
\inst1|TSUM[2][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][3]~combout\ = LCELL((((\inst1|Add2~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][3]~combout\);

-- Location: LC_X2_Y6_N2
\inst1|TSUM[2][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][2]~combout\ = LCELL((((\inst1|Add2~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][2]~combout\);

-- Location: LC_X2_Y8_N3
\inst1|TSUM[2][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][1]~combout\ = LCELL((((\inst1|Add2~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][1]~combout\);

-- Location: LC_X2_Y6_N3
\inst1|TSUM[2][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][0]~combout\ = LCELL((((\inst1|Add2~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][0]~combout\);

-- Location: LC_X2_Y8_N5
\inst1|Add3~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~150_combout\ = \inst1|PSUM[4][0]~regout\ $ ((\inst1|TSUM[2][0]~combout\))
-- \inst1|Add3~152\ = CARRY((\inst1|PSUM[4][0]~regout\ & (\inst1|TSUM[2][0]~combout\)))
-- \inst1|Add3~152COUT1_156\ = CARRY((\inst1|PSUM[4][0]~regout\ & (\inst1|TSUM[2][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[4][0]~regout\,
	datab => \inst1|TSUM[2][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~150_combout\,
	cout0 => \inst1|Add3~152\,
	cout1 => \inst1|Add3~152COUT1_156\);

-- Location: LC_X2_Y8_N6
\inst1|Add3~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~145_combout\ = \inst1|TSUM[2][1]~combout\ $ (\inst1|PSUM[4][1]~regout\ $ ((\inst1|Add3~152\)))
-- \inst1|Add3~147\ = CARRY((\inst1|TSUM[2][1]~combout\ & (!\inst1|PSUM[4][1]~regout\ & !\inst1|Add3~152\)) # (!\inst1|TSUM[2][1]~combout\ & ((!\inst1|Add3~152\) # (!\inst1|PSUM[4][1]~regout\))))
-- \inst1|Add3~147COUT1_157\ = CARRY((\inst1|TSUM[2][1]~combout\ & (!\inst1|PSUM[4][1]~regout\ & !\inst1|Add3~152COUT1_156\)) # (!\inst1|TSUM[2][1]~combout\ & ((!\inst1|Add3~152COUT1_156\) # (!\inst1|PSUM[4][1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][1]~combout\,
	datab => \inst1|PSUM[4][1]~regout\,
	cin0 => \inst1|Add3~152\,
	cin1 => \inst1|Add3~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~145_combout\,
	cout0 => \inst1|Add3~147\,
	cout1 => \inst1|Add3~147COUT1_157\);

-- Location: LC_X2_Y8_N7
\inst1|Add3~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~140_combout\ = \inst1|PSUM[4][2]~regout\ $ (\inst1|TSUM[2][2]~combout\ $ ((!\inst1|Add3~147\)))
-- \inst1|Add3~142\ = CARRY((\inst1|PSUM[4][2]~regout\ & ((\inst1|TSUM[2][2]~combout\) # (!\inst1|Add3~147\))) # (!\inst1|PSUM[4][2]~regout\ & (\inst1|TSUM[2][2]~combout\ & !\inst1|Add3~147\)))
-- \inst1|Add3~142COUT1_158\ = CARRY((\inst1|PSUM[4][2]~regout\ & ((\inst1|TSUM[2][2]~combout\) # (!\inst1|Add3~147COUT1_157\))) # (!\inst1|PSUM[4][2]~regout\ & (\inst1|TSUM[2][2]~combout\ & !\inst1|Add3~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[4][2]~regout\,
	datab => \inst1|TSUM[2][2]~combout\,
	cin0 => \inst1|Add3~147\,
	cin1 => \inst1|Add3~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~140_combout\,
	cout0 => \inst1|Add3~142\,
	cout1 => \inst1|Add3~142COUT1_158\);

-- Location: LC_X2_Y8_N8
\inst1|Add3~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~135_combout\ = \inst1|PSUM[4][3]~regout\ $ (\inst1|TSUM[2][3]~combout\ $ ((\inst1|Add3~142\)))
-- \inst1|Add3~137\ = CARRY((\inst1|PSUM[4][3]~regout\ & (!\inst1|TSUM[2][3]~combout\ & !\inst1|Add3~142\)) # (!\inst1|PSUM[4][3]~regout\ & ((!\inst1|Add3~142\) # (!\inst1|TSUM[2][3]~combout\))))
-- \inst1|Add3~137COUT1_159\ = CARRY((\inst1|PSUM[4][3]~regout\ & (!\inst1|TSUM[2][3]~combout\ & !\inst1|Add3~142COUT1_158\)) # (!\inst1|PSUM[4][3]~regout\ & ((!\inst1|Add3~142COUT1_158\) # (!\inst1|TSUM[2][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[4][3]~regout\,
	datab => \inst1|TSUM[2][3]~combout\,
	cin0 => \inst1|Add3~142\,
	cin1 => \inst1|Add3~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~135_combout\,
	cout0 => \inst1|Add3~137\,
	cout1 => \inst1|Add3~137COUT1_159\);

-- Location: LC_X2_Y8_N9
\inst1|Add3~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~130_combout\ = \inst1|PSUM[4][4]~regout\ $ (\inst1|TSUM[2][4]~combout\ $ ((!\inst1|Add3~137\)))
-- \inst1|Add3~132\ = CARRY((\inst1|PSUM[4][4]~regout\ & ((\inst1|TSUM[2][4]~combout\) # (!\inst1|Add3~137COUT1_159\))) # (!\inst1|PSUM[4][4]~regout\ & (\inst1|TSUM[2][4]~combout\ & !\inst1|Add3~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[4][4]~regout\,
	datab => \inst1|TSUM[2][4]~combout\,
	cin0 => \inst1|Add3~137\,
	cin1 => \inst1|Add3~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~130_combout\,
	cout => \inst1|Add3~132\);

-- Location: LC_X2_Y9_N4
\inst1|TSUM[2][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][8]~combout\ = LCELL((((\inst1|Add2~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][8]~combout\);

-- Location: LC_X2_Y9_N2
\inst1|TSUM[2][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][7]~combout\ = LCELL((((\inst1|Add2~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][7]~combout\);

-- Location: LC_X2_Y9_N1
\inst1|TSUM[2][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][6]~combout\ = LCELL((((\inst1|Add2~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][6]~combout\);

-- Location: LC_X2_Y8_N0
\inst1|TSUM[2][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][5]~combout\ = LCELL((((\inst1|Add2~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][5]~combout\);

-- Location: LC_X3_Y8_N0
\inst1|Add3~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~125_combout\ = \inst1|TSUM[2][5]~combout\ $ (\inst1|PSUM[4][5]~regout\ $ ((\inst1|Add3~132\)))
-- \inst1|Add3~127\ = CARRY((\inst1|TSUM[2][5]~combout\ & (!\inst1|PSUM[4][5]~regout\ & !\inst1|Add3~132\)) # (!\inst1|TSUM[2][5]~combout\ & ((!\inst1|Add3~132\) # (!\inst1|PSUM[4][5]~regout\))))
-- \inst1|Add3~127COUT1_160\ = CARRY((\inst1|TSUM[2][5]~combout\ & (!\inst1|PSUM[4][5]~regout\ & !\inst1|Add3~132\)) # (!\inst1|TSUM[2][5]~combout\ & ((!\inst1|Add3~132\) # (!\inst1|PSUM[4][5]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][5]~combout\,
	datab => \inst1|PSUM[4][5]~regout\,
	cin => \inst1|Add3~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~125_combout\,
	cout0 => \inst1|Add3~127\,
	cout1 => \inst1|Add3~127COUT1_160\);

-- Location: LC_X3_Y8_N1
\inst1|Add3~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~120_combout\ = \inst1|TSUM[2][6]~combout\ $ (\inst1|PSUM[4][6]~regout\ $ ((!(!\inst1|Add3~132\ & \inst1|Add3~127\) # (\inst1|Add3~132\ & \inst1|Add3~127COUT1_160\))))
-- \inst1|Add3~122\ = CARRY((\inst1|TSUM[2][6]~combout\ & ((\inst1|PSUM[4][6]~regout\) # (!\inst1|Add3~127\))) # (!\inst1|TSUM[2][6]~combout\ & (\inst1|PSUM[4][6]~regout\ & !\inst1|Add3~127\)))
-- \inst1|Add3~122COUT1_161\ = CARRY((\inst1|TSUM[2][6]~combout\ & ((\inst1|PSUM[4][6]~regout\) # (!\inst1|Add3~127COUT1_160\))) # (!\inst1|TSUM[2][6]~combout\ & (\inst1|PSUM[4][6]~regout\ & !\inst1|Add3~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][6]~combout\,
	datab => \inst1|PSUM[4][6]~regout\,
	cin => \inst1|Add3~132\,
	cin0 => \inst1|Add3~127\,
	cin1 => \inst1|Add3~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~120_combout\,
	cout0 => \inst1|Add3~122\,
	cout1 => \inst1|Add3~122COUT1_161\);

-- Location: LC_X3_Y8_N2
\inst1|Add3~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~115_combout\ = \inst1|TSUM[2][7]~combout\ $ (\inst1|PSUM[4][7]~regout\ $ (((!\inst1|Add3~132\ & \inst1|Add3~122\) # (\inst1|Add3~132\ & \inst1|Add3~122COUT1_161\))))
-- \inst1|Add3~117\ = CARRY((\inst1|TSUM[2][7]~combout\ & (!\inst1|PSUM[4][7]~regout\ & !\inst1|Add3~122\)) # (!\inst1|TSUM[2][7]~combout\ & ((!\inst1|Add3~122\) # (!\inst1|PSUM[4][7]~regout\))))
-- \inst1|Add3~117COUT1_162\ = CARRY((\inst1|TSUM[2][7]~combout\ & (!\inst1|PSUM[4][7]~regout\ & !\inst1|Add3~122COUT1_161\)) # (!\inst1|TSUM[2][7]~combout\ & ((!\inst1|Add3~122COUT1_161\) # (!\inst1|PSUM[4][7]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][7]~combout\,
	datab => \inst1|PSUM[4][7]~regout\,
	cin => \inst1|Add3~132\,
	cin0 => \inst1|Add3~122\,
	cin1 => \inst1|Add3~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~115_combout\,
	cout0 => \inst1|Add3~117\,
	cout1 => \inst1|Add3~117COUT1_162\);

-- Location: LC_X3_Y8_N3
\inst1|Add3~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~110_combout\ = \inst1|PSUM[4][8]~regout\ $ (\inst1|TSUM[2][8]~combout\ $ ((!(!\inst1|Add3~132\ & \inst1|Add3~117\) # (\inst1|Add3~132\ & \inst1|Add3~117COUT1_162\))))
-- \inst1|Add3~112\ = CARRY((\inst1|PSUM[4][8]~regout\ & ((\inst1|TSUM[2][8]~combout\) # (!\inst1|Add3~117\))) # (!\inst1|PSUM[4][8]~regout\ & (\inst1|TSUM[2][8]~combout\ & !\inst1|Add3~117\)))
-- \inst1|Add3~112COUT1_163\ = CARRY((\inst1|PSUM[4][8]~regout\ & ((\inst1|TSUM[2][8]~combout\) # (!\inst1|Add3~117COUT1_162\))) # (!\inst1|PSUM[4][8]~regout\ & (\inst1|TSUM[2][8]~combout\ & !\inst1|Add3~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[4][8]~regout\,
	datab => \inst1|TSUM[2][8]~combout\,
	cin => \inst1|Add3~132\,
	cin0 => \inst1|Add3~117\,
	cin1 => \inst1|Add3~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~110_combout\,
	cout0 => \inst1|Add3~112\,
	cout1 => \inst1|Add3~112COUT1_163\);

-- Location: LC_X3_Y8_N4
\inst1|Add3~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~105_combout\ = \inst1|PSUM[4][9]~regout\ $ (\inst1|TSUM[2][9]~combout\ $ (((!\inst1|Add3~132\ & \inst1|Add3~112\) # (\inst1|Add3~132\ & \inst1|Add3~112COUT1_163\))))
-- \inst1|Add3~107\ = CARRY((\inst1|PSUM[4][9]~regout\ & (!\inst1|TSUM[2][9]~combout\ & !\inst1|Add3~112COUT1_163\)) # (!\inst1|PSUM[4][9]~regout\ & ((!\inst1|Add3~112COUT1_163\) # (!\inst1|TSUM[2][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[4][9]~regout\,
	datab => \inst1|TSUM[2][9]~combout\,
	cin => \inst1|Add3~132\,
	cin0 => \inst1|Add3~112\,
	cin1 => \inst1|Add3~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~105_combout\,
	cout => \inst1|Add3~107\);

-- Location: LC_X2_Y8_N1
\inst1|TSUM[2][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][13]~combout\ = LCELL((((\inst1|Add2~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][13]~combout\);

-- Location: LC_X2_Y9_N7
\inst1|TSUM[2][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][12]~combout\ = LCELL((((\inst1|Add2~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][12]~combout\);

-- Location: LC_X2_Y9_N5
\inst1|TSUM[2][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][11]~combout\ = LCELL((((\inst1|Add2~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][11]~combout\);

-- Location: LC_X2_Y8_N4
\inst1|TSUM[2][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][10]~combout\ = LCELL((((\inst1|Add2~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][10]~combout\);

-- Location: LC_X3_Y8_N5
\inst1|Add3~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~100_combout\ = (\inst1|TSUM[2][10]~combout\ $ ((!\inst1|Add3~107\)))
-- \inst1|Add3~102\ = CARRY(((\inst1|TSUM[2][10]~combout\ & !\inst1|Add3~107\)))
-- \inst1|Add3~102COUT1_164\ = CARRY(((\inst1|TSUM[2][10]~combout\ & !\inst1|Add3~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][10]~combout\,
	cin => \inst1|Add3~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~100_combout\,
	cout0 => \inst1|Add3~102\,
	cout1 => \inst1|Add3~102COUT1_164\);

-- Location: LC_X3_Y8_N6
\inst1|Add3~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~95_combout\ = \inst1|TSUM[2][11]~combout\ $ (((((!\inst1|Add3~107\ & \inst1|Add3~102\) # (\inst1|Add3~107\ & \inst1|Add3~102COUT1_164\)))))
-- \inst1|Add3~97\ = CARRY(((!\inst1|Add3~102\)) # (!\inst1|TSUM[2][11]~combout\))
-- \inst1|Add3~97COUT1_165\ = CARRY(((!\inst1|Add3~102COUT1_164\)) # (!\inst1|TSUM[2][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][11]~combout\,
	cin => \inst1|Add3~107\,
	cin0 => \inst1|Add3~102\,
	cin1 => \inst1|Add3~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~95_combout\,
	cout0 => \inst1|Add3~97\,
	cout1 => \inst1|Add3~97COUT1_165\);

-- Location: LC_X3_Y8_N7
\inst1|Add3~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~90_combout\ = \inst1|TSUM[2][12]~combout\ $ ((((!(!\inst1|Add3~107\ & \inst1|Add3~97\) # (\inst1|Add3~107\ & \inst1|Add3~97COUT1_165\)))))
-- \inst1|Add3~92\ = CARRY((\inst1|TSUM[2][12]~combout\ & ((!\inst1|Add3~97\))))
-- \inst1|Add3~92COUT1_166\ = CARRY((\inst1|TSUM[2][12]~combout\ & ((!\inst1|Add3~97COUT1_165\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][12]~combout\,
	cin => \inst1|Add3~107\,
	cin0 => \inst1|Add3~97\,
	cin1 => \inst1|Add3~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~90_combout\,
	cout0 => \inst1|Add3~92\,
	cout1 => \inst1|Add3~92COUT1_166\);

-- Location: LC_X3_Y8_N8
\inst1|Add3~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~85_combout\ = \inst1|TSUM[2][13]~combout\ $ (((((!\inst1|Add3~107\ & \inst1|Add3~92\) # (\inst1|Add3~107\ & \inst1|Add3~92COUT1_166\)))))
-- \inst1|Add3~87\ = CARRY(((!\inst1|Add3~92\)) # (!\inst1|TSUM[2][13]~combout\))
-- \inst1|Add3~87COUT1_167\ = CARRY(((!\inst1|Add3~92COUT1_166\)) # (!\inst1|TSUM[2][13]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][13]~combout\,
	cin => \inst1|Add3~107\,
	cin0 => \inst1|Add3~92\,
	cin1 => \inst1|Add3~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~85_combout\,
	cout0 => \inst1|Add3~87\,
	cout1 => \inst1|Add3~87COUT1_167\);

-- Location: LC_X3_Y8_N9
\inst1|Add3~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~80_combout\ = (\inst1|TSUM[2][14]~combout\ $ ((!(!\inst1|Add3~107\ & \inst1|Add3~87\) # (\inst1|Add3~107\ & \inst1|Add3~87COUT1_167\))))
-- \inst1|Add3~82\ = CARRY(((\inst1|TSUM[2][14]~combout\ & !\inst1|Add3~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][14]~combout\,
	cin => \inst1|Add3~107\,
	cin0 => \inst1|Add3~87\,
	cin1 => \inst1|Add3~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~80_combout\,
	cout => \inst1|Add3~82\);

-- Location: LC_X4_Y7_N7
\inst1|TSUM[2][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][18]~combout\ = LCELL((((\inst1|Add2~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][18]~combout\);

-- Location: LC_X4_Y7_N2
\inst1|TSUM[2][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][17]~combout\ = LCELL((((\inst1|Add2~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][17]~combout\);

-- Location: LC_X4_Y7_N3
\inst1|TSUM[2][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][16]~combout\ = LCELL((((\inst1|Add2~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][16]~combout\);

-- Location: LC_X4_Y7_N0
\inst1|TSUM[2][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][15]~combout\ = LCELL((((\inst1|Add2~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][15]~combout\);

-- Location: LC_X4_Y8_N0
\inst1|Add3~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~75_combout\ = \inst1|TSUM[2][15]~combout\ $ ((((\inst1|Add3~82\))))
-- \inst1|Add3~77\ = CARRY(((!\inst1|Add3~82\)) # (!\inst1|TSUM[2][15]~combout\))
-- \inst1|Add3~77COUT1_168\ = CARRY(((!\inst1|Add3~82\)) # (!\inst1|TSUM[2][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][15]~combout\,
	cin => \inst1|Add3~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~75_combout\,
	cout0 => \inst1|Add3~77\,
	cout1 => \inst1|Add3~77COUT1_168\);

-- Location: LC_X4_Y8_N1
\inst1|Add3~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~70_combout\ = (\inst1|TSUM[2][16]~combout\ $ ((!(!\inst1|Add3~82\ & \inst1|Add3~77\) # (\inst1|Add3~82\ & \inst1|Add3~77COUT1_168\))))
-- \inst1|Add3~72\ = CARRY(((\inst1|TSUM[2][16]~combout\ & !\inst1|Add3~77\)))
-- \inst1|Add3~72COUT1_169\ = CARRY(((\inst1|TSUM[2][16]~combout\ & !\inst1|Add3~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][16]~combout\,
	cin => \inst1|Add3~82\,
	cin0 => \inst1|Add3~77\,
	cin1 => \inst1|Add3~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~70_combout\,
	cout0 => \inst1|Add3~72\,
	cout1 => \inst1|Add3~72COUT1_169\);

-- Location: LC_X4_Y8_N2
\inst1|Add3~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~65_combout\ = (\inst1|TSUM[2][17]~combout\ $ (((!\inst1|Add3~82\ & \inst1|Add3~72\) # (\inst1|Add3~82\ & \inst1|Add3~72COUT1_169\))))
-- \inst1|Add3~67\ = CARRY(((!\inst1|Add3~72\) # (!\inst1|TSUM[2][17]~combout\)))
-- \inst1|Add3~67COUT1_170\ = CARRY(((!\inst1|Add3~72COUT1_169\) # (!\inst1|TSUM[2][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][17]~combout\,
	cin => \inst1|Add3~82\,
	cin0 => \inst1|Add3~72\,
	cin1 => \inst1|Add3~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~65_combout\,
	cout0 => \inst1|Add3~67\,
	cout1 => \inst1|Add3~67COUT1_170\);

-- Location: LC_X4_Y8_N3
\inst1|Add3~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~60_combout\ = \inst1|TSUM[2][18]~combout\ $ ((((!(!\inst1|Add3~82\ & \inst1|Add3~67\) # (\inst1|Add3~82\ & \inst1|Add3~67COUT1_170\)))))
-- \inst1|Add3~62\ = CARRY((\inst1|TSUM[2][18]~combout\ & ((!\inst1|Add3~67\))))
-- \inst1|Add3~62COUT1_171\ = CARRY((\inst1|TSUM[2][18]~combout\ & ((!\inst1|Add3~67COUT1_170\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][18]~combout\,
	cin => \inst1|Add3~82\,
	cin0 => \inst1|Add3~67\,
	cin1 => \inst1|Add3~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~60_combout\,
	cout0 => \inst1|Add3~62\,
	cout1 => \inst1|Add3~62COUT1_171\);

-- Location: LC_X4_Y8_N4
\inst1|Add3~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~55_combout\ = \inst1|TSUM[2][19]~combout\ $ (((((!\inst1|Add3~82\ & \inst1|Add3~62\) # (\inst1|Add3~82\ & \inst1|Add3~62COUT1_171\)))))
-- \inst1|Add3~57\ = CARRY(((!\inst1|Add3~62COUT1_171\)) # (!\inst1|TSUM[2][19]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][19]~combout\,
	cin => \inst1|Add3~82\,
	cin0 => \inst1|Add3~62\,
	cin1 => \inst1|Add3~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~55_combout\,
	cout => \inst1|Add3~57\);

-- Location: LC_X4_Y7_N5
\inst1|TSUM[2][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][23]~combout\ = LCELL((((\inst1|Add2~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][23]~combout\);

-- Location: LC_X4_Y7_N8
\inst1|TSUM[2][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][22]~combout\ = LCELL((((\inst1|Add2~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][22]~combout\);

-- Location: LC_X4_Y7_N1
\inst1|TSUM[2][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][21]~combout\ = LCELL((((\inst1|Add2~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][21]~combout\);

-- Location: LC_X4_Y7_N6
\inst1|TSUM[2][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][20]~combout\ = LCELL((((\inst1|Add2~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][20]~combout\);

-- Location: LC_X4_Y8_N5
\inst1|Add3~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~50_combout\ = \inst1|TSUM[2][20]~combout\ $ ((((!\inst1|Add3~57\))))
-- \inst1|Add3~52\ = CARRY((\inst1|TSUM[2][20]~combout\ & ((!\inst1|Add3~57\))))
-- \inst1|Add3~52COUT1_172\ = CARRY((\inst1|TSUM[2][20]~combout\ & ((!\inst1|Add3~57\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][20]~combout\,
	cin => \inst1|Add3~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~50_combout\,
	cout0 => \inst1|Add3~52\,
	cout1 => \inst1|Add3~52COUT1_172\);

-- Location: LC_X4_Y8_N6
\inst1|Add3~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~45_combout\ = (\inst1|TSUM[2][21]~combout\ $ (((!\inst1|Add3~57\ & \inst1|Add3~52\) # (\inst1|Add3~57\ & \inst1|Add3~52COUT1_172\))))
-- \inst1|Add3~47\ = CARRY(((!\inst1|Add3~52\) # (!\inst1|TSUM[2][21]~combout\)))
-- \inst1|Add3~47COUT1_173\ = CARRY(((!\inst1|Add3~52COUT1_172\) # (!\inst1|TSUM[2][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][21]~combout\,
	cin => \inst1|Add3~57\,
	cin0 => \inst1|Add3~52\,
	cin1 => \inst1|Add3~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~45_combout\,
	cout0 => \inst1|Add3~47\,
	cout1 => \inst1|Add3~47COUT1_173\);

-- Location: LC_X4_Y8_N7
\inst1|Add3~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~40_combout\ = \inst1|TSUM[2][22]~combout\ $ ((((!(!\inst1|Add3~57\ & \inst1|Add3~47\) # (\inst1|Add3~57\ & \inst1|Add3~47COUT1_173\)))))
-- \inst1|Add3~42\ = CARRY((\inst1|TSUM[2][22]~combout\ & ((!\inst1|Add3~47\))))
-- \inst1|Add3~42COUT1_174\ = CARRY((\inst1|TSUM[2][22]~combout\ & ((!\inst1|Add3~47COUT1_173\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][22]~combout\,
	cin => \inst1|Add3~57\,
	cin0 => \inst1|Add3~47\,
	cin1 => \inst1|Add3~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~40_combout\,
	cout0 => \inst1|Add3~42\,
	cout1 => \inst1|Add3~42COUT1_174\);

-- Location: LC_X4_Y8_N8
\inst1|Add3~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~35_combout\ = (\inst1|TSUM[2][23]~combout\ $ (((!\inst1|Add3~57\ & \inst1|Add3~42\) # (\inst1|Add3~57\ & \inst1|Add3~42COUT1_174\))))
-- \inst1|Add3~37\ = CARRY(((!\inst1|Add3~42\) # (!\inst1|TSUM[2][23]~combout\)))
-- \inst1|Add3~37COUT1_175\ = CARRY(((!\inst1|Add3~42COUT1_174\) # (!\inst1|TSUM[2][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][23]~combout\,
	cin => \inst1|Add3~57\,
	cin0 => \inst1|Add3~42\,
	cin1 => \inst1|Add3~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~35_combout\,
	cout0 => \inst1|Add3~37\,
	cout1 => \inst1|Add3~37COUT1_175\);

-- Location: LC_X4_Y8_N9
\inst1|Add3~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~30_combout\ = (\inst1|TSUM[2][24]~combout\ $ ((!(!\inst1|Add3~57\ & \inst1|Add3~37\) # (\inst1|Add3~57\ & \inst1|Add3~37COUT1_175\))))
-- \inst1|Add3~32\ = CARRY(((\inst1|TSUM[2][24]~combout\ & !\inst1|Add3~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][24]~combout\,
	cin => \inst1|Add3~57\,
	cin0 => \inst1|Add3~37\,
	cin1 => \inst1|Add3~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~30_combout\,
	cout => \inst1|Add3~32\);

-- Location: LC_X5_Y5_N5
\inst1|TSUM[2][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][28]~combout\ = LCELL((((\inst1|Add2~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][28]~combout\);

-- Location: LC_X5_Y6_N9
\inst1|TSUM[2][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][27]~combout\ = LCELL((((\inst1|Add2~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][27]~combout\);

-- Location: LC_X5_Y6_N6
\inst1|TSUM[2][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][26]~combout\ = LCELL((((\inst1|Add2~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add2~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][26]~combout\);

-- Location: LC_X5_Y6_N7
\inst1|TSUM[2][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[2][25]~combout\ = LCELL((((\inst1|Add2~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add2~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[2][25]~combout\);

-- Location: LC_X5_Y8_N0
\inst1|Add3~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~25_combout\ = (\inst1|TSUM[2][25]~combout\ $ ((\inst1|Add3~32\)))
-- \inst1|Add3~27\ = CARRY(((!\inst1|Add3~32\) # (!\inst1|TSUM[2][25]~combout\)))
-- \inst1|Add3~27COUT1_176\ = CARRY(((!\inst1|Add3~32\) # (!\inst1|TSUM[2][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][25]~combout\,
	cin => \inst1|Add3~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~25_combout\,
	cout0 => \inst1|Add3~27\,
	cout1 => \inst1|Add3~27COUT1_176\);

-- Location: LC_X5_Y8_N1
\inst1|Add3~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~20_combout\ = \inst1|TSUM[2][26]~combout\ $ ((((!(!\inst1|Add3~32\ & \inst1|Add3~27\) # (\inst1|Add3~32\ & \inst1|Add3~27COUT1_176\)))))
-- \inst1|Add3~22\ = CARRY((\inst1|TSUM[2][26]~combout\ & ((!\inst1|Add3~27\))))
-- \inst1|Add3~22COUT1_177\ = CARRY((\inst1|TSUM[2][26]~combout\ & ((!\inst1|Add3~27COUT1_176\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][26]~combout\,
	cin => \inst1|Add3~32\,
	cin0 => \inst1|Add3~27\,
	cin1 => \inst1|Add3~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~20_combout\,
	cout0 => \inst1|Add3~22\,
	cout1 => \inst1|Add3~22COUT1_177\);

-- Location: LC_X5_Y8_N2
\inst1|Add3~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~15_combout\ = (\inst1|TSUM[2][27]~combout\ $ (((!\inst1|Add3~32\ & \inst1|Add3~22\) # (\inst1|Add3~32\ & \inst1|Add3~22COUT1_177\))))
-- \inst1|Add3~17\ = CARRY(((!\inst1|Add3~22\) # (!\inst1|TSUM[2][27]~combout\)))
-- \inst1|Add3~17COUT1_178\ = CARRY(((!\inst1|Add3~22COUT1_177\) # (!\inst1|TSUM[2][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][27]~combout\,
	cin => \inst1|Add3~32\,
	cin0 => \inst1|Add3~22\,
	cin1 => \inst1|Add3~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~15_combout\,
	cout0 => \inst1|Add3~17\,
	cout1 => \inst1|Add3~17COUT1_178\);

-- Location: LC_X5_Y8_N3
\inst1|Add3~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~10_combout\ = \inst1|TSUM[2][28]~combout\ $ ((((!(!\inst1|Add3~32\ & \inst1|Add3~17\) # (\inst1|Add3~32\ & \inst1|Add3~17COUT1_178\)))))
-- \inst1|Add3~12\ = CARRY((\inst1|TSUM[2][28]~combout\ & ((!\inst1|Add3~17\))))
-- \inst1|Add3~12COUT1_179\ = CARRY((\inst1|TSUM[2][28]~combout\ & ((!\inst1|Add3~17COUT1_178\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][28]~combout\,
	cin => \inst1|Add3~32\,
	cin0 => \inst1|Add3~17\,
	cin1 => \inst1|Add3~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~10_combout\,
	cout0 => \inst1|Add3~12\,
	cout1 => \inst1|Add3~12COUT1_179\);

-- Location: LC_X5_Y8_N4
\inst1|Add3~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~5_combout\ = (\inst1|TSUM[2][29]~combout\ $ (((!\inst1|Add3~32\ & \inst1|Add3~12\) # (\inst1|Add3~32\ & \inst1|Add3~12COUT1_179\))))
-- \inst1|Add3~7\ = CARRY(((!\inst1|Add3~12COUT1_179\) # (!\inst1|TSUM[2][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[2][29]~combout\,
	cin => \inst1|Add3~32\,
	cin0 => \inst1|Add3~12\,
	cin1 => \inst1|Add3~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~5_combout\,
	cout => \inst1|Add3~7\);

-- Location: LC_X5_Y8_N5
\inst1|Add3~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add3~0_combout\ = \inst1|TSUM[2][30]~combout\ $ ((((!\inst1|Add3~7\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a5a5",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[2][30]~combout\,
	cin => \inst1|Add3~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add3~0_combout\);

-- Location: LC_X5_Y8_N6
\inst1|TSUM[3][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][30]~combout\ = LCELL((((\inst1|Add3~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][30]~combout\);

-- Location: LC_X5_Y8_N8
\inst1|TSUM[3][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][29]~combout\ = LCELL((((\inst1|Add3~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][29]~combout\);

-- Location: LC_X4_Y9_N2
\inst1|TSUM[3][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][24]~combout\ = LCELL((((\inst1|Add3~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][24]~combout\);

-- Location: LC_X4_Y9_N4
\inst1|TSUM[3][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][19]~combout\ = LCELL((((\inst1|Add3~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][19]~combout\);

-- Location: LC_X3_Y9_N0
\inst1|TSUM[3][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][14]~combout\ = LCELL((((\inst1|Add3~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][14]~combout\);

-- Location: LC_X3_Y7_N0
\inst1|PSUM[5][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][0]~regout\ = DFFEAS(((!\inst1|PSUM[5][0]~regout\)), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][0]~219\ = CARRY(((\inst1|PSUM[5][0]~regout\)))
-- \inst1|PSUM[5][0]~219COUT1_401\ = CARRY(((\inst1|PSUM[5][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	datab => \inst1|PSUM[5][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][0]~regout\,
	cout0 => \inst1|PSUM[5][0]~219\,
	cout1 => \inst1|PSUM[5][0]~219COUT1_401\);

-- Location: LC_X3_Y7_N1
\inst1|PSUM[5][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][1]~regout\ = DFFEAS((\inst1|PSUM[5][1]~regout\ $ ((\inst1|PSUM[5][0]~219\))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][1]~217\ = CARRY(((!\inst1|PSUM[5][0]~219\) # (!\inst1|PSUM[5][1]~regout\)))
-- \inst1|PSUM[5][1]~217COUT1_402\ = CARRY(((!\inst1|PSUM[5][0]~219COUT1_401\) # (!\inst1|PSUM[5][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	datab => \inst1|PSUM[5][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[5][0]~219\,
	cin1 => \inst1|PSUM[5][0]~219COUT1_401\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][1]~regout\,
	cout0 => \inst1|PSUM[5][1]~217\,
	cout1 => \inst1|PSUM[5][1]~217COUT1_402\);

-- Location: LC_X3_Y7_N2
\inst1|PSUM[5][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][2]~regout\ = DFFEAS((\inst1|PSUM[5][2]~regout\ $ ((!\inst1|PSUM[5][1]~217\))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][2]~215\ = CARRY(((\inst1|PSUM[5][2]~regout\ & !\inst1|PSUM[5][1]~217\)))
-- \inst1|PSUM[5][2]~215COUT1_403\ = CARRY(((\inst1|PSUM[5][2]~regout\ & !\inst1|PSUM[5][1]~217COUT1_402\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	datab => \inst1|PSUM[5][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[5][1]~217\,
	cin1 => \inst1|PSUM[5][1]~217COUT1_402\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][2]~regout\,
	cout0 => \inst1|PSUM[5][2]~215\,
	cout1 => \inst1|PSUM[5][2]~215COUT1_403\);

-- Location: LC_X3_Y7_N3
\inst1|PSUM[5][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][3]~regout\ = DFFEAS(\inst1|PSUM[5][3]~regout\ $ ((((\inst1|PSUM[5][2]~215\)))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][3]~213\ = CARRY(((!\inst1|PSUM[5][2]~215\)) # (!\inst1|PSUM[5][3]~regout\))
-- \inst1|PSUM[5][3]~213COUT1_404\ = CARRY(((!\inst1|PSUM[5][2]~215COUT1_403\)) # (!\inst1|PSUM[5][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	dataa => \inst1|PSUM[5][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[5][2]~215\,
	cin1 => \inst1|PSUM[5][2]~215COUT1_403\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][3]~regout\,
	cout0 => \inst1|PSUM[5][3]~213\,
	cout1 => \inst1|PSUM[5][3]~213COUT1_404\);

-- Location: LC_X3_Y7_N4
\inst1|PSUM[5][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][4]~regout\ = DFFEAS(\inst1|PSUM[5][4]~regout\ $ ((((!\inst1|PSUM[5][3]~213\)))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][4]~211\ = CARRY((\inst1|PSUM[5][4]~regout\ & ((!\inst1|PSUM[5][3]~213COUT1_404\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	dataa => \inst1|PSUM[5][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[5][3]~213\,
	cin1 => \inst1|PSUM[5][3]~213COUT1_404\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][4]~regout\,
	cout => \inst1|PSUM[5][4]~211\);

-- Location: LC_X3_Y7_N5
\inst1|PSUM[5][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][5]~regout\ = DFFEAS(\inst1|PSUM[5][5]~regout\ $ ((((\inst1|PSUM[5][4]~211\)))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][5]~209\ = CARRY(((!\inst1|PSUM[5][4]~211\)) # (!\inst1|PSUM[5][5]~regout\))
-- \inst1|PSUM[5][5]~209COUT1_405\ = CARRY(((!\inst1|PSUM[5][4]~211\)) # (!\inst1|PSUM[5][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	dataa => \inst1|PSUM[5][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[5][4]~211\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][5]~regout\,
	cout0 => \inst1|PSUM[5][5]~209\,
	cout1 => \inst1|PSUM[5][5]~209COUT1_405\);

-- Location: LC_X3_Y7_N6
\inst1|PSUM[5][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][6]~regout\ = DFFEAS(\inst1|PSUM[5][6]~regout\ $ ((((!(!\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][5]~209\) # (\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][5]~209COUT1_405\))))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][6]~207\ = CARRY((\inst1|PSUM[5][6]~regout\ & ((!\inst1|PSUM[5][5]~209\))))
-- \inst1|PSUM[5][6]~207COUT1_406\ = CARRY((\inst1|PSUM[5][6]~regout\ & ((!\inst1|PSUM[5][5]~209COUT1_405\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	dataa => \inst1|PSUM[5][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[5][4]~211\,
	cin0 => \inst1|PSUM[5][5]~209\,
	cin1 => \inst1|PSUM[5][5]~209COUT1_405\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][6]~regout\,
	cout0 => \inst1|PSUM[5][6]~207\,
	cout1 => \inst1|PSUM[5][6]~207COUT1_406\);

-- Location: LC_X3_Y7_N7
\inst1|PSUM[5][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][7]~regout\ = DFFEAS((\inst1|PSUM[5][7]~regout\ $ (((!\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][6]~207\) # (\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][6]~207COUT1_406\)))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][7]~205\ = CARRY(((!\inst1|PSUM[5][6]~207\) # (!\inst1|PSUM[5][7]~regout\)))
-- \inst1|PSUM[5][7]~205COUT1_407\ = CARRY(((!\inst1|PSUM[5][6]~207COUT1_406\) # (!\inst1|PSUM[5][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	datab => \inst1|PSUM[5][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[5][4]~211\,
	cin0 => \inst1|PSUM[5][6]~207\,
	cin1 => \inst1|PSUM[5][6]~207COUT1_406\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][7]~regout\,
	cout0 => \inst1|PSUM[5][7]~205\,
	cout1 => \inst1|PSUM[5][7]~205COUT1_407\);

-- Location: LC_X3_Y7_N8
\inst1|PSUM[5][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][8]~regout\ = DFFEAS(\inst1|PSUM[5][8]~regout\ $ ((((!(!\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][7]~205\) # (\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][7]~205COUT1_407\))))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[5][8]~203\ = CARRY((\inst1|PSUM[5][8]~regout\ & ((!\inst1|PSUM[5][7]~205\))))
-- \inst1|PSUM[5][8]~203COUT1_408\ = CARRY((\inst1|PSUM[5][8]~regout\ & ((!\inst1|PSUM[5][7]~205COUT1_407\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	dataa => \inst1|PSUM[5][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[5][4]~211\,
	cin0 => \inst1|PSUM[5][7]~205\,
	cin1 => \inst1|PSUM[5][7]~205COUT1_407\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][8]~regout\,
	cout0 => \inst1|PSUM[5][8]~203\,
	cout1 => \inst1|PSUM[5][8]~203COUT1_408\);

-- Location: LC_X3_Y7_N9
\inst1|PSUM[5][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[5][9]~regout\ = DFFEAS((((!\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][8]~203\) # (\inst1|PSUM[5][4]~211\ & \inst1|PSUM[5][8]~203COUT1_408\) $ (\inst1|PSUM[5][9]~regout\))), \inst6|STEMP\(5), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(5),
	datad => \inst1|PSUM[5][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[5][4]~211\,
	cin0 => \inst1|PSUM[5][8]~203\,
	cin1 => \inst1|PSUM[5][8]~203COUT1_408\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[5][9]~regout\);

-- Location: LC_X3_Y9_N8
\inst1|TSUM[3][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][9]~combout\ = LCELL((((\inst1|Add3~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][9]~combout\);

-- Location: LC_X2_Y10_N1
\inst1|TSUM[3][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][4]~combout\ = LCELL((((\inst1|Add3~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][4]~combout\);

-- Location: LC_X2_Y10_N0
\inst1|TSUM[3][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][3]~combout\ = LCELL((((\inst1|Add3~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][3]~combout\);

-- Location: LC_X2_Y10_N3
\inst1|TSUM[3][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][2]~combout\ = LCELL((((\inst1|Add3~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][2]~combout\);

-- Location: LC_X2_Y10_N4
\inst1|TSUM[3][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][1]~combout\ = LCELL((((\inst1|Add3~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][1]~combout\);

-- Location: LC_X2_Y10_N2
\inst1|TSUM[3][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][0]~combout\ = LCELL((((\inst1|Add3~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][0]~combout\);

-- Location: LC_X2_Y10_N5
\inst1|Add4~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~150_combout\ = \inst1|PSUM[5][0]~regout\ $ ((\inst1|TSUM[3][0]~combout\))
-- \inst1|Add4~152\ = CARRY((\inst1|PSUM[5][0]~regout\ & (\inst1|TSUM[3][0]~combout\)))
-- \inst1|Add4~152COUT1_156\ = CARRY((\inst1|PSUM[5][0]~regout\ & (\inst1|TSUM[3][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[5][0]~regout\,
	datab => \inst1|TSUM[3][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~150_combout\,
	cout0 => \inst1|Add4~152\,
	cout1 => \inst1|Add4~152COUT1_156\);

-- Location: LC_X2_Y10_N6
\inst1|Add4~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~145_combout\ = \inst1|TSUM[3][1]~combout\ $ (\inst1|PSUM[5][1]~regout\ $ ((\inst1|Add4~152\)))
-- \inst1|Add4~147\ = CARRY((\inst1|TSUM[3][1]~combout\ & (!\inst1|PSUM[5][1]~regout\ & !\inst1|Add4~152\)) # (!\inst1|TSUM[3][1]~combout\ & ((!\inst1|Add4~152\) # (!\inst1|PSUM[5][1]~regout\))))
-- \inst1|Add4~147COUT1_157\ = CARRY((\inst1|TSUM[3][1]~combout\ & (!\inst1|PSUM[5][1]~regout\ & !\inst1|Add4~152COUT1_156\)) # (!\inst1|TSUM[3][1]~combout\ & ((!\inst1|Add4~152COUT1_156\) # (!\inst1|PSUM[5][1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][1]~combout\,
	datab => \inst1|PSUM[5][1]~regout\,
	cin0 => \inst1|Add4~152\,
	cin1 => \inst1|Add4~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~145_combout\,
	cout0 => \inst1|Add4~147\,
	cout1 => \inst1|Add4~147COUT1_157\);

-- Location: LC_X2_Y10_N7
\inst1|Add4~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~140_combout\ = \inst1|TSUM[3][2]~combout\ $ (\inst1|PSUM[5][2]~regout\ $ ((!\inst1|Add4~147\)))
-- \inst1|Add4~142\ = CARRY((\inst1|TSUM[3][2]~combout\ & ((\inst1|PSUM[5][2]~regout\) # (!\inst1|Add4~147\))) # (!\inst1|TSUM[3][2]~combout\ & (\inst1|PSUM[5][2]~regout\ & !\inst1|Add4~147\)))
-- \inst1|Add4~142COUT1_158\ = CARRY((\inst1|TSUM[3][2]~combout\ & ((\inst1|PSUM[5][2]~regout\) # (!\inst1|Add4~147COUT1_157\))) # (!\inst1|TSUM[3][2]~combout\ & (\inst1|PSUM[5][2]~regout\ & !\inst1|Add4~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][2]~combout\,
	datab => \inst1|PSUM[5][2]~regout\,
	cin0 => \inst1|Add4~147\,
	cin1 => \inst1|Add4~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~140_combout\,
	cout0 => \inst1|Add4~142\,
	cout1 => \inst1|Add4~142COUT1_158\);

-- Location: LC_X2_Y10_N8
\inst1|Add4~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~135_combout\ = \inst1|PSUM[5][3]~regout\ $ (\inst1|TSUM[3][3]~combout\ $ ((\inst1|Add4~142\)))
-- \inst1|Add4~137\ = CARRY((\inst1|PSUM[5][3]~regout\ & (!\inst1|TSUM[3][3]~combout\ & !\inst1|Add4~142\)) # (!\inst1|PSUM[5][3]~regout\ & ((!\inst1|Add4~142\) # (!\inst1|TSUM[3][3]~combout\))))
-- \inst1|Add4~137COUT1_159\ = CARRY((\inst1|PSUM[5][3]~regout\ & (!\inst1|TSUM[3][3]~combout\ & !\inst1|Add4~142COUT1_158\)) # (!\inst1|PSUM[5][3]~regout\ & ((!\inst1|Add4~142COUT1_158\) # (!\inst1|TSUM[3][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[5][3]~regout\,
	datab => \inst1|TSUM[3][3]~combout\,
	cin0 => \inst1|Add4~142\,
	cin1 => \inst1|Add4~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~135_combout\,
	cout0 => \inst1|Add4~137\,
	cout1 => \inst1|Add4~137COUT1_159\);

-- Location: LC_X2_Y10_N9
\inst1|Add4~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~130_combout\ = \inst1|PSUM[5][4]~regout\ $ (\inst1|TSUM[3][4]~combout\ $ ((!\inst1|Add4~137\)))
-- \inst1|Add4~132\ = CARRY((\inst1|PSUM[5][4]~regout\ & ((\inst1|TSUM[3][4]~combout\) # (!\inst1|Add4~137COUT1_159\))) # (!\inst1|PSUM[5][4]~regout\ & (\inst1|TSUM[3][4]~combout\ & !\inst1|Add4~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[5][4]~regout\,
	datab => \inst1|TSUM[3][4]~combout\,
	cin0 => \inst1|Add4~137\,
	cin1 => \inst1|Add4~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~130_combout\,
	cout => \inst1|Add4~132\);

-- Location: LC_X3_Y9_N3
\inst1|TSUM[3][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][8]~combout\ = LCELL((((\inst1|Add3~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][8]~combout\);

-- Location: LC_X3_Y9_N5
\inst1|TSUM[3][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][7]~combout\ = LCELL((((\inst1|Add3~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][7]~combout\);

-- Location: LC_X3_Y9_N9
\inst1|TSUM[3][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][6]~combout\ = LCELL((((\inst1|Add3~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][6]~combout\);

-- Location: LC_X3_Y9_N6
\inst1|TSUM[3][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][5]~combout\ = LCELL((((\inst1|Add3~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][5]~combout\);

-- Location: LC_X3_Y10_N0
\inst1|Add4~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~125_combout\ = \inst1|PSUM[5][5]~regout\ $ (\inst1|TSUM[3][5]~combout\ $ ((\inst1|Add4~132\)))
-- \inst1|Add4~127\ = CARRY((\inst1|PSUM[5][5]~regout\ & (!\inst1|TSUM[3][5]~combout\ & !\inst1|Add4~132\)) # (!\inst1|PSUM[5][5]~regout\ & ((!\inst1|Add4~132\) # (!\inst1|TSUM[3][5]~combout\))))
-- \inst1|Add4~127COUT1_160\ = CARRY((\inst1|PSUM[5][5]~regout\ & (!\inst1|TSUM[3][5]~combout\ & !\inst1|Add4~132\)) # (!\inst1|PSUM[5][5]~regout\ & ((!\inst1|Add4~132\) # (!\inst1|TSUM[3][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[5][5]~regout\,
	datab => \inst1|TSUM[3][5]~combout\,
	cin => \inst1|Add4~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~125_combout\,
	cout0 => \inst1|Add4~127\,
	cout1 => \inst1|Add4~127COUT1_160\);

-- Location: LC_X3_Y10_N1
\inst1|Add4~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~120_combout\ = \inst1|PSUM[5][6]~regout\ $ (\inst1|TSUM[3][6]~combout\ $ ((!(!\inst1|Add4~132\ & \inst1|Add4~127\) # (\inst1|Add4~132\ & \inst1|Add4~127COUT1_160\))))
-- \inst1|Add4~122\ = CARRY((\inst1|PSUM[5][6]~regout\ & ((\inst1|TSUM[3][6]~combout\) # (!\inst1|Add4~127\))) # (!\inst1|PSUM[5][6]~regout\ & (\inst1|TSUM[3][6]~combout\ & !\inst1|Add4~127\)))
-- \inst1|Add4~122COUT1_161\ = CARRY((\inst1|PSUM[5][6]~regout\ & ((\inst1|TSUM[3][6]~combout\) # (!\inst1|Add4~127COUT1_160\))) # (!\inst1|PSUM[5][6]~regout\ & (\inst1|TSUM[3][6]~combout\ & !\inst1|Add4~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[5][6]~regout\,
	datab => \inst1|TSUM[3][6]~combout\,
	cin => \inst1|Add4~132\,
	cin0 => \inst1|Add4~127\,
	cin1 => \inst1|Add4~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~120_combout\,
	cout0 => \inst1|Add4~122\,
	cout1 => \inst1|Add4~122COUT1_161\);

-- Location: LC_X3_Y10_N2
\inst1|Add4~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~115_combout\ = \inst1|PSUM[5][7]~regout\ $ (\inst1|TSUM[3][7]~combout\ $ (((!\inst1|Add4~132\ & \inst1|Add4~122\) # (\inst1|Add4~132\ & \inst1|Add4~122COUT1_161\))))
-- \inst1|Add4~117\ = CARRY((\inst1|PSUM[5][7]~regout\ & (!\inst1|TSUM[3][7]~combout\ & !\inst1|Add4~122\)) # (!\inst1|PSUM[5][7]~regout\ & ((!\inst1|Add4~122\) # (!\inst1|TSUM[3][7]~combout\))))
-- \inst1|Add4~117COUT1_162\ = CARRY((\inst1|PSUM[5][7]~regout\ & (!\inst1|TSUM[3][7]~combout\ & !\inst1|Add4~122COUT1_161\)) # (!\inst1|PSUM[5][7]~regout\ & ((!\inst1|Add4~122COUT1_161\) # (!\inst1|TSUM[3][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[5][7]~regout\,
	datab => \inst1|TSUM[3][7]~combout\,
	cin => \inst1|Add4~132\,
	cin0 => \inst1|Add4~122\,
	cin1 => \inst1|Add4~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~115_combout\,
	cout0 => \inst1|Add4~117\,
	cout1 => \inst1|Add4~117COUT1_162\);

-- Location: LC_X3_Y10_N3
\inst1|Add4~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~110_combout\ = \inst1|TSUM[3][8]~combout\ $ (\inst1|PSUM[5][8]~regout\ $ ((!(!\inst1|Add4~132\ & \inst1|Add4~117\) # (\inst1|Add4~132\ & \inst1|Add4~117COUT1_162\))))
-- \inst1|Add4~112\ = CARRY((\inst1|TSUM[3][8]~combout\ & ((\inst1|PSUM[5][8]~regout\) # (!\inst1|Add4~117\))) # (!\inst1|TSUM[3][8]~combout\ & (\inst1|PSUM[5][8]~regout\ & !\inst1|Add4~117\)))
-- \inst1|Add4~112COUT1_163\ = CARRY((\inst1|TSUM[3][8]~combout\ & ((\inst1|PSUM[5][8]~regout\) # (!\inst1|Add4~117COUT1_162\))) # (!\inst1|TSUM[3][8]~combout\ & (\inst1|PSUM[5][8]~regout\ & !\inst1|Add4~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][8]~combout\,
	datab => \inst1|PSUM[5][8]~regout\,
	cin => \inst1|Add4~132\,
	cin0 => \inst1|Add4~117\,
	cin1 => \inst1|Add4~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~110_combout\,
	cout0 => \inst1|Add4~112\,
	cout1 => \inst1|Add4~112COUT1_163\);

-- Location: LC_X3_Y10_N4
\inst1|Add4~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~105_combout\ = \inst1|PSUM[5][9]~regout\ $ (\inst1|TSUM[3][9]~combout\ $ (((!\inst1|Add4~132\ & \inst1|Add4~112\) # (\inst1|Add4~132\ & \inst1|Add4~112COUT1_163\))))
-- \inst1|Add4~107\ = CARRY((\inst1|PSUM[5][9]~regout\ & (!\inst1|TSUM[3][9]~combout\ & !\inst1|Add4~112COUT1_163\)) # (!\inst1|PSUM[5][9]~regout\ & ((!\inst1|Add4~112COUT1_163\) # (!\inst1|TSUM[3][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[5][9]~regout\,
	datab => \inst1|TSUM[3][9]~combout\,
	cin => \inst1|Add4~132\,
	cin0 => \inst1|Add4~112\,
	cin1 => \inst1|Add4~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~105_combout\,
	cout => \inst1|Add4~107\);

-- Location: LC_X3_Y9_N2
\inst1|TSUM[3][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][13]~combout\ = LCELL((((\inst1|Add3~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][13]~combout\);

-- Location: LC_X3_Y9_N7
\inst1|TSUM[3][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][12]~combout\ = LCELL((((\inst1|Add3~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][12]~combout\);

-- Location: LC_X3_Y9_N1
\inst1|TSUM[3][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][11]~combout\ = LCELL((((\inst1|Add3~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][11]~combout\);

-- Location: LC_X3_Y9_N4
\inst1|TSUM[3][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][10]~combout\ = LCELL((((\inst1|Add3~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][10]~combout\);

-- Location: LC_X3_Y10_N5
\inst1|Add4~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~100_combout\ = (\inst1|TSUM[3][10]~combout\ $ ((!\inst1|Add4~107\)))
-- \inst1|Add4~102\ = CARRY(((\inst1|TSUM[3][10]~combout\ & !\inst1|Add4~107\)))
-- \inst1|Add4~102COUT1_164\ = CARRY(((\inst1|TSUM[3][10]~combout\ & !\inst1|Add4~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][10]~combout\,
	cin => \inst1|Add4~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~100_combout\,
	cout0 => \inst1|Add4~102\,
	cout1 => \inst1|Add4~102COUT1_164\);

-- Location: LC_X3_Y10_N6
\inst1|Add4~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~95_combout\ = (\inst1|TSUM[3][11]~combout\ $ (((!\inst1|Add4~107\ & \inst1|Add4~102\) # (\inst1|Add4~107\ & \inst1|Add4~102COUT1_164\))))
-- \inst1|Add4~97\ = CARRY(((!\inst1|Add4~102\) # (!\inst1|TSUM[3][11]~combout\)))
-- \inst1|Add4~97COUT1_165\ = CARRY(((!\inst1|Add4~102COUT1_164\) # (!\inst1|TSUM[3][11]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][11]~combout\,
	cin => \inst1|Add4~107\,
	cin0 => \inst1|Add4~102\,
	cin1 => \inst1|Add4~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~95_combout\,
	cout0 => \inst1|Add4~97\,
	cout1 => \inst1|Add4~97COUT1_165\);

-- Location: LC_X3_Y10_N7
\inst1|Add4~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~90_combout\ = \inst1|TSUM[3][12]~combout\ $ ((((!(!\inst1|Add4~107\ & \inst1|Add4~97\) # (\inst1|Add4~107\ & \inst1|Add4~97COUT1_165\)))))
-- \inst1|Add4~92\ = CARRY((\inst1|TSUM[3][12]~combout\ & ((!\inst1|Add4~97\))))
-- \inst1|Add4~92COUT1_166\ = CARRY((\inst1|TSUM[3][12]~combout\ & ((!\inst1|Add4~97COUT1_165\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][12]~combout\,
	cin => \inst1|Add4~107\,
	cin0 => \inst1|Add4~97\,
	cin1 => \inst1|Add4~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~90_combout\,
	cout0 => \inst1|Add4~92\,
	cout1 => \inst1|Add4~92COUT1_166\);

-- Location: LC_X3_Y10_N8
\inst1|Add4~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~85_combout\ = (\inst1|TSUM[3][13]~combout\ $ (((!\inst1|Add4~107\ & \inst1|Add4~92\) # (\inst1|Add4~107\ & \inst1|Add4~92COUT1_166\))))
-- \inst1|Add4~87\ = CARRY(((!\inst1|Add4~92\) # (!\inst1|TSUM[3][13]~combout\)))
-- \inst1|Add4~87COUT1_167\ = CARRY(((!\inst1|Add4~92COUT1_166\) # (!\inst1|TSUM[3][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][13]~combout\,
	cin => \inst1|Add4~107\,
	cin0 => \inst1|Add4~92\,
	cin1 => \inst1|Add4~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~85_combout\,
	cout0 => \inst1|Add4~87\,
	cout1 => \inst1|Add4~87COUT1_167\);

-- Location: LC_X3_Y10_N9
\inst1|Add4~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~80_combout\ = (\inst1|TSUM[3][14]~combout\ $ ((!(!\inst1|Add4~107\ & \inst1|Add4~87\) # (\inst1|Add4~107\ & \inst1|Add4~87COUT1_167\))))
-- \inst1|Add4~82\ = CARRY(((\inst1|TSUM[3][14]~combout\ & !\inst1|Add4~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][14]~combout\,
	cin => \inst1|Add4~107\,
	cin0 => \inst1|Add4~87\,
	cin1 => \inst1|Add4~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~80_combout\,
	cout => \inst1|Add4~82\);

-- Location: LC_X4_Y9_N5
\inst1|TSUM[3][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][18]~combout\ = LCELL((((\inst1|Add3~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][18]~combout\);

-- Location: LC_X4_Y9_N9
\inst1|TSUM[3][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][17]~combout\ = LCELL((((\inst1|Add3~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][17]~combout\);

-- Location: LC_X4_Y9_N7
\inst1|TSUM[3][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][16]~combout\ = LCELL((((\inst1|Add3~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][16]~combout\);

-- Location: LC_X4_Y9_N6
\inst1|TSUM[3][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][15]~combout\ = LCELL((((\inst1|Add3~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][15]~combout\);

-- Location: LC_X4_Y10_N0
\inst1|Add4~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~75_combout\ = \inst1|TSUM[3][15]~combout\ $ ((((\inst1|Add4~82\))))
-- \inst1|Add4~77\ = CARRY(((!\inst1|Add4~82\)) # (!\inst1|TSUM[3][15]~combout\))
-- \inst1|Add4~77COUT1_168\ = CARRY(((!\inst1|Add4~82\)) # (!\inst1|TSUM[3][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][15]~combout\,
	cin => \inst1|Add4~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~75_combout\,
	cout0 => \inst1|Add4~77\,
	cout1 => \inst1|Add4~77COUT1_168\);

-- Location: LC_X4_Y10_N1
\inst1|Add4~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~70_combout\ = \inst1|TSUM[3][16]~combout\ $ ((((!(!\inst1|Add4~82\ & \inst1|Add4~77\) # (\inst1|Add4~82\ & \inst1|Add4~77COUT1_168\)))))
-- \inst1|Add4~72\ = CARRY((\inst1|TSUM[3][16]~combout\ & ((!\inst1|Add4~77\))))
-- \inst1|Add4~72COUT1_169\ = CARRY((\inst1|TSUM[3][16]~combout\ & ((!\inst1|Add4~77COUT1_168\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][16]~combout\,
	cin => \inst1|Add4~82\,
	cin0 => \inst1|Add4~77\,
	cin1 => \inst1|Add4~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~70_combout\,
	cout0 => \inst1|Add4~72\,
	cout1 => \inst1|Add4~72COUT1_169\);

-- Location: LC_X4_Y10_N2
\inst1|Add4~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~65_combout\ = (\inst1|TSUM[3][17]~combout\ $ (((!\inst1|Add4~82\ & \inst1|Add4~72\) # (\inst1|Add4~82\ & \inst1|Add4~72COUT1_169\))))
-- \inst1|Add4~67\ = CARRY(((!\inst1|Add4~72\) # (!\inst1|TSUM[3][17]~combout\)))
-- \inst1|Add4~67COUT1_170\ = CARRY(((!\inst1|Add4~72COUT1_169\) # (!\inst1|TSUM[3][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][17]~combout\,
	cin => \inst1|Add4~82\,
	cin0 => \inst1|Add4~72\,
	cin1 => \inst1|Add4~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~65_combout\,
	cout0 => \inst1|Add4~67\,
	cout1 => \inst1|Add4~67COUT1_170\);

-- Location: LC_X4_Y10_N3
\inst1|Add4~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~60_combout\ = (\inst1|TSUM[3][18]~combout\ $ ((!(!\inst1|Add4~82\ & \inst1|Add4~67\) # (\inst1|Add4~82\ & \inst1|Add4~67COUT1_170\))))
-- \inst1|Add4~62\ = CARRY(((\inst1|TSUM[3][18]~combout\ & !\inst1|Add4~67\)))
-- \inst1|Add4~62COUT1_171\ = CARRY(((\inst1|TSUM[3][18]~combout\ & !\inst1|Add4~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][18]~combout\,
	cin => \inst1|Add4~82\,
	cin0 => \inst1|Add4~67\,
	cin1 => \inst1|Add4~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~60_combout\,
	cout0 => \inst1|Add4~62\,
	cout1 => \inst1|Add4~62COUT1_171\);

-- Location: LC_X4_Y10_N4
\inst1|Add4~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~55_combout\ = (\inst1|TSUM[3][19]~combout\ $ (((!\inst1|Add4~82\ & \inst1|Add4~62\) # (\inst1|Add4~82\ & \inst1|Add4~62COUT1_171\))))
-- \inst1|Add4~57\ = CARRY(((!\inst1|Add4~62COUT1_171\) # (!\inst1|TSUM[3][19]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][19]~combout\,
	cin => \inst1|Add4~82\,
	cin0 => \inst1|Add4~62\,
	cin1 => \inst1|Add4~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~55_combout\,
	cout => \inst1|Add4~57\);

-- Location: LC_X4_Y9_N0
\inst1|TSUM[3][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][23]~combout\ = LCELL((((\inst1|Add3~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][23]~combout\);

-- Location: LC_X4_Y9_N3
\inst1|TSUM[3][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][22]~combout\ = LCELL((((\inst1|Add3~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][22]~combout\);

-- Location: LC_X4_Y9_N1
\inst1|TSUM[3][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][21]~combout\ = LCELL((((\inst1|Add3~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][21]~combout\);

-- Location: LC_X4_Y9_N8
\inst1|TSUM[3][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][20]~combout\ = LCELL((((\inst1|Add3~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][20]~combout\);

-- Location: LC_X4_Y10_N5
\inst1|Add4~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~50_combout\ = (\inst1|TSUM[3][20]~combout\ $ ((!\inst1|Add4~57\)))
-- \inst1|Add4~52\ = CARRY(((\inst1|TSUM[3][20]~combout\ & !\inst1|Add4~57\)))
-- \inst1|Add4~52COUT1_172\ = CARRY(((\inst1|TSUM[3][20]~combout\ & !\inst1|Add4~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][20]~combout\,
	cin => \inst1|Add4~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~50_combout\,
	cout0 => \inst1|Add4~52\,
	cout1 => \inst1|Add4~52COUT1_172\);

-- Location: LC_X4_Y10_N6
\inst1|Add4~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~45_combout\ = (\inst1|TSUM[3][21]~combout\ $ (((!\inst1|Add4~57\ & \inst1|Add4~52\) # (\inst1|Add4~57\ & \inst1|Add4~52COUT1_172\))))
-- \inst1|Add4~47\ = CARRY(((!\inst1|Add4~52\) # (!\inst1|TSUM[3][21]~combout\)))
-- \inst1|Add4~47COUT1_173\ = CARRY(((!\inst1|Add4~52COUT1_172\) # (!\inst1|TSUM[3][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][21]~combout\,
	cin => \inst1|Add4~57\,
	cin0 => \inst1|Add4~52\,
	cin1 => \inst1|Add4~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~45_combout\,
	cout0 => \inst1|Add4~47\,
	cout1 => \inst1|Add4~47COUT1_173\);

-- Location: LC_X4_Y10_N7
\inst1|Add4~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~40_combout\ = (\inst1|TSUM[3][22]~combout\ $ ((!(!\inst1|Add4~57\ & \inst1|Add4~47\) # (\inst1|Add4~57\ & \inst1|Add4~47COUT1_173\))))
-- \inst1|Add4~42\ = CARRY(((\inst1|TSUM[3][22]~combout\ & !\inst1|Add4~47\)))
-- \inst1|Add4~42COUT1_174\ = CARRY(((\inst1|TSUM[3][22]~combout\ & !\inst1|Add4~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][22]~combout\,
	cin => \inst1|Add4~57\,
	cin0 => \inst1|Add4~47\,
	cin1 => \inst1|Add4~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~40_combout\,
	cout0 => \inst1|Add4~42\,
	cout1 => \inst1|Add4~42COUT1_174\);

-- Location: LC_X4_Y10_N8
\inst1|Add4~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~35_combout\ = \inst1|TSUM[3][23]~combout\ $ (((((!\inst1|Add4~57\ & \inst1|Add4~42\) # (\inst1|Add4~57\ & \inst1|Add4~42COUT1_174\)))))
-- \inst1|Add4~37\ = CARRY(((!\inst1|Add4~42\)) # (!\inst1|TSUM[3][23]~combout\))
-- \inst1|Add4~37COUT1_175\ = CARRY(((!\inst1|Add4~42COUT1_174\)) # (!\inst1|TSUM[3][23]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][23]~combout\,
	cin => \inst1|Add4~57\,
	cin0 => \inst1|Add4~42\,
	cin1 => \inst1|Add4~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~35_combout\,
	cout0 => \inst1|Add4~37\,
	cout1 => \inst1|Add4~37COUT1_175\);

-- Location: LC_X4_Y10_N9
\inst1|Add4~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~30_combout\ = (\inst1|TSUM[3][24]~combout\ $ ((!(!\inst1|Add4~57\ & \inst1|Add4~37\) # (\inst1|Add4~57\ & \inst1|Add4~37COUT1_175\))))
-- \inst1|Add4~32\ = CARRY(((\inst1|TSUM[3][24]~combout\ & !\inst1|Add4~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][24]~combout\,
	cin => \inst1|Add4~57\,
	cin0 => \inst1|Add4~37\,
	cin1 => \inst1|Add4~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~30_combout\,
	cout => \inst1|Add4~32\);

-- Location: LC_X5_Y10_N6
\inst1|TSUM[3][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][28]~combout\ = LCELL((((\inst1|Add3~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add3~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][28]~combout\);

-- Location: LC_X5_Y8_N7
\inst1|TSUM[3][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][27]~combout\ = LCELL((((\inst1|Add3~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][27]~combout\);

-- Location: LC_X5_Y8_N9
\inst1|TSUM[3][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][26]~combout\ = LCELL((((\inst1|Add3~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][26]~combout\);

-- Location: LC_X5_Y10_N7
\inst1|TSUM[3][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[3][25]~combout\ = LCELL((((\inst1|Add3~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add3~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[3][25]~combout\);

-- Location: LC_X5_Y10_N0
\inst1|Add4~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~25_combout\ = (\inst1|TSUM[3][25]~combout\ $ ((\inst1|Add4~32\)))
-- \inst1|Add4~27\ = CARRY(((!\inst1|Add4~32\) # (!\inst1|TSUM[3][25]~combout\)))
-- \inst1|Add4~27COUT1_176\ = CARRY(((!\inst1|Add4~32\) # (!\inst1|TSUM[3][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][25]~combout\,
	cin => \inst1|Add4~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~25_combout\,
	cout0 => \inst1|Add4~27\,
	cout1 => \inst1|Add4~27COUT1_176\);

-- Location: LC_X5_Y10_N1
\inst1|Add4~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~20_combout\ = \inst1|TSUM[3][26]~combout\ $ ((((!(!\inst1|Add4~32\ & \inst1|Add4~27\) # (\inst1|Add4~32\ & \inst1|Add4~27COUT1_176\)))))
-- \inst1|Add4~22\ = CARRY((\inst1|TSUM[3][26]~combout\ & ((!\inst1|Add4~27\))))
-- \inst1|Add4~22COUT1_177\ = CARRY((\inst1|TSUM[3][26]~combout\ & ((!\inst1|Add4~27COUT1_176\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][26]~combout\,
	cin => \inst1|Add4~32\,
	cin0 => \inst1|Add4~27\,
	cin1 => \inst1|Add4~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~20_combout\,
	cout0 => \inst1|Add4~22\,
	cout1 => \inst1|Add4~22COUT1_177\);

-- Location: LC_X5_Y10_N2
\inst1|Add4~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~15_combout\ = (\inst1|TSUM[3][27]~combout\ $ (((!\inst1|Add4~32\ & \inst1|Add4~22\) # (\inst1|Add4~32\ & \inst1|Add4~22COUT1_177\))))
-- \inst1|Add4~17\ = CARRY(((!\inst1|Add4~22\) # (!\inst1|TSUM[3][27]~combout\)))
-- \inst1|Add4~17COUT1_178\ = CARRY(((!\inst1|Add4~22COUT1_177\) # (!\inst1|TSUM[3][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][27]~combout\,
	cin => \inst1|Add4~32\,
	cin0 => \inst1|Add4~22\,
	cin1 => \inst1|Add4~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~15_combout\,
	cout0 => \inst1|Add4~17\,
	cout1 => \inst1|Add4~17COUT1_178\);

-- Location: LC_X5_Y10_N3
\inst1|Add4~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~10_combout\ = \inst1|TSUM[3][28]~combout\ $ ((((!(!\inst1|Add4~32\ & \inst1|Add4~17\) # (\inst1|Add4~32\ & \inst1|Add4~17COUT1_178\)))))
-- \inst1|Add4~12\ = CARRY((\inst1|TSUM[3][28]~combout\ & ((!\inst1|Add4~17\))))
-- \inst1|Add4~12COUT1_179\ = CARRY((\inst1|TSUM[3][28]~combout\ & ((!\inst1|Add4~17COUT1_178\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[3][28]~combout\,
	cin => \inst1|Add4~32\,
	cin0 => \inst1|Add4~17\,
	cin1 => \inst1|Add4~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~10_combout\,
	cout0 => \inst1|Add4~12\,
	cout1 => \inst1|Add4~12COUT1_179\);

-- Location: LC_X5_Y10_N4
\inst1|Add4~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~5_combout\ = (\inst1|TSUM[3][29]~combout\ $ (((!\inst1|Add4~32\ & \inst1|Add4~12\) # (\inst1|Add4~32\ & \inst1|Add4~12COUT1_179\))))
-- \inst1|Add4~7\ = CARRY(((!\inst1|Add4~12COUT1_179\) # (!\inst1|TSUM[3][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[3][29]~combout\,
	cin => \inst1|Add4~32\,
	cin0 => \inst1|Add4~12\,
	cin1 => \inst1|Add4~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~5_combout\,
	cout => \inst1|Add4~7\);

-- Location: LC_X5_Y10_N5
\inst1|Add4~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add4~0_combout\ = ((\inst1|Add4~7\ $ (!\inst1|TSUM[3][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[3][30]~combout\,
	cin => \inst1|Add4~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add4~0_combout\);

-- Location: LC_X9_Y10_N7
\inst1|TSUM[4][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][30]~combout\ = LCELL((((\inst1|Add4~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][30]~combout\);

-- Location: LC_X5_Y10_N8
\inst1|TSUM[4][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][29]~combout\ = LCELL((((\inst1|Add4~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][29]~combout\);

-- Location: LC_X8_Y10_N7
\inst1|TSUM[4][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][24]~combout\ = LCELL((((\inst1|Add4~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][24]~combout\);

-- Location: LC_X9_Y10_N8
\inst1|TSUM[4][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][19]~combout\ = LCELL((((\inst1|Add4~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][19]~combout\);

-- Location: LC_X6_Y10_N3
\inst1|TSUM[4][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][14]~combout\ = LCELL((((\inst1|Add4~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][14]~combout\);

-- Location: LC_X5_Y9_N0
\inst1|PSUM[6][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][0]~regout\ = DFFEAS(((!\inst1|PSUM[6][0]~regout\)), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][0]~199\ = CARRY(((\inst1|PSUM[6][0]~regout\)))
-- \inst1|PSUM[6][0]~199COUT1_393\ = CARRY(((\inst1|PSUM[6][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	datab => \inst1|PSUM[6][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][0]~regout\,
	cout0 => \inst1|PSUM[6][0]~199\,
	cout1 => \inst1|PSUM[6][0]~199COUT1_393\);

-- Location: LC_X5_Y9_N1
\inst1|PSUM[6][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][1]~regout\ = DFFEAS((\inst1|PSUM[6][1]~regout\ $ ((\inst1|PSUM[6][0]~199\))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][1]~197\ = CARRY(((!\inst1|PSUM[6][0]~199\) # (!\inst1|PSUM[6][1]~regout\)))
-- \inst1|PSUM[6][1]~197COUT1_394\ = CARRY(((!\inst1|PSUM[6][0]~199COUT1_393\) # (!\inst1|PSUM[6][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	datab => \inst1|PSUM[6][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[6][0]~199\,
	cin1 => \inst1|PSUM[6][0]~199COUT1_393\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][1]~regout\,
	cout0 => \inst1|PSUM[6][1]~197\,
	cout1 => \inst1|PSUM[6][1]~197COUT1_394\);

-- Location: LC_X5_Y9_N2
\inst1|PSUM[6][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][2]~regout\ = DFFEAS((\inst1|PSUM[6][2]~regout\ $ ((!\inst1|PSUM[6][1]~197\))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][2]~195\ = CARRY(((\inst1|PSUM[6][2]~regout\ & !\inst1|PSUM[6][1]~197\)))
-- \inst1|PSUM[6][2]~195COUT1_395\ = CARRY(((\inst1|PSUM[6][2]~regout\ & !\inst1|PSUM[6][1]~197COUT1_394\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	datab => \inst1|PSUM[6][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[6][1]~197\,
	cin1 => \inst1|PSUM[6][1]~197COUT1_394\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][2]~regout\,
	cout0 => \inst1|PSUM[6][2]~195\,
	cout1 => \inst1|PSUM[6][2]~195COUT1_395\);

-- Location: LC_X5_Y9_N3
\inst1|PSUM[6][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][3]~regout\ = DFFEAS(\inst1|PSUM[6][3]~regout\ $ ((((\inst1|PSUM[6][2]~195\)))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][3]~193\ = CARRY(((!\inst1|PSUM[6][2]~195\)) # (!\inst1|PSUM[6][3]~regout\))
-- \inst1|PSUM[6][3]~193COUT1_396\ = CARRY(((!\inst1|PSUM[6][2]~195COUT1_395\)) # (!\inst1|PSUM[6][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	dataa => \inst1|PSUM[6][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[6][2]~195\,
	cin1 => \inst1|PSUM[6][2]~195COUT1_395\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][3]~regout\,
	cout0 => \inst1|PSUM[6][3]~193\,
	cout1 => \inst1|PSUM[6][3]~193COUT1_396\);

-- Location: LC_X5_Y9_N4
\inst1|PSUM[6][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][4]~regout\ = DFFEAS(\inst1|PSUM[6][4]~regout\ $ ((((!\inst1|PSUM[6][3]~193\)))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][4]~191\ = CARRY((\inst1|PSUM[6][4]~regout\ & ((!\inst1|PSUM[6][3]~193COUT1_396\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	dataa => \inst1|PSUM[6][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[6][3]~193\,
	cin1 => \inst1|PSUM[6][3]~193COUT1_396\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][4]~regout\,
	cout => \inst1|PSUM[6][4]~191\);

-- Location: LC_X5_Y9_N5
\inst1|PSUM[6][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][5]~regout\ = DFFEAS(\inst1|PSUM[6][5]~regout\ $ ((((\inst1|PSUM[6][4]~191\)))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][5]~189\ = CARRY(((!\inst1|PSUM[6][4]~191\)) # (!\inst1|PSUM[6][5]~regout\))
-- \inst1|PSUM[6][5]~189COUT1_397\ = CARRY(((!\inst1|PSUM[6][4]~191\)) # (!\inst1|PSUM[6][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	dataa => \inst1|PSUM[6][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[6][4]~191\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][5]~regout\,
	cout0 => \inst1|PSUM[6][5]~189\,
	cout1 => \inst1|PSUM[6][5]~189COUT1_397\);

-- Location: LC_X5_Y9_N6
\inst1|PSUM[6][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][6]~regout\ = DFFEAS(\inst1|PSUM[6][6]~regout\ $ ((((!(!\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][5]~189\) # (\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][5]~189COUT1_397\))))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][6]~187\ = CARRY((\inst1|PSUM[6][6]~regout\ & ((!\inst1|PSUM[6][5]~189\))))
-- \inst1|PSUM[6][6]~187COUT1_398\ = CARRY((\inst1|PSUM[6][6]~regout\ & ((!\inst1|PSUM[6][5]~189COUT1_397\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	dataa => \inst1|PSUM[6][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[6][4]~191\,
	cin0 => \inst1|PSUM[6][5]~189\,
	cin1 => \inst1|PSUM[6][5]~189COUT1_397\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][6]~regout\,
	cout0 => \inst1|PSUM[6][6]~187\,
	cout1 => \inst1|PSUM[6][6]~187COUT1_398\);

-- Location: LC_X5_Y9_N7
\inst1|PSUM[6][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][7]~regout\ = DFFEAS((\inst1|PSUM[6][7]~regout\ $ (((!\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][6]~187\) # (\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][6]~187COUT1_398\)))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][7]~185\ = CARRY(((!\inst1|PSUM[6][6]~187\) # (!\inst1|PSUM[6][7]~regout\)))
-- \inst1|PSUM[6][7]~185COUT1_399\ = CARRY(((!\inst1|PSUM[6][6]~187COUT1_398\) # (!\inst1|PSUM[6][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	datab => \inst1|PSUM[6][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[6][4]~191\,
	cin0 => \inst1|PSUM[6][6]~187\,
	cin1 => \inst1|PSUM[6][6]~187COUT1_398\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][7]~regout\,
	cout0 => \inst1|PSUM[6][7]~185\,
	cout1 => \inst1|PSUM[6][7]~185COUT1_399\);

-- Location: LC_X5_Y9_N8
\inst1|PSUM[6][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][8]~regout\ = DFFEAS(\inst1|PSUM[6][8]~regout\ $ ((((!(!\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][7]~185\) # (\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][7]~185COUT1_399\))))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[6][8]~183\ = CARRY((\inst1|PSUM[6][8]~regout\ & ((!\inst1|PSUM[6][7]~185\))))
-- \inst1|PSUM[6][8]~183COUT1_400\ = CARRY((\inst1|PSUM[6][8]~regout\ & ((!\inst1|PSUM[6][7]~185COUT1_399\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	dataa => \inst1|PSUM[6][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[6][4]~191\,
	cin0 => \inst1|PSUM[6][7]~185\,
	cin1 => \inst1|PSUM[6][7]~185COUT1_399\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][8]~regout\,
	cout0 => \inst1|PSUM[6][8]~183\,
	cout1 => \inst1|PSUM[6][8]~183COUT1_400\);

-- Location: LC_X5_Y9_N9
\inst1|PSUM[6][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[6][9]~regout\ = DFFEAS((((!\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][8]~183\) # (\inst1|PSUM[6][4]~191\ & \inst1|PSUM[6][8]~183COUT1_400\) $ (\inst1|PSUM[6][9]~regout\))), \inst6|STEMP\(6), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(6),
	datad => \inst1|PSUM[6][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[6][4]~191\,
	cin0 => \inst1|PSUM[6][8]~183\,
	cin1 => \inst1|PSUM[6][8]~183COUT1_400\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[6][9]~regout\);

-- Location: LC_X6_Y10_N4
\inst1|TSUM[4][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][9]~combout\ = LCELL((((\inst1|Add4~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][9]~combout\);

-- Location: LC_X6_Y9_N0
\inst1|TSUM[4][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][4]~combout\ = LCELL((((\inst1|Add4~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][4]~combout\);

-- Location: LC_X6_Y9_N4
\inst1|TSUM[4][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][3]~combout\ = LCELL((((\inst1|Add4~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][3]~combout\);

-- Location: LC_X2_Y9_N3
\inst1|TSUM[4][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][2]~combout\ = LCELL((((\inst1|Add4~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][2]~combout\);

-- Location: LC_X2_Y9_N6
\inst1|TSUM[4][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][1]~combout\ = LCELL((((\inst1|Add4~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][1]~combout\);

-- Location: LC_X2_Y9_N8
\inst1|TSUM[4][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][0]~combout\ = LCELL((((\inst1|Add4~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][0]~combout\);

-- Location: LC_X6_Y9_N5
\inst1|Add5~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~150_combout\ = \inst1|PSUM[6][0]~regout\ $ ((\inst1|TSUM[4][0]~combout\))
-- \inst1|Add5~152\ = CARRY((\inst1|PSUM[6][0]~regout\ & (\inst1|TSUM[4][0]~combout\)))
-- \inst1|Add5~152COUT1_156\ = CARRY((\inst1|PSUM[6][0]~regout\ & (\inst1|TSUM[4][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[6][0]~regout\,
	datab => \inst1|TSUM[4][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~150_combout\,
	cout0 => \inst1|Add5~152\,
	cout1 => \inst1|Add5~152COUT1_156\);

-- Location: LC_X6_Y9_N6
\inst1|Add5~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~145_combout\ = \inst1|PSUM[6][1]~regout\ $ (\inst1|TSUM[4][1]~combout\ $ ((\inst1|Add5~152\)))
-- \inst1|Add5~147\ = CARRY((\inst1|PSUM[6][1]~regout\ & (!\inst1|TSUM[4][1]~combout\ & !\inst1|Add5~152\)) # (!\inst1|PSUM[6][1]~regout\ & ((!\inst1|Add5~152\) # (!\inst1|TSUM[4][1]~combout\))))
-- \inst1|Add5~147COUT1_157\ = CARRY((\inst1|PSUM[6][1]~regout\ & (!\inst1|TSUM[4][1]~combout\ & !\inst1|Add5~152COUT1_156\)) # (!\inst1|PSUM[6][1]~regout\ & ((!\inst1|Add5~152COUT1_156\) # (!\inst1|TSUM[4][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[6][1]~regout\,
	datab => \inst1|TSUM[4][1]~combout\,
	cin0 => \inst1|Add5~152\,
	cin1 => \inst1|Add5~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~145_combout\,
	cout0 => \inst1|Add5~147\,
	cout1 => \inst1|Add5~147COUT1_157\);

-- Location: LC_X6_Y9_N7
\inst1|Add5~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~140_combout\ = \inst1|TSUM[4][2]~combout\ $ (\inst1|PSUM[6][2]~regout\ $ ((!\inst1|Add5~147\)))
-- \inst1|Add5~142\ = CARRY((\inst1|TSUM[4][2]~combout\ & ((\inst1|PSUM[6][2]~regout\) # (!\inst1|Add5~147\))) # (!\inst1|TSUM[4][2]~combout\ & (\inst1|PSUM[6][2]~regout\ & !\inst1|Add5~147\)))
-- \inst1|Add5~142COUT1_158\ = CARRY((\inst1|TSUM[4][2]~combout\ & ((\inst1|PSUM[6][2]~regout\) # (!\inst1|Add5~147COUT1_157\))) # (!\inst1|TSUM[4][2]~combout\ & (\inst1|PSUM[6][2]~regout\ & !\inst1|Add5~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][2]~combout\,
	datab => \inst1|PSUM[6][2]~regout\,
	cin0 => \inst1|Add5~147\,
	cin1 => \inst1|Add5~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~140_combout\,
	cout0 => \inst1|Add5~142\,
	cout1 => \inst1|Add5~142COUT1_158\);

-- Location: LC_X6_Y9_N8
\inst1|Add5~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~135_combout\ = \inst1|TSUM[4][3]~combout\ $ (\inst1|PSUM[6][3]~regout\ $ ((\inst1|Add5~142\)))
-- \inst1|Add5~137\ = CARRY((\inst1|TSUM[4][3]~combout\ & (!\inst1|PSUM[6][3]~regout\ & !\inst1|Add5~142\)) # (!\inst1|TSUM[4][3]~combout\ & ((!\inst1|Add5~142\) # (!\inst1|PSUM[6][3]~regout\))))
-- \inst1|Add5~137COUT1_159\ = CARRY((\inst1|TSUM[4][3]~combout\ & (!\inst1|PSUM[6][3]~regout\ & !\inst1|Add5~142COUT1_158\)) # (!\inst1|TSUM[4][3]~combout\ & ((!\inst1|Add5~142COUT1_158\) # (!\inst1|PSUM[6][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][3]~combout\,
	datab => \inst1|PSUM[6][3]~regout\,
	cin0 => \inst1|Add5~142\,
	cin1 => \inst1|Add5~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~135_combout\,
	cout0 => \inst1|Add5~137\,
	cout1 => \inst1|Add5~137COUT1_159\);

-- Location: LC_X6_Y9_N9
\inst1|Add5~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~130_combout\ = \inst1|PSUM[6][4]~regout\ $ (\inst1|TSUM[4][4]~combout\ $ ((!\inst1|Add5~137\)))
-- \inst1|Add5~132\ = CARRY((\inst1|PSUM[6][4]~regout\ & ((\inst1|TSUM[4][4]~combout\) # (!\inst1|Add5~137COUT1_159\))) # (!\inst1|PSUM[6][4]~regout\ & (\inst1|TSUM[4][4]~combout\ & !\inst1|Add5~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[6][4]~regout\,
	datab => \inst1|TSUM[4][4]~combout\,
	cin0 => \inst1|Add5~137\,
	cin1 => \inst1|Add5~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~130_combout\,
	cout => \inst1|Add5~132\);

-- Location: LC_X6_Y10_N9
\inst1|TSUM[4][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][8]~combout\ = LCELL((((\inst1|Add4~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][8]~combout\);

-- Location: LC_X6_Y10_N6
\inst1|TSUM[4][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][7]~combout\ = LCELL((((\inst1|Add4~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][7]~combout\);

-- Location: LC_X6_Y10_N8
\inst1|TSUM[4][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][6]~combout\ = LCELL((((\inst1|Add4~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][6]~combout\);

-- Location: LC_X6_Y10_N5
\inst1|TSUM[4][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][5]~combout\ = LCELL((((\inst1|Add4~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][5]~combout\);

-- Location: LC_X7_Y9_N0
\inst1|Add5~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~125_combout\ = \inst1|PSUM[6][5]~regout\ $ (\inst1|TSUM[4][5]~combout\ $ ((\inst1|Add5~132\)))
-- \inst1|Add5~127\ = CARRY((\inst1|PSUM[6][5]~regout\ & (!\inst1|TSUM[4][5]~combout\ & !\inst1|Add5~132\)) # (!\inst1|PSUM[6][5]~regout\ & ((!\inst1|Add5~132\) # (!\inst1|TSUM[4][5]~combout\))))
-- \inst1|Add5~127COUT1_160\ = CARRY((\inst1|PSUM[6][5]~regout\ & (!\inst1|TSUM[4][5]~combout\ & !\inst1|Add5~132\)) # (!\inst1|PSUM[6][5]~regout\ & ((!\inst1|Add5~132\) # (!\inst1|TSUM[4][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[6][5]~regout\,
	datab => \inst1|TSUM[4][5]~combout\,
	cin => \inst1|Add5~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~125_combout\,
	cout0 => \inst1|Add5~127\,
	cout1 => \inst1|Add5~127COUT1_160\);

-- Location: LC_X7_Y9_N1
\inst1|Add5~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~120_combout\ = \inst1|TSUM[4][6]~combout\ $ (\inst1|PSUM[6][6]~regout\ $ ((!(!\inst1|Add5~132\ & \inst1|Add5~127\) # (\inst1|Add5~132\ & \inst1|Add5~127COUT1_160\))))
-- \inst1|Add5~122\ = CARRY((\inst1|TSUM[4][6]~combout\ & ((\inst1|PSUM[6][6]~regout\) # (!\inst1|Add5~127\))) # (!\inst1|TSUM[4][6]~combout\ & (\inst1|PSUM[6][6]~regout\ & !\inst1|Add5~127\)))
-- \inst1|Add5~122COUT1_161\ = CARRY((\inst1|TSUM[4][6]~combout\ & ((\inst1|PSUM[6][6]~regout\) # (!\inst1|Add5~127COUT1_160\))) # (!\inst1|TSUM[4][6]~combout\ & (\inst1|PSUM[6][6]~regout\ & !\inst1|Add5~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][6]~combout\,
	datab => \inst1|PSUM[6][6]~regout\,
	cin => \inst1|Add5~132\,
	cin0 => \inst1|Add5~127\,
	cin1 => \inst1|Add5~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~120_combout\,
	cout0 => \inst1|Add5~122\,
	cout1 => \inst1|Add5~122COUT1_161\);

-- Location: LC_X7_Y9_N2
\inst1|Add5~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~115_combout\ = \inst1|PSUM[6][7]~regout\ $ (\inst1|TSUM[4][7]~combout\ $ (((!\inst1|Add5~132\ & \inst1|Add5~122\) # (\inst1|Add5~132\ & \inst1|Add5~122COUT1_161\))))
-- \inst1|Add5~117\ = CARRY((\inst1|PSUM[6][7]~regout\ & (!\inst1|TSUM[4][7]~combout\ & !\inst1|Add5~122\)) # (!\inst1|PSUM[6][7]~regout\ & ((!\inst1|Add5~122\) # (!\inst1|TSUM[4][7]~combout\))))
-- \inst1|Add5~117COUT1_162\ = CARRY((\inst1|PSUM[6][7]~regout\ & (!\inst1|TSUM[4][7]~combout\ & !\inst1|Add5~122COUT1_161\)) # (!\inst1|PSUM[6][7]~regout\ & ((!\inst1|Add5~122COUT1_161\) # (!\inst1|TSUM[4][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[6][7]~regout\,
	datab => \inst1|TSUM[4][7]~combout\,
	cin => \inst1|Add5~132\,
	cin0 => \inst1|Add5~122\,
	cin1 => \inst1|Add5~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~115_combout\,
	cout0 => \inst1|Add5~117\,
	cout1 => \inst1|Add5~117COUT1_162\);

-- Location: LC_X7_Y9_N3
\inst1|Add5~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~110_combout\ = \inst1|TSUM[4][8]~combout\ $ (\inst1|PSUM[6][8]~regout\ $ ((!(!\inst1|Add5~132\ & \inst1|Add5~117\) # (\inst1|Add5~132\ & \inst1|Add5~117COUT1_162\))))
-- \inst1|Add5~112\ = CARRY((\inst1|TSUM[4][8]~combout\ & ((\inst1|PSUM[6][8]~regout\) # (!\inst1|Add5~117\))) # (!\inst1|TSUM[4][8]~combout\ & (\inst1|PSUM[6][8]~regout\ & !\inst1|Add5~117\)))
-- \inst1|Add5~112COUT1_163\ = CARRY((\inst1|TSUM[4][8]~combout\ & ((\inst1|PSUM[6][8]~regout\) # (!\inst1|Add5~117COUT1_162\))) # (!\inst1|TSUM[4][8]~combout\ & (\inst1|PSUM[6][8]~regout\ & !\inst1|Add5~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][8]~combout\,
	datab => \inst1|PSUM[6][8]~regout\,
	cin => \inst1|Add5~132\,
	cin0 => \inst1|Add5~117\,
	cin1 => \inst1|Add5~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~110_combout\,
	cout0 => \inst1|Add5~112\,
	cout1 => \inst1|Add5~112COUT1_163\);

-- Location: LC_X7_Y9_N4
\inst1|Add5~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~105_combout\ = \inst1|PSUM[6][9]~regout\ $ (\inst1|TSUM[4][9]~combout\ $ (((!\inst1|Add5~132\ & \inst1|Add5~112\) # (\inst1|Add5~132\ & \inst1|Add5~112COUT1_163\))))
-- \inst1|Add5~107\ = CARRY((\inst1|PSUM[6][9]~regout\ & (!\inst1|TSUM[4][9]~combout\ & !\inst1|Add5~112COUT1_163\)) # (!\inst1|PSUM[6][9]~regout\ & ((!\inst1|Add5~112COUT1_163\) # (!\inst1|TSUM[4][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[6][9]~regout\,
	datab => \inst1|TSUM[4][9]~combout\,
	cin => \inst1|Add5~132\,
	cin0 => \inst1|Add5~112\,
	cin1 => \inst1|Add5~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~105_combout\,
	cout => \inst1|Add5~107\);

-- Location: LC_X6_Y10_N7
\inst1|TSUM[4][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][13]~combout\ = LCELL((((\inst1|Add4~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][13]~combout\);

-- Location: LC_X6_Y10_N2
\inst1|TSUM[4][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][12]~combout\ = LCELL((((\inst1|Add4~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][12]~combout\);

-- Location: LC_X6_Y10_N0
\inst1|TSUM[4][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][11]~combout\ = LCELL((((\inst1|Add4~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][11]~combout\);

-- Location: LC_X6_Y10_N1
\inst1|TSUM[4][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][10]~combout\ = LCELL((((\inst1|Add4~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][10]~combout\);

-- Location: LC_X7_Y9_N5
\inst1|Add5~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~100_combout\ = (\inst1|TSUM[4][10]~combout\ $ ((!\inst1|Add5~107\)))
-- \inst1|Add5~102\ = CARRY(((\inst1|TSUM[4][10]~combout\ & !\inst1|Add5~107\)))
-- \inst1|Add5~102COUT1_164\ = CARRY(((\inst1|TSUM[4][10]~combout\ & !\inst1|Add5~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][10]~combout\,
	cin => \inst1|Add5~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~100_combout\,
	cout0 => \inst1|Add5~102\,
	cout1 => \inst1|Add5~102COUT1_164\);

-- Location: LC_X7_Y9_N6
\inst1|Add5~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~95_combout\ = \inst1|TSUM[4][11]~combout\ $ (((((!\inst1|Add5~107\ & \inst1|Add5~102\) # (\inst1|Add5~107\ & \inst1|Add5~102COUT1_164\)))))
-- \inst1|Add5~97\ = CARRY(((!\inst1|Add5~102\)) # (!\inst1|TSUM[4][11]~combout\))
-- \inst1|Add5~97COUT1_165\ = CARRY(((!\inst1|Add5~102COUT1_164\)) # (!\inst1|TSUM[4][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][11]~combout\,
	cin => \inst1|Add5~107\,
	cin0 => \inst1|Add5~102\,
	cin1 => \inst1|Add5~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~95_combout\,
	cout0 => \inst1|Add5~97\,
	cout1 => \inst1|Add5~97COUT1_165\);

-- Location: LC_X7_Y9_N7
\inst1|Add5~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~90_combout\ = (\inst1|TSUM[4][12]~combout\ $ ((!(!\inst1|Add5~107\ & \inst1|Add5~97\) # (\inst1|Add5~107\ & \inst1|Add5~97COUT1_165\))))
-- \inst1|Add5~92\ = CARRY(((\inst1|TSUM[4][12]~combout\ & !\inst1|Add5~97\)))
-- \inst1|Add5~92COUT1_166\ = CARRY(((\inst1|TSUM[4][12]~combout\ & !\inst1|Add5~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][12]~combout\,
	cin => \inst1|Add5~107\,
	cin0 => \inst1|Add5~97\,
	cin1 => \inst1|Add5~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~90_combout\,
	cout0 => \inst1|Add5~92\,
	cout1 => \inst1|Add5~92COUT1_166\);

-- Location: LC_X7_Y9_N8
\inst1|Add5~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~85_combout\ = (\inst1|TSUM[4][13]~combout\ $ (((!\inst1|Add5~107\ & \inst1|Add5~92\) # (\inst1|Add5~107\ & \inst1|Add5~92COUT1_166\))))
-- \inst1|Add5~87\ = CARRY(((!\inst1|Add5~92\) # (!\inst1|TSUM[4][13]~combout\)))
-- \inst1|Add5~87COUT1_167\ = CARRY(((!\inst1|Add5~92COUT1_166\) # (!\inst1|TSUM[4][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][13]~combout\,
	cin => \inst1|Add5~107\,
	cin0 => \inst1|Add5~92\,
	cin1 => \inst1|Add5~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~85_combout\,
	cout0 => \inst1|Add5~87\,
	cout1 => \inst1|Add5~87COUT1_167\);

-- Location: LC_X7_Y9_N9
\inst1|Add5~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~80_combout\ = \inst1|TSUM[4][14]~combout\ $ ((((!(!\inst1|Add5~107\ & \inst1|Add5~87\) # (\inst1|Add5~107\ & \inst1|Add5~87COUT1_167\)))))
-- \inst1|Add5~82\ = CARRY((\inst1|TSUM[4][14]~combout\ & ((!\inst1|Add5~87COUT1_167\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][14]~combout\,
	cin => \inst1|Add5~107\,
	cin0 => \inst1|Add5~87\,
	cin1 => \inst1|Add5~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~80_combout\,
	cout => \inst1|Add5~82\);

-- Location: LC_X8_Y10_N2
\inst1|TSUM[4][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][18]~combout\ = LCELL((((\inst1|Add4~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][18]~combout\);

-- Location: LC_X8_Y10_N4
\inst1|TSUM[4][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][17]~combout\ = LCELL((((\inst1|Add4~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][17]~combout\);

-- Location: LC_X8_Y10_N9
\inst1|TSUM[4][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][16]~combout\ = LCELL((((\inst1|Add4~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][16]~combout\);

-- Location: LC_X8_Y10_N6
\inst1|TSUM[4][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][15]~combout\ = LCELL((((\inst1|Add4~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][15]~combout\);

-- Location: LC_X8_Y9_N0
\inst1|Add5~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~75_combout\ = (\inst1|TSUM[4][15]~combout\ $ ((\inst1|Add5~82\)))
-- \inst1|Add5~77\ = CARRY(((!\inst1|Add5~82\) # (!\inst1|TSUM[4][15]~combout\)))
-- \inst1|Add5~77COUT1_168\ = CARRY(((!\inst1|Add5~82\) # (!\inst1|TSUM[4][15]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][15]~combout\,
	cin => \inst1|Add5~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~75_combout\,
	cout0 => \inst1|Add5~77\,
	cout1 => \inst1|Add5~77COUT1_168\);

-- Location: LC_X8_Y9_N1
\inst1|Add5~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~70_combout\ = (\inst1|TSUM[4][16]~combout\ $ ((!(!\inst1|Add5~82\ & \inst1|Add5~77\) # (\inst1|Add5~82\ & \inst1|Add5~77COUT1_168\))))
-- \inst1|Add5~72\ = CARRY(((\inst1|TSUM[4][16]~combout\ & !\inst1|Add5~77\)))
-- \inst1|Add5~72COUT1_169\ = CARRY(((\inst1|TSUM[4][16]~combout\ & !\inst1|Add5~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][16]~combout\,
	cin => \inst1|Add5~82\,
	cin0 => \inst1|Add5~77\,
	cin1 => \inst1|Add5~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~70_combout\,
	cout0 => \inst1|Add5~72\,
	cout1 => \inst1|Add5~72COUT1_169\);

-- Location: LC_X8_Y9_N2
\inst1|Add5~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~65_combout\ = (\inst1|TSUM[4][17]~combout\ $ (((!\inst1|Add5~82\ & \inst1|Add5~72\) # (\inst1|Add5~82\ & \inst1|Add5~72COUT1_169\))))
-- \inst1|Add5~67\ = CARRY(((!\inst1|Add5~72\) # (!\inst1|TSUM[4][17]~combout\)))
-- \inst1|Add5~67COUT1_170\ = CARRY(((!\inst1|Add5~72COUT1_169\) # (!\inst1|TSUM[4][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][17]~combout\,
	cin => \inst1|Add5~82\,
	cin0 => \inst1|Add5~72\,
	cin1 => \inst1|Add5~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~65_combout\,
	cout0 => \inst1|Add5~67\,
	cout1 => \inst1|Add5~67COUT1_170\);

-- Location: LC_X8_Y9_N3
\inst1|Add5~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~60_combout\ = (\inst1|TSUM[4][18]~combout\ $ ((!(!\inst1|Add5~82\ & \inst1|Add5~67\) # (\inst1|Add5~82\ & \inst1|Add5~67COUT1_170\))))
-- \inst1|Add5~62\ = CARRY(((\inst1|TSUM[4][18]~combout\ & !\inst1|Add5~67\)))
-- \inst1|Add5~62COUT1_171\ = CARRY(((\inst1|TSUM[4][18]~combout\ & !\inst1|Add5~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][18]~combout\,
	cin => \inst1|Add5~82\,
	cin0 => \inst1|Add5~67\,
	cin1 => \inst1|Add5~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~60_combout\,
	cout0 => \inst1|Add5~62\,
	cout1 => \inst1|Add5~62COUT1_171\);

-- Location: LC_X8_Y9_N4
\inst1|Add5~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~55_combout\ = \inst1|TSUM[4][19]~combout\ $ (((((!\inst1|Add5~82\ & \inst1|Add5~62\) # (\inst1|Add5~82\ & \inst1|Add5~62COUT1_171\)))))
-- \inst1|Add5~57\ = CARRY(((!\inst1|Add5~62COUT1_171\)) # (!\inst1|TSUM[4][19]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][19]~combout\,
	cin => \inst1|Add5~82\,
	cin0 => \inst1|Add5~62\,
	cin1 => \inst1|Add5~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~55_combout\,
	cout => \inst1|Add5~57\);

-- Location: LC_X8_Y10_N8
\inst1|TSUM[4][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][23]~combout\ = LCELL((((\inst1|Add4~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][23]~combout\);

-- Location: LC_X8_Y10_N0
\inst1|TSUM[4][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][22]~combout\ = LCELL((((\inst1|Add4~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][22]~combout\);

-- Location: LC_X8_Y10_N1
\inst1|TSUM[4][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][21]~combout\ = LCELL((((\inst1|Add4~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][21]~combout\);

-- Location: LC_X8_Y10_N3
\inst1|TSUM[4][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][20]~combout\ = LCELL((((\inst1|Add4~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][20]~combout\);

-- Location: LC_X8_Y9_N5
\inst1|Add5~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~50_combout\ = (\inst1|TSUM[4][20]~combout\ $ ((!\inst1|Add5~57\)))
-- \inst1|Add5~52\ = CARRY(((\inst1|TSUM[4][20]~combout\ & !\inst1|Add5~57\)))
-- \inst1|Add5~52COUT1_172\ = CARRY(((\inst1|TSUM[4][20]~combout\ & !\inst1|Add5~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][20]~combout\,
	cin => \inst1|Add5~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~50_combout\,
	cout0 => \inst1|Add5~52\,
	cout1 => \inst1|Add5~52COUT1_172\);

-- Location: LC_X8_Y9_N6
\inst1|Add5~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~45_combout\ = \inst1|TSUM[4][21]~combout\ $ (((((!\inst1|Add5~57\ & \inst1|Add5~52\) # (\inst1|Add5~57\ & \inst1|Add5~52COUT1_172\)))))
-- \inst1|Add5~47\ = CARRY(((!\inst1|Add5~52\)) # (!\inst1|TSUM[4][21]~combout\))
-- \inst1|Add5~47COUT1_173\ = CARRY(((!\inst1|Add5~52COUT1_172\)) # (!\inst1|TSUM[4][21]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][21]~combout\,
	cin => \inst1|Add5~57\,
	cin0 => \inst1|Add5~52\,
	cin1 => \inst1|Add5~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~45_combout\,
	cout0 => \inst1|Add5~47\,
	cout1 => \inst1|Add5~47COUT1_173\);

-- Location: LC_X8_Y9_N7
\inst1|Add5~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~40_combout\ = \inst1|TSUM[4][22]~combout\ $ ((((!(!\inst1|Add5~57\ & \inst1|Add5~47\) # (\inst1|Add5~57\ & \inst1|Add5~47COUT1_173\)))))
-- \inst1|Add5~42\ = CARRY((\inst1|TSUM[4][22]~combout\ & ((!\inst1|Add5~47\))))
-- \inst1|Add5~42COUT1_174\ = CARRY((\inst1|TSUM[4][22]~combout\ & ((!\inst1|Add5~47COUT1_173\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][22]~combout\,
	cin => \inst1|Add5~57\,
	cin0 => \inst1|Add5~47\,
	cin1 => \inst1|Add5~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~40_combout\,
	cout0 => \inst1|Add5~42\,
	cout1 => \inst1|Add5~42COUT1_174\);

-- Location: LC_X8_Y9_N8
\inst1|Add5~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~35_combout\ = (\inst1|TSUM[4][23]~combout\ $ (((!\inst1|Add5~57\ & \inst1|Add5~42\) # (\inst1|Add5~57\ & \inst1|Add5~42COUT1_174\))))
-- \inst1|Add5~37\ = CARRY(((!\inst1|Add5~42\) # (!\inst1|TSUM[4][23]~combout\)))
-- \inst1|Add5~37COUT1_175\ = CARRY(((!\inst1|Add5~42COUT1_174\) # (!\inst1|TSUM[4][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][23]~combout\,
	cin => \inst1|Add5~57\,
	cin0 => \inst1|Add5~42\,
	cin1 => \inst1|Add5~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~35_combout\,
	cout0 => \inst1|Add5~37\,
	cout1 => \inst1|Add5~37COUT1_175\);

-- Location: LC_X8_Y9_N9
\inst1|Add5~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~30_combout\ = (\inst1|TSUM[4][24]~combout\ $ ((!(!\inst1|Add5~57\ & \inst1|Add5~37\) # (\inst1|Add5~57\ & \inst1|Add5~37COUT1_175\))))
-- \inst1|Add5~32\ = CARRY(((\inst1|TSUM[4][24]~combout\ & !\inst1|Add5~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][24]~combout\,
	cin => \inst1|Add5~57\,
	cin0 => \inst1|Add5~37\,
	cin1 => \inst1|Add5~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~30_combout\,
	cout => \inst1|Add5~32\);

-- Location: LC_X9_Y10_N2
\inst1|TSUM[4][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][28]~combout\ = LCELL((((\inst1|Add4~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][28]~combout\);

-- Location: LC_X9_Y10_N5
\inst1|TSUM[4][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][27]~combout\ = LCELL((((\inst1|Add4~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add4~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][27]~combout\);

-- Location: LC_X9_Y10_N1
\inst1|TSUM[4][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][26]~combout\ = LCELL((((\inst1|Add4~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][26]~combout\);

-- Location: LC_X5_Y10_N9
\inst1|TSUM[4][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[4][25]~combout\ = LCELL((((\inst1|Add4~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add4~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[4][25]~combout\);

-- Location: LC_X9_Y9_N0
\inst1|Add5~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~25_combout\ = \inst1|TSUM[4][25]~combout\ $ ((((\inst1|Add5~32\))))
-- \inst1|Add5~27\ = CARRY(((!\inst1|Add5~32\)) # (!\inst1|TSUM[4][25]~combout\))
-- \inst1|Add5~27COUT1_176\ = CARRY(((!\inst1|Add5~32\)) # (!\inst1|TSUM[4][25]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][25]~combout\,
	cin => \inst1|Add5~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~25_combout\,
	cout0 => \inst1|Add5~27\,
	cout1 => \inst1|Add5~27COUT1_176\);

-- Location: LC_X9_Y9_N1
\inst1|Add5~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~20_combout\ = \inst1|TSUM[4][26]~combout\ $ ((((!(!\inst1|Add5~32\ & \inst1|Add5~27\) # (\inst1|Add5~32\ & \inst1|Add5~27COUT1_176\)))))
-- \inst1|Add5~22\ = CARRY((\inst1|TSUM[4][26]~combout\ & ((!\inst1|Add5~27\))))
-- \inst1|Add5~22COUT1_177\ = CARRY((\inst1|TSUM[4][26]~combout\ & ((!\inst1|Add5~27COUT1_176\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][26]~combout\,
	cin => \inst1|Add5~32\,
	cin0 => \inst1|Add5~27\,
	cin1 => \inst1|Add5~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~20_combout\,
	cout0 => \inst1|Add5~22\,
	cout1 => \inst1|Add5~22COUT1_177\);

-- Location: LC_X9_Y9_N2
\inst1|Add5~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~15_combout\ = \inst1|TSUM[4][27]~combout\ $ (((((!\inst1|Add5~32\ & \inst1|Add5~22\) # (\inst1|Add5~32\ & \inst1|Add5~22COUT1_177\)))))
-- \inst1|Add5~17\ = CARRY(((!\inst1|Add5~22\)) # (!\inst1|TSUM[4][27]~combout\))
-- \inst1|Add5~17COUT1_178\ = CARRY(((!\inst1|Add5~22COUT1_177\)) # (!\inst1|TSUM[4][27]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[4][27]~combout\,
	cin => \inst1|Add5~32\,
	cin0 => \inst1|Add5~22\,
	cin1 => \inst1|Add5~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~15_combout\,
	cout0 => \inst1|Add5~17\,
	cout1 => \inst1|Add5~17COUT1_178\);

-- Location: LC_X9_Y9_N3
\inst1|Add5~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~10_combout\ = (\inst1|TSUM[4][28]~combout\ $ ((!(!\inst1|Add5~32\ & \inst1|Add5~17\) # (\inst1|Add5~32\ & \inst1|Add5~17COUT1_178\))))
-- \inst1|Add5~12\ = CARRY(((\inst1|TSUM[4][28]~combout\ & !\inst1|Add5~17\)))
-- \inst1|Add5~12COUT1_179\ = CARRY(((\inst1|TSUM[4][28]~combout\ & !\inst1|Add5~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][28]~combout\,
	cin => \inst1|Add5~32\,
	cin0 => \inst1|Add5~17\,
	cin1 => \inst1|Add5~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~10_combout\,
	cout0 => \inst1|Add5~12\,
	cout1 => \inst1|Add5~12COUT1_179\);

-- Location: LC_X9_Y9_N4
\inst1|Add5~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~5_combout\ = (\inst1|TSUM[4][29]~combout\ $ (((!\inst1|Add5~32\ & \inst1|Add5~12\) # (\inst1|Add5~32\ & \inst1|Add5~12COUT1_179\))))
-- \inst1|Add5~7\ = CARRY(((!\inst1|Add5~12COUT1_179\) # (!\inst1|TSUM[4][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[4][29]~combout\,
	cin => \inst1|Add5~32\,
	cin0 => \inst1|Add5~12\,
	cin1 => \inst1|Add5~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~5_combout\,
	cout => \inst1|Add5~7\);

-- Location: LC_X9_Y9_N5
\inst1|Add5~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add5~0_combout\ = ((\inst1|Add5~7\ $ (!\inst1|TSUM[4][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[4][30]~combout\,
	cin => \inst1|Add5~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add5~0_combout\);

-- Location: LC_X9_Y9_N7
\inst1|TSUM[5][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][30]~combout\ = LCELL((((\inst1|Add5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][30]~combout\);

-- Location: LC_X9_Y9_N9
\inst1|TSUM[5][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][29]~combout\ = LCELL((((\inst1|Add5~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][29]~combout\);

-- Location: LC_X10_Y9_N1
\inst1|TSUM[5][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][24]~combout\ = LCELL((((\inst1|Add5~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][24]~combout\);

-- Location: LC_X10_Y9_N8
\inst1|TSUM[5][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][19]~combout\ = LCELL((((\inst1|Add5~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][19]~combout\);

-- Location: LC_X7_Y10_N8
\inst1|TSUM[5][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][14]~combout\ = LCELL((((\inst1|Add5~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][14]~combout\);

-- Location: LC_X7_Y10_N2
\inst1|TSUM[5][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][9]~combout\ = LCELL((((\inst1|Add5~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][9]~combout\);

-- Location: LC_X7_Y7_N0
\inst1|PSUM[7][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][0]~regout\ = DFFEAS(((!\inst1|PSUM[7][0]~regout\)), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][0]~179\ = CARRY(((\inst1|PSUM[7][0]~regout\)))
-- \inst1|PSUM[7][0]~179COUT1_385\ = CARRY(((\inst1|PSUM[7][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	datab => \inst1|PSUM[7][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][0]~regout\,
	cout0 => \inst1|PSUM[7][0]~179\,
	cout1 => \inst1|PSUM[7][0]~179COUT1_385\);

-- Location: LC_X7_Y7_N1
\inst1|PSUM[7][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][1]~regout\ = DFFEAS((\inst1|PSUM[7][1]~regout\ $ ((\inst1|PSUM[7][0]~179\))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][1]~177\ = CARRY(((!\inst1|PSUM[7][0]~179\) # (!\inst1|PSUM[7][1]~regout\)))
-- \inst1|PSUM[7][1]~177COUT1_386\ = CARRY(((!\inst1|PSUM[7][0]~179COUT1_385\) # (!\inst1|PSUM[7][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	datab => \inst1|PSUM[7][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[7][0]~179\,
	cin1 => \inst1|PSUM[7][0]~179COUT1_385\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][1]~regout\,
	cout0 => \inst1|PSUM[7][1]~177\,
	cout1 => \inst1|PSUM[7][1]~177COUT1_386\);

-- Location: LC_X7_Y7_N2
\inst1|PSUM[7][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][2]~regout\ = DFFEAS((\inst1|PSUM[7][2]~regout\ $ ((!\inst1|PSUM[7][1]~177\))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][2]~175\ = CARRY(((\inst1|PSUM[7][2]~regout\ & !\inst1|PSUM[7][1]~177\)))
-- \inst1|PSUM[7][2]~175COUT1_387\ = CARRY(((\inst1|PSUM[7][2]~regout\ & !\inst1|PSUM[7][1]~177COUT1_386\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	datab => \inst1|PSUM[7][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[7][1]~177\,
	cin1 => \inst1|PSUM[7][1]~177COUT1_386\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][2]~regout\,
	cout0 => \inst1|PSUM[7][2]~175\,
	cout1 => \inst1|PSUM[7][2]~175COUT1_387\);

-- Location: LC_X7_Y7_N3
\inst1|PSUM[7][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][3]~regout\ = DFFEAS(\inst1|PSUM[7][3]~regout\ $ ((((\inst1|PSUM[7][2]~175\)))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][3]~173\ = CARRY(((!\inst1|PSUM[7][2]~175\)) # (!\inst1|PSUM[7][3]~regout\))
-- \inst1|PSUM[7][3]~173COUT1_388\ = CARRY(((!\inst1|PSUM[7][2]~175COUT1_387\)) # (!\inst1|PSUM[7][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	dataa => \inst1|PSUM[7][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[7][2]~175\,
	cin1 => \inst1|PSUM[7][2]~175COUT1_387\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][3]~regout\,
	cout0 => \inst1|PSUM[7][3]~173\,
	cout1 => \inst1|PSUM[7][3]~173COUT1_388\);

-- Location: LC_X7_Y7_N4
\inst1|PSUM[7][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][4]~regout\ = DFFEAS(\inst1|PSUM[7][4]~regout\ $ ((((!\inst1|PSUM[7][3]~173\)))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][4]~171\ = CARRY((\inst1|PSUM[7][4]~regout\ & ((!\inst1|PSUM[7][3]~173COUT1_388\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	dataa => \inst1|PSUM[7][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[7][3]~173\,
	cin1 => \inst1|PSUM[7][3]~173COUT1_388\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][4]~regout\,
	cout => \inst1|PSUM[7][4]~171\);

-- Location: LC_X7_Y7_N5
\inst1|PSUM[7][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][5]~regout\ = DFFEAS(\inst1|PSUM[7][5]~regout\ $ ((((\inst1|PSUM[7][4]~171\)))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][5]~169\ = CARRY(((!\inst1|PSUM[7][4]~171\)) # (!\inst1|PSUM[7][5]~regout\))
-- \inst1|PSUM[7][5]~169COUT1_389\ = CARRY(((!\inst1|PSUM[7][4]~171\)) # (!\inst1|PSUM[7][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	dataa => \inst1|PSUM[7][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[7][4]~171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][5]~regout\,
	cout0 => \inst1|PSUM[7][5]~169\,
	cout1 => \inst1|PSUM[7][5]~169COUT1_389\);

-- Location: LC_X7_Y7_N6
\inst1|PSUM[7][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][6]~regout\ = DFFEAS(\inst1|PSUM[7][6]~regout\ $ ((((!(!\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][5]~169\) # (\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][5]~169COUT1_389\))))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][6]~167\ = CARRY((\inst1|PSUM[7][6]~regout\ & ((!\inst1|PSUM[7][5]~169\))))
-- \inst1|PSUM[7][6]~167COUT1_390\ = CARRY((\inst1|PSUM[7][6]~regout\ & ((!\inst1|PSUM[7][5]~169COUT1_389\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	dataa => \inst1|PSUM[7][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[7][4]~171\,
	cin0 => \inst1|PSUM[7][5]~169\,
	cin1 => \inst1|PSUM[7][5]~169COUT1_389\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][6]~regout\,
	cout0 => \inst1|PSUM[7][6]~167\,
	cout1 => \inst1|PSUM[7][6]~167COUT1_390\);

-- Location: LC_X7_Y7_N7
\inst1|PSUM[7][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][7]~regout\ = DFFEAS((\inst1|PSUM[7][7]~regout\ $ (((!\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][6]~167\) # (\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][6]~167COUT1_390\)))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][7]~165\ = CARRY(((!\inst1|PSUM[7][6]~167\) # (!\inst1|PSUM[7][7]~regout\)))
-- \inst1|PSUM[7][7]~165COUT1_391\ = CARRY(((!\inst1|PSUM[7][6]~167COUT1_390\) # (!\inst1|PSUM[7][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	datab => \inst1|PSUM[7][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[7][4]~171\,
	cin0 => \inst1|PSUM[7][6]~167\,
	cin1 => \inst1|PSUM[7][6]~167COUT1_390\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][7]~regout\,
	cout0 => \inst1|PSUM[7][7]~165\,
	cout1 => \inst1|PSUM[7][7]~165COUT1_391\);

-- Location: LC_X7_Y7_N8
\inst1|PSUM[7][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][8]~regout\ = DFFEAS(\inst1|PSUM[7][8]~regout\ $ ((((!(!\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][7]~165\) # (\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][7]~165COUT1_391\))))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[7][8]~163\ = CARRY((\inst1|PSUM[7][8]~regout\ & ((!\inst1|PSUM[7][7]~165\))))
-- \inst1|PSUM[7][8]~163COUT1_392\ = CARRY((\inst1|PSUM[7][8]~regout\ & ((!\inst1|PSUM[7][7]~165COUT1_391\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	dataa => \inst1|PSUM[7][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[7][4]~171\,
	cin0 => \inst1|PSUM[7][7]~165\,
	cin1 => \inst1|PSUM[7][7]~165COUT1_391\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][8]~regout\,
	cout0 => \inst1|PSUM[7][8]~163\,
	cout1 => \inst1|PSUM[7][8]~163COUT1_392\);

-- Location: LC_X7_Y7_N9
\inst1|PSUM[7][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[7][9]~regout\ = DFFEAS((((!\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][8]~163\) # (\inst1|PSUM[7][4]~171\ & \inst1|PSUM[7][8]~163COUT1_392\) $ (\inst1|PSUM[7][9]~regout\))), \inst6|STEMP\(7), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(7),
	datad => \inst1|PSUM[7][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[7][4]~171\,
	cin0 => \inst1|PSUM[7][8]~163\,
	cin1 => \inst1|PSUM[7][8]~163COUT1_392\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[7][9]~regout\);

-- Location: LC_X6_Y9_N2
\inst1|TSUM[5][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][4]~combout\ = LCELL((((\inst1|Add5~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][4]~combout\);

-- Location: LC_X6_Y8_N3
\inst1|TSUM[5][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][3]~combout\ = LCELL((((\inst1|Add5~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][3]~combout\);

-- Location: LC_X6_Y8_N4
\inst1|TSUM[5][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][2]~combout\ = LCELL((((\inst1|Add5~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][2]~combout\);

-- Location: LC_X6_Y9_N1
\inst1|TSUM[5][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][1]~combout\ = LCELL((((\inst1|Add5~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][1]~combout\);

-- Location: LC_X6_Y9_N3
\inst1|TSUM[5][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][0]~combout\ = LCELL((((\inst1|Add5~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][0]~combout\);

-- Location: LC_X6_Y8_N5
\inst1|Add6~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~150_combout\ = \inst1|TSUM[5][0]~combout\ $ ((\inst1|PSUM[7][0]~regout\))
-- \inst1|Add6~152\ = CARRY((\inst1|TSUM[5][0]~combout\ & (\inst1|PSUM[7][0]~regout\)))
-- \inst1|Add6~152COUT1_156\ = CARRY((\inst1|TSUM[5][0]~combout\ & (\inst1|PSUM[7][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][0]~combout\,
	datab => \inst1|PSUM[7][0]~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~150_combout\,
	cout0 => \inst1|Add6~152\,
	cout1 => \inst1|Add6~152COUT1_156\);

-- Location: LC_X6_Y8_N6
\inst1|Add6~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~145_combout\ = \inst1|PSUM[7][1]~regout\ $ (\inst1|TSUM[5][1]~combout\ $ ((\inst1|Add6~152\)))
-- \inst1|Add6~147\ = CARRY((\inst1|PSUM[7][1]~regout\ & (!\inst1|TSUM[5][1]~combout\ & !\inst1|Add6~152\)) # (!\inst1|PSUM[7][1]~regout\ & ((!\inst1|Add6~152\) # (!\inst1|TSUM[5][1]~combout\))))
-- \inst1|Add6~147COUT1_157\ = CARRY((\inst1|PSUM[7][1]~regout\ & (!\inst1|TSUM[5][1]~combout\ & !\inst1|Add6~152COUT1_156\)) # (!\inst1|PSUM[7][1]~regout\ & ((!\inst1|Add6~152COUT1_156\) # (!\inst1|TSUM[5][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[7][1]~regout\,
	datab => \inst1|TSUM[5][1]~combout\,
	cin0 => \inst1|Add6~152\,
	cin1 => \inst1|Add6~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~145_combout\,
	cout0 => \inst1|Add6~147\,
	cout1 => \inst1|Add6~147COUT1_157\);

-- Location: LC_X6_Y8_N7
\inst1|Add6~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~140_combout\ = \inst1|TSUM[5][2]~combout\ $ (\inst1|PSUM[7][2]~regout\ $ ((!\inst1|Add6~147\)))
-- \inst1|Add6~142\ = CARRY((\inst1|TSUM[5][2]~combout\ & ((\inst1|PSUM[7][2]~regout\) # (!\inst1|Add6~147\))) # (!\inst1|TSUM[5][2]~combout\ & (\inst1|PSUM[7][2]~regout\ & !\inst1|Add6~147\)))
-- \inst1|Add6~142COUT1_158\ = CARRY((\inst1|TSUM[5][2]~combout\ & ((\inst1|PSUM[7][2]~regout\) # (!\inst1|Add6~147COUT1_157\))) # (!\inst1|TSUM[5][2]~combout\ & (\inst1|PSUM[7][2]~regout\ & !\inst1|Add6~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][2]~combout\,
	datab => \inst1|PSUM[7][2]~regout\,
	cin0 => \inst1|Add6~147\,
	cin1 => \inst1|Add6~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~140_combout\,
	cout0 => \inst1|Add6~142\,
	cout1 => \inst1|Add6~142COUT1_158\);

-- Location: LC_X6_Y8_N8
\inst1|Add6~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~135_combout\ = \inst1|TSUM[5][3]~combout\ $ (\inst1|PSUM[7][3]~regout\ $ ((\inst1|Add6~142\)))
-- \inst1|Add6~137\ = CARRY((\inst1|TSUM[5][3]~combout\ & (!\inst1|PSUM[7][3]~regout\ & !\inst1|Add6~142\)) # (!\inst1|TSUM[5][3]~combout\ & ((!\inst1|Add6~142\) # (!\inst1|PSUM[7][3]~regout\))))
-- \inst1|Add6~137COUT1_159\ = CARRY((\inst1|TSUM[5][3]~combout\ & (!\inst1|PSUM[7][3]~regout\ & !\inst1|Add6~142COUT1_158\)) # (!\inst1|TSUM[5][3]~combout\ & ((!\inst1|Add6~142COUT1_158\) # (!\inst1|PSUM[7][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][3]~combout\,
	datab => \inst1|PSUM[7][3]~regout\,
	cin0 => \inst1|Add6~142\,
	cin1 => \inst1|Add6~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~135_combout\,
	cout0 => \inst1|Add6~137\,
	cout1 => \inst1|Add6~137COUT1_159\);

-- Location: LC_X6_Y8_N9
\inst1|Add6~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~130_combout\ = \inst1|PSUM[7][4]~regout\ $ (\inst1|TSUM[5][4]~combout\ $ ((!\inst1|Add6~137\)))
-- \inst1|Add6~132\ = CARRY((\inst1|PSUM[7][4]~regout\ & ((\inst1|TSUM[5][4]~combout\) # (!\inst1|Add6~137COUT1_159\))) # (!\inst1|PSUM[7][4]~regout\ & (\inst1|TSUM[5][4]~combout\ & !\inst1|Add6~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[7][4]~regout\,
	datab => \inst1|TSUM[5][4]~combout\,
	cin0 => \inst1|Add6~137\,
	cin1 => \inst1|Add6~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~130_combout\,
	cout => \inst1|Add6~132\);

-- Location: LC_X7_Y10_N0
\inst1|TSUM[5][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][8]~combout\ = LCELL((((\inst1|Add5~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][8]~combout\);

-- Location: LC_X7_Y10_N7
\inst1|TSUM[5][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][7]~combout\ = LCELL((((\inst1|Add5~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][7]~combout\);

-- Location: LC_X7_Y10_N6
\inst1|TSUM[5][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][6]~combout\ = LCELL((((\inst1|Add5~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][6]~combout\);

-- Location: LC_X7_Y10_N3
\inst1|TSUM[5][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][5]~combout\ = LCELL((((\inst1|Add5~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][5]~combout\);

-- Location: LC_X7_Y8_N0
\inst1|Add6~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~125_combout\ = \inst1|TSUM[5][5]~combout\ $ (\inst1|PSUM[7][5]~regout\ $ ((\inst1|Add6~132\)))
-- \inst1|Add6~127\ = CARRY((\inst1|TSUM[5][5]~combout\ & (!\inst1|PSUM[7][5]~regout\ & !\inst1|Add6~132\)) # (!\inst1|TSUM[5][5]~combout\ & ((!\inst1|Add6~132\) # (!\inst1|PSUM[7][5]~regout\))))
-- \inst1|Add6~127COUT1_160\ = CARRY((\inst1|TSUM[5][5]~combout\ & (!\inst1|PSUM[7][5]~regout\ & !\inst1|Add6~132\)) # (!\inst1|TSUM[5][5]~combout\ & ((!\inst1|Add6~132\) # (!\inst1|PSUM[7][5]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][5]~combout\,
	datab => \inst1|PSUM[7][5]~regout\,
	cin => \inst1|Add6~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~125_combout\,
	cout0 => \inst1|Add6~127\,
	cout1 => \inst1|Add6~127COUT1_160\);

-- Location: LC_X7_Y8_N1
\inst1|Add6~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~120_combout\ = \inst1|PSUM[7][6]~regout\ $ (\inst1|TSUM[5][6]~combout\ $ ((!(!\inst1|Add6~132\ & \inst1|Add6~127\) # (\inst1|Add6~132\ & \inst1|Add6~127COUT1_160\))))
-- \inst1|Add6~122\ = CARRY((\inst1|PSUM[7][6]~regout\ & ((\inst1|TSUM[5][6]~combout\) # (!\inst1|Add6~127\))) # (!\inst1|PSUM[7][6]~regout\ & (\inst1|TSUM[5][6]~combout\ & !\inst1|Add6~127\)))
-- \inst1|Add6~122COUT1_161\ = CARRY((\inst1|PSUM[7][6]~regout\ & ((\inst1|TSUM[5][6]~combout\) # (!\inst1|Add6~127COUT1_160\))) # (!\inst1|PSUM[7][6]~regout\ & (\inst1|TSUM[5][6]~combout\ & !\inst1|Add6~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[7][6]~regout\,
	datab => \inst1|TSUM[5][6]~combout\,
	cin => \inst1|Add6~132\,
	cin0 => \inst1|Add6~127\,
	cin1 => \inst1|Add6~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~120_combout\,
	cout0 => \inst1|Add6~122\,
	cout1 => \inst1|Add6~122COUT1_161\);

-- Location: LC_X7_Y8_N2
\inst1|Add6~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~115_combout\ = \inst1|PSUM[7][7]~regout\ $ (\inst1|TSUM[5][7]~combout\ $ (((!\inst1|Add6~132\ & \inst1|Add6~122\) # (\inst1|Add6~132\ & \inst1|Add6~122COUT1_161\))))
-- \inst1|Add6~117\ = CARRY((\inst1|PSUM[7][7]~regout\ & (!\inst1|TSUM[5][7]~combout\ & !\inst1|Add6~122\)) # (!\inst1|PSUM[7][7]~regout\ & ((!\inst1|Add6~122\) # (!\inst1|TSUM[5][7]~combout\))))
-- \inst1|Add6~117COUT1_162\ = CARRY((\inst1|PSUM[7][7]~regout\ & (!\inst1|TSUM[5][7]~combout\ & !\inst1|Add6~122COUT1_161\)) # (!\inst1|PSUM[7][7]~regout\ & ((!\inst1|Add6~122COUT1_161\) # (!\inst1|TSUM[5][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[7][7]~regout\,
	datab => \inst1|TSUM[5][7]~combout\,
	cin => \inst1|Add6~132\,
	cin0 => \inst1|Add6~122\,
	cin1 => \inst1|Add6~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~115_combout\,
	cout0 => \inst1|Add6~117\,
	cout1 => \inst1|Add6~117COUT1_162\);

-- Location: LC_X7_Y8_N3
\inst1|Add6~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~110_combout\ = \inst1|TSUM[5][8]~combout\ $ (\inst1|PSUM[7][8]~regout\ $ ((!(!\inst1|Add6~132\ & \inst1|Add6~117\) # (\inst1|Add6~132\ & \inst1|Add6~117COUT1_162\))))
-- \inst1|Add6~112\ = CARRY((\inst1|TSUM[5][8]~combout\ & ((\inst1|PSUM[7][8]~regout\) # (!\inst1|Add6~117\))) # (!\inst1|TSUM[5][8]~combout\ & (\inst1|PSUM[7][8]~regout\ & !\inst1|Add6~117\)))
-- \inst1|Add6~112COUT1_163\ = CARRY((\inst1|TSUM[5][8]~combout\ & ((\inst1|PSUM[7][8]~regout\) # (!\inst1|Add6~117COUT1_162\))) # (!\inst1|TSUM[5][8]~combout\ & (\inst1|PSUM[7][8]~regout\ & !\inst1|Add6~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][8]~combout\,
	datab => \inst1|PSUM[7][8]~regout\,
	cin => \inst1|Add6~132\,
	cin0 => \inst1|Add6~117\,
	cin1 => \inst1|Add6~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~110_combout\,
	cout0 => \inst1|Add6~112\,
	cout1 => \inst1|Add6~112COUT1_163\);

-- Location: LC_X7_Y8_N4
\inst1|Add6~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~105_combout\ = \inst1|TSUM[5][9]~combout\ $ (\inst1|PSUM[7][9]~regout\ $ (((!\inst1|Add6~132\ & \inst1|Add6~112\) # (\inst1|Add6~132\ & \inst1|Add6~112COUT1_163\))))
-- \inst1|Add6~107\ = CARRY((\inst1|TSUM[5][9]~combout\ & (!\inst1|PSUM[7][9]~regout\ & !\inst1|Add6~112COUT1_163\)) # (!\inst1|TSUM[5][9]~combout\ & ((!\inst1|Add6~112COUT1_163\) # (!\inst1|PSUM[7][9]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][9]~combout\,
	datab => \inst1|PSUM[7][9]~regout\,
	cin => \inst1|Add6~132\,
	cin0 => \inst1|Add6~112\,
	cin1 => \inst1|Add6~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~105_combout\,
	cout => \inst1|Add6~107\);

-- Location: LC_X7_Y10_N4
\inst1|TSUM[5][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][13]~combout\ = LCELL((((\inst1|Add5~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][13]~combout\);

-- Location: LC_X7_Y10_N5
\inst1|TSUM[5][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][12]~combout\ = LCELL((((\inst1|Add5~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][12]~combout\);

-- Location: LC_X7_Y10_N9
\inst1|TSUM[5][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][11]~combout\ = LCELL((((\inst1|Add5~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][11]~combout\);

-- Location: LC_X7_Y10_N1
\inst1|TSUM[5][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][10]~combout\ = LCELL((((\inst1|Add5~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][10]~combout\);

-- Location: LC_X7_Y8_N5
\inst1|Add6~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~100_combout\ = (\inst1|TSUM[5][10]~combout\ $ ((!\inst1|Add6~107\)))
-- \inst1|Add6~102\ = CARRY(((\inst1|TSUM[5][10]~combout\ & !\inst1|Add6~107\)))
-- \inst1|Add6~102COUT1_164\ = CARRY(((\inst1|TSUM[5][10]~combout\ & !\inst1|Add6~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][10]~combout\,
	cin => \inst1|Add6~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~100_combout\,
	cout0 => \inst1|Add6~102\,
	cout1 => \inst1|Add6~102COUT1_164\);

-- Location: LC_X7_Y8_N6
\inst1|Add6~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~95_combout\ = \inst1|TSUM[5][11]~combout\ $ (((((!\inst1|Add6~107\ & \inst1|Add6~102\) # (\inst1|Add6~107\ & \inst1|Add6~102COUT1_164\)))))
-- \inst1|Add6~97\ = CARRY(((!\inst1|Add6~102\)) # (!\inst1|TSUM[5][11]~combout\))
-- \inst1|Add6~97COUT1_165\ = CARRY(((!\inst1|Add6~102COUT1_164\)) # (!\inst1|TSUM[5][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][11]~combout\,
	cin => \inst1|Add6~107\,
	cin0 => \inst1|Add6~102\,
	cin1 => \inst1|Add6~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~95_combout\,
	cout0 => \inst1|Add6~97\,
	cout1 => \inst1|Add6~97COUT1_165\);

-- Location: LC_X7_Y8_N7
\inst1|Add6~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~90_combout\ = (\inst1|TSUM[5][12]~combout\ $ ((!(!\inst1|Add6~107\ & \inst1|Add6~97\) # (\inst1|Add6~107\ & \inst1|Add6~97COUT1_165\))))
-- \inst1|Add6~92\ = CARRY(((\inst1|TSUM[5][12]~combout\ & !\inst1|Add6~97\)))
-- \inst1|Add6~92COUT1_166\ = CARRY(((\inst1|TSUM[5][12]~combout\ & !\inst1|Add6~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][12]~combout\,
	cin => \inst1|Add6~107\,
	cin0 => \inst1|Add6~97\,
	cin1 => \inst1|Add6~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~90_combout\,
	cout0 => \inst1|Add6~92\,
	cout1 => \inst1|Add6~92COUT1_166\);

-- Location: LC_X7_Y8_N8
\inst1|Add6~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~85_combout\ = (\inst1|TSUM[5][13]~combout\ $ (((!\inst1|Add6~107\ & \inst1|Add6~92\) # (\inst1|Add6~107\ & \inst1|Add6~92COUT1_166\))))
-- \inst1|Add6~87\ = CARRY(((!\inst1|Add6~92\) # (!\inst1|TSUM[5][13]~combout\)))
-- \inst1|Add6~87COUT1_167\ = CARRY(((!\inst1|Add6~92COUT1_166\) # (!\inst1|TSUM[5][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][13]~combout\,
	cin => \inst1|Add6~107\,
	cin0 => \inst1|Add6~92\,
	cin1 => \inst1|Add6~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~85_combout\,
	cout0 => \inst1|Add6~87\,
	cout1 => \inst1|Add6~87COUT1_167\);

-- Location: LC_X7_Y8_N9
\inst1|Add6~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~80_combout\ = (\inst1|TSUM[5][14]~combout\ $ ((!(!\inst1|Add6~107\ & \inst1|Add6~87\) # (\inst1|Add6~107\ & \inst1|Add6~87COUT1_167\))))
-- \inst1|Add6~82\ = CARRY(((\inst1|TSUM[5][14]~combout\ & !\inst1|Add6~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][14]~combout\,
	cin => \inst1|Add6~107\,
	cin0 => \inst1|Add6~87\,
	cin1 => \inst1|Add6~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~80_combout\,
	cout => \inst1|Add6~82\);

-- Location: LC_X8_Y10_N5
\inst1|TSUM[5][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][18]~combout\ = LCELL((((\inst1|Add5~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][18]~combout\);

-- Location: LC_X10_Y9_N6
\inst1|TSUM[5][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][17]~combout\ = LCELL((((\inst1|Add5~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][17]~combout\);

-- Location: LC_X10_Y9_N4
\inst1|TSUM[5][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][16]~combout\ = LCELL((((\inst1|Add5~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][16]~combout\);

-- Location: LC_X10_Y9_N2
\inst1|TSUM[5][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][15]~combout\ = LCELL((((\inst1|Add5~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][15]~combout\);

-- Location: LC_X8_Y8_N0
\inst1|Add6~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~75_combout\ = \inst1|TSUM[5][15]~combout\ $ ((((\inst1|Add6~82\))))
-- \inst1|Add6~77\ = CARRY(((!\inst1|Add6~82\)) # (!\inst1|TSUM[5][15]~combout\))
-- \inst1|Add6~77COUT1_168\ = CARRY(((!\inst1|Add6~82\)) # (!\inst1|TSUM[5][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][15]~combout\,
	cin => \inst1|Add6~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~75_combout\,
	cout0 => \inst1|Add6~77\,
	cout1 => \inst1|Add6~77COUT1_168\);

-- Location: LC_X8_Y8_N1
\inst1|Add6~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~70_combout\ = (\inst1|TSUM[5][16]~combout\ $ ((!(!\inst1|Add6~82\ & \inst1|Add6~77\) # (\inst1|Add6~82\ & \inst1|Add6~77COUT1_168\))))
-- \inst1|Add6~72\ = CARRY(((\inst1|TSUM[5][16]~combout\ & !\inst1|Add6~77\)))
-- \inst1|Add6~72COUT1_169\ = CARRY(((\inst1|TSUM[5][16]~combout\ & !\inst1|Add6~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][16]~combout\,
	cin => \inst1|Add6~82\,
	cin0 => \inst1|Add6~77\,
	cin1 => \inst1|Add6~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~70_combout\,
	cout0 => \inst1|Add6~72\,
	cout1 => \inst1|Add6~72COUT1_169\);

-- Location: LC_X8_Y8_N2
\inst1|Add6~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~65_combout\ = (\inst1|TSUM[5][17]~combout\ $ (((!\inst1|Add6~82\ & \inst1|Add6~72\) # (\inst1|Add6~82\ & \inst1|Add6~72COUT1_169\))))
-- \inst1|Add6~67\ = CARRY(((!\inst1|Add6~72\) # (!\inst1|TSUM[5][17]~combout\)))
-- \inst1|Add6~67COUT1_170\ = CARRY(((!\inst1|Add6~72COUT1_169\) # (!\inst1|TSUM[5][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][17]~combout\,
	cin => \inst1|Add6~82\,
	cin0 => \inst1|Add6~72\,
	cin1 => \inst1|Add6~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~65_combout\,
	cout0 => \inst1|Add6~67\,
	cout1 => \inst1|Add6~67COUT1_170\);

-- Location: LC_X8_Y8_N3
\inst1|Add6~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~60_combout\ = (\inst1|TSUM[5][18]~combout\ $ ((!(!\inst1|Add6~82\ & \inst1|Add6~67\) # (\inst1|Add6~82\ & \inst1|Add6~67COUT1_170\))))
-- \inst1|Add6~62\ = CARRY(((\inst1|TSUM[5][18]~combout\ & !\inst1|Add6~67\)))
-- \inst1|Add6~62COUT1_171\ = CARRY(((\inst1|TSUM[5][18]~combout\ & !\inst1|Add6~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][18]~combout\,
	cin => \inst1|Add6~82\,
	cin0 => \inst1|Add6~67\,
	cin1 => \inst1|Add6~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~60_combout\,
	cout0 => \inst1|Add6~62\,
	cout1 => \inst1|Add6~62COUT1_171\);

-- Location: LC_X8_Y8_N4
\inst1|Add6~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~55_combout\ = (\inst1|TSUM[5][19]~combout\ $ (((!\inst1|Add6~82\ & \inst1|Add6~62\) # (\inst1|Add6~82\ & \inst1|Add6~62COUT1_171\))))
-- \inst1|Add6~57\ = CARRY(((!\inst1|Add6~62COUT1_171\) # (!\inst1|TSUM[5][19]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][19]~combout\,
	cin => \inst1|Add6~82\,
	cin0 => \inst1|Add6~62\,
	cin1 => \inst1|Add6~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~55_combout\,
	cout => \inst1|Add6~57\);

-- Location: LC_X10_Y9_N5
\inst1|TSUM[5][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][23]~combout\ = LCELL((((\inst1|Add5~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][23]~combout\);

-- Location: LC_X10_Y9_N7
\inst1|TSUM[5][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][22]~combout\ = LCELL((((\inst1|Add5~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][22]~combout\);

-- Location: LC_X8_Y7_N3
\inst1|TSUM[5][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][21]~combout\ = LCELL((((\inst1|Add5~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][21]~combout\);

-- Location: LC_X10_Y9_N3
\inst1|TSUM[5][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][20]~combout\ = LCELL((((\inst1|Add5~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][20]~combout\);

-- Location: LC_X8_Y8_N5
\inst1|Add6~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~50_combout\ = (\inst1|TSUM[5][20]~combout\ $ ((!\inst1|Add6~57\)))
-- \inst1|Add6~52\ = CARRY(((\inst1|TSUM[5][20]~combout\ & !\inst1|Add6~57\)))
-- \inst1|Add6~52COUT1_172\ = CARRY(((\inst1|TSUM[5][20]~combout\ & !\inst1|Add6~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][20]~combout\,
	cin => \inst1|Add6~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~50_combout\,
	cout0 => \inst1|Add6~52\,
	cout1 => \inst1|Add6~52COUT1_172\);

-- Location: LC_X8_Y8_N6
\inst1|Add6~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~45_combout\ = (\inst1|TSUM[5][21]~combout\ $ (((!\inst1|Add6~57\ & \inst1|Add6~52\) # (\inst1|Add6~57\ & \inst1|Add6~52COUT1_172\))))
-- \inst1|Add6~47\ = CARRY(((!\inst1|Add6~52\) # (!\inst1|TSUM[5][21]~combout\)))
-- \inst1|Add6~47COUT1_173\ = CARRY(((!\inst1|Add6~52COUT1_172\) # (!\inst1|TSUM[5][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][21]~combout\,
	cin => \inst1|Add6~57\,
	cin0 => \inst1|Add6~52\,
	cin1 => \inst1|Add6~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~45_combout\,
	cout0 => \inst1|Add6~47\,
	cout1 => \inst1|Add6~47COUT1_173\);

-- Location: LC_X8_Y8_N7
\inst1|Add6~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~40_combout\ = (\inst1|TSUM[5][22]~combout\ $ ((!(!\inst1|Add6~57\ & \inst1|Add6~47\) # (\inst1|Add6~57\ & \inst1|Add6~47COUT1_173\))))
-- \inst1|Add6~42\ = CARRY(((\inst1|TSUM[5][22]~combout\ & !\inst1|Add6~47\)))
-- \inst1|Add6~42COUT1_174\ = CARRY(((\inst1|TSUM[5][22]~combout\ & !\inst1|Add6~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][22]~combout\,
	cin => \inst1|Add6~57\,
	cin0 => \inst1|Add6~47\,
	cin1 => \inst1|Add6~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~40_combout\,
	cout0 => \inst1|Add6~42\,
	cout1 => \inst1|Add6~42COUT1_174\);

-- Location: LC_X8_Y8_N8
\inst1|Add6~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~35_combout\ = \inst1|TSUM[5][23]~combout\ $ (((((!\inst1|Add6~57\ & \inst1|Add6~42\) # (\inst1|Add6~57\ & \inst1|Add6~42COUT1_174\)))))
-- \inst1|Add6~37\ = CARRY(((!\inst1|Add6~42\)) # (!\inst1|TSUM[5][23]~combout\))
-- \inst1|Add6~37COUT1_175\ = CARRY(((!\inst1|Add6~42COUT1_174\)) # (!\inst1|TSUM[5][23]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][23]~combout\,
	cin => \inst1|Add6~57\,
	cin0 => \inst1|Add6~42\,
	cin1 => \inst1|Add6~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~35_combout\,
	cout0 => \inst1|Add6~37\,
	cout1 => \inst1|Add6~37COUT1_175\);

-- Location: LC_X8_Y8_N9
\inst1|Add6~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~30_combout\ = (\inst1|TSUM[5][24]~combout\ $ ((!(!\inst1|Add6~57\ & \inst1|Add6~37\) # (\inst1|Add6~57\ & \inst1|Add6~37COUT1_175\))))
-- \inst1|Add6~32\ = CARRY(((\inst1|TSUM[5][24]~combout\ & !\inst1|Add6~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][24]~combout\,
	cin => \inst1|Add6~57\,
	cin0 => \inst1|Add6~37\,
	cin1 => \inst1|Add6~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~30_combout\,
	cout => \inst1|Add6~32\);

-- Location: LC_X9_Y9_N6
\inst1|TSUM[5][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][28]~combout\ = LCELL((((\inst1|Add5~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][28]~combout\);

-- Location: LC_X10_Y9_N0
\inst1|TSUM[5][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][27]~combout\ = LCELL((((\inst1|Add5~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add5~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][27]~combout\);

-- Location: LC_X10_Y9_N9
\inst1|TSUM[5][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][26]~combout\ = LCELL((((\inst1|Add5~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][26]~combout\);

-- Location: LC_X9_Y9_N8
\inst1|TSUM[5][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[5][25]~combout\ = LCELL((((\inst1|Add5~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add5~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[5][25]~combout\);

-- Location: LC_X9_Y8_N0
\inst1|Add6~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~25_combout\ = \inst1|TSUM[5][25]~combout\ $ ((((\inst1|Add6~32\))))
-- \inst1|Add6~27\ = CARRY(((!\inst1|Add6~32\)) # (!\inst1|TSUM[5][25]~combout\))
-- \inst1|Add6~27COUT1_176\ = CARRY(((!\inst1|Add6~32\)) # (!\inst1|TSUM[5][25]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][25]~combout\,
	cin => \inst1|Add6~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~25_combout\,
	cout0 => \inst1|Add6~27\,
	cout1 => \inst1|Add6~27COUT1_176\);

-- Location: LC_X9_Y8_N1
\inst1|Add6~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~20_combout\ = (\inst1|TSUM[5][26]~combout\ $ ((!(!\inst1|Add6~32\ & \inst1|Add6~27\) # (\inst1|Add6~32\ & \inst1|Add6~27COUT1_176\))))
-- \inst1|Add6~22\ = CARRY(((\inst1|TSUM[5][26]~combout\ & !\inst1|Add6~27\)))
-- \inst1|Add6~22COUT1_177\ = CARRY(((\inst1|TSUM[5][26]~combout\ & !\inst1|Add6~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][26]~combout\,
	cin => \inst1|Add6~32\,
	cin0 => \inst1|Add6~27\,
	cin1 => \inst1|Add6~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~20_combout\,
	cout0 => \inst1|Add6~22\,
	cout1 => \inst1|Add6~22COUT1_177\);

-- Location: LC_X9_Y8_N2
\inst1|Add6~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~15_combout\ = \inst1|TSUM[5][27]~combout\ $ (((((!\inst1|Add6~32\ & \inst1|Add6~22\) # (\inst1|Add6~32\ & \inst1|Add6~22COUT1_177\)))))
-- \inst1|Add6~17\ = CARRY(((!\inst1|Add6~22\)) # (!\inst1|TSUM[5][27]~combout\))
-- \inst1|Add6~17COUT1_178\ = CARRY(((!\inst1|Add6~22COUT1_177\)) # (!\inst1|TSUM[5][27]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[5][27]~combout\,
	cin => \inst1|Add6~32\,
	cin0 => \inst1|Add6~22\,
	cin1 => \inst1|Add6~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~15_combout\,
	cout0 => \inst1|Add6~17\,
	cout1 => \inst1|Add6~17COUT1_178\);

-- Location: LC_X9_Y8_N3
\inst1|Add6~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~10_combout\ = (\inst1|TSUM[5][28]~combout\ $ ((!(!\inst1|Add6~32\ & \inst1|Add6~17\) # (\inst1|Add6~32\ & \inst1|Add6~17COUT1_178\))))
-- \inst1|Add6~12\ = CARRY(((\inst1|TSUM[5][28]~combout\ & !\inst1|Add6~17\)))
-- \inst1|Add6~12COUT1_179\ = CARRY(((\inst1|TSUM[5][28]~combout\ & !\inst1|Add6~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][28]~combout\,
	cin => \inst1|Add6~32\,
	cin0 => \inst1|Add6~17\,
	cin1 => \inst1|Add6~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~10_combout\,
	cout0 => \inst1|Add6~12\,
	cout1 => \inst1|Add6~12COUT1_179\);

-- Location: LC_X9_Y8_N4
\inst1|Add6~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~5_combout\ = (\inst1|TSUM[5][29]~combout\ $ (((!\inst1|Add6~32\ & \inst1|Add6~12\) # (\inst1|Add6~32\ & \inst1|Add6~12COUT1_179\))))
-- \inst1|Add6~7\ = CARRY(((!\inst1|Add6~12COUT1_179\) # (!\inst1|TSUM[5][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[5][29]~combout\,
	cin => \inst1|Add6~32\,
	cin0 => \inst1|Add6~12\,
	cin1 => \inst1|Add6~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~5_combout\,
	cout => \inst1|Add6~7\);

-- Location: LC_X9_Y8_N5
\inst1|Add6~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add6~0_combout\ = ((\inst1|Add6~7\ $ (!\inst1|TSUM[5][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[5][30]~combout\,
	cin => \inst1|Add6~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add6~0_combout\);

-- Location: LC_X9_Y8_N9
\inst1|TSUM[6][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][30]~combout\ = LCELL((((\inst1|Add6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][30]~combout\);

-- Location: LC_X10_Y7_N2
\inst1|TSUM[6][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][29]~combout\ = LCELL((((\inst1|Add6~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][29]~combout\);

-- Location: LC_X8_Y7_N7
\inst1|TSUM[6][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][24]~combout\ = LCELL((((\inst1|Add6~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][24]~combout\);

-- Location: LC_X9_Y8_N8
\inst1|TSUM[6][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][19]~combout\ = LCELL((((\inst1|Add6~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][19]~combout\);

-- Location: LC_X7_Y6_N2
\inst1|TSUM[6][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][14]~combout\ = LCELL((((\inst1|Add6~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][14]~combout\);

-- Location: LC_X6_Y6_N0
\inst1|PSUM[8][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][0]~regout\ = DFFEAS(((!\inst1|PSUM[8][0]~regout\)), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][0]~159\ = CARRY(((\inst1|PSUM[8][0]~regout\)))
-- \inst1|PSUM[8][0]~159COUT1_377\ = CARRY(((\inst1|PSUM[8][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	datab => \inst1|PSUM[8][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][0]~regout\,
	cout0 => \inst1|PSUM[8][0]~159\,
	cout1 => \inst1|PSUM[8][0]~159COUT1_377\);

-- Location: LC_X6_Y6_N1
\inst1|PSUM[8][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][1]~regout\ = DFFEAS((\inst1|PSUM[8][1]~regout\ $ ((\inst1|PSUM[8][0]~159\))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][1]~157\ = CARRY(((!\inst1|PSUM[8][0]~159\) # (!\inst1|PSUM[8][1]~regout\)))
-- \inst1|PSUM[8][1]~157COUT1_378\ = CARRY(((!\inst1|PSUM[8][0]~159COUT1_377\) # (!\inst1|PSUM[8][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	datab => \inst1|PSUM[8][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[8][0]~159\,
	cin1 => \inst1|PSUM[8][0]~159COUT1_377\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][1]~regout\,
	cout0 => \inst1|PSUM[8][1]~157\,
	cout1 => \inst1|PSUM[8][1]~157COUT1_378\);

-- Location: LC_X6_Y6_N2
\inst1|PSUM[8][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][2]~regout\ = DFFEAS((\inst1|PSUM[8][2]~regout\ $ ((!\inst1|PSUM[8][1]~157\))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][2]~155\ = CARRY(((\inst1|PSUM[8][2]~regout\ & !\inst1|PSUM[8][1]~157\)))
-- \inst1|PSUM[8][2]~155COUT1_379\ = CARRY(((\inst1|PSUM[8][2]~regout\ & !\inst1|PSUM[8][1]~157COUT1_378\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	datab => \inst1|PSUM[8][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[8][1]~157\,
	cin1 => \inst1|PSUM[8][1]~157COUT1_378\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][2]~regout\,
	cout0 => \inst1|PSUM[8][2]~155\,
	cout1 => \inst1|PSUM[8][2]~155COUT1_379\);

-- Location: LC_X6_Y6_N3
\inst1|PSUM[8][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][3]~regout\ = DFFEAS(\inst1|PSUM[8][3]~regout\ $ ((((\inst1|PSUM[8][2]~155\)))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][3]~153\ = CARRY(((!\inst1|PSUM[8][2]~155\)) # (!\inst1|PSUM[8][3]~regout\))
-- \inst1|PSUM[8][3]~153COUT1_380\ = CARRY(((!\inst1|PSUM[8][2]~155COUT1_379\)) # (!\inst1|PSUM[8][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	dataa => \inst1|PSUM[8][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[8][2]~155\,
	cin1 => \inst1|PSUM[8][2]~155COUT1_379\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][3]~regout\,
	cout0 => \inst1|PSUM[8][3]~153\,
	cout1 => \inst1|PSUM[8][3]~153COUT1_380\);

-- Location: LC_X6_Y6_N4
\inst1|PSUM[8][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][4]~regout\ = DFFEAS(\inst1|PSUM[8][4]~regout\ $ ((((!\inst1|PSUM[8][3]~153\)))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][4]~151\ = CARRY((\inst1|PSUM[8][4]~regout\ & ((!\inst1|PSUM[8][3]~153COUT1_380\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	dataa => \inst1|PSUM[8][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[8][3]~153\,
	cin1 => \inst1|PSUM[8][3]~153COUT1_380\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][4]~regout\,
	cout => \inst1|PSUM[8][4]~151\);

-- Location: LC_X6_Y6_N5
\inst1|PSUM[8][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][5]~regout\ = DFFEAS(\inst1|PSUM[8][5]~regout\ $ ((((\inst1|PSUM[8][4]~151\)))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][5]~149\ = CARRY(((!\inst1|PSUM[8][4]~151\)) # (!\inst1|PSUM[8][5]~regout\))
-- \inst1|PSUM[8][5]~149COUT1_381\ = CARRY(((!\inst1|PSUM[8][4]~151\)) # (!\inst1|PSUM[8][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	dataa => \inst1|PSUM[8][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[8][4]~151\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][5]~regout\,
	cout0 => \inst1|PSUM[8][5]~149\,
	cout1 => \inst1|PSUM[8][5]~149COUT1_381\);

-- Location: LC_X6_Y6_N6
\inst1|PSUM[8][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][6]~regout\ = DFFEAS(\inst1|PSUM[8][6]~regout\ $ ((((!(!\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][5]~149\) # (\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][5]~149COUT1_381\))))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][6]~147\ = CARRY((\inst1|PSUM[8][6]~regout\ & ((!\inst1|PSUM[8][5]~149\))))
-- \inst1|PSUM[8][6]~147COUT1_382\ = CARRY((\inst1|PSUM[8][6]~regout\ & ((!\inst1|PSUM[8][5]~149COUT1_381\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	dataa => \inst1|PSUM[8][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[8][4]~151\,
	cin0 => \inst1|PSUM[8][5]~149\,
	cin1 => \inst1|PSUM[8][5]~149COUT1_381\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][6]~regout\,
	cout0 => \inst1|PSUM[8][6]~147\,
	cout1 => \inst1|PSUM[8][6]~147COUT1_382\);

-- Location: LC_X6_Y6_N7
\inst1|PSUM[8][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][7]~regout\ = DFFEAS((\inst1|PSUM[8][7]~regout\ $ (((!\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][6]~147\) # (\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][6]~147COUT1_382\)))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][7]~145\ = CARRY(((!\inst1|PSUM[8][6]~147\) # (!\inst1|PSUM[8][7]~regout\)))
-- \inst1|PSUM[8][7]~145COUT1_383\ = CARRY(((!\inst1|PSUM[8][6]~147COUT1_382\) # (!\inst1|PSUM[8][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	datab => \inst1|PSUM[8][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[8][4]~151\,
	cin0 => \inst1|PSUM[8][6]~147\,
	cin1 => \inst1|PSUM[8][6]~147COUT1_382\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][7]~regout\,
	cout0 => \inst1|PSUM[8][7]~145\,
	cout1 => \inst1|PSUM[8][7]~145COUT1_383\);

-- Location: LC_X6_Y6_N8
\inst1|PSUM[8][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][8]~regout\ = DFFEAS(\inst1|PSUM[8][8]~regout\ $ ((((!(!\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][7]~145\) # (\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][7]~145COUT1_383\))))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[8][8]~143\ = CARRY((\inst1|PSUM[8][8]~regout\ & ((!\inst1|PSUM[8][7]~145\))))
-- \inst1|PSUM[8][8]~143COUT1_384\ = CARRY((\inst1|PSUM[8][8]~regout\ & ((!\inst1|PSUM[8][7]~145COUT1_383\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	dataa => \inst1|PSUM[8][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[8][4]~151\,
	cin0 => \inst1|PSUM[8][7]~145\,
	cin1 => \inst1|PSUM[8][7]~145COUT1_383\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][8]~regout\,
	cout0 => \inst1|PSUM[8][8]~143\,
	cout1 => \inst1|PSUM[8][8]~143COUT1_384\);

-- Location: LC_X6_Y6_N9
\inst1|PSUM[8][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[8][9]~regout\ = DFFEAS((((!\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][8]~143\) # (\inst1|PSUM[8][4]~151\ & \inst1|PSUM[8][8]~143COUT1_384\) $ (\inst1|PSUM[8][9]~regout\))), \inst6|STEMP\(8), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(8),
	datad => \inst1|PSUM[8][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[8][4]~151\,
	cin0 => \inst1|PSUM[8][8]~143\,
	cin1 => \inst1|PSUM[8][8]~143COUT1_384\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[8][9]~regout\);

-- Location: LC_X8_Y7_N2
\inst1|TSUM[6][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][9]~combout\ = LCELL((((\inst1|Add6~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][9]~combout\);

-- Location: LC_X6_Y8_N1
\inst1|TSUM[6][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][4]~combout\ = LCELL((((\inst1|Add6~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][4]~combout\);

-- Location: LC_X6_Y7_N9
\inst1|TSUM[6][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][3]~combout\ = LCELL((((\inst1|Add6~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][3]~combout\);

-- Location: LC_X6_Y8_N0
\inst1|TSUM[6][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][2]~combout\ = LCELL((((\inst1|Add6~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][2]~combout\);

-- Location: LC_X6_Y7_N8
\inst1|TSUM[6][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][1]~combout\ = LCELL((((\inst1|Add6~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][1]~combout\);

-- Location: LC_X6_Y8_N2
\inst1|TSUM[6][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][0]~combout\ = LCELL((((\inst1|Add6~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][0]~combout\);

-- Location: LC_X7_Y6_N5
\inst1|Add7~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~150_combout\ = \inst1|PSUM[8][0]~regout\ $ ((\inst1|TSUM[6][0]~combout\))
-- \inst1|Add7~152\ = CARRY((\inst1|PSUM[8][0]~regout\ & (\inst1|TSUM[6][0]~combout\)))
-- \inst1|Add7~152COUT1_156\ = CARRY((\inst1|PSUM[8][0]~regout\ & (\inst1|TSUM[6][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[8][0]~regout\,
	datab => \inst1|TSUM[6][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~150_combout\,
	cout0 => \inst1|Add7~152\,
	cout1 => \inst1|Add7~152COUT1_156\);

-- Location: LC_X7_Y6_N6
\inst1|Add7~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~145_combout\ = \inst1|PSUM[8][1]~regout\ $ (\inst1|TSUM[6][1]~combout\ $ ((\inst1|Add7~152\)))
-- \inst1|Add7~147\ = CARRY((\inst1|PSUM[8][1]~regout\ & (!\inst1|TSUM[6][1]~combout\ & !\inst1|Add7~152\)) # (!\inst1|PSUM[8][1]~regout\ & ((!\inst1|Add7~152\) # (!\inst1|TSUM[6][1]~combout\))))
-- \inst1|Add7~147COUT1_157\ = CARRY((\inst1|PSUM[8][1]~regout\ & (!\inst1|TSUM[6][1]~combout\ & !\inst1|Add7~152COUT1_156\)) # (!\inst1|PSUM[8][1]~regout\ & ((!\inst1|Add7~152COUT1_156\) # (!\inst1|TSUM[6][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[8][1]~regout\,
	datab => \inst1|TSUM[6][1]~combout\,
	cin0 => \inst1|Add7~152\,
	cin1 => \inst1|Add7~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~145_combout\,
	cout0 => \inst1|Add7~147\,
	cout1 => \inst1|Add7~147COUT1_157\);

-- Location: LC_X7_Y6_N7
\inst1|Add7~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~140_combout\ = \inst1|TSUM[6][2]~combout\ $ (\inst1|PSUM[8][2]~regout\ $ ((!\inst1|Add7~147\)))
-- \inst1|Add7~142\ = CARRY((\inst1|TSUM[6][2]~combout\ & ((\inst1|PSUM[8][2]~regout\) # (!\inst1|Add7~147\))) # (!\inst1|TSUM[6][2]~combout\ & (\inst1|PSUM[8][2]~regout\ & !\inst1|Add7~147\)))
-- \inst1|Add7~142COUT1_158\ = CARRY((\inst1|TSUM[6][2]~combout\ & ((\inst1|PSUM[8][2]~regout\) # (!\inst1|Add7~147COUT1_157\))) # (!\inst1|TSUM[6][2]~combout\ & (\inst1|PSUM[8][2]~regout\ & !\inst1|Add7~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][2]~combout\,
	datab => \inst1|PSUM[8][2]~regout\,
	cin0 => \inst1|Add7~147\,
	cin1 => \inst1|Add7~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~140_combout\,
	cout0 => \inst1|Add7~142\,
	cout1 => \inst1|Add7~142COUT1_158\);

-- Location: LC_X7_Y6_N8
\inst1|Add7~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~135_combout\ = \inst1|PSUM[8][3]~regout\ $ (\inst1|TSUM[6][3]~combout\ $ ((\inst1|Add7~142\)))
-- \inst1|Add7~137\ = CARRY((\inst1|PSUM[8][3]~regout\ & (!\inst1|TSUM[6][3]~combout\ & !\inst1|Add7~142\)) # (!\inst1|PSUM[8][3]~regout\ & ((!\inst1|Add7~142\) # (!\inst1|TSUM[6][3]~combout\))))
-- \inst1|Add7~137COUT1_159\ = CARRY((\inst1|PSUM[8][3]~regout\ & (!\inst1|TSUM[6][3]~combout\ & !\inst1|Add7~142COUT1_158\)) # (!\inst1|PSUM[8][3]~regout\ & ((!\inst1|Add7~142COUT1_158\) # (!\inst1|TSUM[6][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[8][3]~regout\,
	datab => \inst1|TSUM[6][3]~combout\,
	cin0 => \inst1|Add7~142\,
	cin1 => \inst1|Add7~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~135_combout\,
	cout0 => \inst1|Add7~137\,
	cout1 => \inst1|Add7~137COUT1_159\);

-- Location: LC_X7_Y6_N9
\inst1|Add7~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~130_combout\ = \inst1|TSUM[6][4]~combout\ $ (\inst1|PSUM[8][4]~regout\ $ ((!\inst1|Add7~137\)))
-- \inst1|Add7~132\ = CARRY((\inst1|TSUM[6][4]~combout\ & ((\inst1|PSUM[8][4]~regout\) # (!\inst1|Add7~137COUT1_159\))) # (!\inst1|TSUM[6][4]~combout\ & (\inst1|PSUM[8][4]~regout\ & !\inst1|Add7~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][4]~combout\,
	datab => \inst1|PSUM[8][4]~regout\,
	cin0 => \inst1|Add7~137\,
	cin1 => \inst1|Add7~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~130_combout\,
	cout => \inst1|Add7~132\);

-- Location: LC_X7_Y6_N1
\inst1|TSUM[6][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][8]~combout\ = LCELL((((\inst1|Add6~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][8]~combout\);

-- Location: LC_X8_Y7_N9
\inst1|TSUM[6][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][7]~combout\ = LCELL((((\inst1|Add6~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][7]~combout\);

-- Location: LC_X7_Y6_N0
\inst1|TSUM[6][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][6]~combout\ = LCELL((((\inst1|Add6~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][6]~combout\);

-- Location: LC_X7_Y6_N4
\inst1|TSUM[6][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][5]~combout\ = LCELL((((\inst1|Add6~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][5]~combout\);

-- Location: LC_X8_Y6_N0
\inst1|Add7~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~125_combout\ = \inst1|PSUM[8][5]~regout\ $ (\inst1|TSUM[6][5]~combout\ $ ((\inst1|Add7~132\)))
-- \inst1|Add7~127\ = CARRY((\inst1|PSUM[8][5]~regout\ & (!\inst1|TSUM[6][5]~combout\ & !\inst1|Add7~132\)) # (!\inst1|PSUM[8][5]~regout\ & ((!\inst1|Add7~132\) # (!\inst1|TSUM[6][5]~combout\))))
-- \inst1|Add7~127COUT1_160\ = CARRY((\inst1|PSUM[8][5]~regout\ & (!\inst1|TSUM[6][5]~combout\ & !\inst1|Add7~132\)) # (!\inst1|PSUM[8][5]~regout\ & ((!\inst1|Add7~132\) # (!\inst1|TSUM[6][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[8][5]~regout\,
	datab => \inst1|TSUM[6][5]~combout\,
	cin => \inst1|Add7~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~125_combout\,
	cout0 => \inst1|Add7~127\,
	cout1 => \inst1|Add7~127COUT1_160\);

-- Location: LC_X8_Y6_N1
\inst1|Add7~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~120_combout\ = \inst1|TSUM[6][6]~combout\ $ (\inst1|PSUM[8][6]~regout\ $ ((!(!\inst1|Add7~132\ & \inst1|Add7~127\) # (\inst1|Add7~132\ & \inst1|Add7~127COUT1_160\))))
-- \inst1|Add7~122\ = CARRY((\inst1|TSUM[6][6]~combout\ & ((\inst1|PSUM[8][6]~regout\) # (!\inst1|Add7~127\))) # (!\inst1|TSUM[6][6]~combout\ & (\inst1|PSUM[8][6]~regout\ & !\inst1|Add7~127\)))
-- \inst1|Add7~122COUT1_161\ = CARRY((\inst1|TSUM[6][6]~combout\ & ((\inst1|PSUM[8][6]~regout\) # (!\inst1|Add7~127COUT1_160\))) # (!\inst1|TSUM[6][6]~combout\ & (\inst1|PSUM[8][6]~regout\ & !\inst1|Add7~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][6]~combout\,
	datab => \inst1|PSUM[8][6]~regout\,
	cin => \inst1|Add7~132\,
	cin0 => \inst1|Add7~127\,
	cin1 => \inst1|Add7~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~120_combout\,
	cout0 => \inst1|Add7~122\,
	cout1 => \inst1|Add7~122COUT1_161\);

-- Location: LC_X8_Y6_N2
\inst1|Add7~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~115_combout\ = \inst1|TSUM[6][7]~combout\ $ (\inst1|PSUM[8][7]~regout\ $ (((!\inst1|Add7~132\ & \inst1|Add7~122\) # (\inst1|Add7~132\ & \inst1|Add7~122COUT1_161\))))
-- \inst1|Add7~117\ = CARRY((\inst1|TSUM[6][7]~combout\ & (!\inst1|PSUM[8][7]~regout\ & !\inst1|Add7~122\)) # (!\inst1|TSUM[6][7]~combout\ & ((!\inst1|Add7~122\) # (!\inst1|PSUM[8][7]~regout\))))
-- \inst1|Add7~117COUT1_162\ = CARRY((\inst1|TSUM[6][7]~combout\ & (!\inst1|PSUM[8][7]~regout\ & !\inst1|Add7~122COUT1_161\)) # (!\inst1|TSUM[6][7]~combout\ & ((!\inst1|Add7~122COUT1_161\) # (!\inst1|PSUM[8][7]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][7]~combout\,
	datab => \inst1|PSUM[8][7]~regout\,
	cin => \inst1|Add7~132\,
	cin0 => \inst1|Add7~122\,
	cin1 => \inst1|Add7~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~115_combout\,
	cout0 => \inst1|Add7~117\,
	cout1 => \inst1|Add7~117COUT1_162\);

-- Location: LC_X8_Y6_N3
\inst1|Add7~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~110_combout\ = \inst1|PSUM[8][8]~regout\ $ (\inst1|TSUM[6][8]~combout\ $ ((!(!\inst1|Add7~132\ & \inst1|Add7~117\) # (\inst1|Add7~132\ & \inst1|Add7~117COUT1_162\))))
-- \inst1|Add7~112\ = CARRY((\inst1|PSUM[8][8]~regout\ & ((\inst1|TSUM[6][8]~combout\) # (!\inst1|Add7~117\))) # (!\inst1|PSUM[8][8]~regout\ & (\inst1|TSUM[6][8]~combout\ & !\inst1|Add7~117\)))
-- \inst1|Add7~112COUT1_163\ = CARRY((\inst1|PSUM[8][8]~regout\ & ((\inst1|TSUM[6][8]~combout\) # (!\inst1|Add7~117COUT1_162\))) # (!\inst1|PSUM[8][8]~regout\ & (\inst1|TSUM[6][8]~combout\ & !\inst1|Add7~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[8][8]~regout\,
	datab => \inst1|TSUM[6][8]~combout\,
	cin => \inst1|Add7~132\,
	cin0 => \inst1|Add7~117\,
	cin1 => \inst1|Add7~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~110_combout\,
	cout0 => \inst1|Add7~112\,
	cout1 => \inst1|Add7~112COUT1_163\);

-- Location: LC_X8_Y6_N4
\inst1|Add7~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~105_combout\ = \inst1|PSUM[8][9]~regout\ $ (\inst1|TSUM[6][9]~combout\ $ (((!\inst1|Add7~132\ & \inst1|Add7~112\) # (\inst1|Add7~132\ & \inst1|Add7~112COUT1_163\))))
-- \inst1|Add7~107\ = CARRY((\inst1|PSUM[8][9]~regout\ & (!\inst1|TSUM[6][9]~combout\ & !\inst1|Add7~112COUT1_163\)) # (!\inst1|PSUM[8][9]~regout\ & ((!\inst1|Add7~112COUT1_163\) # (!\inst1|TSUM[6][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[8][9]~regout\,
	datab => \inst1|TSUM[6][9]~combout\,
	cin => \inst1|Add7~132\,
	cin0 => \inst1|Add7~112\,
	cin1 => \inst1|Add7~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~105_combout\,
	cout => \inst1|Add7~107\);

-- Location: LC_X8_Y7_N6
\inst1|TSUM[6][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][13]~combout\ = LCELL((((\inst1|Add6~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][13]~combout\);

-- Location: LC_X8_Y7_N1
\inst1|TSUM[6][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][12]~combout\ = LCELL((((\inst1|Add6~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][12]~combout\);

-- Location: LC_X8_Y7_N8
\inst1|TSUM[6][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][11]~combout\ = LCELL((((\inst1|Add6~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][11]~combout\);

-- Location: LC_X8_Y5_N4
\inst1|TSUM[6][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][10]~combout\ = LCELL((((\inst1|Add6~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][10]~combout\);

-- Location: LC_X8_Y6_N5
\inst1|Add7~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~100_combout\ = \inst1|TSUM[6][10]~combout\ $ ((((!\inst1|Add7~107\))))
-- \inst1|Add7~102\ = CARRY((\inst1|TSUM[6][10]~combout\ & ((!\inst1|Add7~107\))))
-- \inst1|Add7~102COUT1_164\ = CARRY((\inst1|TSUM[6][10]~combout\ & ((!\inst1|Add7~107\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][10]~combout\,
	cin => \inst1|Add7~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~100_combout\,
	cout0 => \inst1|Add7~102\,
	cout1 => \inst1|Add7~102COUT1_164\);

-- Location: LC_X8_Y6_N6
\inst1|Add7~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~95_combout\ = (\inst1|TSUM[6][11]~combout\ $ (((!\inst1|Add7~107\ & \inst1|Add7~102\) # (\inst1|Add7~107\ & \inst1|Add7~102COUT1_164\))))
-- \inst1|Add7~97\ = CARRY(((!\inst1|Add7~102\) # (!\inst1|TSUM[6][11]~combout\)))
-- \inst1|Add7~97COUT1_165\ = CARRY(((!\inst1|Add7~102COUT1_164\) # (!\inst1|TSUM[6][11]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][11]~combout\,
	cin => \inst1|Add7~107\,
	cin0 => \inst1|Add7~102\,
	cin1 => \inst1|Add7~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~95_combout\,
	cout0 => \inst1|Add7~97\,
	cout1 => \inst1|Add7~97COUT1_165\);

-- Location: LC_X8_Y6_N7
\inst1|Add7~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~90_combout\ = (\inst1|TSUM[6][12]~combout\ $ ((!(!\inst1|Add7~107\ & \inst1|Add7~97\) # (\inst1|Add7~107\ & \inst1|Add7~97COUT1_165\))))
-- \inst1|Add7~92\ = CARRY(((\inst1|TSUM[6][12]~combout\ & !\inst1|Add7~97\)))
-- \inst1|Add7~92COUT1_166\ = CARRY(((\inst1|TSUM[6][12]~combout\ & !\inst1|Add7~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][12]~combout\,
	cin => \inst1|Add7~107\,
	cin0 => \inst1|Add7~97\,
	cin1 => \inst1|Add7~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~90_combout\,
	cout0 => \inst1|Add7~92\,
	cout1 => \inst1|Add7~92COUT1_166\);

-- Location: LC_X8_Y6_N8
\inst1|Add7~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~85_combout\ = (\inst1|TSUM[6][13]~combout\ $ (((!\inst1|Add7~107\ & \inst1|Add7~92\) # (\inst1|Add7~107\ & \inst1|Add7~92COUT1_166\))))
-- \inst1|Add7~87\ = CARRY(((!\inst1|Add7~92\) # (!\inst1|TSUM[6][13]~combout\)))
-- \inst1|Add7~87COUT1_167\ = CARRY(((!\inst1|Add7~92COUT1_166\) # (!\inst1|TSUM[6][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][13]~combout\,
	cin => \inst1|Add7~107\,
	cin0 => \inst1|Add7~92\,
	cin1 => \inst1|Add7~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~85_combout\,
	cout0 => \inst1|Add7~87\,
	cout1 => \inst1|Add7~87COUT1_167\);

-- Location: LC_X8_Y6_N9
\inst1|Add7~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~80_combout\ = \inst1|TSUM[6][14]~combout\ $ ((((!(!\inst1|Add7~107\ & \inst1|Add7~87\) # (\inst1|Add7~107\ & \inst1|Add7~87COUT1_167\)))))
-- \inst1|Add7~82\ = CARRY((\inst1|TSUM[6][14]~combout\ & ((!\inst1|Add7~87COUT1_167\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][14]~combout\,
	cin => \inst1|Add7~107\,
	cin0 => \inst1|Add7~87\,
	cin1 => \inst1|Add7~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~80_combout\,
	cout => \inst1|Add7~82\);

-- Location: LC_X8_Y7_N5
\inst1|TSUM[6][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][18]~combout\ = LCELL((((\inst1|Add6~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][18]~combout\);

-- Location: LC_X9_Y7_N2
\inst1|TSUM[6][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][17]~combout\ = LCELL((((\inst1|Add6~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][17]~combout\);

-- Location: LC_X9_Y7_N0
\inst1|TSUM[6][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][16]~combout\ = LCELL((((\inst1|Add6~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][16]~combout\);

-- Location: LC_X9_Y7_N3
\inst1|TSUM[6][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][15]~combout\ = LCELL((((\inst1|Add6~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][15]~combout\);

-- Location: LC_X9_Y6_N0
\inst1|Add7~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~75_combout\ = \inst1|TSUM[6][15]~combout\ $ ((((\inst1|Add7~82\))))
-- \inst1|Add7~77\ = CARRY(((!\inst1|Add7~82\)) # (!\inst1|TSUM[6][15]~combout\))
-- \inst1|Add7~77COUT1_168\ = CARRY(((!\inst1|Add7~82\)) # (!\inst1|TSUM[6][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][15]~combout\,
	cin => \inst1|Add7~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~75_combout\,
	cout0 => \inst1|Add7~77\,
	cout1 => \inst1|Add7~77COUT1_168\);

-- Location: LC_X9_Y6_N1
\inst1|Add7~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~70_combout\ = \inst1|TSUM[6][16]~combout\ $ ((((!(!\inst1|Add7~82\ & \inst1|Add7~77\) # (\inst1|Add7~82\ & \inst1|Add7~77COUT1_168\)))))
-- \inst1|Add7~72\ = CARRY((\inst1|TSUM[6][16]~combout\ & ((!\inst1|Add7~77\))))
-- \inst1|Add7~72COUT1_169\ = CARRY((\inst1|TSUM[6][16]~combout\ & ((!\inst1|Add7~77COUT1_168\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][16]~combout\,
	cin => \inst1|Add7~82\,
	cin0 => \inst1|Add7~77\,
	cin1 => \inst1|Add7~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~70_combout\,
	cout0 => \inst1|Add7~72\,
	cout1 => \inst1|Add7~72COUT1_169\);

-- Location: LC_X9_Y6_N2
\inst1|Add7~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~65_combout\ = (\inst1|TSUM[6][17]~combout\ $ (((!\inst1|Add7~82\ & \inst1|Add7~72\) # (\inst1|Add7~82\ & \inst1|Add7~72COUT1_169\))))
-- \inst1|Add7~67\ = CARRY(((!\inst1|Add7~72\) # (!\inst1|TSUM[6][17]~combout\)))
-- \inst1|Add7~67COUT1_170\ = CARRY(((!\inst1|Add7~72COUT1_169\) # (!\inst1|TSUM[6][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][17]~combout\,
	cin => \inst1|Add7~82\,
	cin0 => \inst1|Add7~72\,
	cin1 => \inst1|Add7~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~65_combout\,
	cout0 => \inst1|Add7~67\,
	cout1 => \inst1|Add7~67COUT1_170\);

-- Location: LC_X9_Y6_N3
\inst1|Add7~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~60_combout\ = (\inst1|TSUM[6][18]~combout\ $ ((!(!\inst1|Add7~82\ & \inst1|Add7~67\) # (\inst1|Add7~82\ & \inst1|Add7~67COUT1_170\))))
-- \inst1|Add7~62\ = CARRY(((\inst1|TSUM[6][18]~combout\ & !\inst1|Add7~67\)))
-- \inst1|Add7~62COUT1_171\ = CARRY(((\inst1|TSUM[6][18]~combout\ & !\inst1|Add7~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][18]~combout\,
	cin => \inst1|Add7~82\,
	cin0 => \inst1|Add7~67\,
	cin1 => \inst1|Add7~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~60_combout\,
	cout0 => \inst1|Add7~62\,
	cout1 => \inst1|Add7~62COUT1_171\);

-- Location: LC_X9_Y6_N4
\inst1|Add7~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~55_combout\ = \inst1|TSUM[6][19]~combout\ $ (((((!\inst1|Add7~82\ & \inst1|Add7~62\) # (\inst1|Add7~82\ & \inst1|Add7~62COUT1_171\)))))
-- \inst1|Add7~57\ = CARRY(((!\inst1|Add7~62COUT1_171\)) # (!\inst1|TSUM[6][19]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][19]~combout\,
	cin => \inst1|Add7~82\,
	cin0 => \inst1|Add7~62\,
	cin1 => \inst1|Add7~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~55_combout\,
	cout => \inst1|Add7~57\);

-- Location: LC_X9_Y7_N8
\inst1|TSUM[6][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][23]~combout\ = LCELL((((\inst1|Add6~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][23]~combout\);

-- Location: LC_X8_Y7_N0
\inst1|TSUM[6][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][22]~combout\ = LCELL((((\inst1|Add6~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][22]~combout\);

-- Location: LC_X9_Y7_N6
\inst1|TSUM[6][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][21]~combout\ = LCELL((((\inst1|Add6~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][21]~combout\);

-- Location: LC_X9_Y7_N7
\inst1|TSUM[6][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][20]~combout\ = LCELL((((\inst1|Add6~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][20]~combout\);

-- Location: LC_X9_Y6_N5
\inst1|Add7~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~50_combout\ = \inst1|TSUM[6][20]~combout\ $ ((((!\inst1|Add7~57\))))
-- \inst1|Add7~52\ = CARRY((\inst1|TSUM[6][20]~combout\ & ((!\inst1|Add7~57\))))
-- \inst1|Add7~52COUT1_172\ = CARRY((\inst1|TSUM[6][20]~combout\ & ((!\inst1|Add7~57\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][20]~combout\,
	cin => \inst1|Add7~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~50_combout\,
	cout0 => \inst1|Add7~52\,
	cout1 => \inst1|Add7~52COUT1_172\);

-- Location: LC_X9_Y6_N6
\inst1|Add7~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~45_combout\ = (\inst1|TSUM[6][21]~combout\ $ (((!\inst1|Add7~57\ & \inst1|Add7~52\) # (\inst1|Add7~57\ & \inst1|Add7~52COUT1_172\))))
-- \inst1|Add7~47\ = CARRY(((!\inst1|Add7~52\) # (!\inst1|TSUM[6][21]~combout\)))
-- \inst1|Add7~47COUT1_173\ = CARRY(((!\inst1|Add7~52COUT1_172\) # (!\inst1|TSUM[6][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][21]~combout\,
	cin => \inst1|Add7~57\,
	cin0 => \inst1|Add7~52\,
	cin1 => \inst1|Add7~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~45_combout\,
	cout0 => \inst1|Add7~47\,
	cout1 => \inst1|Add7~47COUT1_173\);

-- Location: LC_X9_Y6_N7
\inst1|Add7~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~40_combout\ = \inst1|TSUM[6][22]~combout\ $ ((((!(!\inst1|Add7~57\ & \inst1|Add7~47\) # (\inst1|Add7~57\ & \inst1|Add7~47COUT1_173\)))))
-- \inst1|Add7~42\ = CARRY((\inst1|TSUM[6][22]~combout\ & ((!\inst1|Add7~47\))))
-- \inst1|Add7~42COUT1_174\ = CARRY((\inst1|TSUM[6][22]~combout\ & ((!\inst1|Add7~47COUT1_173\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][22]~combout\,
	cin => \inst1|Add7~57\,
	cin0 => \inst1|Add7~47\,
	cin1 => \inst1|Add7~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~40_combout\,
	cout0 => \inst1|Add7~42\,
	cout1 => \inst1|Add7~42COUT1_174\);

-- Location: LC_X9_Y6_N8
\inst1|Add7~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~35_combout\ = (\inst1|TSUM[6][23]~combout\ $ (((!\inst1|Add7~57\ & \inst1|Add7~42\) # (\inst1|Add7~57\ & \inst1|Add7~42COUT1_174\))))
-- \inst1|Add7~37\ = CARRY(((!\inst1|Add7~42\) # (!\inst1|TSUM[6][23]~combout\)))
-- \inst1|Add7~37COUT1_175\ = CARRY(((!\inst1|Add7~42COUT1_174\) # (!\inst1|TSUM[6][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][23]~combout\,
	cin => \inst1|Add7~57\,
	cin0 => \inst1|Add7~42\,
	cin1 => \inst1|Add7~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~35_combout\,
	cout0 => \inst1|Add7~37\,
	cout1 => \inst1|Add7~37COUT1_175\);

-- Location: LC_X9_Y6_N9
\inst1|Add7~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~30_combout\ = (\inst1|TSUM[6][24]~combout\ $ ((!(!\inst1|Add7~57\ & \inst1|Add7~37\) # (\inst1|Add7~57\ & \inst1|Add7~37COUT1_175\))))
-- \inst1|Add7~32\ = CARRY(((\inst1|TSUM[6][24]~combout\ & !\inst1|Add7~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][24]~combout\,
	cin => \inst1|Add7~57\,
	cin0 => \inst1|Add7~37\,
	cin1 => \inst1|Add7~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~30_combout\,
	cout => \inst1|Add7~32\);

-- Location: LC_X10_Y7_N5
\inst1|TSUM[6][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][28]~combout\ = LCELL((((\inst1|Add6~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][28]~combout\);

-- Location: LC_X10_Y7_N3
\inst1|TSUM[6][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][27]~combout\ = LCELL((((\inst1|Add6~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][27]~combout\);

-- Location: LC_X9_Y8_N7
\inst1|TSUM[6][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][26]~combout\ = LCELL((((\inst1|Add6~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add6~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][26]~combout\);

-- Location: LC_X9_Y8_N6
\inst1|TSUM[6][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[6][25]~combout\ = LCELL((((\inst1|Add6~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add6~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[6][25]~combout\);

-- Location: LC_X10_Y6_N0
\inst1|Add7~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~25_combout\ = (\inst1|TSUM[6][25]~combout\ $ ((\inst1|Add7~32\)))
-- \inst1|Add7~27\ = CARRY(((!\inst1|Add7~32\) # (!\inst1|TSUM[6][25]~combout\)))
-- \inst1|Add7~27COUT1_176\ = CARRY(((!\inst1|Add7~32\) # (!\inst1|TSUM[6][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][25]~combout\,
	cin => \inst1|Add7~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~25_combout\,
	cout0 => \inst1|Add7~27\,
	cout1 => \inst1|Add7~27COUT1_176\);

-- Location: LC_X10_Y6_N1
\inst1|Add7~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~20_combout\ = (\inst1|TSUM[6][26]~combout\ $ ((!(!\inst1|Add7~32\ & \inst1|Add7~27\) # (\inst1|Add7~32\ & \inst1|Add7~27COUT1_176\))))
-- \inst1|Add7~22\ = CARRY(((\inst1|TSUM[6][26]~combout\ & !\inst1|Add7~27\)))
-- \inst1|Add7~22COUT1_177\ = CARRY(((\inst1|TSUM[6][26]~combout\ & !\inst1|Add7~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][26]~combout\,
	cin => \inst1|Add7~32\,
	cin0 => \inst1|Add7~27\,
	cin1 => \inst1|Add7~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~20_combout\,
	cout0 => \inst1|Add7~22\,
	cout1 => \inst1|Add7~22COUT1_177\);

-- Location: LC_X10_Y6_N2
\inst1|Add7~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~15_combout\ = (\inst1|TSUM[6][27]~combout\ $ (((!\inst1|Add7~32\ & \inst1|Add7~22\) # (\inst1|Add7~32\ & \inst1|Add7~22COUT1_177\))))
-- \inst1|Add7~17\ = CARRY(((!\inst1|Add7~22\) # (!\inst1|TSUM[6][27]~combout\)))
-- \inst1|Add7~17COUT1_178\ = CARRY(((!\inst1|Add7~22COUT1_177\) # (!\inst1|TSUM[6][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][27]~combout\,
	cin => \inst1|Add7~32\,
	cin0 => \inst1|Add7~22\,
	cin1 => \inst1|Add7~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~15_combout\,
	cout0 => \inst1|Add7~17\,
	cout1 => \inst1|Add7~17COUT1_178\);

-- Location: LC_X10_Y6_N3
\inst1|Add7~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~10_combout\ = (\inst1|TSUM[6][28]~combout\ $ ((!(!\inst1|Add7~32\ & \inst1|Add7~17\) # (\inst1|Add7~32\ & \inst1|Add7~17COUT1_178\))))
-- \inst1|Add7~12\ = CARRY(((\inst1|TSUM[6][28]~combout\ & !\inst1|Add7~17\)))
-- \inst1|Add7~12COUT1_179\ = CARRY(((\inst1|TSUM[6][28]~combout\ & !\inst1|Add7~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][28]~combout\,
	cin => \inst1|Add7~32\,
	cin0 => \inst1|Add7~17\,
	cin1 => \inst1|Add7~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~10_combout\,
	cout0 => \inst1|Add7~12\,
	cout1 => \inst1|Add7~12COUT1_179\);

-- Location: LC_X10_Y6_N4
\inst1|Add7~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~5_combout\ = \inst1|TSUM[6][29]~combout\ $ (((((!\inst1|Add7~32\ & \inst1|Add7~12\) # (\inst1|Add7~32\ & \inst1|Add7~12COUT1_179\)))))
-- \inst1|Add7~7\ = CARRY(((!\inst1|Add7~12COUT1_179\)) # (!\inst1|TSUM[6][29]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[6][29]~combout\,
	cin => \inst1|Add7~32\,
	cin0 => \inst1|Add7~12\,
	cin1 => \inst1|Add7~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~5_combout\,
	cout => \inst1|Add7~7\);

-- Location: LC_X10_Y6_N5
\inst1|Add7~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add7~0_combout\ = (\inst1|TSUM[6][30]~combout\ $ ((!\inst1|Add7~7\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c3c3",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[6][30]~combout\,
	cin => \inst1|Add7~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add7~0_combout\);

-- Location: LC_X10_Y6_N8
\inst1|TSUM[7][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][30]~combout\ = LCELL((((\inst1|Add7~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][30]~combout\);

-- Location: LC_X10_Y6_N6
\inst1|TSUM[7][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][29]~combout\ = LCELL((((\inst1|Add7~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add7~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][29]~combout\);

-- Location: LC_X9_Y5_N8
\inst1|TSUM[7][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][24]~combout\ = LCELL((((\inst1|Add7~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add7~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][24]~combout\);

-- Location: LC_X9_Y5_N2
\inst1|TSUM[7][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][19]~combout\ = LCELL((((\inst1|Add7~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][19]~combout\);

-- Location: LC_X8_Y5_N7
\inst1|TSUM[7][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][14]~combout\ = LCELL((((\inst1|Add7~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][14]~combout\);

-- Location: LC_X6_Y5_N0
\inst1|PSUM[9][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][0]~regout\ = DFFEAS(((!\inst1|PSUM[9][0]~regout\)), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][0]~139\ = CARRY(((\inst1|PSUM[9][0]~regout\)))
-- \inst1|PSUM[9][0]~139COUT1_369\ = CARRY(((\inst1|PSUM[9][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	datab => \inst1|PSUM[9][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][0]~regout\,
	cout0 => \inst1|PSUM[9][0]~139\,
	cout1 => \inst1|PSUM[9][0]~139COUT1_369\);

-- Location: LC_X6_Y5_N1
\inst1|PSUM[9][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][1]~regout\ = DFFEAS((\inst1|PSUM[9][1]~regout\ $ ((\inst1|PSUM[9][0]~139\))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][1]~137\ = CARRY(((!\inst1|PSUM[9][0]~139\) # (!\inst1|PSUM[9][1]~regout\)))
-- \inst1|PSUM[9][1]~137COUT1_370\ = CARRY(((!\inst1|PSUM[9][0]~139COUT1_369\) # (!\inst1|PSUM[9][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	datab => \inst1|PSUM[9][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[9][0]~139\,
	cin1 => \inst1|PSUM[9][0]~139COUT1_369\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][1]~regout\,
	cout0 => \inst1|PSUM[9][1]~137\,
	cout1 => \inst1|PSUM[9][1]~137COUT1_370\);

-- Location: LC_X6_Y5_N2
\inst1|PSUM[9][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][2]~regout\ = DFFEAS((\inst1|PSUM[9][2]~regout\ $ ((!\inst1|PSUM[9][1]~137\))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][2]~135\ = CARRY(((\inst1|PSUM[9][2]~regout\ & !\inst1|PSUM[9][1]~137\)))
-- \inst1|PSUM[9][2]~135COUT1_371\ = CARRY(((\inst1|PSUM[9][2]~regout\ & !\inst1|PSUM[9][1]~137COUT1_370\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	datab => \inst1|PSUM[9][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[9][1]~137\,
	cin1 => \inst1|PSUM[9][1]~137COUT1_370\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][2]~regout\,
	cout0 => \inst1|PSUM[9][2]~135\,
	cout1 => \inst1|PSUM[9][2]~135COUT1_371\);

-- Location: LC_X6_Y5_N3
\inst1|PSUM[9][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][3]~regout\ = DFFEAS(\inst1|PSUM[9][3]~regout\ $ ((((\inst1|PSUM[9][2]~135\)))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][3]~133\ = CARRY(((!\inst1|PSUM[9][2]~135\)) # (!\inst1|PSUM[9][3]~regout\))
-- \inst1|PSUM[9][3]~133COUT1_372\ = CARRY(((!\inst1|PSUM[9][2]~135COUT1_371\)) # (!\inst1|PSUM[9][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	dataa => \inst1|PSUM[9][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[9][2]~135\,
	cin1 => \inst1|PSUM[9][2]~135COUT1_371\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][3]~regout\,
	cout0 => \inst1|PSUM[9][3]~133\,
	cout1 => \inst1|PSUM[9][3]~133COUT1_372\);

-- Location: LC_X6_Y5_N4
\inst1|PSUM[9][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][4]~regout\ = DFFEAS(\inst1|PSUM[9][4]~regout\ $ ((((!\inst1|PSUM[9][3]~133\)))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][4]~131\ = CARRY((\inst1|PSUM[9][4]~regout\ & ((!\inst1|PSUM[9][3]~133COUT1_372\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	dataa => \inst1|PSUM[9][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[9][3]~133\,
	cin1 => \inst1|PSUM[9][3]~133COUT1_372\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][4]~regout\,
	cout => \inst1|PSUM[9][4]~131\);

-- Location: LC_X6_Y5_N5
\inst1|PSUM[9][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][5]~regout\ = DFFEAS(\inst1|PSUM[9][5]~regout\ $ ((((\inst1|PSUM[9][4]~131\)))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][5]~129\ = CARRY(((!\inst1|PSUM[9][4]~131\)) # (!\inst1|PSUM[9][5]~regout\))
-- \inst1|PSUM[9][5]~129COUT1_373\ = CARRY(((!\inst1|PSUM[9][4]~131\)) # (!\inst1|PSUM[9][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	dataa => \inst1|PSUM[9][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[9][4]~131\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][5]~regout\,
	cout0 => \inst1|PSUM[9][5]~129\,
	cout1 => \inst1|PSUM[9][5]~129COUT1_373\);

-- Location: LC_X6_Y5_N6
\inst1|PSUM[9][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][6]~regout\ = DFFEAS(\inst1|PSUM[9][6]~regout\ $ ((((!(!\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][5]~129\) # (\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][5]~129COUT1_373\))))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][6]~127\ = CARRY((\inst1|PSUM[9][6]~regout\ & ((!\inst1|PSUM[9][5]~129\))))
-- \inst1|PSUM[9][6]~127COUT1_374\ = CARRY((\inst1|PSUM[9][6]~regout\ & ((!\inst1|PSUM[9][5]~129COUT1_373\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	dataa => \inst1|PSUM[9][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[9][4]~131\,
	cin0 => \inst1|PSUM[9][5]~129\,
	cin1 => \inst1|PSUM[9][5]~129COUT1_373\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][6]~regout\,
	cout0 => \inst1|PSUM[9][6]~127\,
	cout1 => \inst1|PSUM[9][6]~127COUT1_374\);

-- Location: LC_X6_Y5_N7
\inst1|PSUM[9][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][7]~regout\ = DFFEAS((\inst1|PSUM[9][7]~regout\ $ (((!\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][6]~127\) # (\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][6]~127COUT1_374\)))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][7]~125\ = CARRY(((!\inst1|PSUM[9][6]~127\) # (!\inst1|PSUM[9][7]~regout\)))
-- \inst1|PSUM[9][7]~125COUT1_375\ = CARRY(((!\inst1|PSUM[9][6]~127COUT1_374\) # (!\inst1|PSUM[9][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	datab => \inst1|PSUM[9][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[9][4]~131\,
	cin0 => \inst1|PSUM[9][6]~127\,
	cin1 => \inst1|PSUM[9][6]~127COUT1_374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][7]~regout\,
	cout0 => \inst1|PSUM[9][7]~125\,
	cout1 => \inst1|PSUM[9][7]~125COUT1_375\);

-- Location: LC_X6_Y5_N8
\inst1|PSUM[9][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][8]~regout\ = DFFEAS(\inst1|PSUM[9][8]~regout\ $ ((((!(!\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][7]~125\) # (\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][7]~125COUT1_375\))))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[9][8]~123\ = CARRY((\inst1|PSUM[9][8]~regout\ & ((!\inst1|PSUM[9][7]~125\))))
-- \inst1|PSUM[9][8]~123COUT1_376\ = CARRY((\inst1|PSUM[9][8]~regout\ & ((!\inst1|PSUM[9][7]~125COUT1_375\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	dataa => \inst1|PSUM[9][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[9][4]~131\,
	cin0 => \inst1|PSUM[9][7]~125\,
	cin1 => \inst1|PSUM[9][7]~125COUT1_375\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][8]~regout\,
	cout0 => \inst1|PSUM[9][8]~123\,
	cout1 => \inst1|PSUM[9][8]~123COUT1_376\);

-- Location: LC_X6_Y5_N9
\inst1|PSUM[9][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[9][9]~regout\ = DFFEAS((((!\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][8]~123\) # (\inst1|PSUM[9][4]~131\ & \inst1|PSUM[9][8]~123COUT1_376\) $ (\inst1|PSUM[9][9]~regout\))), \inst6|STEMP\(9), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(9),
	datad => \inst1|PSUM[9][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[9][4]~131\,
	cin0 => \inst1|PSUM[9][8]~123\,
	cin1 => \inst1|PSUM[9][8]~123COUT1_376\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[9][9]~regout\);

-- Location: LC_X8_Y5_N8
\inst1|TSUM[7][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][9]~combout\ = LCELL((((\inst1|Add7~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][9]~combout\);

-- Location: LC_X7_Y4_N1
\inst1|TSUM[7][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][4]~combout\ = LCELL((((\inst1|Add7~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][4]~combout\);

-- Location: LC_X7_Y5_N5
\inst1|TSUM[7][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][3]~combout\ = LCELL((((\inst1|Add7~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][3]~combout\);

-- Location: LC_X7_Y4_N2
\inst1|TSUM[7][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][2]~combout\ = LCELL((((\inst1|Add7~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][2]~combout\);

-- Location: LC_X7_Y5_N6
\inst1|TSUM[7][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][1]~combout\ = LCELL((((\inst1|Add7~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][1]~combout\);

-- Location: LC_X7_Y5_N8
\inst1|TSUM[7][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][0]~combout\ = LCELL((((\inst1|Add7~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][0]~combout\);

-- Location: LC_X7_Y4_N5
\inst1|Add8~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~150_combout\ = \inst1|PSUM[9][0]~regout\ $ ((\inst1|TSUM[7][0]~combout\))
-- \inst1|Add8~152\ = CARRY((\inst1|PSUM[9][0]~regout\ & (\inst1|TSUM[7][0]~combout\)))
-- \inst1|Add8~152COUT1_156\ = CARRY((\inst1|PSUM[9][0]~regout\ & (\inst1|TSUM[7][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[9][0]~regout\,
	datab => \inst1|TSUM[7][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~150_combout\,
	cout0 => \inst1|Add8~152\,
	cout1 => \inst1|Add8~152COUT1_156\);

-- Location: LC_X7_Y4_N6
\inst1|Add8~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~145_combout\ = \inst1|PSUM[9][1]~regout\ $ (\inst1|TSUM[7][1]~combout\ $ ((\inst1|Add8~152\)))
-- \inst1|Add8~147\ = CARRY((\inst1|PSUM[9][1]~regout\ & (!\inst1|TSUM[7][1]~combout\ & !\inst1|Add8~152\)) # (!\inst1|PSUM[9][1]~regout\ & ((!\inst1|Add8~152\) # (!\inst1|TSUM[7][1]~combout\))))
-- \inst1|Add8~147COUT1_157\ = CARRY((\inst1|PSUM[9][1]~regout\ & (!\inst1|TSUM[7][1]~combout\ & !\inst1|Add8~152COUT1_156\)) # (!\inst1|PSUM[9][1]~regout\ & ((!\inst1|Add8~152COUT1_156\) # (!\inst1|TSUM[7][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[9][1]~regout\,
	datab => \inst1|TSUM[7][1]~combout\,
	cin0 => \inst1|Add8~152\,
	cin1 => \inst1|Add8~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~145_combout\,
	cout0 => \inst1|Add8~147\,
	cout1 => \inst1|Add8~147COUT1_157\);

-- Location: LC_X7_Y4_N7
\inst1|Add8~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~140_combout\ = \inst1|PSUM[9][2]~regout\ $ (\inst1|TSUM[7][2]~combout\ $ ((!\inst1|Add8~147\)))
-- \inst1|Add8~142\ = CARRY((\inst1|PSUM[9][2]~regout\ & ((\inst1|TSUM[7][2]~combout\) # (!\inst1|Add8~147\))) # (!\inst1|PSUM[9][2]~regout\ & (\inst1|TSUM[7][2]~combout\ & !\inst1|Add8~147\)))
-- \inst1|Add8~142COUT1_158\ = CARRY((\inst1|PSUM[9][2]~regout\ & ((\inst1|TSUM[7][2]~combout\) # (!\inst1|Add8~147COUT1_157\))) # (!\inst1|PSUM[9][2]~regout\ & (\inst1|TSUM[7][2]~combout\ & !\inst1|Add8~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[9][2]~regout\,
	datab => \inst1|TSUM[7][2]~combout\,
	cin0 => \inst1|Add8~147\,
	cin1 => \inst1|Add8~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~140_combout\,
	cout0 => \inst1|Add8~142\,
	cout1 => \inst1|Add8~142COUT1_158\);

-- Location: LC_X7_Y4_N8
\inst1|Add8~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~135_combout\ = \inst1|TSUM[7][3]~combout\ $ (\inst1|PSUM[9][3]~regout\ $ ((\inst1|Add8~142\)))
-- \inst1|Add8~137\ = CARRY((\inst1|TSUM[7][3]~combout\ & (!\inst1|PSUM[9][3]~regout\ & !\inst1|Add8~142\)) # (!\inst1|TSUM[7][3]~combout\ & ((!\inst1|Add8~142\) # (!\inst1|PSUM[9][3]~regout\))))
-- \inst1|Add8~137COUT1_159\ = CARRY((\inst1|TSUM[7][3]~combout\ & (!\inst1|PSUM[9][3]~regout\ & !\inst1|Add8~142COUT1_158\)) # (!\inst1|TSUM[7][3]~combout\ & ((!\inst1|Add8~142COUT1_158\) # (!\inst1|PSUM[9][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][3]~combout\,
	datab => \inst1|PSUM[9][3]~regout\,
	cin0 => \inst1|Add8~142\,
	cin1 => \inst1|Add8~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~135_combout\,
	cout0 => \inst1|Add8~137\,
	cout1 => \inst1|Add8~137COUT1_159\);

-- Location: LC_X7_Y4_N9
\inst1|Add8~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~130_combout\ = \inst1|PSUM[9][4]~regout\ $ (\inst1|TSUM[7][4]~combout\ $ ((!\inst1|Add8~137\)))
-- \inst1|Add8~132\ = CARRY((\inst1|PSUM[9][4]~regout\ & ((\inst1|TSUM[7][4]~combout\) # (!\inst1|Add8~137COUT1_159\))) # (!\inst1|PSUM[9][4]~regout\ & (\inst1|TSUM[7][4]~combout\ & !\inst1|Add8~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[9][4]~regout\,
	datab => \inst1|TSUM[7][4]~combout\,
	cin0 => \inst1|Add8~137\,
	cin1 => \inst1|Add8~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~130_combout\,
	cout => \inst1|Add8~132\);

-- Location: LC_X8_Y5_N0
\inst1|TSUM[7][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][8]~combout\ = LCELL((((\inst1|Add7~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][8]~combout\);

-- Location: LC_X8_Y5_N9
\inst1|TSUM[7][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][7]~combout\ = LCELL((((\inst1|Add7~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add7~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][7]~combout\);

-- Location: LC_X8_Y5_N3
\inst1|TSUM[7][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][6]~combout\ = LCELL((((\inst1|Add7~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][6]~combout\);

-- Location: LC_X7_Y5_N7
\inst1|TSUM[7][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][5]~combout\ = LCELL((((\inst1|Add7~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add7~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][5]~combout\);

-- Location: LC_X8_Y4_N0
\inst1|Add8~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~125_combout\ = \inst1|TSUM[7][5]~combout\ $ (\inst1|PSUM[9][5]~regout\ $ ((\inst1|Add8~132\)))
-- \inst1|Add8~127\ = CARRY((\inst1|TSUM[7][5]~combout\ & (!\inst1|PSUM[9][5]~regout\ & !\inst1|Add8~132\)) # (!\inst1|TSUM[7][5]~combout\ & ((!\inst1|Add8~132\) # (!\inst1|PSUM[9][5]~regout\))))
-- \inst1|Add8~127COUT1_160\ = CARRY((\inst1|TSUM[7][5]~combout\ & (!\inst1|PSUM[9][5]~regout\ & !\inst1|Add8~132\)) # (!\inst1|TSUM[7][5]~combout\ & ((!\inst1|Add8~132\) # (!\inst1|PSUM[9][5]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][5]~combout\,
	datab => \inst1|PSUM[9][5]~regout\,
	cin => \inst1|Add8~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~125_combout\,
	cout0 => \inst1|Add8~127\,
	cout1 => \inst1|Add8~127COUT1_160\);

-- Location: LC_X8_Y4_N1
\inst1|Add8~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~120_combout\ = \inst1|TSUM[7][6]~combout\ $ (\inst1|PSUM[9][6]~regout\ $ ((!(!\inst1|Add8~132\ & \inst1|Add8~127\) # (\inst1|Add8~132\ & \inst1|Add8~127COUT1_160\))))
-- \inst1|Add8~122\ = CARRY((\inst1|TSUM[7][6]~combout\ & ((\inst1|PSUM[9][6]~regout\) # (!\inst1|Add8~127\))) # (!\inst1|TSUM[7][6]~combout\ & (\inst1|PSUM[9][6]~regout\ & !\inst1|Add8~127\)))
-- \inst1|Add8~122COUT1_161\ = CARRY((\inst1|TSUM[7][6]~combout\ & ((\inst1|PSUM[9][6]~regout\) # (!\inst1|Add8~127COUT1_160\))) # (!\inst1|TSUM[7][6]~combout\ & (\inst1|PSUM[9][6]~regout\ & !\inst1|Add8~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][6]~combout\,
	datab => \inst1|PSUM[9][6]~regout\,
	cin => \inst1|Add8~132\,
	cin0 => \inst1|Add8~127\,
	cin1 => \inst1|Add8~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~120_combout\,
	cout0 => \inst1|Add8~122\,
	cout1 => \inst1|Add8~122COUT1_161\);

-- Location: LC_X8_Y4_N2
\inst1|Add8~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~115_combout\ = \inst1|PSUM[9][7]~regout\ $ (\inst1|TSUM[7][7]~combout\ $ (((!\inst1|Add8~132\ & \inst1|Add8~122\) # (\inst1|Add8~132\ & \inst1|Add8~122COUT1_161\))))
-- \inst1|Add8~117\ = CARRY((\inst1|PSUM[9][7]~regout\ & (!\inst1|TSUM[7][7]~combout\ & !\inst1|Add8~122\)) # (!\inst1|PSUM[9][7]~regout\ & ((!\inst1|Add8~122\) # (!\inst1|TSUM[7][7]~combout\))))
-- \inst1|Add8~117COUT1_162\ = CARRY((\inst1|PSUM[9][7]~regout\ & (!\inst1|TSUM[7][7]~combout\ & !\inst1|Add8~122COUT1_161\)) # (!\inst1|PSUM[9][7]~regout\ & ((!\inst1|Add8~122COUT1_161\) # (!\inst1|TSUM[7][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[9][7]~regout\,
	datab => \inst1|TSUM[7][7]~combout\,
	cin => \inst1|Add8~132\,
	cin0 => \inst1|Add8~122\,
	cin1 => \inst1|Add8~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~115_combout\,
	cout0 => \inst1|Add8~117\,
	cout1 => \inst1|Add8~117COUT1_162\);

-- Location: LC_X8_Y4_N3
\inst1|Add8~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~110_combout\ = \inst1|TSUM[7][8]~combout\ $ (\inst1|PSUM[9][8]~regout\ $ ((!(!\inst1|Add8~132\ & \inst1|Add8~117\) # (\inst1|Add8~132\ & \inst1|Add8~117COUT1_162\))))
-- \inst1|Add8~112\ = CARRY((\inst1|TSUM[7][8]~combout\ & ((\inst1|PSUM[9][8]~regout\) # (!\inst1|Add8~117\))) # (!\inst1|TSUM[7][8]~combout\ & (\inst1|PSUM[9][8]~regout\ & !\inst1|Add8~117\)))
-- \inst1|Add8~112COUT1_163\ = CARRY((\inst1|TSUM[7][8]~combout\ & ((\inst1|PSUM[9][8]~regout\) # (!\inst1|Add8~117COUT1_162\))) # (!\inst1|TSUM[7][8]~combout\ & (\inst1|PSUM[9][8]~regout\ & !\inst1|Add8~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][8]~combout\,
	datab => \inst1|PSUM[9][8]~regout\,
	cin => \inst1|Add8~132\,
	cin0 => \inst1|Add8~117\,
	cin1 => \inst1|Add8~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~110_combout\,
	cout0 => \inst1|Add8~112\,
	cout1 => \inst1|Add8~112COUT1_163\);

-- Location: LC_X8_Y4_N4
\inst1|Add8~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~105_combout\ = \inst1|PSUM[9][9]~regout\ $ (\inst1|TSUM[7][9]~combout\ $ (((!\inst1|Add8~132\ & \inst1|Add8~112\) # (\inst1|Add8~132\ & \inst1|Add8~112COUT1_163\))))
-- \inst1|Add8~107\ = CARRY((\inst1|PSUM[9][9]~regout\ & (!\inst1|TSUM[7][9]~combout\ & !\inst1|Add8~112COUT1_163\)) # (!\inst1|PSUM[9][9]~regout\ & ((!\inst1|Add8~112COUT1_163\) # (!\inst1|TSUM[7][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[9][9]~regout\,
	datab => \inst1|TSUM[7][9]~combout\,
	cin => \inst1|Add8~132\,
	cin0 => \inst1|Add8~112\,
	cin1 => \inst1|Add8~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~105_combout\,
	cout => \inst1|Add8~107\);

-- Location: LC_X8_Y5_N1
\inst1|TSUM[7][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][13]~combout\ = LCELL((((\inst1|Add7~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][13]~combout\);

-- Location: LC_X8_Y5_N5
\inst1|TSUM[7][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][12]~combout\ = LCELL((((\inst1|Add7~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][12]~combout\);

-- Location: LC_X8_Y5_N2
\inst1|TSUM[7][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][11]~combout\ = LCELL((((\inst1|Add7~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][11]~combout\);

-- Location: LC_X8_Y5_N6
\inst1|TSUM[7][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][10]~combout\ = LCELL((((\inst1|Add7~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add7~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][10]~combout\);

-- Location: LC_X8_Y4_N5
\inst1|Add8~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~100_combout\ = (\inst1|TSUM[7][10]~combout\ $ ((!\inst1|Add8~107\)))
-- \inst1|Add8~102\ = CARRY(((\inst1|TSUM[7][10]~combout\ & !\inst1|Add8~107\)))
-- \inst1|Add8~102COUT1_164\ = CARRY(((\inst1|TSUM[7][10]~combout\ & !\inst1|Add8~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][10]~combout\,
	cin => \inst1|Add8~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~100_combout\,
	cout0 => \inst1|Add8~102\,
	cout1 => \inst1|Add8~102COUT1_164\);

-- Location: LC_X8_Y4_N6
\inst1|Add8~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~95_combout\ = \inst1|TSUM[7][11]~combout\ $ (((((!\inst1|Add8~107\ & \inst1|Add8~102\) # (\inst1|Add8~107\ & \inst1|Add8~102COUT1_164\)))))
-- \inst1|Add8~97\ = CARRY(((!\inst1|Add8~102\)) # (!\inst1|TSUM[7][11]~combout\))
-- \inst1|Add8~97COUT1_165\ = CARRY(((!\inst1|Add8~102COUT1_164\)) # (!\inst1|TSUM[7][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][11]~combout\,
	cin => \inst1|Add8~107\,
	cin0 => \inst1|Add8~102\,
	cin1 => \inst1|Add8~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~95_combout\,
	cout0 => \inst1|Add8~97\,
	cout1 => \inst1|Add8~97COUT1_165\);

-- Location: LC_X8_Y4_N7
\inst1|Add8~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~90_combout\ = \inst1|TSUM[7][12]~combout\ $ ((((!(!\inst1|Add8~107\ & \inst1|Add8~97\) # (\inst1|Add8~107\ & \inst1|Add8~97COUT1_165\)))))
-- \inst1|Add8~92\ = CARRY((\inst1|TSUM[7][12]~combout\ & ((!\inst1|Add8~97\))))
-- \inst1|Add8~92COUT1_166\ = CARRY((\inst1|TSUM[7][12]~combout\ & ((!\inst1|Add8~97COUT1_165\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][12]~combout\,
	cin => \inst1|Add8~107\,
	cin0 => \inst1|Add8~97\,
	cin1 => \inst1|Add8~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~90_combout\,
	cout0 => \inst1|Add8~92\,
	cout1 => \inst1|Add8~92COUT1_166\);

-- Location: LC_X8_Y4_N8
\inst1|Add8~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~85_combout\ = \inst1|TSUM[7][13]~combout\ $ (((((!\inst1|Add8~107\ & \inst1|Add8~92\) # (\inst1|Add8~107\ & \inst1|Add8~92COUT1_166\)))))
-- \inst1|Add8~87\ = CARRY(((!\inst1|Add8~92\)) # (!\inst1|TSUM[7][13]~combout\))
-- \inst1|Add8~87COUT1_167\ = CARRY(((!\inst1|Add8~92COUT1_166\)) # (!\inst1|TSUM[7][13]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][13]~combout\,
	cin => \inst1|Add8~107\,
	cin0 => \inst1|Add8~92\,
	cin1 => \inst1|Add8~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~85_combout\,
	cout0 => \inst1|Add8~87\,
	cout1 => \inst1|Add8~87COUT1_167\);

-- Location: LC_X8_Y4_N9
\inst1|Add8~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~80_combout\ = \inst1|TSUM[7][14]~combout\ $ ((((!(!\inst1|Add8~107\ & \inst1|Add8~87\) # (\inst1|Add8~107\ & \inst1|Add8~87COUT1_167\)))))
-- \inst1|Add8~82\ = CARRY((\inst1|TSUM[7][14]~combout\ & ((!\inst1|Add8~87COUT1_167\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][14]~combout\,
	cin => \inst1|Add8~107\,
	cin0 => \inst1|Add8~87\,
	cin1 => \inst1|Add8~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~80_combout\,
	cout => \inst1|Add8~82\);

-- Location: LC_X9_Y5_N3
\inst1|TSUM[7][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][18]~combout\ = LCELL((((\inst1|Add7~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][18]~combout\);

-- Location: LC_X9_Y5_N1
\inst1|TSUM[7][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][17]~combout\ = LCELL((((\inst1|Add7~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add7~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][17]~combout\);

-- Location: LC_X9_Y7_N5
\inst1|TSUM[7][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][16]~combout\ = LCELL((((\inst1|Add7~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][16]~combout\);

-- Location: LC_X9_Y5_N6
\inst1|TSUM[7][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][15]~combout\ = LCELL((((\inst1|Add7~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][15]~combout\);

-- Location: LC_X9_Y4_N0
\inst1|Add8~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~75_combout\ = \inst1|TSUM[7][15]~combout\ $ ((((\inst1|Add8~82\))))
-- \inst1|Add8~77\ = CARRY(((!\inst1|Add8~82\)) # (!\inst1|TSUM[7][15]~combout\))
-- \inst1|Add8~77COUT1_168\ = CARRY(((!\inst1|Add8~82\)) # (!\inst1|TSUM[7][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][15]~combout\,
	cin => \inst1|Add8~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~75_combout\,
	cout0 => \inst1|Add8~77\,
	cout1 => \inst1|Add8~77COUT1_168\);

-- Location: LC_X9_Y4_N1
\inst1|Add8~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~70_combout\ = (\inst1|TSUM[7][16]~combout\ $ ((!(!\inst1|Add8~82\ & \inst1|Add8~77\) # (\inst1|Add8~82\ & \inst1|Add8~77COUT1_168\))))
-- \inst1|Add8~72\ = CARRY(((\inst1|TSUM[7][16]~combout\ & !\inst1|Add8~77\)))
-- \inst1|Add8~72COUT1_169\ = CARRY(((\inst1|TSUM[7][16]~combout\ & !\inst1|Add8~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][16]~combout\,
	cin => \inst1|Add8~82\,
	cin0 => \inst1|Add8~77\,
	cin1 => \inst1|Add8~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~70_combout\,
	cout0 => \inst1|Add8~72\,
	cout1 => \inst1|Add8~72COUT1_169\);

-- Location: LC_X9_Y4_N2
\inst1|Add8~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~65_combout\ = (\inst1|TSUM[7][17]~combout\ $ (((!\inst1|Add8~82\ & \inst1|Add8~72\) # (\inst1|Add8~82\ & \inst1|Add8~72COUT1_169\))))
-- \inst1|Add8~67\ = CARRY(((!\inst1|Add8~72\) # (!\inst1|TSUM[7][17]~combout\)))
-- \inst1|Add8~67COUT1_170\ = CARRY(((!\inst1|Add8~72COUT1_169\) # (!\inst1|TSUM[7][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][17]~combout\,
	cin => \inst1|Add8~82\,
	cin0 => \inst1|Add8~72\,
	cin1 => \inst1|Add8~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~65_combout\,
	cout0 => \inst1|Add8~67\,
	cout1 => \inst1|Add8~67COUT1_170\);

-- Location: LC_X9_Y4_N3
\inst1|Add8~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~60_combout\ = (\inst1|TSUM[7][18]~combout\ $ ((!(!\inst1|Add8~82\ & \inst1|Add8~67\) # (\inst1|Add8~82\ & \inst1|Add8~67COUT1_170\))))
-- \inst1|Add8~62\ = CARRY(((\inst1|TSUM[7][18]~combout\ & !\inst1|Add8~67\)))
-- \inst1|Add8~62COUT1_171\ = CARRY(((\inst1|TSUM[7][18]~combout\ & !\inst1|Add8~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][18]~combout\,
	cin => \inst1|Add8~82\,
	cin0 => \inst1|Add8~67\,
	cin1 => \inst1|Add8~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~60_combout\,
	cout0 => \inst1|Add8~62\,
	cout1 => \inst1|Add8~62COUT1_171\);

-- Location: LC_X9_Y4_N4
\inst1|Add8~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~55_combout\ = (\inst1|TSUM[7][19]~combout\ $ (((!\inst1|Add8~82\ & \inst1|Add8~62\) # (\inst1|Add8~82\ & \inst1|Add8~62COUT1_171\))))
-- \inst1|Add8~57\ = CARRY(((!\inst1|Add8~62COUT1_171\) # (!\inst1|TSUM[7][19]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][19]~combout\,
	cin => \inst1|Add8~82\,
	cin0 => \inst1|Add8~62\,
	cin1 => \inst1|Add8~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~55_combout\,
	cout => \inst1|Add8~57\);

-- Location: LC_X9_Y5_N0
\inst1|TSUM[7][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][23]~combout\ = LCELL((((\inst1|Add7~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][23]~combout\);

-- Location: LC_X9_Y5_N7
\inst1|TSUM[7][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][22]~combout\ = LCELL((((\inst1|Add7~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][22]~combout\);

-- Location: LC_X9_Y5_N9
\inst1|TSUM[7][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][21]~combout\ = LCELL((((\inst1|Add7~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][21]~combout\);

-- Location: LC_X9_Y5_N5
\inst1|TSUM[7][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][20]~combout\ = LCELL((((\inst1|Add7~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][20]~combout\);

-- Location: LC_X9_Y4_N5
\inst1|Add8~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~50_combout\ = \inst1|TSUM[7][20]~combout\ $ ((((!\inst1|Add8~57\))))
-- \inst1|Add8~52\ = CARRY((\inst1|TSUM[7][20]~combout\ & ((!\inst1|Add8~57\))))
-- \inst1|Add8~52COUT1_172\ = CARRY((\inst1|TSUM[7][20]~combout\ & ((!\inst1|Add8~57\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][20]~combout\,
	cin => \inst1|Add8~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~50_combout\,
	cout0 => \inst1|Add8~52\,
	cout1 => \inst1|Add8~52COUT1_172\);

-- Location: LC_X9_Y4_N6
\inst1|Add8~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~45_combout\ = (\inst1|TSUM[7][21]~combout\ $ (((!\inst1|Add8~57\ & \inst1|Add8~52\) # (\inst1|Add8~57\ & \inst1|Add8~52COUT1_172\))))
-- \inst1|Add8~47\ = CARRY(((!\inst1|Add8~52\) # (!\inst1|TSUM[7][21]~combout\)))
-- \inst1|Add8~47COUT1_173\ = CARRY(((!\inst1|Add8~52COUT1_172\) # (!\inst1|TSUM[7][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][21]~combout\,
	cin => \inst1|Add8~57\,
	cin0 => \inst1|Add8~52\,
	cin1 => \inst1|Add8~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~45_combout\,
	cout0 => \inst1|Add8~47\,
	cout1 => \inst1|Add8~47COUT1_173\);

-- Location: LC_X9_Y4_N7
\inst1|Add8~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~40_combout\ = \inst1|TSUM[7][22]~combout\ $ ((((!(!\inst1|Add8~57\ & \inst1|Add8~47\) # (\inst1|Add8~57\ & \inst1|Add8~47COUT1_173\)))))
-- \inst1|Add8~42\ = CARRY((\inst1|TSUM[7][22]~combout\ & ((!\inst1|Add8~47\))))
-- \inst1|Add8~42COUT1_174\ = CARRY((\inst1|TSUM[7][22]~combout\ & ((!\inst1|Add8~47COUT1_173\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][22]~combout\,
	cin => \inst1|Add8~57\,
	cin0 => \inst1|Add8~47\,
	cin1 => \inst1|Add8~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~40_combout\,
	cout0 => \inst1|Add8~42\,
	cout1 => \inst1|Add8~42COUT1_174\);

-- Location: LC_X9_Y4_N8
\inst1|Add8~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~35_combout\ = \inst1|TSUM[7][23]~combout\ $ (((((!\inst1|Add8~57\ & \inst1|Add8~42\) # (\inst1|Add8~57\ & \inst1|Add8~42COUT1_174\)))))
-- \inst1|Add8~37\ = CARRY(((!\inst1|Add8~42\)) # (!\inst1|TSUM[7][23]~combout\))
-- \inst1|Add8~37COUT1_175\ = CARRY(((!\inst1|Add8~42COUT1_174\)) # (!\inst1|TSUM[7][23]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][23]~combout\,
	cin => \inst1|Add8~57\,
	cin0 => \inst1|Add8~42\,
	cin1 => \inst1|Add8~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~35_combout\,
	cout0 => \inst1|Add8~37\,
	cout1 => \inst1|Add8~37COUT1_175\);

-- Location: LC_X9_Y4_N9
\inst1|Add8~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~30_combout\ = (\inst1|TSUM[7][24]~combout\ $ ((!(!\inst1|Add8~57\ & \inst1|Add8~37\) # (\inst1|Add8~57\ & \inst1|Add8~37COUT1_175\))))
-- \inst1|Add8~32\ = CARRY(((\inst1|TSUM[7][24]~combout\ & !\inst1|Add8~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][24]~combout\,
	cin => \inst1|Add8~57\,
	cin0 => \inst1|Add8~37\,
	cin1 => \inst1|Add8~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~30_combout\,
	cout => \inst1|Add8~32\);

-- Location: LC_X10_Y5_N2
\inst1|TSUM[7][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][28]~combout\ = LCELL((((\inst1|Add7~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][28]~combout\);

-- Location: LC_X10_Y6_N7
\inst1|TSUM[7][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][27]~combout\ = LCELL((((\inst1|Add7~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][27]~combout\);

-- Location: LC_X10_Y6_N9
\inst1|TSUM[7][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][26]~combout\ = LCELL((((\inst1|Add7~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][26]~combout\);

-- Location: LC_X10_Y5_N5
\inst1|TSUM[7][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[7][25]~combout\ = LCELL((((\inst1|Add7~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add7~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[7][25]~combout\);

-- Location: LC_X10_Y4_N0
\inst1|Add8~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~25_combout\ = \inst1|TSUM[7][25]~combout\ $ ((((\inst1|Add8~32\))))
-- \inst1|Add8~27\ = CARRY(((!\inst1|Add8~32\)) # (!\inst1|TSUM[7][25]~combout\))
-- \inst1|Add8~27COUT1_176\ = CARRY(((!\inst1|Add8~32\)) # (!\inst1|TSUM[7][25]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][25]~combout\,
	cin => \inst1|Add8~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~25_combout\,
	cout0 => \inst1|Add8~27\,
	cout1 => \inst1|Add8~27COUT1_176\);

-- Location: LC_X10_Y4_N1
\inst1|Add8~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~20_combout\ = (\inst1|TSUM[7][26]~combout\ $ ((!(!\inst1|Add8~32\ & \inst1|Add8~27\) # (\inst1|Add8~32\ & \inst1|Add8~27COUT1_176\))))
-- \inst1|Add8~22\ = CARRY(((\inst1|TSUM[7][26]~combout\ & !\inst1|Add8~27\)))
-- \inst1|Add8~22COUT1_177\ = CARRY(((\inst1|TSUM[7][26]~combout\ & !\inst1|Add8~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][26]~combout\,
	cin => \inst1|Add8~32\,
	cin0 => \inst1|Add8~27\,
	cin1 => \inst1|Add8~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~20_combout\,
	cout0 => \inst1|Add8~22\,
	cout1 => \inst1|Add8~22COUT1_177\);

-- Location: LC_X10_Y4_N2
\inst1|Add8~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~15_combout\ = \inst1|TSUM[7][27]~combout\ $ (((((!\inst1|Add8~32\ & \inst1|Add8~22\) # (\inst1|Add8~32\ & \inst1|Add8~22COUT1_177\)))))
-- \inst1|Add8~17\ = CARRY(((!\inst1|Add8~22\)) # (!\inst1|TSUM[7][27]~combout\))
-- \inst1|Add8~17COUT1_178\ = CARRY(((!\inst1|Add8~22COUT1_177\)) # (!\inst1|TSUM[7][27]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[7][27]~combout\,
	cin => \inst1|Add8~32\,
	cin0 => \inst1|Add8~22\,
	cin1 => \inst1|Add8~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~15_combout\,
	cout0 => \inst1|Add8~17\,
	cout1 => \inst1|Add8~17COUT1_178\);

-- Location: LC_X10_Y4_N3
\inst1|Add8~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~10_combout\ = (\inst1|TSUM[7][28]~combout\ $ ((!(!\inst1|Add8~32\ & \inst1|Add8~17\) # (\inst1|Add8~32\ & \inst1|Add8~17COUT1_178\))))
-- \inst1|Add8~12\ = CARRY(((\inst1|TSUM[7][28]~combout\ & !\inst1|Add8~17\)))
-- \inst1|Add8~12COUT1_179\ = CARRY(((\inst1|TSUM[7][28]~combout\ & !\inst1|Add8~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][28]~combout\,
	cin => \inst1|Add8~32\,
	cin0 => \inst1|Add8~17\,
	cin1 => \inst1|Add8~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~10_combout\,
	cout0 => \inst1|Add8~12\,
	cout1 => \inst1|Add8~12COUT1_179\);

-- Location: LC_X10_Y4_N4
\inst1|Add8~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~5_combout\ = (\inst1|TSUM[7][29]~combout\ $ (((!\inst1|Add8~32\ & \inst1|Add8~12\) # (\inst1|Add8~32\ & \inst1|Add8~12COUT1_179\))))
-- \inst1|Add8~7\ = CARRY(((!\inst1|Add8~12COUT1_179\) # (!\inst1|TSUM[7][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[7][29]~combout\,
	cin => \inst1|Add8~32\,
	cin0 => \inst1|Add8~12\,
	cin1 => \inst1|Add8~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~5_combout\,
	cout => \inst1|Add8~7\);

-- Location: LC_X10_Y4_N5
\inst1|Add8~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add8~0_combout\ = ((\inst1|Add8~7\ $ (!\inst1|TSUM[7][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[7][30]~combout\,
	cin => \inst1|Add8~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add8~0_combout\);

-- Location: LC_X15_Y3_N9
\inst1|TSUM[8][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][30]~combout\ = LCELL((((\inst1|Add8~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][30]~combout\);

-- Location: LC_X10_Y4_N6
\inst1|TSUM[8][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][29]~combout\ = LCELL((((\inst1|Add8~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][29]~combout\);

-- Location: LC_X12_Y4_N1
\inst1|TSUM[8][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][24]~combout\ = LCELL((((\inst1|Add8~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][24]~combout\);

-- Location: LC_X11_Y5_N3
\inst1|TSUM[8][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][19]~combout\ = LCELL((((\inst1|Add8~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][19]~combout\);

-- Location: LC_X11_Y4_N2
\inst1|TSUM[8][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][14]~combout\ = LCELL((((\inst1|Add8~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][14]~combout\);

-- Location: LC_X11_Y4_N7
\inst1|TSUM[8][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][9]~combout\ = LCELL((((\inst1|Add8~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][9]~combout\);

-- Location: LC_X12_Y5_N0
\inst1|PSUM[10][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][0]~regout\ = DFFEAS(((!\inst1|PSUM[10][0]~regout\)), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][0]~119\ = CARRY(((\inst1|PSUM[10][0]~regout\)))
-- \inst1|PSUM[10][0]~119COUT1_361\ = CARRY(((\inst1|PSUM[10][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	datab => \inst1|PSUM[10][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][0]~regout\,
	cout0 => \inst1|PSUM[10][0]~119\,
	cout1 => \inst1|PSUM[10][0]~119COUT1_361\);

-- Location: LC_X12_Y5_N1
\inst1|PSUM[10][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][1]~regout\ = DFFEAS((\inst1|PSUM[10][1]~regout\ $ ((\inst1|PSUM[10][0]~119\))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][1]~117\ = CARRY(((!\inst1|PSUM[10][0]~119\) # (!\inst1|PSUM[10][1]~regout\)))
-- \inst1|PSUM[10][1]~117COUT1_362\ = CARRY(((!\inst1|PSUM[10][0]~119COUT1_361\) # (!\inst1|PSUM[10][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	datab => \inst1|PSUM[10][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[10][0]~119\,
	cin1 => \inst1|PSUM[10][0]~119COUT1_361\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][1]~regout\,
	cout0 => \inst1|PSUM[10][1]~117\,
	cout1 => \inst1|PSUM[10][1]~117COUT1_362\);

-- Location: LC_X12_Y5_N2
\inst1|PSUM[10][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][2]~regout\ = DFFEAS((\inst1|PSUM[10][2]~regout\ $ ((!\inst1|PSUM[10][1]~117\))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][2]~115\ = CARRY(((\inst1|PSUM[10][2]~regout\ & !\inst1|PSUM[10][1]~117\)))
-- \inst1|PSUM[10][2]~115COUT1_363\ = CARRY(((\inst1|PSUM[10][2]~regout\ & !\inst1|PSUM[10][1]~117COUT1_362\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	datab => \inst1|PSUM[10][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[10][1]~117\,
	cin1 => \inst1|PSUM[10][1]~117COUT1_362\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][2]~regout\,
	cout0 => \inst1|PSUM[10][2]~115\,
	cout1 => \inst1|PSUM[10][2]~115COUT1_363\);

-- Location: LC_X12_Y5_N3
\inst1|PSUM[10][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][3]~regout\ = DFFEAS(\inst1|PSUM[10][3]~regout\ $ ((((\inst1|PSUM[10][2]~115\)))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][3]~113\ = CARRY(((!\inst1|PSUM[10][2]~115\)) # (!\inst1|PSUM[10][3]~regout\))
-- \inst1|PSUM[10][3]~113COUT1_364\ = CARRY(((!\inst1|PSUM[10][2]~115COUT1_363\)) # (!\inst1|PSUM[10][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	dataa => \inst1|PSUM[10][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[10][2]~115\,
	cin1 => \inst1|PSUM[10][2]~115COUT1_363\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][3]~regout\,
	cout0 => \inst1|PSUM[10][3]~113\,
	cout1 => \inst1|PSUM[10][3]~113COUT1_364\);

-- Location: LC_X12_Y5_N4
\inst1|PSUM[10][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][4]~regout\ = DFFEAS(\inst1|PSUM[10][4]~regout\ $ ((((!\inst1|PSUM[10][3]~113\)))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][4]~111\ = CARRY((\inst1|PSUM[10][4]~regout\ & ((!\inst1|PSUM[10][3]~113COUT1_364\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	dataa => \inst1|PSUM[10][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[10][3]~113\,
	cin1 => \inst1|PSUM[10][3]~113COUT1_364\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][4]~regout\,
	cout => \inst1|PSUM[10][4]~111\);

-- Location: LC_X12_Y5_N5
\inst1|PSUM[10][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][5]~regout\ = DFFEAS(\inst1|PSUM[10][5]~regout\ $ ((((\inst1|PSUM[10][4]~111\)))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][5]~109\ = CARRY(((!\inst1|PSUM[10][4]~111\)) # (!\inst1|PSUM[10][5]~regout\))
-- \inst1|PSUM[10][5]~109COUT1_365\ = CARRY(((!\inst1|PSUM[10][4]~111\)) # (!\inst1|PSUM[10][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	dataa => \inst1|PSUM[10][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[10][4]~111\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][5]~regout\,
	cout0 => \inst1|PSUM[10][5]~109\,
	cout1 => \inst1|PSUM[10][5]~109COUT1_365\);

-- Location: LC_X12_Y5_N6
\inst1|PSUM[10][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][6]~regout\ = DFFEAS(\inst1|PSUM[10][6]~regout\ $ ((((!(!\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][5]~109\) # (\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][5]~109COUT1_365\))))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][6]~107\ = CARRY((\inst1|PSUM[10][6]~regout\ & ((!\inst1|PSUM[10][5]~109\))))
-- \inst1|PSUM[10][6]~107COUT1_366\ = CARRY((\inst1|PSUM[10][6]~regout\ & ((!\inst1|PSUM[10][5]~109COUT1_365\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	dataa => \inst1|PSUM[10][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[10][4]~111\,
	cin0 => \inst1|PSUM[10][5]~109\,
	cin1 => \inst1|PSUM[10][5]~109COUT1_365\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][6]~regout\,
	cout0 => \inst1|PSUM[10][6]~107\,
	cout1 => \inst1|PSUM[10][6]~107COUT1_366\);

-- Location: LC_X12_Y5_N7
\inst1|PSUM[10][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][7]~regout\ = DFFEAS((\inst1|PSUM[10][7]~regout\ $ (((!\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][6]~107\) # (\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][6]~107COUT1_366\)))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][7]~105\ = CARRY(((!\inst1|PSUM[10][6]~107\) # (!\inst1|PSUM[10][7]~regout\)))
-- \inst1|PSUM[10][7]~105COUT1_367\ = CARRY(((!\inst1|PSUM[10][6]~107COUT1_366\) # (!\inst1|PSUM[10][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	datab => \inst1|PSUM[10][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[10][4]~111\,
	cin0 => \inst1|PSUM[10][6]~107\,
	cin1 => \inst1|PSUM[10][6]~107COUT1_366\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][7]~regout\,
	cout0 => \inst1|PSUM[10][7]~105\,
	cout1 => \inst1|PSUM[10][7]~105COUT1_367\);

-- Location: LC_X12_Y5_N8
\inst1|PSUM[10][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][8]~regout\ = DFFEAS(\inst1|PSUM[10][8]~regout\ $ ((((!(!\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][7]~105\) # (\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][7]~105COUT1_367\))))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[10][8]~103\ = CARRY((\inst1|PSUM[10][8]~regout\ & ((!\inst1|PSUM[10][7]~105\))))
-- \inst1|PSUM[10][8]~103COUT1_368\ = CARRY((\inst1|PSUM[10][8]~regout\ & ((!\inst1|PSUM[10][7]~105COUT1_367\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	dataa => \inst1|PSUM[10][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[10][4]~111\,
	cin0 => \inst1|PSUM[10][7]~105\,
	cin1 => \inst1|PSUM[10][7]~105COUT1_367\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][8]~regout\,
	cout0 => \inst1|PSUM[10][8]~103\,
	cout1 => \inst1|PSUM[10][8]~103COUT1_368\);

-- Location: LC_X12_Y5_N9
\inst1|PSUM[10][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[10][9]~regout\ = DFFEAS((((!\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][8]~103\) # (\inst1|PSUM[10][4]~111\ & \inst1|PSUM[10][8]~103COUT1_368\) $ (\inst1|PSUM[10][9]~regout\))), GLOBAL(\inst6|STEMP\(10)), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(10),
	datad => \inst1|PSUM[10][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[10][4]~111\,
	cin0 => \inst1|PSUM[10][8]~103\,
	cin1 => \inst1|PSUM[10][8]~103COUT1_368\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[10][9]~regout\);

-- Location: LC_X7_Y4_N0
\inst1|TSUM[8][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][4]~combout\ = LCELL((((\inst1|Add8~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][4]~combout\);

-- Location: LC_X12_Y4_N5
\inst1|TSUM[8][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][3]~combout\ = LCELL((((\inst1|Add8~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][3]~combout\);

-- Location: LC_X7_Y4_N3
\inst1|TSUM[8][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][2]~combout\ = LCELL((((\inst1|Add8~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][2]~combout\);

-- Location: LC_X7_Y4_N4
\inst1|TSUM[8][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][1]~combout\ = LCELL((((\inst1|Add8~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][1]~combout\);

-- Location: LC_X12_Y4_N3
\inst1|TSUM[8][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][0]~combout\ = LCELL((((\inst1|Add8~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][0]~combout\);

-- Location: LC_X12_Y3_N5
\inst1|Add9~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~150_combout\ = \inst1|PSUM[10][0]~regout\ $ ((\inst1|TSUM[8][0]~combout\))
-- \inst1|Add9~152\ = CARRY((\inst1|PSUM[10][0]~regout\ & (\inst1|TSUM[8][0]~combout\)))
-- \inst1|Add9~152COUT1_156\ = CARRY((\inst1|PSUM[10][0]~regout\ & (\inst1|TSUM[8][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[10][0]~regout\,
	datab => \inst1|TSUM[8][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~150_combout\,
	cout0 => \inst1|Add9~152\,
	cout1 => \inst1|Add9~152COUT1_156\);

-- Location: LC_X12_Y3_N6
\inst1|Add9~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~145_combout\ = \inst1|TSUM[8][1]~combout\ $ (\inst1|PSUM[10][1]~regout\ $ ((\inst1|Add9~152\)))
-- \inst1|Add9~147\ = CARRY((\inst1|TSUM[8][1]~combout\ & (!\inst1|PSUM[10][1]~regout\ & !\inst1|Add9~152\)) # (!\inst1|TSUM[8][1]~combout\ & ((!\inst1|Add9~152\) # (!\inst1|PSUM[10][1]~regout\))))
-- \inst1|Add9~147COUT1_157\ = CARRY((\inst1|TSUM[8][1]~combout\ & (!\inst1|PSUM[10][1]~regout\ & !\inst1|Add9~152COUT1_156\)) # (!\inst1|TSUM[8][1]~combout\ & ((!\inst1|Add9~152COUT1_156\) # (!\inst1|PSUM[10][1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][1]~combout\,
	datab => \inst1|PSUM[10][1]~regout\,
	cin0 => \inst1|Add9~152\,
	cin1 => \inst1|Add9~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~145_combout\,
	cout0 => \inst1|Add9~147\,
	cout1 => \inst1|Add9~147COUT1_157\);

-- Location: LC_X12_Y3_N7
\inst1|Add9~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~140_combout\ = \inst1|TSUM[8][2]~combout\ $ (\inst1|PSUM[10][2]~regout\ $ ((!\inst1|Add9~147\)))
-- \inst1|Add9~142\ = CARRY((\inst1|TSUM[8][2]~combout\ & ((\inst1|PSUM[10][2]~regout\) # (!\inst1|Add9~147\))) # (!\inst1|TSUM[8][2]~combout\ & (\inst1|PSUM[10][2]~regout\ & !\inst1|Add9~147\)))
-- \inst1|Add9~142COUT1_158\ = CARRY((\inst1|TSUM[8][2]~combout\ & ((\inst1|PSUM[10][2]~regout\) # (!\inst1|Add9~147COUT1_157\))) # (!\inst1|TSUM[8][2]~combout\ & (\inst1|PSUM[10][2]~regout\ & !\inst1|Add9~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][2]~combout\,
	datab => \inst1|PSUM[10][2]~regout\,
	cin0 => \inst1|Add9~147\,
	cin1 => \inst1|Add9~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~140_combout\,
	cout0 => \inst1|Add9~142\,
	cout1 => \inst1|Add9~142COUT1_158\);

-- Location: LC_X12_Y3_N8
\inst1|Add9~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~135_combout\ = \inst1|TSUM[8][3]~combout\ $ (\inst1|PSUM[10][3]~regout\ $ ((\inst1|Add9~142\)))
-- \inst1|Add9~137\ = CARRY((\inst1|TSUM[8][3]~combout\ & (!\inst1|PSUM[10][3]~regout\ & !\inst1|Add9~142\)) # (!\inst1|TSUM[8][3]~combout\ & ((!\inst1|Add9~142\) # (!\inst1|PSUM[10][3]~regout\))))
-- \inst1|Add9~137COUT1_159\ = CARRY((\inst1|TSUM[8][3]~combout\ & (!\inst1|PSUM[10][3]~regout\ & !\inst1|Add9~142COUT1_158\)) # (!\inst1|TSUM[8][3]~combout\ & ((!\inst1|Add9~142COUT1_158\) # (!\inst1|PSUM[10][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][3]~combout\,
	datab => \inst1|PSUM[10][3]~regout\,
	cin0 => \inst1|Add9~142\,
	cin1 => \inst1|Add9~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~135_combout\,
	cout0 => \inst1|Add9~137\,
	cout1 => \inst1|Add9~137COUT1_159\);

-- Location: LC_X12_Y3_N9
\inst1|Add9~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~130_combout\ = \inst1|PSUM[10][4]~regout\ $ (\inst1|TSUM[8][4]~combout\ $ ((!\inst1|Add9~137\)))
-- \inst1|Add9~132\ = CARRY((\inst1|PSUM[10][4]~regout\ & ((\inst1|TSUM[8][4]~combout\) # (!\inst1|Add9~137COUT1_159\))) # (!\inst1|PSUM[10][4]~regout\ & (\inst1|TSUM[8][4]~combout\ & !\inst1|Add9~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[10][4]~regout\,
	datab => \inst1|TSUM[8][4]~combout\,
	cin0 => \inst1|Add9~137\,
	cin1 => \inst1|Add9~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~130_combout\,
	cout => \inst1|Add9~132\);

-- Location: LC_X11_Y4_N5
\inst1|TSUM[8][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][8]~combout\ = LCELL((((\inst1|Add8~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][8]~combout\);

-- Location: LC_X12_Y4_N9
\inst1|TSUM[8][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][7]~combout\ = LCELL((((\inst1|Add8~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][7]~combout\);

-- Location: LC_X10_Y4_N8
\inst1|TSUM[8][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][6]~combout\ = LCELL((((\inst1|Add8~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][6]~combout\);

-- Location: LC_X11_Y4_N6
\inst1|TSUM[8][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][5]~combout\ = LCELL((((\inst1|Add8~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][5]~combout\);

-- Location: LC_X13_Y3_N0
\inst1|Add9~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~125_combout\ = \inst1|PSUM[10][5]~regout\ $ (\inst1|TSUM[8][5]~combout\ $ ((\inst1|Add9~132\)))
-- \inst1|Add9~127\ = CARRY((\inst1|PSUM[10][5]~regout\ & (!\inst1|TSUM[8][5]~combout\ & !\inst1|Add9~132\)) # (!\inst1|PSUM[10][5]~regout\ & ((!\inst1|Add9~132\) # (!\inst1|TSUM[8][5]~combout\))))
-- \inst1|Add9~127COUT1_160\ = CARRY((\inst1|PSUM[10][5]~regout\ & (!\inst1|TSUM[8][5]~combout\ & !\inst1|Add9~132\)) # (!\inst1|PSUM[10][5]~regout\ & ((!\inst1|Add9~132\) # (!\inst1|TSUM[8][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[10][5]~regout\,
	datab => \inst1|TSUM[8][5]~combout\,
	cin => \inst1|Add9~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~125_combout\,
	cout0 => \inst1|Add9~127\,
	cout1 => \inst1|Add9~127COUT1_160\);

-- Location: LC_X13_Y3_N1
\inst1|Add9~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~120_combout\ = \inst1|TSUM[8][6]~combout\ $ (\inst1|PSUM[10][6]~regout\ $ ((!(!\inst1|Add9~132\ & \inst1|Add9~127\) # (\inst1|Add9~132\ & \inst1|Add9~127COUT1_160\))))
-- \inst1|Add9~122\ = CARRY((\inst1|TSUM[8][6]~combout\ & ((\inst1|PSUM[10][6]~regout\) # (!\inst1|Add9~127\))) # (!\inst1|TSUM[8][6]~combout\ & (\inst1|PSUM[10][6]~regout\ & !\inst1|Add9~127\)))
-- \inst1|Add9~122COUT1_161\ = CARRY((\inst1|TSUM[8][6]~combout\ & ((\inst1|PSUM[10][6]~regout\) # (!\inst1|Add9~127COUT1_160\))) # (!\inst1|TSUM[8][6]~combout\ & (\inst1|PSUM[10][6]~regout\ & !\inst1|Add9~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][6]~combout\,
	datab => \inst1|PSUM[10][6]~regout\,
	cin => \inst1|Add9~132\,
	cin0 => \inst1|Add9~127\,
	cin1 => \inst1|Add9~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~120_combout\,
	cout0 => \inst1|Add9~122\,
	cout1 => \inst1|Add9~122COUT1_161\);

-- Location: LC_X13_Y3_N2
\inst1|Add9~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~115_combout\ = \inst1|PSUM[10][7]~regout\ $ (\inst1|TSUM[8][7]~combout\ $ (((!\inst1|Add9~132\ & \inst1|Add9~122\) # (\inst1|Add9~132\ & \inst1|Add9~122COUT1_161\))))
-- \inst1|Add9~117\ = CARRY((\inst1|PSUM[10][7]~regout\ & (!\inst1|TSUM[8][7]~combout\ & !\inst1|Add9~122\)) # (!\inst1|PSUM[10][7]~regout\ & ((!\inst1|Add9~122\) # (!\inst1|TSUM[8][7]~combout\))))
-- \inst1|Add9~117COUT1_162\ = CARRY((\inst1|PSUM[10][7]~regout\ & (!\inst1|TSUM[8][7]~combout\ & !\inst1|Add9~122COUT1_161\)) # (!\inst1|PSUM[10][7]~regout\ & ((!\inst1|Add9~122COUT1_161\) # (!\inst1|TSUM[8][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[10][7]~regout\,
	datab => \inst1|TSUM[8][7]~combout\,
	cin => \inst1|Add9~132\,
	cin0 => \inst1|Add9~122\,
	cin1 => \inst1|Add9~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~115_combout\,
	cout0 => \inst1|Add9~117\,
	cout1 => \inst1|Add9~117COUT1_162\);

-- Location: LC_X13_Y3_N3
\inst1|Add9~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~110_combout\ = \inst1|PSUM[10][8]~regout\ $ (\inst1|TSUM[8][8]~combout\ $ ((!(!\inst1|Add9~132\ & \inst1|Add9~117\) # (\inst1|Add9~132\ & \inst1|Add9~117COUT1_162\))))
-- \inst1|Add9~112\ = CARRY((\inst1|PSUM[10][8]~regout\ & ((\inst1|TSUM[8][8]~combout\) # (!\inst1|Add9~117\))) # (!\inst1|PSUM[10][8]~regout\ & (\inst1|TSUM[8][8]~combout\ & !\inst1|Add9~117\)))
-- \inst1|Add9~112COUT1_163\ = CARRY((\inst1|PSUM[10][8]~regout\ & ((\inst1|TSUM[8][8]~combout\) # (!\inst1|Add9~117COUT1_162\))) # (!\inst1|PSUM[10][8]~regout\ & (\inst1|TSUM[8][8]~combout\ & !\inst1|Add9~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[10][8]~regout\,
	datab => \inst1|TSUM[8][8]~combout\,
	cin => \inst1|Add9~132\,
	cin0 => \inst1|Add9~117\,
	cin1 => \inst1|Add9~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~110_combout\,
	cout0 => \inst1|Add9~112\,
	cout1 => \inst1|Add9~112COUT1_163\);

-- Location: LC_X13_Y3_N4
\inst1|Add9~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~105_combout\ = \inst1|TSUM[8][9]~combout\ $ (\inst1|PSUM[10][9]~regout\ $ (((!\inst1|Add9~132\ & \inst1|Add9~112\) # (\inst1|Add9~132\ & \inst1|Add9~112COUT1_163\))))
-- \inst1|Add9~107\ = CARRY((\inst1|TSUM[8][9]~combout\ & (!\inst1|PSUM[10][9]~regout\ & !\inst1|Add9~112COUT1_163\)) # (!\inst1|TSUM[8][9]~combout\ & ((!\inst1|Add9~112COUT1_163\) # (!\inst1|PSUM[10][9]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][9]~combout\,
	datab => \inst1|PSUM[10][9]~regout\,
	cin => \inst1|Add9~132\,
	cin0 => \inst1|Add9~112\,
	cin1 => \inst1|Add9~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~105_combout\,
	cout => \inst1|Add9~107\);

-- Location: LC_X11_Y4_N3
\inst1|TSUM[8][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][13]~combout\ = LCELL((((\inst1|Add8~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][13]~combout\);

-- Location: LC_X11_Y4_N8
\inst1|TSUM[8][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][12]~combout\ = LCELL((((\inst1|Add8~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][12]~combout\);

-- Location: LC_X10_Y5_N4
\inst1|TSUM[8][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][11]~combout\ = LCELL((((\inst1|Add8~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][11]~combout\);

-- Location: LC_X11_Y4_N0
\inst1|TSUM[8][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][10]~combout\ = LCELL((((\inst1|Add8~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][10]~combout\);

-- Location: LC_X13_Y3_N5
\inst1|Add9~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~100_combout\ = \inst1|TSUM[8][10]~combout\ $ ((((!\inst1|Add9~107\))))
-- \inst1|Add9~102\ = CARRY((\inst1|TSUM[8][10]~combout\ & ((!\inst1|Add9~107\))))
-- \inst1|Add9~102COUT1_164\ = CARRY((\inst1|TSUM[8][10]~combout\ & ((!\inst1|Add9~107\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][10]~combout\,
	cin => \inst1|Add9~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~100_combout\,
	cout0 => \inst1|Add9~102\,
	cout1 => \inst1|Add9~102COUT1_164\);

-- Location: LC_X13_Y3_N6
\inst1|Add9~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~95_combout\ = (\inst1|TSUM[8][11]~combout\ $ (((!\inst1|Add9~107\ & \inst1|Add9~102\) # (\inst1|Add9~107\ & \inst1|Add9~102COUT1_164\))))
-- \inst1|Add9~97\ = CARRY(((!\inst1|Add9~102\) # (!\inst1|TSUM[8][11]~combout\)))
-- \inst1|Add9~97COUT1_165\ = CARRY(((!\inst1|Add9~102COUT1_164\) # (!\inst1|TSUM[8][11]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][11]~combout\,
	cin => \inst1|Add9~107\,
	cin0 => \inst1|Add9~102\,
	cin1 => \inst1|Add9~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~95_combout\,
	cout0 => \inst1|Add9~97\,
	cout1 => \inst1|Add9~97COUT1_165\);

-- Location: LC_X13_Y3_N7
\inst1|Add9~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~90_combout\ = \inst1|TSUM[8][12]~combout\ $ ((((!(!\inst1|Add9~107\ & \inst1|Add9~97\) # (\inst1|Add9~107\ & \inst1|Add9~97COUT1_165\)))))
-- \inst1|Add9~92\ = CARRY((\inst1|TSUM[8][12]~combout\ & ((!\inst1|Add9~97\))))
-- \inst1|Add9~92COUT1_166\ = CARRY((\inst1|TSUM[8][12]~combout\ & ((!\inst1|Add9~97COUT1_165\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][12]~combout\,
	cin => \inst1|Add9~107\,
	cin0 => \inst1|Add9~97\,
	cin1 => \inst1|Add9~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~90_combout\,
	cout0 => \inst1|Add9~92\,
	cout1 => \inst1|Add9~92COUT1_166\);

-- Location: LC_X13_Y3_N8
\inst1|Add9~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~85_combout\ = \inst1|TSUM[8][13]~combout\ $ (((((!\inst1|Add9~107\ & \inst1|Add9~92\) # (\inst1|Add9~107\ & \inst1|Add9~92COUT1_166\)))))
-- \inst1|Add9~87\ = CARRY(((!\inst1|Add9~92\)) # (!\inst1|TSUM[8][13]~combout\))
-- \inst1|Add9~87COUT1_167\ = CARRY(((!\inst1|Add9~92COUT1_166\)) # (!\inst1|TSUM[8][13]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][13]~combout\,
	cin => \inst1|Add9~107\,
	cin0 => \inst1|Add9~92\,
	cin1 => \inst1|Add9~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~85_combout\,
	cout0 => \inst1|Add9~87\,
	cout1 => \inst1|Add9~87COUT1_167\);

-- Location: LC_X13_Y3_N9
\inst1|Add9~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~80_combout\ = (\inst1|TSUM[8][14]~combout\ $ ((!(!\inst1|Add9~107\ & \inst1|Add9~87\) # (\inst1|Add9~107\ & \inst1|Add9~87COUT1_167\))))
-- \inst1|Add9~82\ = CARRY(((\inst1|TSUM[8][14]~combout\ & !\inst1|Add9~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][14]~combout\,
	cin => \inst1|Add9~107\,
	cin0 => \inst1|Add9~87\,
	cin1 => \inst1|Add9~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~80_combout\,
	cout => \inst1|Add9~82\);

-- Location: LC_X11_Y4_N4
\inst1|TSUM[8][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][18]~combout\ = LCELL((((\inst1|Add8~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][18]~combout\);

-- Location: LC_X12_Y4_N2
\inst1|TSUM[8][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][17]~combout\ = LCELL((((\inst1|Add8~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][17]~combout\);

-- Location: LC_X12_Y4_N4
\inst1|TSUM[8][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][16]~combout\ = LCELL((((\inst1|Add8~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][16]~combout\);

-- Location: LC_X11_Y4_N1
\inst1|TSUM[8][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][15]~combout\ = LCELL((((\inst1|Add8~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][15]~combout\);

-- Location: LC_X14_Y3_N0
\inst1|Add9~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~75_combout\ = (\inst1|TSUM[8][15]~combout\ $ ((\inst1|Add9~82\)))
-- \inst1|Add9~77\ = CARRY(((!\inst1|Add9~82\) # (!\inst1|TSUM[8][15]~combout\)))
-- \inst1|Add9~77COUT1_168\ = CARRY(((!\inst1|Add9~82\) # (!\inst1|TSUM[8][15]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][15]~combout\,
	cin => \inst1|Add9~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~75_combout\,
	cout0 => \inst1|Add9~77\,
	cout1 => \inst1|Add9~77COUT1_168\);

-- Location: LC_X14_Y3_N1
\inst1|Add9~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~70_combout\ = (\inst1|TSUM[8][16]~combout\ $ ((!(!\inst1|Add9~82\ & \inst1|Add9~77\) # (\inst1|Add9~82\ & \inst1|Add9~77COUT1_168\))))
-- \inst1|Add9~72\ = CARRY(((\inst1|TSUM[8][16]~combout\ & !\inst1|Add9~77\)))
-- \inst1|Add9~72COUT1_169\ = CARRY(((\inst1|TSUM[8][16]~combout\ & !\inst1|Add9~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][16]~combout\,
	cin => \inst1|Add9~82\,
	cin0 => \inst1|Add9~77\,
	cin1 => \inst1|Add9~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~70_combout\,
	cout0 => \inst1|Add9~72\,
	cout1 => \inst1|Add9~72COUT1_169\);

-- Location: LC_X14_Y3_N2
\inst1|Add9~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~65_combout\ = (\inst1|TSUM[8][17]~combout\ $ (((!\inst1|Add9~82\ & \inst1|Add9~72\) # (\inst1|Add9~82\ & \inst1|Add9~72COUT1_169\))))
-- \inst1|Add9~67\ = CARRY(((!\inst1|Add9~72\) # (!\inst1|TSUM[8][17]~combout\)))
-- \inst1|Add9~67COUT1_170\ = CARRY(((!\inst1|Add9~72COUT1_169\) # (!\inst1|TSUM[8][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][17]~combout\,
	cin => \inst1|Add9~82\,
	cin0 => \inst1|Add9~72\,
	cin1 => \inst1|Add9~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~65_combout\,
	cout0 => \inst1|Add9~67\,
	cout1 => \inst1|Add9~67COUT1_170\);

-- Location: LC_X14_Y3_N3
\inst1|Add9~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~60_combout\ = \inst1|TSUM[8][18]~combout\ $ ((((!(!\inst1|Add9~82\ & \inst1|Add9~67\) # (\inst1|Add9~82\ & \inst1|Add9~67COUT1_170\)))))
-- \inst1|Add9~62\ = CARRY((\inst1|TSUM[8][18]~combout\ & ((!\inst1|Add9~67\))))
-- \inst1|Add9~62COUT1_171\ = CARRY((\inst1|TSUM[8][18]~combout\ & ((!\inst1|Add9~67COUT1_170\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][18]~combout\,
	cin => \inst1|Add9~82\,
	cin0 => \inst1|Add9~67\,
	cin1 => \inst1|Add9~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~60_combout\,
	cout0 => \inst1|Add9~62\,
	cout1 => \inst1|Add9~62COUT1_171\);

-- Location: LC_X14_Y3_N4
\inst1|Add9~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~55_combout\ = \inst1|TSUM[8][19]~combout\ $ (((((!\inst1|Add9~82\ & \inst1|Add9~62\) # (\inst1|Add9~82\ & \inst1|Add9~62COUT1_171\)))))
-- \inst1|Add9~57\ = CARRY(((!\inst1|Add9~62COUT1_171\)) # (!\inst1|TSUM[8][19]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][19]~combout\,
	cin => \inst1|Add9~82\,
	cin0 => \inst1|Add9~62\,
	cin1 => \inst1|Add9~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~55_combout\,
	cout => \inst1|Add9~57\);

-- Location: LC_X9_Y5_N4
\inst1|TSUM[8][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][23]~combout\ = LCELL((((\inst1|Add8~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][23]~combout\);

-- Location: LC_X12_Y4_N8
\inst1|TSUM[8][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][22]~combout\ = LCELL((((\inst1|Add8~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][22]~combout\);

-- Location: LC_X12_Y4_N6
\inst1|TSUM[8][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][21]~combout\ = LCELL((((\inst1|Add8~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][21]~combout\);

-- Location: LC_X11_Y4_N9
\inst1|TSUM[8][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][20]~combout\ = LCELL((((\inst1|Add8~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add8~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][20]~combout\);

-- Location: LC_X14_Y3_N5
\inst1|Add9~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~50_combout\ = (\inst1|TSUM[8][20]~combout\ $ ((!\inst1|Add9~57\)))
-- \inst1|Add9~52\ = CARRY(((\inst1|TSUM[8][20]~combout\ & !\inst1|Add9~57\)))
-- \inst1|Add9~52COUT1_172\ = CARRY(((\inst1|TSUM[8][20]~combout\ & !\inst1|Add9~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][20]~combout\,
	cin => \inst1|Add9~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~50_combout\,
	cout0 => \inst1|Add9~52\,
	cout1 => \inst1|Add9~52COUT1_172\);

-- Location: LC_X14_Y3_N6
\inst1|Add9~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~45_combout\ = \inst1|TSUM[8][21]~combout\ $ (((((!\inst1|Add9~57\ & \inst1|Add9~52\) # (\inst1|Add9~57\ & \inst1|Add9~52COUT1_172\)))))
-- \inst1|Add9~47\ = CARRY(((!\inst1|Add9~52\)) # (!\inst1|TSUM[8][21]~combout\))
-- \inst1|Add9~47COUT1_173\ = CARRY(((!\inst1|Add9~52COUT1_172\)) # (!\inst1|TSUM[8][21]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][21]~combout\,
	cin => \inst1|Add9~57\,
	cin0 => \inst1|Add9~52\,
	cin1 => \inst1|Add9~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~45_combout\,
	cout0 => \inst1|Add9~47\,
	cout1 => \inst1|Add9~47COUT1_173\);

-- Location: LC_X14_Y3_N7
\inst1|Add9~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~40_combout\ = (\inst1|TSUM[8][22]~combout\ $ ((!(!\inst1|Add9~57\ & \inst1|Add9~47\) # (\inst1|Add9~57\ & \inst1|Add9~47COUT1_173\))))
-- \inst1|Add9~42\ = CARRY(((\inst1|TSUM[8][22]~combout\ & !\inst1|Add9~47\)))
-- \inst1|Add9~42COUT1_174\ = CARRY(((\inst1|TSUM[8][22]~combout\ & !\inst1|Add9~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][22]~combout\,
	cin => \inst1|Add9~57\,
	cin0 => \inst1|Add9~47\,
	cin1 => \inst1|Add9~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~40_combout\,
	cout0 => \inst1|Add9~42\,
	cout1 => \inst1|Add9~42COUT1_174\);

-- Location: LC_X14_Y3_N8
\inst1|Add9~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~35_combout\ = \inst1|TSUM[8][23]~combout\ $ (((((!\inst1|Add9~57\ & \inst1|Add9~42\) # (\inst1|Add9~57\ & \inst1|Add9~42COUT1_174\)))))
-- \inst1|Add9~37\ = CARRY(((!\inst1|Add9~42\)) # (!\inst1|TSUM[8][23]~combout\))
-- \inst1|Add9~37COUT1_175\ = CARRY(((!\inst1|Add9~42COUT1_174\)) # (!\inst1|TSUM[8][23]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[8][23]~combout\,
	cin => \inst1|Add9~57\,
	cin0 => \inst1|Add9~42\,
	cin1 => \inst1|Add9~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~35_combout\,
	cout0 => \inst1|Add9~37\,
	cout1 => \inst1|Add9~37COUT1_175\);

-- Location: LC_X14_Y3_N9
\inst1|Add9~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~30_combout\ = (\inst1|TSUM[8][24]~combout\ $ ((!(!\inst1|Add9~57\ & \inst1|Add9~37\) # (\inst1|Add9~57\ & \inst1|Add9~37COUT1_175\))))
-- \inst1|Add9~32\ = CARRY(((\inst1|TSUM[8][24]~combout\ & !\inst1|Add9~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][24]~combout\,
	cin => \inst1|Add9~57\,
	cin0 => \inst1|Add9~37\,
	cin1 => \inst1|Add9~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~30_combout\,
	cout => \inst1|Add9~32\);

-- Location: LC_X10_Y4_N9
\inst1|TSUM[8][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][28]~combout\ = LCELL((((\inst1|Add8~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][28]~combout\);

-- Location: LC_X16_Y3_N2
\inst1|TSUM[8][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][27]~combout\ = LCELL((((\inst1|Add8~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][27]~combout\);

-- Location: LC_X10_Y4_N7
\inst1|TSUM[8][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][26]~combout\ = LCELL((((\inst1|Add8~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][26]~combout\);

-- Location: LC_X16_Y3_N4
\inst1|TSUM[8][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[8][25]~combout\ = LCELL((((\inst1|Add8~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add8~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[8][25]~combout\);

-- Location: LC_X15_Y3_N0
\inst1|Add9~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~25_combout\ = (\inst1|TSUM[8][25]~combout\ $ ((\inst1|Add9~32\)))
-- \inst1|Add9~27\ = CARRY(((!\inst1|Add9~32\) # (!\inst1|TSUM[8][25]~combout\)))
-- \inst1|Add9~27COUT1_176\ = CARRY(((!\inst1|Add9~32\) # (!\inst1|TSUM[8][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][25]~combout\,
	cin => \inst1|Add9~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~25_combout\,
	cout0 => \inst1|Add9~27\,
	cout1 => \inst1|Add9~27COUT1_176\);

-- Location: LC_X15_Y3_N1
\inst1|Add9~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~20_combout\ = (\inst1|TSUM[8][26]~combout\ $ ((!(!\inst1|Add9~32\ & \inst1|Add9~27\) # (\inst1|Add9~32\ & \inst1|Add9~27COUT1_176\))))
-- \inst1|Add9~22\ = CARRY(((\inst1|TSUM[8][26]~combout\ & !\inst1|Add9~27\)))
-- \inst1|Add9~22COUT1_177\ = CARRY(((\inst1|TSUM[8][26]~combout\ & !\inst1|Add9~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][26]~combout\,
	cin => \inst1|Add9~32\,
	cin0 => \inst1|Add9~27\,
	cin1 => \inst1|Add9~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~20_combout\,
	cout0 => \inst1|Add9~22\,
	cout1 => \inst1|Add9~22COUT1_177\);

-- Location: LC_X15_Y3_N2
\inst1|Add9~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~15_combout\ = (\inst1|TSUM[8][27]~combout\ $ (((!\inst1|Add9~32\ & \inst1|Add9~22\) # (\inst1|Add9~32\ & \inst1|Add9~22COUT1_177\))))
-- \inst1|Add9~17\ = CARRY(((!\inst1|Add9~22\) # (!\inst1|TSUM[8][27]~combout\)))
-- \inst1|Add9~17COUT1_178\ = CARRY(((!\inst1|Add9~22COUT1_177\) # (!\inst1|TSUM[8][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][27]~combout\,
	cin => \inst1|Add9~32\,
	cin0 => \inst1|Add9~22\,
	cin1 => \inst1|Add9~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~15_combout\,
	cout0 => \inst1|Add9~17\,
	cout1 => \inst1|Add9~17COUT1_178\);

-- Location: LC_X15_Y3_N3
\inst1|Add9~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~10_combout\ = (\inst1|TSUM[8][28]~combout\ $ ((!(!\inst1|Add9~32\ & \inst1|Add9~17\) # (\inst1|Add9~32\ & \inst1|Add9~17COUT1_178\))))
-- \inst1|Add9~12\ = CARRY(((\inst1|TSUM[8][28]~combout\ & !\inst1|Add9~17\)))
-- \inst1|Add9~12COUT1_179\ = CARRY(((\inst1|TSUM[8][28]~combout\ & !\inst1|Add9~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][28]~combout\,
	cin => \inst1|Add9~32\,
	cin0 => \inst1|Add9~17\,
	cin1 => \inst1|Add9~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~10_combout\,
	cout0 => \inst1|Add9~12\,
	cout1 => \inst1|Add9~12COUT1_179\);

-- Location: LC_X15_Y3_N4
\inst1|Add9~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~5_combout\ = (\inst1|TSUM[8][29]~combout\ $ (((!\inst1|Add9~32\ & \inst1|Add9~12\) # (\inst1|Add9~32\ & \inst1|Add9~12COUT1_179\))))
-- \inst1|Add9~7\ = CARRY(((!\inst1|Add9~12COUT1_179\) # (!\inst1|TSUM[8][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[8][29]~combout\,
	cin => \inst1|Add9~32\,
	cin0 => \inst1|Add9~12\,
	cin1 => \inst1|Add9~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~5_combout\,
	cout => \inst1|Add9~7\);

-- Location: LC_X15_Y3_N5
\inst1|Add9~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add9~0_combout\ = ((\inst1|Add9~7\ $ (!\inst1|TSUM[8][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[8][30]~combout\,
	cin => \inst1|Add9~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add9~0_combout\);

-- Location: LC_X15_Y3_N8
\inst1|TSUM[9][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][30]~combout\ = LCELL((((\inst1|Add9~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][30]~combout\);

-- Location: LC_X15_Y3_N7
\inst1|TSUM[9][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][29]~combout\ = LCELL((((\inst1|Add9~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][29]~combout\);

-- Location: LC_X14_Y1_N7
\inst1|TSUM[9][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][24]~combout\ = LCELL((((\inst1|Add9~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][24]~combout\);

-- Location: LC_X14_Y1_N4
\inst1|TSUM[9][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][19]~combout\ = LCELL((((\inst1|Add9~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][19]~combout\);

-- Location: LC_X13_Y1_N9
\inst1|TSUM[9][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][14]~combout\ = LCELL((((\inst1|Add9~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][14]~combout\);

-- Location: LC_X13_Y1_N1
\inst1|TSUM[9][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][9]~combout\ = LCELL((((\inst1|Add9~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][9]~combout\);

-- Location: LC_X12_Y1_N0
\inst1|PSUM[11][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][0]~regout\ = DFFEAS(((!\inst1|PSUM[11][0]~regout\)), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][0]~99\ = CARRY(((\inst1|PSUM[11][0]~regout\)))
-- \inst1|PSUM[11][0]~99COUT1_353\ = CARRY(((\inst1|PSUM[11][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	datab => \inst1|PSUM[11][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][0]~regout\,
	cout0 => \inst1|PSUM[11][0]~99\,
	cout1 => \inst1|PSUM[11][0]~99COUT1_353\);

-- Location: LC_X12_Y1_N1
\inst1|PSUM[11][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][1]~regout\ = DFFEAS((\inst1|PSUM[11][1]~regout\ $ ((\inst1|PSUM[11][0]~99\))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][1]~97\ = CARRY(((!\inst1|PSUM[11][0]~99\) # (!\inst1|PSUM[11][1]~regout\)))
-- \inst1|PSUM[11][1]~97COUT1_354\ = CARRY(((!\inst1|PSUM[11][0]~99COUT1_353\) # (!\inst1|PSUM[11][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	datab => \inst1|PSUM[11][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[11][0]~99\,
	cin1 => \inst1|PSUM[11][0]~99COUT1_353\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][1]~regout\,
	cout0 => \inst1|PSUM[11][1]~97\,
	cout1 => \inst1|PSUM[11][1]~97COUT1_354\);

-- Location: LC_X12_Y1_N2
\inst1|PSUM[11][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][2]~regout\ = DFFEAS((\inst1|PSUM[11][2]~regout\ $ ((!\inst1|PSUM[11][1]~97\))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][2]~95\ = CARRY(((\inst1|PSUM[11][2]~regout\ & !\inst1|PSUM[11][1]~97\)))
-- \inst1|PSUM[11][2]~95COUT1_355\ = CARRY(((\inst1|PSUM[11][2]~regout\ & !\inst1|PSUM[11][1]~97COUT1_354\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	datab => \inst1|PSUM[11][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[11][1]~97\,
	cin1 => \inst1|PSUM[11][1]~97COUT1_354\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][2]~regout\,
	cout0 => \inst1|PSUM[11][2]~95\,
	cout1 => \inst1|PSUM[11][2]~95COUT1_355\);

-- Location: LC_X12_Y1_N3
\inst1|PSUM[11][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][3]~regout\ = DFFEAS(\inst1|PSUM[11][3]~regout\ $ ((((\inst1|PSUM[11][2]~95\)))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][3]~93\ = CARRY(((!\inst1|PSUM[11][2]~95\)) # (!\inst1|PSUM[11][3]~regout\))
-- \inst1|PSUM[11][3]~93COUT1_356\ = CARRY(((!\inst1|PSUM[11][2]~95COUT1_355\)) # (!\inst1|PSUM[11][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	dataa => \inst1|PSUM[11][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[11][2]~95\,
	cin1 => \inst1|PSUM[11][2]~95COUT1_355\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][3]~regout\,
	cout0 => \inst1|PSUM[11][3]~93\,
	cout1 => \inst1|PSUM[11][3]~93COUT1_356\);

-- Location: LC_X12_Y1_N4
\inst1|PSUM[11][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][4]~regout\ = DFFEAS(\inst1|PSUM[11][4]~regout\ $ ((((!\inst1|PSUM[11][3]~93\)))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][4]~91\ = CARRY((\inst1|PSUM[11][4]~regout\ & ((!\inst1|PSUM[11][3]~93COUT1_356\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	dataa => \inst1|PSUM[11][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[11][3]~93\,
	cin1 => \inst1|PSUM[11][3]~93COUT1_356\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][4]~regout\,
	cout => \inst1|PSUM[11][4]~91\);

-- Location: LC_X12_Y1_N5
\inst1|PSUM[11][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][5]~regout\ = DFFEAS(\inst1|PSUM[11][5]~regout\ $ ((((\inst1|PSUM[11][4]~91\)))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][5]~89\ = CARRY(((!\inst1|PSUM[11][4]~91\)) # (!\inst1|PSUM[11][5]~regout\))
-- \inst1|PSUM[11][5]~89COUT1_357\ = CARRY(((!\inst1|PSUM[11][4]~91\)) # (!\inst1|PSUM[11][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	dataa => \inst1|PSUM[11][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[11][4]~91\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][5]~regout\,
	cout0 => \inst1|PSUM[11][5]~89\,
	cout1 => \inst1|PSUM[11][5]~89COUT1_357\);

-- Location: LC_X12_Y1_N6
\inst1|PSUM[11][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][6]~regout\ = DFFEAS(\inst1|PSUM[11][6]~regout\ $ ((((!(!\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][5]~89\) # (\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][5]~89COUT1_357\))))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][6]~87\ = CARRY((\inst1|PSUM[11][6]~regout\ & ((!\inst1|PSUM[11][5]~89\))))
-- \inst1|PSUM[11][6]~87COUT1_358\ = CARRY((\inst1|PSUM[11][6]~regout\ & ((!\inst1|PSUM[11][5]~89COUT1_357\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	dataa => \inst1|PSUM[11][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[11][4]~91\,
	cin0 => \inst1|PSUM[11][5]~89\,
	cin1 => \inst1|PSUM[11][5]~89COUT1_357\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][6]~regout\,
	cout0 => \inst1|PSUM[11][6]~87\,
	cout1 => \inst1|PSUM[11][6]~87COUT1_358\);

-- Location: LC_X12_Y1_N7
\inst1|PSUM[11][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][7]~regout\ = DFFEAS((\inst1|PSUM[11][7]~regout\ $ (((!\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][6]~87\) # (\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][6]~87COUT1_358\)))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][7]~85\ = CARRY(((!\inst1|PSUM[11][6]~87\) # (!\inst1|PSUM[11][7]~regout\)))
-- \inst1|PSUM[11][7]~85COUT1_359\ = CARRY(((!\inst1|PSUM[11][6]~87COUT1_358\) # (!\inst1|PSUM[11][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	datab => \inst1|PSUM[11][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[11][4]~91\,
	cin0 => \inst1|PSUM[11][6]~87\,
	cin1 => \inst1|PSUM[11][6]~87COUT1_358\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][7]~regout\,
	cout0 => \inst1|PSUM[11][7]~85\,
	cout1 => \inst1|PSUM[11][7]~85COUT1_359\);

-- Location: LC_X12_Y1_N8
\inst1|PSUM[11][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][8]~regout\ = DFFEAS(\inst1|PSUM[11][8]~regout\ $ ((((!(!\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][7]~85\) # (\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][7]~85COUT1_359\))))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[11][8]~83\ = CARRY((\inst1|PSUM[11][8]~regout\ & ((!\inst1|PSUM[11][7]~85\))))
-- \inst1|PSUM[11][8]~83COUT1_360\ = CARRY((\inst1|PSUM[11][8]~regout\ & ((!\inst1|PSUM[11][7]~85COUT1_359\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	dataa => \inst1|PSUM[11][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[11][4]~91\,
	cin0 => \inst1|PSUM[11][7]~85\,
	cin1 => \inst1|PSUM[11][7]~85COUT1_359\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][8]~regout\,
	cout0 => \inst1|PSUM[11][8]~83\,
	cout1 => \inst1|PSUM[11][8]~83COUT1_360\);

-- Location: LC_X12_Y1_N9
\inst1|PSUM[11][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[11][9]~regout\ = DFFEAS((((!\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][8]~83\) # (\inst1|PSUM[11][4]~91\ & \inst1|PSUM[11][8]~83COUT1_360\) $ (\inst1|PSUM[11][9]~regout\))), GLOBAL(\inst6|STEMP\(11)), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(11),
	datad => \inst1|PSUM[11][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[11][4]~91\,
	cin0 => \inst1|PSUM[11][8]~83\,
	cin1 => \inst1|PSUM[11][8]~83COUT1_360\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[11][9]~regout\);

-- Location: LC_X12_Y2_N1
\inst1|TSUM[9][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][4]~combout\ = LCELL((((\inst1|Add9~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][4]~combout\);

-- Location: LC_X12_Y3_N0
\inst1|TSUM[9][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][3]~combout\ = LCELL((((\inst1|Add9~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][3]~combout\);

-- Location: LC_X12_Y2_N3
\inst1|TSUM[9][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][2]~combout\ = LCELL((((\inst1|Add9~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][2]~combout\);

-- Location: LC_X12_Y2_N0
\inst1|TSUM[9][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][1]~combout\ = LCELL((((\inst1|Add9~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][1]~combout\);

-- Location: LC_X12_Y3_N3
\inst1|TSUM[9][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][0]~combout\ = LCELL((((\inst1|Add9~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][0]~combout\);

-- Location: LC_X12_Y2_N5
\inst1|Add10~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~150_combout\ = \inst1|PSUM[11][0]~regout\ $ ((\inst1|TSUM[9][0]~combout\))
-- \inst1|Add10~152\ = CARRY((\inst1|PSUM[11][0]~regout\ & (\inst1|TSUM[9][0]~combout\)))
-- \inst1|Add10~152COUT1_156\ = CARRY((\inst1|PSUM[11][0]~regout\ & (\inst1|TSUM[9][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[11][0]~regout\,
	datab => \inst1|TSUM[9][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~150_combout\,
	cout0 => \inst1|Add10~152\,
	cout1 => \inst1|Add10~152COUT1_156\);

-- Location: LC_X12_Y2_N6
\inst1|Add10~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~145_combout\ = \inst1|PSUM[11][1]~regout\ $ (\inst1|TSUM[9][1]~combout\ $ ((\inst1|Add10~152\)))
-- \inst1|Add10~147\ = CARRY((\inst1|PSUM[11][1]~regout\ & (!\inst1|TSUM[9][1]~combout\ & !\inst1|Add10~152\)) # (!\inst1|PSUM[11][1]~regout\ & ((!\inst1|Add10~152\) # (!\inst1|TSUM[9][1]~combout\))))
-- \inst1|Add10~147COUT1_157\ = CARRY((\inst1|PSUM[11][1]~regout\ & (!\inst1|TSUM[9][1]~combout\ & !\inst1|Add10~152COUT1_156\)) # (!\inst1|PSUM[11][1]~regout\ & ((!\inst1|Add10~152COUT1_156\) # (!\inst1|TSUM[9][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[11][1]~regout\,
	datab => \inst1|TSUM[9][1]~combout\,
	cin0 => \inst1|Add10~152\,
	cin1 => \inst1|Add10~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~145_combout\,
	cout0 => \inst1|Add10~147\,
	cout1 => \inst1|Add10~147COUT1_157\);

-- Location: LC_X12_Y2_N7
\inst1|Add10~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~140_combout\ = \inst1|TSUM[9][2]~combout\ $ (\inst1|PSUM[11][2]~regout\ $ ((!\inst1|Add10~147\)))
-- \inst1|Add10~142\ = CARRY((\inst1|TSUM[9][2]~combout\ & ((\inst1|PSUM[11][2]~regout\) # (!\inst1|Add10~147\))) # (!\inst1|TSUM[9][2]~combout\ & (\inst1|PSUM[11][2]~regout\ & !\inst1|Add10~147\)))
-- \inst1|Add10~142COUT1_158\ = CARRY((\inst1|TSUM[9][2]~combout\ & ((\inst1|PSUM[11][2]~regout\) # (!\inst1|Add10~147COUT1_157\))) # (!\inst1|TSUM[9][2]~combout\ & (\inst1|PSUM[11][2]~regout\ & !\inst1|Add10~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][2]~combout\,
	datab => \inst1|PSUM[11][2]~regout\,
	cin0 => \inst1|Add10~147\,
	cin1 => \inst1|Add10~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~140_combout\,
	cout0 => \inst1|Add10~142\,
	cout1 => \inst1|Add10~142COUT1_158\);

-- Location: LC_X12_Y2_N8
\inst1|Add10~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~135_combout\ = \inst1|TSUM[9][3]~combout\ $ (\inst1|PSUM[11][3]~regout\ $ ((\inst1|Add10~142\)))
-- \inst1|Add10~137\ = CARRY((\inst1|TSUM[9][3]~combout\ & (!\inst1|PSUM[11][3]~regout\ & !\inst1|Add10~142\)) # (!\inst1|TSUM[9][3]~combout\ & ((!\inst1|Add10~142\) # (!\inst1|PSUM[11][3]~regout\))))
-- \inst1|Add10~137COUT1_159\ = CARRY((\inst1|TSUM[9][3]~combout\ & (!\inst1|PSUM[11][3]~regout\ & !\inst1|Add10~142COUT1_158\)) # (!\inst1|TSUM[9][3]~combout\ & ((!\inst1|Add10~142COUT1_158\) # (!\inst1|PSUM[11][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][3]~combout\,
	datab => \inst1|PSUM[11][3]~regout\,
	cin0 => \inst1|Add10~142\,
	cin1 => \inst1|Add10~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~135_combout\,
	cout0 => \inst1|Add10~137\,
	cout1 => \inst1|Add10~137COUT1_159\);

-- Location: LC_X12_Y2_N9
\inst1|Add10~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~130_combout\ = \inst1|PSUM[11][4]~regout\ $ (\inst1|TSUM[9][4]~combout\ $ ((!\inst1|Add10~137\)))
-- \inst1|Add10~132\ = CARRY((\inst1|PSUM[11][4]~regout\ & ((\inst1|TSUM[9][4]~combout\) # (!\inst1|Add10~137COUT1_159\))) # (!\inst1|PSUM[11][4]~regout\ & (\inst1|TSUM[9][4]~combout\ & !\inst1|Add10~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[11][4]~regout\,
	datab => \inst1|TSUM[9][4]~combout\,
	cin0 => \inst1|Add10~137\,
	cin1 => \inst1|Add10~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~130_combout\,
	cout => \inst1|Add10~132\);

-- Location: LC_X13_Y1_N8
\inst1|TSUM[9][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][8]~combout\ = LCELL((((\inst1|Add9~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][8]~combout\);

-- Location: LC_X13_Y1_N4
\inst1|TSUM[9][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][7]~combout\ = LCELL((((\inst1|Add9~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][7]~combout\);

-- Location: LC_X13_Y1_N5
\inst1|TSUM[9][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][6]~combout\ = LCELL((((\inst1|Add9~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][6]~combout\);

-- Location: LC_X13_Y1_N0
\inst1|TSUM[9][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][5]~combout\ = LCELL((((\inst1|Add9~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][5]~combout\);

-- Location: LC_X13_Y2_N0
\inst1|Add10~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~125_combout\ = \inst1|PSUM[11][5]~regout\ $ (\inst1|TSUM[9][5]~combout\ $ ((\inst1|Add10~132\)))
-- \inst1|Add10~127\ = CARRY((\inst1|PSUM[11][5]~regout\ & (!\inst1|TSUM[9][5]~combout\ & !\inst1|Add10~132\)) # (!\inst1|PSUM[11][5]~regout\ & ((!\inst1|Add10~132\) # (!\inst1|TSUM[9][5]~combout\))))
-- \inst1|Add10~127COUT1_160\ = CARRY((\inst1|PSUM[11][5]~regout\ & (!\inst1|TSUM[9][5]~combout\ & !\inst1|Add10~132\)) # (!\inst1|PSUM[11][5]~regout\ & ((!\inst1|Add10~132\) # (!\inst1|TSUM[9][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[11][5]~regout\,
	datab => \inst1|TSUM[9][5]~combout\,
	cin => \inst1|Add10~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~125_combout\,
	cout0 => \inst1|Add10~127\,
	cout1 => \inst1|Add10~127COUT1_160\);

-- Location: LC_X13_Y2_N1
\inst1|Add10~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~120_combout\ = \inst1|PSUM[11][6]~regout\ $ (\inst1|TSUM[9][6]~combout\ $ ((!(!\inst1|Add10~132\ & \inst1|Add10~127\) # (\inst1|Add10~132\ & \inst1|Add10~127COUT1_160\))))
-- \inst1|Add10~122\ = CARRY((\inst1|PSUM[11][6]~regout\ & ((\inst1|TSUM[9][6]~combout\) # (!\inst1|Add10~127\))) # (!\inst1|PSUM[11][6]~regout\ & (\inst1|TSUM[9][6]~combout\ & !\inst1|Add10~127\)))
-- \inst1|Add10~122COUT1_161\ = CARRY((\inst1|PSUM[11][6]~regout\ & ((\inst1|TSUM[9][6]~combout\) # (!\inst1|Add10~127COUT1_160\))) # (!\inst1|PSUM[11][6]~regout\ & (\inst1|TSUM[9][6]~combout\ & !\inst1|Add10~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[11][6]~regout\,
	datab => \inst1|TSUM[9][6]~combout\,
	cin => \inst1|Add10~132\,
	cin0 => \inst1|Add10~127\,
	cin1 => \inst1|Add10~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~120_combout\,
	cout0 => \inst1|Add10~122\,
	cout1 => \inst1|Add10~122COUT1_161\);

-- Location: LC_X13_Y2_N2
\inst1|Add10~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~115_combout\ = \inst1|PSUM[11][7]~regout\ $ (\inst1|TSUM[9][7]~combout\ $ (((!\inst1|Add10~132\ & \inst1|Add10~122\) # (\inst1|Add10~132\ & \inst1|Add10~122COUT1_161\))))
-- \inst1|Add10~117\ = CARRY((\inst1|PSUM[11][7]~regout\ & (!\inst1|TSUM[9][7]~combout\ & !\inst1|Add10~122\)) # (!\inst1|PSUM[11][7]~regout\ & ((!\inst1|Add10~122\) # (!\inst1|TSUM[9][7]~combout\))))
-- \inst1|Add10~117COUT1_162\ = CARRY((\inst1|PSUM[11][7]~regout\ & (!\inst1|TSUM[9][7]~combout\ & !\inst1|Add10~122COUT1_161\)) # (!\inst1|PSUM[11][7]~regout\ & ((!\inst1|Add10~122COUT1_161\) # (!\inst1|TSUM[9][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[11][7]~regout\,
	datab => \inst1|TSUM[9][7]~combout\,
	cin => \inst1|Add10~132\,
	cin0 => \inst1|Add10~122\,
	cin1 => \inst1|Add10~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~115_combout\,
	cout0 => \inst1|Add10~117\,
	cout1 => \inst1|Add10~117COUT1_162\);

-- Location: LC_X13_Y2_N3
\inst1|Add10~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~110_combout\ = \inst1|TSUM[9][8]~combout\ $ (\inst1|PSUM[11][8]~regout\ $ ((!(!\inst1|Add10~132\ & \inst1|Add10~117\) # (\inst1|Add10~132\ & \inst1|Add10~117COUT1_162\))))
-- \inst1|Add10~112\ = CARRY((\inst1|TSUM[9][8]~combout\ & ((\inst1|PSUM[11][8]~regout\) # (!\inst1|Add10~117\))) # (!\inst1|TSUM[9][8]~combout\ & (\inst1|PSUM[11][8]~regout\ & !\inst1|Add10~117\)))
-- \inst1|Add10~112COUT1_163\ = CARRY((\inst1|TSUM[9][8]~combout\ & ((\inst1|PSUM[11][8]~regout\) # (!\inst1|Add10~117COUT1_162\))) # (!\inst1|TSUM[9][8]~combout\ & (\inst1|PSUM[11][8]~regout\ & !\inst1|Add10~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][8]~combout\,
	datab => \inst1|PSUM[11][8]~regout\,
	cin => \inst1|Add10~132\,
	cin0 => \inst1|Add10~117\,
	cin1 => \inst1|Add10~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~110_combout\,
	cout0 => \inst1|Add10~112\,
	cout1 => \inst1|Add10~112COUT1_163\);

-- Location: LC_X13_Y2_N4
\inst1|Add10~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~105_combout\ = \inst1|TSUM[9][9]~combout\ $ (\inst1|PSUM[11][9]~regout\ $ (((!\inst1|Add10~132\ & \inst1|Add10~112\) # (\inst1|Add10~132\ & \inst1|Add10~112COUT1_163\))))
-- \inst1|Add10~107\ = CARRY((\inst1|TSUM[9][9]~combout\ & (!\inst1|PSUM[11][9]~regout\ & !\inst1|Add10~112COUT1_163\)) # (!\inst1|TSUM[9][9]~combout\ & ((!\inst1|Add10~112COUT1_163\) # (!\inst1|PSUM[11][9]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][9]~combout\,
	datab => \inst1|PSUM[11][9]~regout\,
	cin => \inst1|Add10~132\,
	cin0 => \inst1|Add10~112\,
	cin1 => \inst1|Add10~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~105_combout\,
	cout => \inst1|Add10~107\);

-- Location: LC_X13_Y1_N7
\inst1|TSUM[9][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][13]~combout\ = LCELL((((\inst1|Add9~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][13]~combout\);

-- Location: LC_X13_Y1_N3
\inst1|TSUM[9][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][12]~combout\ = LCELL((((\inst1|Add9~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][12]~combout\);

-- Location: LC_X13_Y1_N2
\inst1|TSUM[9][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][11]~combout\ = LCELL((((\inst1|Add9~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][11]~combout\);

-- Location: LC_X13_Y1_N6
\inst1|TSUM[9][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][10]~combout\ = LCELL((((\inst1|Add9~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][10]~combout\);

-- Location: LC_X13_Y2_N5
\inst1|Add10~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~100_combout\ = \inst1|TSUM[9][10]~combout\ $ ((((!\inst1|Add10~107\))))
-- \inst1|Add10~102\ = CARRY((\inst1|TSUM[9][10]~combout\ & ((!\inst1|Add10~107\))))
-- \inst1|Add10~102COUT1_164\ = CARRY((\inst1|TSUM[9][10]~combout\ & ((!\inst1|Add10~107\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][10]~combout\,
	cin => \inst1|Add10~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~100_combout\,
	cout0 => \inst1|Add10~102\,
	cout1 => \inst1|Add10~102COUT1_164\);

-- Location: LC_X13_Y2_N6
\inst1|Add10~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~95_combout\ = (\inst1|TSUM[9][11]~combout\ $ (((!\inst1|Add10~107\ & \inst1|Add10~102\) # (\inst1|Add10~107\ & \inst1|Add10~102COUT1_164\))))
-- \inst1|Add10~97\ = CARRY(((!\inst1|Add10~102\) # (!\inst1|TSUM[9][11]~combout\)))
-- \inst1|Add10~97COUT1_165\ = CARRY(((!\inst1|Add10~102COUT1_164\) # (!\inst1|TSUM[9][11]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][11]~combout\,
	cin => \inst1|Add10~107\,
	cin0 => \inst1|Add10~102\,
	cin1 => \inst1|Add10~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~95_combout\,
	cout0 => \inst1|Add10~97\,
	cout1 => \inst1|Add10~97COUT1_165\);

-- Location: LC_X13_Y2_N7
\inst1|Add10~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~90_combout\ = (\inst1|TSUM[9][12]~combout\ $ ((!(!\inst1|Add10~107\ & \inst1|Add10~97\) # (\inst1|Add10~107\ & \inst1|Add10~97COUT1_165\))))
-- \inst1|Add10~92\ = CARRY(((\inst1|TSUM[9][12]~combout\ & !\inst1|Add10~97\)))
-- \inst1|Add10~92COUT1_166\ = CARRY(((\inst1|TSUM[9][12]~combout\ & !\inst1|Add10~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][12]~combout\,
	cin => \inst1|Add10~107\,
	cin0 => \inst1|Add10~97\,
	cin1 => \inst1|Add10~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~90_combout\,
	cout0 => \inst1|Add10~92\,
	cout1 => \inst1|Add10~92COUT1_166\);

-- Location: LC_X13_Y2_N8
\inst1|Add10~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~85_combout\ = (\inst1|TSUM[9][13]~combout\ $ (((!\inst1|Add10~107\ & \inst1|Add10~92\) # (\inst1|Add10~107\ & \inst1|Add10~92COUT1_166\))))
-- \inst1|Add10~87\ = CARRY(((!\inst1|Add10~92\) # (!\inst1|TSUM[9][13]~combout\)))
-- \inst1|Add10~87COUT1_167\ = CARRY(((!\inst1|Add10~92COUT1_166\) # (!\inst1|TSUM[9][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][13]~combout\,
	cin => \inst1|Add10~107\,
	cin0 => \inst1|Add10~92\,
	cin1 => \inst1|Add10~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~85_combout\,
	cout0 => \inst1|Add10~87\,
	cout1 => \inst1|Add10~87COUT1_167\);

-- Location: LC_X13_Y2_N9
\inst1|Add10~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~80_combout\ = (\inst1|TSUM[9][14]~combout\ $ ((!(!\inst1|Add10~107\ & \inst1|Add10~87\) # (\inst1|Add10~107\ & \inst1|Add10~87COUT1_167\))))
-- \inst1|Add10~82\ = CARRY(((\inst1|TSUM[9][14]~combout\ & !\inst1|Add10~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][14]~combout\,
	cin => \inst1|Add10~107\,
	cin0 => \inst1|Add10~87\,
	cin1 => \inst1|Add10~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~80_combout\,
	cout => \inst1|Add10~82\);

-- Location: LC_X14_Y1_N3
\inst1|TSUM[9][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][18]~combout\ = LCELL((((\inst1|Add9~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][18]~combout\);

-- Location: LC_X14_Y1_N1
\inst1|TSUM[9][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][17]~combout\ = LCELL((((\inst1|Add9~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][17]~combout\);

-- Location: LC_X14_Y1_N2
\inst1|TSUM[9][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][16]~combout\ = LCELL((((\inst1|Add9~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][16]~combout\);

-- Location: LC_X14_Y1_N0
\inst1|TSUM[9][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][15]~combout\ = LCELL((((\inst1|Add9~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][15]~combout\);

-- Location: LC_X14_Y2_N0
\inst1|Add10~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~75_combout\ = (\inst1|TSUM[9][15]~combout\ $ ((\inst1|Add10~82\)))
-- \inst1|Add10~77\ = CARRY(((!\inst1|Add10~82\) # (!\inst1|TSUM[9][15]~combout\)))
-- \inst1|Add10~77COUT1_168\ = CARRY(((!\inst1|Add10~82\) # (!\inst1|TSUM[9][15]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][15]~combout\,
	cin => \inst1|Add10~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~75_combout\,
	cout0 => \inst1|Add10~77\,
	cout1 => \inst1|Add10~77COUT1_168\);

-- Location: LC_X14_Y2_N1
\inst1|Add10~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~70_combout\ = (\inst1|TSUM[9][16]~combout\ $ ((!(!\inst1|Add10~82\ & \inst1|Add10~77\) # (\inst1|Add10~82\ & \inst1|Add10~77COUT1_168\))))
-- \inst1|Add10~72\ = CARRY(((\inst1|TSUM[9][16]~combout\ & !\inst1|Add10~77\)))
-- \inst1|Add10~72COUT1_169\ = CARRY(((\inst1|TSUM[9][16]~combout\ & !\inst1|Add10~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][16]~combout\,
	cin => \inst1|Add10~82\,
	cin0 => \inst1|Add10~77\,
	cin1 => \inst1|Add10~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~70_combout\,
	cout0 => \inst1|Add10~72\,
	cout1 => \inst1|Add10~72COUT1_169\);

-- Location: LC_X14_Y2_N2
\inst1|Add10~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~65_combout\ = \inst1|TSUM[9][17]~combout\ $ (((((!\inst1|Add10~82\ & \inst1|Add10~72\) # (\inst1|Add10~82\ & \inst1|Add10~72COUT1_169\)))))
-- \inst1|Add10~67\ = CARRY(((!\inst1|Add10~72\)) # (!\inst1|TSUM[9][17]~combout\))
-- \inst1|Add10~67COUT1_170\ = CARRY(((!\inst1|Add10~72COUT1_169\)) # (!\inst1|TSUM[9][17]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][17]~combout\,
	cin => \inst1|Add10~82\,
	cin0 => \inst1|Add10~72\,
	cin1 => \inst1|Add10~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~65_combout\,
	cout0 => \inst1|Add10~67\,
	cout1 => \inst1|Add10~67COUT1_170\);

-- Location: LC_X14_Y2_N3
\inst1|Add10~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~60_combout\ = (\inst1|TSUM[9][18]~combout\ $ ((!(!\inst1|Add10~82\ & \inst1|Add10~67\) # (\inst1|Add10~82\ & \inst1|Add10~67COUT1_170\))))
-- \inst1|Add10~62\ = CARRY(((\inst1|TSUM[9][18]~combout\ & !\inst1|Add10~67\)))
-- \inst1|Add10~62COUT1_171\ = CARRY(((\inst1|TSUM[9][18]~combout\ & !\inst1|Add10~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][18]~combout\,
	cin => \inst1|Add10~82\,
	cin0 => \inst1|Add10~67\,
	cin1 => \inst1|Add10~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~60_combout\,
	cout0 => \inst1|Add10~62\,
	cout1 => \inst1|Add10~62COUT1_171\);

-- Location: LC_X14_Y2_N4
\inst1|Add10~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~55_combout\ = (\inst1|TSUM[9][19]~combout\ $ (((!\inst1|Add10~82\ & \inst1|Add10~62\) # (\inst1|Add10~82\ & \inst1|Add10~62COUT1_171\))))
-- \inst1|Add10~57\ = CARRY(((!\inst1|Add10~62COUT1_171\) # (!\inst1|TSUM[9][19]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][19]~combout\,
	cin => \inst1|Add10~82\,
	cin0 => \inst1|Add10~62\,
	cin1 => \inst1|Add10~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~55_combout\,
	cout => \inst1|Add10~57\);

-- Location: LC_X14_Y1_N9
\inst1|TSUM[9][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][23]~combout\ = LCELL((((\inst1|Add9~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][23]~combout\);

-- Location: LC_X14_Y1_N8
\inst1|TSUM[9][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][22]~combout\ = LCELL((((\inst1|Add9~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][22]~combout\);

-- Location: LC_X14_Y1_N6
\inst1|TSUM[9][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][21]~combout\ = LCELL((((\inst1|Add9~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][21]~combout\);

-- Location: LC_X14_Y1_N5
\inst1|TSUM[9][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][20]~combout\ = LCELL((((\inst1|Add9~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][20]~combout\);

-- Location: LC_X14_Y2_N5
\inst1|Add10~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~50_combout\ = (\inst1|TSUM[9][20]~combout\ $ ((!\inst1|Add10~57\)))
-- \inst1|Add10~52\ = CARRY(((\inst1|TSUM[9][20]~combout\ & !\inst1|Add10~57\)))
-- \inst1|Add10~52COUT1_172\ = CARRY(((\inst1|TSUM[9][20]~combout\ & !\inst1|Add10~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][20]~combout\,
	cin => \inst1|Add10~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~50_combout\,
	cout0 => \inst1|Add10~52\,
	cout1 => \inst1|Add10~52COUT1_172\);

-- Location: LC_X14_Y2_N6
\inst1|Add10~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~45_combout\ = \inst1|TSUM[9][21]~combout\ $ (((((!\inst1|Add10~57\ & \inst1|Add10~52\) # (\inst1|Add10~57\ & \inst1|Add10~52COUT1_172\)))))
-- \inst1|Add10~47\ = CARRY(((!\inst1|Add10~52\)) # (!\inst1|TSUM[9][21]~combout\))
-- \inst1|Add10~47COUT1_173\ = CARRY(((!\inst1|Add10~52COUT1_172\)) # (!\inst1|TSUM[9][21]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][21]~combout\,
	cin => \inst1|Add10~57\,
	cin0 => \inst1|Add10~52\,
	cin1 => \inst1|Add10~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~45_combout\,
	cout0 => \inst1|Add10~47\,
	cout1 => \inst1|Add10~47COUT1_173\);

-- Location: LC_X14_Y2_N7
\inst1|Add10~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~40_combout\ = (\inst1|TSUM[9][22]~combout\ $ ((!(!\inst1|Add10~57\ & \inst1|Add10~47\) # (\inst1|Add10~57\ & \inst1|Add10~47COUT1_173\))))
-- \inst1|Add10~42\ = CARRY(((\inst1|TSUM[9][22]~combout\ & !\inst1|Add10~47\)))
-- \inst1|Add10~42COUT1_174\ = CARRY(((\inst1|TSUM[9][22]~combout\ & !\inst1|Add10~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][22]~combout\,
	cin => \inst1|Add10~57\,
	cin0 => \inst1|Add10~47\,
	cin1 => \inst1|Add10~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~40_combout\,
	cout0 => \inst1|Add10~42\,
	cout1 => \inst1|Add10~42COUT1_174\);

-- Location: LC_X14_Y2_N8
\inst1|Add10~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~35_combout\ = (\inst1|TSUM[9][23]~combout\ $ (((!\inst1|Add10~57\ & \inst1|Add10~42\) # (\inst1|Add10~57\ & \inst1|Add10~42COUT1_174\))))
-- \inst1|Add10~37\ = CARRY(((!\inst1|Add10~42\) # (!\inst1|TSUM[9][23]~combout\)))
-- \inst1|Add10~37COUT1_175\ = CARRY(((!\inst1|Add10~42COUT1_174\) # (!\inst1|TSUM[9][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][23]~combout\,
	cin => \inst1|Add10~57\,
	cin0 => \inst1|Add10~42\,
	cin1 => \inst1|Add10~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~35_combout\,
	cout0 => \inst1|Add10~37\,
	cout1 => \inst1|Add10~37COUT1_175\);

-- Location: LC_X14_Y2_N9
\inst1|Add10~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~30_combout\ = \inst1|TSUM[9][24]~combout\ $ ((((!(!\inst1|Add10~57\ & \inst1|Add10~37\) # (\inst1|Add10~57\ & \inst1|Add10~37COUT1_175\)))))
-- \inst1|Add10~32\ = CARRY((\inst1|TSUM[9][24]~combout\ & ((!\inst1|Add10~37COUT1_175\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][24]~combout\,
	cin => \inst1|Add10~57\,
	cin0 => \inst1|Add10~37\,
	cin1 => \inst1|Add10~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~30_combout\,
	cout => \inst1|Add10~32\);

-- Location: LC_X15_Y2_N6
\inst1|TSUM[9][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][28]~combout\ = LCELL((((\inst1|Add9~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][28]~combout\);

-- Location: LC_X15_Y2_N9
\inst1|TSUM[9][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][27]~combout\ = LCELL((((\inst1|Add9~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][27]~combout\);

-- Location: LC_X15_Y3_N6
\inst1|TSUM[9][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][26]~combout\ = LCELL((((\inst1|Add9~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add9~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][26]~combout\);

-- Location: LC_X15_Y2_N7
\inst1|TSUM[9][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[9][25]~combout\ = LCELL((((\inst1|Add9~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add9~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[9][25]~combout\);

-- Location: LC_X15_Y2_N0
\inst1|Add10~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~25_combout\ = (\inst1|TSUM[9][25]~combout\ $ ((\inst1|Add10~32\)))
-- \inst1|Add10~27\ = CARRY(((!\inst1|Add10~32\) # (!\inst1|TSUM[9][25]~combout\)))
-- \inst1|Add10~27COUT1_176\ = CARRY(((!\inst1|Add10~32\) # (!\inst1|TSUM[9][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][25]~combout\,
	cin => \inst1|Add10~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~25_combout\,
	cout0 => \inst1|Add10~27\,
	cout1 => \inst1|Add10~27COUT1_176\);

-- Location: LC_X15_Y2_N1
\inst1|Add10~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~20_combout\ = (\inst1|TSUM[9][26]~combout\ $ ((!(!\inst1|Add10~32\ & \inst1|Add10~27\) # (\inst1|Add10~32\ & \inst1|Add10~27COUT1_176\))))
-- \inst1|Add10~22\ = CARRY(((\inst1|TSUM[9][26]~combout\ & !\inst1|Add10~27\)))
-- \inst1|Add10~22COUT1_177\ = CARRY(((\inst1|TSUM[9][26]~combout\ & !\inst1|Add10~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][26]~combout\,
	cin => \inst1|Add10~32\,
	cin0 => \inst1|Add10~27\,
	cin1 => \inst1|Add10~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~20_combout\,
	cout0 => \inst1|Add10~22\,
	cout1 => \inst1|Add10~22COUT1_177\);

-- Location: LC_X15_Y2_N2
\inst1|Add10~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~15_combout\ = (\inst1|TSUM[9][27]~combout\ $ (((!\inst1|Add10~32\ & \inst1|Add10~22\) # (\inst1|Add10~32\ & \inst1|Add10~22COUT1_177\))))
-- \inst1|Add10~17\ = CARRY(((!\inst1|Add10~22\) # (!\inst1|TSUM[9][27]~combout\)))
-- \inst1|Add10~17COUT1_178\ = CARRY(((!\inst1|Add10~22COUT1_177\) # (!\inst1|TSUM[9][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[9][27]~combout\,
	cin => \inst1|Add10~32\,
	cin0 => \inst1|Add10~22\,
	cin1 => \inst1|Add10~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~15_combout\,
	cout0 => \inst1|Add10~17\,
	cout1 => \inst1|Add10~17COUT1_178\);

-- Location: LC_X15_Y2_N3
\inst1|Add10~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~10_combout\ = \inst1|TSUM[9][28]~combout\ $ ((((!(!\inst1|Add10~32\ & \inst1|Add10~17\) # (\inst1|Add10~32\ & \inst1|Add10~17COUT1_178\)))))
-- \inst1|Add10~12\ = CARRY((\inst1|TSUM[9][28]~combout\ & ((!\inst1|Add10~17\))))
-- \inst1|Add10~12COUT1_179\ = CARRY((\inst1|TSUM[9][28]~combout\ & ((!\inst1|Add10~17COUT1_178\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][28]~combout\,
	cin => \inst1|Add10~32\,
	cin0 => \inst1|Add10~17\,
	cin1 => \inst1|Add10~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~10_combout\,
	cout0 => \inst1|Add10~12\,
	cout1 => \inst1|Add10~12COUT1_179\);

-- Location: LC_X15_Y2_N4
\inst1|Add10~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~5_combout\ = \inst1|TSUM[9][29]~combout\ $ (((((!\inst1|Add10~32\ & \inst1|Add10~12\) # (\inst1|Add10~32\ & \inst1|Add10~12COUT1_179\)))))
-- \inst1|Add10~7\ = CARRY(((!\inst1|Add10~12COUT1_179\)) # (!\inst1|TSUM[9][29]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[9][29]~combout\,
	cin => \inst1|Add10~32\,
	cin0 => \inst1|Add10~12\,
	cin1 => \inst1|Add10~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~5_combout\,
	cout => \inst1|Add10~7\);

-- Location: LC_X15_Y2_N5
\inst1|Add10~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add10~0_combout\ = ((\inst1|Add10~7\ $ (!\inst1|TSUM[9][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[9][30]~combout\,
	cin => \inst1|Add10~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add10~0_combout\);

-- Location: LC_X16_Y2_N5
\inst1|TSUM[10][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][30]~combout\ = LCELL((((\inst1|Add10~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][30]~combout\);

-- Location: LC_X16_Y2_N2
\inst1|TSUM[10][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][29]~combout\ = LCELL((((\inst1|Add10~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][29]~combout\);

-- Location: LC_X16_Y2_N8
\inst1|TSUM[10][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][24]~combout\ = LCELL((((\inst1|Add10~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][24]~combout\);

-- Location: LC_X15_Y1_N0
\inst1|TSUM[10][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][19]~combout\ = LCELL((((\inst1|Add10~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][19]~combout\);

-- Location: LC_X13_Y5_N7
\inst1|TSUM[10][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][14]~combout\ = LCELL((((\inst1|Add10~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][14]~combout\);

-- Location: LC_X13_Y5_N2
\inst1|TSUM[10][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][9]~combout\ = LCELL((((\inst1|Add10~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][9]~combout\);

-- Location: LC_X12_Y6_N0
\inst1|PSUM[12][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][0]~regout\ = DFFEAS(((!\inst1|PSUM[12][0]~regout\)), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][0]~79\ = CARRY(((\inst1|PSUM[12][0]~regout\)))
-- \inst1|PSUM[12][0]~79COUT1_345\ = CARRY(((\inst1|PSUM[12][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	datab => \inst1|PSUM[12][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][0]~regout\,
	cout0 => \inst1|PSUM[12][0]~79\,
	cout1 => \inst1|PSUM[12][0]~79COUT1_345\);

-- Location: LC_X12_Y6_N1
\inst1|PSUM[12][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][1]~regout\ = DFFEAS((\inst1|PSUM[12][1]~regout\ $ ((\inst1|PSUM[12][0]~79\))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][1]~77\ = CARRY(((!\inst1|PSUM[12][0]~79\) # (!\inst1|PSUM[12][1]~regout\)))
-- \inst1|PSUM[12][1]~77COUT1_346\ = CARRY(((!\inst1|PSUM[12][0]~79COUT1_345\) # (!\inst1|PSUM[12][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	datab => \inst1|PSUM[12][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[12][0]~79\,
	cin1 => \inst1|PSUM[12][0]~79COUT1_345\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][1]~regout\,
	cout0 => \inst1|PSUM[12][1]~77\,
	cout1 => \inst1|PSUM[12][1]~77COUT1_346\);

-- Location: LC_X12_Y6_N2
\inst1|PSUM[12][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][2]~regout\ = DFFEAS((\inst1|PSUM[12][2]~regout\ $ ((!\inst1|PSUM[12][1]~77\))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][2]~75\ = CARRY(((\inst1|PSUM[12][2]~regout\ & !\inst1|PSUM[12][1]~77\)))
-- \inst1|PSUM[12][2]~75COUT1_347\ = CARRY(((\inst1|PSUM[12][2]~regout\ & !\inst1|PSUM[12][1]~77COUT1_346\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	datab => \inst1|PSUM[12][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[12][1]~77\,
	cin1 => \inst1|PSUM[12][1]~77COUT1_346\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][2]~regout\,
	cout0 => \inst1|PSUM[12][2]~75\,
	cout1 => \inst1|PSUM[12][2]~75COUT1_347\);

-- Location: LC_X12_Y6_N3
\inst1|PSUM[12][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][3]~regout\ = DFFEAS(\inst1|PSUM[12][3]~regout\ $ ((((\inst1|PSUM[12][2]~75\)))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][3]~73\ = CARRY(((!\inst1|PSUM[12][2]~75\)) # (!\inst1|PSUM[12][3]~regout\))
-- \inst1|PSUM[12][3]~73COUT1_348\ = CARRY(((!\inst1|PSUM[12][2]~75COUT1_347\)) # (!\inst1|PSUM[12][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	dataa => \inst1|PSUM[12][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[12][2]~75\,
	cin1 => \inst1|PSUM[12][2]~75COUT1_347\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][3]~regout\,
	cout0 => \inst1|PSUM[12][3]~73\,
	cout1 => \inst1|PSUM[12][3]~73COUT1_348\);

-- Location: LC_X12_Y6_N4
\inst1|PSUM[12][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][4]~regout\ = DFFEAS(\inst1|PSUM[12][4]~regout\ $ ((((!\inst1|PSUM[12][3]~73\)))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][4]~71\ = CARRY((\inst1|PSUM[12][4]~regout\ & ((!\inst1|PSUM[12][3]~73COUT1_348\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	dataa => \inst1|PSUM[12][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[12][3]~73\,
	cin1 => \inst1|PSUM[12][3]~73COUT1_348\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][4]~regout\,
	cout => \inst1|PSUM[12][4]~71\);

-- Location: LC_X12_Y6_N5
\inst1|PSUM[12][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][5]~regout\ = DFFEAS(\inst1|PSUM[12][5]~regout\ $ ((((\inst1|PSUM[12][4]~71\)))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][5]~69\ = CARRY(((!\inst1|PSUM[12][4]~71\)) # (!\inst1|PSUM[12][5]~regout\))
-- \inst1|PSUM[12][5]~69COUT1_349\ = CARRY(((!\inst1|PSUM[12][4]~71\)) # (!\inst1|PSUM[12][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	dataa => \inst1|PSUM[12][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[12][4]~71\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][5]~regout\,
	cout0 => \inst1|PSUM[12][5]~69\,
	cout1 => \inst1|PSUM[12][5]~69COUT1_349\);

-- Location: LC_X12_Y6_N6
\inst1|PSUM[12][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][6]~regout\ = DFFEAS(\inst1|PSUM[12][6]~regout\ $ ((((!(!\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][5]~69\) # (\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][5]~69COUT1_349\))))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][6]~67\ = CARRY((\inst1|PSUM[12][6]~regout\ & ((!\inst1|PSUM[12][5]~69\))))
-- \inst1|PSUM[12][6]~67COUT1_350\ = CARRY((\inst1|PSUM[12][6]~regout\ & ((!\inst1|PSUM[12][5]~69COUT1_349\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	dataa => \inst1|PSUM[12][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[12][4]~71\,
	cin0 => \inst1|PSUM[12][5]~69\,
	cin1 => \inst1|PSUM[12][5]~69COUT1_349\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][6]~regout\,
	cout0 => \inst1|PSUM[12][6]~67\,
	cout1 => \inst1|PSUM[12][6]~67COUT1_350\);

-- Location: LC_X12_Y6_N7
\inst1|PSUM[12][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][7]~regout\ = DFFEAS((\inst1|PSUM[12][7]~regout\ $ (((!\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][6]~67\) # (\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][6]~67COUT1_350\)))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][7]~65\ = CARRY(((!\inst1|PSUM[12][6]~67\) # (!\inst1|PSUM[12][7]~regout\)))
-- \inst1|PSUM[12][7]~65COUT1_351\ = CARRY(((!\inst1|PSUM[12][6]~67COUT1_350\) # (!\inst1|PSUM[12][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	datab => \inst1|PSUM[12][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[12][4]~71\,
	cin0 => \inst1|PSUM[12][6]~67\,
	cin1 => \inst1|PSUM[12][6]~67COUT1_350\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][7]~regout\,
	cout0 => \inst1|PSUM[12][7]~65\,
	cout1 => \inst1|PSUM[12][7]~65COUT1_351\);

-- Location: LC_X12_Y6_N8
\inst1|PSUM[12][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][8]~regout\ = DFFEAS(\inst1|PSUM[12][8]~regout\ $ ((((!(!\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][7]~65\) # (\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][7]~65COUT1_351\))))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[12][8]~63\ = CARRY((\inst1|PSUM[12][8]~regout\ & ((!\inst1|PSUM[12][7]~65\))))
-- \inst1|PSUM[12][8]~63COUT1_352\ = CARRY((\inst1|PSUM[12][8]~regout\ & ((!\inst1|PSUM[12][7]~65COUT1_351\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	dataa => \inst1|PSUM[12][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[12][4]~71\,
	cin0 => \inst1|PSUM[12][7]~65\,
	cin1 => \inst1|PSUM[12][7]~65COUT1_351\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][8]~regout\,
	cout0 => \inst1|PSUM[12][8]~63\,
	cout1 => \inst1|PSUM[12][8]~63COUT1_352\);

-- Location: LC_X12_Y6_N9
\inst1|PSUM[12][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[12][9]~regout\ = DFFEAS((((!\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][8]~63\) # (\inst1|PSUM[12][4]~71\ & \inst1|PSUM[12][8]~63COUT1_352\) $ (\inst1|PSUM[12][9]~regout\))), \inst6|STEMP\(12), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(12),
	datad => \inst1|PSUM[12][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[12][4]~71\,
	cin0 => \inst1|PSUM[12][8]~63\,
	cin1 => \inst1|PSUM[12][8]~63COUT1_352\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[12][9]~regout\);

-- Location: LC_X12_Y4_N0
\inst1|TSUM[10][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][4]~combout\ = LCELL((((\inst1|Add10~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][4]~combout\);

-- Location: LC_X12_Y2_N4
\inst1|TSUM[10][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][3]~combout\ = LCELL((((\inst1|Add10~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][3]~combout\);

-- Location: LC_X13_Y4_N3
\inst1|TSUM[10][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][2]~combout\ = LCELL((((\inst1|Add10~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][2]~combout\);

-- Location: LC_X12_Y4_N7
\inst1|TSUM[10][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][1]~combout\ = LCELL((((\inst1|Add10~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][1]~combout\);

-- Location: LC_X12_Y2_N2
\inst1|TSUM[10][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][0]~combout\ = LCELL((((\inst1|Add10~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][0]~combout\);

-- Location: LC_X13_Y4_N5
\inst1|Add11~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~150_combout\ = \inst1|TSUM[10][0]~combout\ $ ((\inst1|PSUM[12][0]~regout\))
-- \inst1|Add11~152\ = CARRY((\inst1|TSUM[10][0]~combout\ & (\inst1|PSUM[12][0]~regout\)))
-- \inst1|Add11~152COUT1_156\ = CARRY((\inst1|TSUM[10][0]~combout\ & (\inst1|PSUM[12][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][0]~combout\,
	datab => \inst1|PSUM[12][0]~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~150_combout\,
	cout0 => \inst1|Add11~152\,
	cout1 => \inst1|Add11~152COUT1_156\);

-- Location: LC_X13_Y4_N6
\inst1|Add11~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~145_combout\ = \inst1|PSUM[12][1]~regout\ $ (\inst1|TSUM[10][1]~combout\ $ ((\inst1|Add11~152\)))
-- \inst1|Add11~147\ = CARRY((\inst1|PSUM[12][1]~regout\ & (!\inst1|TSUM[10][1]~combout\ & !\inst1|Add11~152\)) # (!\inst1|PSUM[12][1]~regout\ & ((!\inst1|Add11~152\) # (!\inst1|TSUM[10][1]~combout\))))
-- \inst1|Add11~147COUT1_157\ = CARRY((\inst1|PSUM[12][1]~regout\ & (!\inst1|TSUM[10][1]~combout\ & !\inst1|Add11~152COUT1_156\)) # (!\inst1|PSUM[12][1]~regout\ & ((!\inst1|Add11~152COUT1_156\) # (!\inst1|TSUM[10][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[12][1]~regout\,
	datab => \inst1|TSUM[10][1]~combout\,
	cin0 => \inst1|Add11~152\,
	cin1 => \inst1|Add11~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~145_combout\,
	cout0 => \inst1|Add11~147\,
	cout1 => \inst1|Add11~147COUT1_157\);

-- Location: LC_X13_Y4_N7
\inst1|Add11~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~140_combout\ = \inst1|TSUM[10][2]~combout\ $ (\inst1|PSUM[12][2]~regout\ $ ((!\inst1|Add11~147\)))
-- \inst1|Add11~142\ = CARRY((\inst1|TSUM[10][2]~combout\ & ((\inst1|PSUM[12][2]~regout\) # (!\inst1|Add11~147\))) # (!\inst1|TSUM[10][2]~combout\ & (\inst1|PSUM[12][2]~regout\ & !\inst1|Add11~147\)))
-- \inst1|Add11~142COUT1_158\ = CARRY((\inst1|TSUM[10][2]~combout\ & ((\inst1|PSUM[12][2]~regout\) # (!\inst1|Add11~147COUT1_157\))) # (!\inst1|TSUM[10][2]~combout\ & (\inst1|PSUM[12][2]~regout\ & !\inst1|Add11~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][2]~combout\,
	datab => \inst1|PSUM[12][2]~regout\,
	cin0 => \inst1|Add11~147\,
	cin1 => \inst1|Add11~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~140_combout\,
	cout0 => \inst1|Add11~142\,
	cout1 => \inst1|Add11~142COUT1_158\);

-- Location: LC_X13_Y4_N8
\inst1|Add11~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~135_combout\ = \inst1|PSUM[12][3]~regout\ $ (\inst1|TSUM[10][3]~combout\ $ ((\inst1|Add11~142\)))
-- \inst1|Add11~137\ = CARRY((\inst1|PSUM[12][3]~regout\ & (!\inst1|TSUM[10][3]~combout\ & !\inst1|Add11~142\)) # (!\inst1|PSUM[12][3]~regout\ & ((!\inst1|Add11~142\) # (!\inst1|TSUM[10][3]~combout\))))
-- \inst1|Add11~137COUT1_159\ = CARRY((\inst1|PSUM[12][3]~regout\ & (!\inst1|TSUM[10][3]~combout\ & !\inst1|Add11~142COUT1_158\)) # (!\inst1|PSUM[12][3]~regout\ & ((!\inst1|Add11~142COUT1_158\) # (!\inst1|TSUM[10][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[12][3]~regout\,
	datab => \inst1|TSUM[10][3]~combout\,
	cin0 => \inst1|Add11~142\,
	cin1 => \inst1|Add11~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~135_combout\,
	cout0 => \inst1|Add11~137\,
	cout1 => \inst1|Add11~137COUT1_159\);

-- Location: LC_X13_Y4_N9
\inst1|Add11~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~130_combout\ = \inst1|TSUM[10][4]~combout\ $ (\inst1|PSUM[12][4]~regout\ $ ((!\inst1|Add11~137\)))
-- \inst1|Add11~132\ = CARRY((\inst1|TSUM[10][4]~combout\ & ((\inst1|PSUM[12][4]~regout\) # (!\inst1|Add11~137COUT1_159\))) # (!\inst1|TSUM[10][4]~combout\ & (\inst1|PSUM[12][4]~regout\ & !\inst1|Add11~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][4]~combout\,
	datab => \inst1|PSUM[12][4]~regout\,
	cin0 => \inst1|Add11~137\,
	cin1 => \inst1|Add11~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~130_combout\,
	cout => \inst1|Add11~132\);

-- Location: LC_X13_Y4_N4
\inst1|TSUM[10][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][8]~combout\ = LCELL((((\inst1|Add10~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][8]~combout\);

-- Location: LC_X13_Y5_N6
\inst1|TSUM[10][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][7]~combout\ = LCELL((((\inst1|Add10~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][7]~combout\);

-- Location: LC_X13_Y5_N4
\inst1|TSUM[10][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][6]~combout\ = LCELL((((\inst1|Add10~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][6]~combout\);

-- Location: LC_X13_Y5_N9
\inst1|TSUM[10][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][5]~combout\ = LCELL((((\inst1|Add10~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][5]~combout\);

-- Location: LC_X14_Y4_N0
\inst1|Add11~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~125_combout\ = \inst1|PSUM[12][5]~regout\ $ (\inst1|TSUM[10][5]~combout\ $ ((\inst1|Add11~132\)))
-- \inst1|Add11~127\ = CARRY((\inst1|PSUM[12][5]~regout\ & (!\inst1|TSUM[10][5]~combout\ & !\inst1|Add11~132\)) # (!\inst1|PSUM[12][5]~regout\ & ((!\inst1|Add11~132\) # (!\inst1|TSUM[10][5]~combout\))))
-- \inst1|Add11~127COUT1_160\ = CARRY((\inst1|PSUM[12][5]~regout\ & (!\inst1|TSUM[10][5]~combout\ & !\inst1|Add11~132\)) # (!\inst1|PSUM[12][5]~regout\ & ((!\inst1|Add11~132\) # (!\inst1|TSUM[10][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[12][5]~regout\,
	datab => \inst1|TSUM[10][5]~combout\,
	cin => \inst1|Add11~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~125_combout\,
	cout0 => \inst1|Add11~127\,
	cout1 => \inst1|Add11~127COUT1_160\);

-- Location: LC_X14_Y4_N1
\inst1|Add11~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~120_combout\ = \inst1|PSUM[12][6]~regout\ $ (\inst1|TSUM[10][6]~combout\ $ ((!(!\inst1|Add11~132\ & \inst1|Add11~127\) # (\inst1|Add11~132\ & \inst1|Add11~127COUT1_160\))))
-- \inst1|Add11~122\ = CARRY((\inst1|PSUM[12][6]~regout\ & ((\inst1|TSUM[10][6]~combout\) # (!\inst1|Add11~127\))) # (!\inst1|PSUM[12][6]~regout\ & (\inst1|TSUM[10][6]~combout\ & !\inst1|Add11~127\)))
-- \inst1|Add11~122COUT1_161\ = CARRY((\inst1|PSUM[12][6]~regout\ & ((\inst1|TSUM[10][6]~combout\) # (!\inst1|Add11~127COUT1_160\))) # (!\inst1|PSUM[12][6]~regout\ & (\inst1|TSUM[10][6]~combout\ & !\inst1|Add11~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[12][6]~regout\,
	datab => \inst1|TSUM[10][6]~combout\,
	cin => \inst1|Add11~132\,
	cin0 => \inst1|Add11~127\,
	cin1 => \inst1|Add11~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~120_combout\,
	cout0 => \inst1|Add11~122\,
	cout1 => \inst1|Add11~122COUT1_161\);

-- Location: LC_X14_Y4_N2
\inst1|Add11~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~115_combout\ = \inst1|PSUM[12][7]~regout\ $ (\inst1|TSUM[10][7]~combout\ $ (((!\inst1|Add11~132\ & \inst1|Add11~122\) # (\inst1|Add11~132\ & \inst1|Add11~122COUT1_161\))))
-- \inst1|Add11~117\ = CARRY((\inst1|PSUM[12][7]~regout\ & (!\inst1|TSUM[10][7]~combout\ & !\inst1|Add11~122\)) # (!\inst1|PSUM[12][7]~regout\ & ((!\inst1|Add11~122\) # (!\inst1|TSUM[10][7]~combout\))))
-- \inst1|Add11~117COUT1_162\ = CARRY((\inst1|PSUM[12][7]~regout\ & (!\inst1|TSUM[10][7]~combout\ & !\inst1|Add11~122COUT1_161\)) # (!\inst1|PSUM[12][7]~regout\ & ((!\inst1|Add11~122COUT1_161\) # (!\inst1|TSUM[10][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[12][7]~regout\,
	datab => \inst1|TSUM[10][7]~combout\,
	cin => \inst1|Add11~132\,
	cin0 => \inst1|Add11~122\,
	cin1 => \inst1|Add11~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~115_combout\,
	cout0 => \inst1|Add11~117\,
	cout1 => \inst1|Add11~117COUT1_162\);

-- Location: LC_X14_Y4_N3
\inst1|Add11~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~110_combout\ = \inst1|PSUM[12][8]~regout\ $ (\inst1|TSUM[10][8]~combout\ $ ((!(!\inst1|Add11~132\ & \inst1|Add11~117\) # (\inst1|Add11~132\ & \inst1|Add11~117COUT1_162\))))
-- \inst1|Add11~112\ = CARRY((\inst1|PSUM[12][8]~regout\ & ((\inst1|TSUM[10][8]~combout\) # (!\inst1|Add11~117\))) # (!\inst1|PSUM[12][8]~regout\ & (\inst1|TSUM[10][8]~combout\ & !\inst1|Add11~117\)))
-- \inst1|Add11~112COUT1_163\ = CARRY((\inst1|PSUM[12][8]~regout\ & ((\inst1|TSUM[10][8]~combout\) # (!\inst1|Add11~117COUT1_162\))) # (!\inst1|PSUM[12][8]~regout\ & (\inst1|TSUM[10][8]~combout\ & !\inst1|Add11~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[12][8]~regout\,
	datab => \inst1|TSUM[10][8]~combout\,
	cin => \inst1|Add11~132\,
	cin0 => \inst1|Add11~117\,
	cin1 => \inst1|Add11~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~110_combout\,
	cout0 => \inst1|Add11~112\,
	cout1 => \inst1|Add11~112COUT1_163\);

-- Location: LC_X14_Y4_N4
\inst1|Add11~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~105_combout\ = \inst1|TSUM[10][9]~combout\ $ (\inst1|PSUM[12][9]~regout\ $ (((!\inst1|Add11~132\ & \inst1|Add11~112\) # (\inst1|Add11~132\ & \inst1|Add11~112COUT1_163\))))
-- \inst1|Add11~107\ = CARRY((\inst1|TSUM[10][9]~combout\ & (!\inst1|PSUM[12][9]~regout\ & !\inst1|Add11~112COUT1_163\)) # (!\inst1|TSUM[10][9]~combout\ & ((!\inst1|Add11~112COUT1_163\) # (!\inst1|PSUM[12][9]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][9]~combout\,
	datab => \inst1|PSUM[12][9]~regout\,
	cin => \inst1|Add11~132\,
	cin0 => \inst1|Add11~112\,
	cin1 => \inst1|Add11~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~105_combout\,
	cout => \inst1|Add11~107\);

-- Location: LC_X13_Y4_N2
\inst1|TSUM[10][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][13]~combout\ = LCELL((((\inst1|Add10~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][13]~combout\);

-- Location: LC_X13_Y5_N0
\inst1|TSUM[10][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][12]~combout\ = LCELL((((\inst1|Add10~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][12]~combout\);

-- Location: LC_X13_Y4_N0
\inst1|TSUM[10][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][11]~combout\ = LCELL((((\inst1|Add10~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][11]~combout\);

-- Location: LC_X13_Y4_N1
\inst1|TSUM[10][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][10]~combout\ = LCELL((((\inst1|Add10~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][10]~combout\);

-- Location: LC_X14_Y4_N5
\inst1|Add11~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~100_combout\ = \inst1|TSUM[10][10]~combout\ $ ((((!\inst1|Add11~107\))))
-- \inst1|Add11~102\ = CARRY((\inst1|TSUM[10][10]~combout\ & ((!\inst1|Add11~107\))))
-- \inst1|Add11~102COUT1_164\ = CARRY((\inst1|TSUM[10][10]~combout\ & ((!\inst1|Add11~107\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][10]~combout\,
	cin => \inst1|Add11~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~100_combout\,
	cout0 => \inst1|Add11~102\,
	cout1 => \inst1|Add11~102COUT1_164\);

-- Location: LC_X14_Y4_N6
\inst1|Add11~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~95_combout\ = \inst1|TSUM[10][11]~combout\ $ (((((!\inst1|Add11~107\ & \inst1|Add11~102\) # (\inst1|Add11~107\ & \inst1|Add11~102COUT1_164\)))))
-- \inst1|Add11~97\ = CARRY(((!\inst1|Add11~102\)) # (!\inst1|TSUM[10][11]~combout\))
-- \inst1|Add11~97COUT1_165\ = CARRY(((!\inst1|Add11~102COUT1_164\)) # (!\inst1|TSUM[10][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][11]~combout\,
	cin => \inst1|Add11~107\,
	cin0 => \inst1|Add11~102\,
	cin1 => \inst1|Add11~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~95_combout\,
	cout0 => \inst1|Add11~97\,
	cout1 => \inst1|Add11~97COUT1_165\);

-- Location: LC_X14_Y4_N7
\inst1|Add11~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~90_combout\ = (\inst1|TSUM[10][12]~combout\ $ ((!(!\inst1|Add11~107\ & \inst1|Add11~97\) # (\inst1|Add11~107\ & \inst1|Add11~97COUT1_165\))))
-- \inst1|Add11~92\ = CARRY(((\inst1|TSUM[10][12]~combout\ & !\inst1|Add11~97\)))
-- \inst1|Add11~92COUT1_166\ = CARRY(((\inst1|TSUM[10][12]~combout\ & !\inst1|Add11~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][12]~combout\,
	cin => \inst1|Add11~107\,
	cin0 => \inst1|Add11~97\,
	cin1 => \inst1|Add11~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~90_combout\,
	cout0 => \inst1|Add11~92\,
	cout1 => \inst1|Add11~92COUT1_166\);

-- Location: LC_X14_Y4_N8
\inst1|Add11~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~85_combout\ = (\inst1|TSUM[10][13]~combout\ $ (((!\inst1|Add11~107\ & \inst1|Add11~92\) # (\inst1|Add11~107\ & \inst1|Add11~92COUT1_166\))))
-- \inst1|Add11~87\ = CARRY(((!\inst1|Add11~92\) # (!\inst1|TSUM[10][13]~combout\)))
-- \inst1|Add11~87COUT1_167\ = CARRY(((!\inst1|Add11~92COUT1_166\) # (!\inst1|TSUM[10][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][13]~combout\,
	cin => \inst1|Add11~107\,
	cin0 => \inst1|Add11~92\,
	cin1 => \inst1|Add11~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~85_combout\,
	cout0 => \inst1|Add11~87\,
	cout1 => \inst1|Add11~87COUT1_167\);

-- Location: LC_X14_Y4_N9
\inst1|Add11~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~80_combout\ = (\inst1|TSUM[10][14]~combout\ $ ((!(!\inst1|Add11~107\ & \inst1|Add11~87\) # (\inst1|Add11~107\ & \inst1|Add11~87COUT1_167\))))
-- \inst1|Add11~82\ = CARRY(((\inst1|TSUM[10][14]~combout\ & !\inst1|Add11~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][14]~combout\,
	cin => \inst1|Add11~107\,
	cin0 => \inst1|Add11~87\,
	cin1 => \inst1|Add11~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~80_combout\,
	cout => \inst1|Add11~82\);

-- Location: LC_X13_Y5_N5
\inst1|TSUM[10][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][18]~combout\ = LCELL((((\inst1|Add10~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][18]~combout\);

-- Location: LC_X15_Y2_N8
\inst1|TSUM[10][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][17]~combout\ = LCELL((((\inst1|Add10~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][17]~combout\);

-- Location: LC_X16_Y2_N6
\inst1|TSUM[10][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][16]~combout\ = LCELL((((\inst1|Add10~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][16]~combout\);

-- Location: LC_X16_Y2_N4
\inst1|TSUM[10][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][15]~combout\ = LCELL((((\inst1|Add10~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][15]~combout\);

-- Location: LC_X15_Y4_N0
\inst1|Add11~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~75_combout\ = \inst1|TSUM[10][15]~combout\ $ ((((\inst1|Add11~82\))))
-- \inst1|Add11~77\ = CARRY(((!\inst1|Add11~82\)) # (!\inst1|TSUM[10][15]~combout\))
-- \inst1|Add11~77COUT1_168\ = CARRY(((!\inst1|Add11~82\)) # (!\inst1|TSUM[10][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][15]~combout\,
	cin => \inst1|Add11~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~75_combout\,
	cout0 => \inst1|Add11~77\,
	cout1 => \inst1|Add11~77COUT1_168\);

-- Location: LC_X15_Y4_N1
\inst1|Add11~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~70_combout\ = (\inst1|TSUM[10][16]~combout\ $ ((!(!\inst1|Add11~82\ & \inst1|Add11~77\) # (\inst1|Add11~82\ & \inst1|Add11~77COUT1_168\))))
-- \inst1|Add11~72\ = CARRY(((\inst1|TSUM[10][16]~combout\ & !\inst1|Add11~77\)))
-- \inst1|Add11~72COUT1_169\ = CARRY(((\inst1|TSUM[10][16]~combout\ & !\inst1|Add11~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][16]~combout\,
	cin => \inst1|Add11~82\,
	cin0 => \inst1|Add11~77\,
	cin1 => \inst1|Add11~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~70_combout\,
	cout0 => \inst1|Add11~72\,
	cout1 => \inst1|Add11~72COUT1_169\);

-- Location: LC_X15_Y4_N2
\inst1|Add11~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~65_combout\ = (\inst1|TSUM[10][17]~combout\ $ (((!\inst1|Add11~82\ & \inst1|Add11~72\) # (\inst1|Add11~82\ & \inst1|Add11~72COUT1_169\))))
-- \inst1|Add11~67\ = CARRY(((!\inst1|Add11~72\) # (!\inst1|TSUM[10][17]~combout\)))
-- \inst1|Add11~67COUT1_170\ = CARRY(((!\inst1|Add11~72COUT1_169\) # (!\inst1|TSUM[10][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][17]~combout\,
	cin => \inst1|Add11~82\,
	cin0 => \inst1|Add11~72\,
	cin1 => \inst1|Add11~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~65_combout\,
	cout0 => \inst1|Add11~67\,
	cout1 => \inst1|Add11~67COUT1_170\);

-- Location: LC_X15_Y4_N3
\inst1|Add11~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~60_combout\ = (\inst1|TSUM[10][18]~combout\ $ ((!(!\inst1|Add11~82\ & \inst1|Add11~67\) # (\inst1|Add11~82\ & \inst1|Add11~67COUT1_170\))))
-- \inst1|Add11~62\ = CARRY(((\inst1|TSUM[10][18]~combout\ & !\inst1|Add11~67\)))
-- \inst1|Add11~62COUT1_171\ = CARRY(((\inst1|TSUM[10][18]~combout\ & !\inst1|Add11~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][18]~combout\,
	cin => \inst1|Add11~82\,
	cin0 => \inst1|Add11~67\,
	cin1 => \inst1|Add11~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~60_combout\,
	cout0 => \inst1|Add11~62\,
	cout1 => \inst1|Add11~62COUT1_171\);

-- Location: LC_X15_Y4_N4
\inst1|Add11~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~55_combout\ = \inst1|TSUM[10][19]~combout\ $ (((((!\inst1|Add11~82\ & \inst1|Add11~62\) # (\inst1|Add11~82\ & \inst1|Add11~62COUT1_171\)))))
-- \inst1|Add11~57\ = CARRY(((!\inst1|Add11~62COUT1_171\)) # (!\inst1|TSUM[10][19]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][19]~combout\,
	cin => \inst1|Add11~82\,
	cin0 => \inst1|Add11~62\,
	cin1 => \inst1|Add11~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~55_combout\,
	cout => \inst1|Add11~57\);

-- Location: LC_X15_Y1_N2
\inst1|TSUM[10][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][23]~combout\ = LCELL((((\inst1|Add10~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][23]~combout\);

-- Location: LC_X15_Y1_N1
\inst1|TSUM[10][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][22]~combout\ = LCELL((((\inst1|Add10~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][22]~combout\);

-- Location: LC_X15_Y1_N3
\inst1|TSUM[10][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][21]~combout\ = LCELL((((\inst1|Add10~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][21]~combout\);

-- Location: LC_X16_Y2_N7
\inst1|TSUM[10][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][20]~combout\ = LCELL((((\inst1|Add10~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][20]~combout\);

-- Location: LC_X15_Y4_N5
\inst1|Add11~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~50_combout\ = \inst1|TSUM[10][20]~combout\ $ ((((!\inst1|Add11~57\))))
-- \inst1|Add11~52\ = CARRY((\inst1|TSUM[10][20]~combout\ & ((!\inst1|Add11~57\))))
-- \inst1|Add11~52COUT1_172\ = CARRY((\inst1|TSUM[10][20]~combout\ & ((!\inst1|Add11~57\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[10][20]~combout\,
	cin => \inst1|Add11~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~50_combout\,
	cout0 => \inst1|Add11~52\,
	cout1 => \inst1|Add11~52COUT1_172\);

-- Location: LC_X15_Y4_N6
\inst1|Add11~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~45_combout\ = (\inst1|TSUM[10][21]~combout\ $ (((!\inst1|Add11~57\ & \inst1|Add11~52\) # (\inst1|Add11~57\ & \inst1|Add11~52COUT1_172\))))
-- \inst1|Add11~47\ = CARRY(((!\inst1|Add11~52\) # (!\inst1|TSUM[10][21]~combout\)))
-- \inst1|Add11~47COUT1_173\ = CARRY(((!\inst1|Add11~52COUT1_172\) # (!\inst1|TSUM[10][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][21]~combout\,
	cin => \inst1|Add11~57\,
	cin0 => \inst1|Add11~52\,
	cin1 => \inst1|Add11~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~45_combout\,
	cout0 => \inst1|Add11~47\,
	cout1 => \inst1|Add11~47COUT1_173\);

-- Location: LC_X15_Y4_N7
\inst1|Add11~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~40_combout\ = (\inst1|TSUM[10][22]~combout\ $ ((!(!\inst1|Add11~57\ & \inst1|Add11~47\) # (\inst1|Add11~57\ & \inst1|Add11~47COUT1_173\))))
-- \inst1|Add11~42\ = CARRY(((\inst1|TSUM[10][22]~combout\ & !\inst1|Add11~47\)))
-- \inst1|Add11~42COUT1_174\ = CARRY(((\inst1|TSUM[10][22]~combout\ & !\inst1|Add11~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][22]~combout\,
	cin => \inst1|Add11~57\,
	cin0 => \inst1|Add11~47\,
	cin1 => \inst1|Add11~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~40_combout\,
	cout0 => \inst1|Add11~42\,
	cout1 => \inst1|Add11~42COUT1_174\);

-- Location: LC_X15_Y4_N8
\inst1|Add11~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~35_combout\ = (\inst1|TSUM[10][23]~combout\ $ (((!\inst1|Add11~57\ & \inst1|Add11~42\) # (\inst1|Add11~57\ & \inst1|Add11~42COUT1_174\))))
-- \inst1|Add11~37\ = CARRY(((!\inst1|Add11~42\) # (!\inst1|TSUM[10][23]~combout\)))
-- \inst1|Add11~37COUT1_175\ = CARRY(((!\inst1|Add11~42COUT1_174\) # (!\inst1|TSUM[10][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][23]~combout\,
	cin => \inst1|Add11~57\,
	cin0 => \inst1|Add11~42\,
	cin1 => \inst1|Add11~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~35_combout\,
	cout0 => \inst1|Add11~37\,
	cout1 => \inst1|Add11~37COUT1_175\);

-- Location: LC_X15_Y4_N9
\inst1|Add11~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~30_combout\ = (\inst1|TSUM[10][24]~combout\ $ ((!(!\inst1|Add11~57\ & \inst1|Add11~37\) # (\inst1|Add11~57\ & \inst1|Add11~37COUT1_175\))))
-- \inst1|Add11~32\ = CARRY(((\inst1|TSUM[10][24]~combout\ & !\inst1|Add11~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][24]~combout\,
	cin => \inst1|Add11~57\,
	cin0 => \inst1|Add11~37\,
	cin1 => \inst1|Add11~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~30_combout\,
	cout => \inst1|Add11~32\);

-- Location: LC_X16_Y4_N7
\inst1|TSUM[10][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][28]~combout\ = LCELL((((\inst1|Add10~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][28]~combout\);

-- Location: LC_X16_Y2_N3
\inst1|TSUM[10][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][27]~combout\ = LCELL((((\inst1|Add10~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][27]~combout\);

-- Location: LC_X16_Y4_N9
\inst1|TSUM[10][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][26]~combout\ = LCELL((((\inst1|Add10~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add10~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][26]~combout\);

-- Location: LC_X16_Y2_N0
\inst1|TSUM[10][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[10][25]~combout\ = LCELL((((\inst1|Add10~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add10~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[10][25]~combout\);

-- Location: LC_X16_Y4_N0
\inst1|Add11~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~25_combout\ = (\inst1|TSUM[10][25]~combout\ $ ((\inst1|Add11~32\)))
-- \inst1|Add11~27\ = CARRY(((!\inst1|Add11~32\) # (!\inst1|TSUM[10][25]~combout\)))
-- \inst1|Add11~27COUT1_176\ = CARRY(((!\inst1|Add11~32\) # (!\inst1|TSUM[10][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][25]~combout\,
	cin => \inst1|Add11~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~25_combout\,
	cout0 => \inst1|Add11~27\,
	cout1 => \inst1|Add11~27COUT1_176\);

-- Location: LC_X16_Y4_N1
\inst1|Add11~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~20_combout\ = (\inst1|TSUM[10][26]~combout\ $ ((!(!\inst1|Add11~32\ & \inst1|Add11~27\) # (\inst1|Add11~32\ & \inst1|Add11~27COUT1_176\))))
-- \inst1|Add11~22\ = CARRY(((\inst1|TSUM[10][26]~combout\ & !\inst1|Add11~27\)))
-- \inst1|Add11~22COUT1_177\ = CARRY(((\inst1|TSUM[10][26]~combout\ & !\inst1|Add11~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][26]~combout\,
	cin => \inst1|Add11~32\,
	cin0 => \inst1|Add11~27\,
	cin1 => \inst1|Add11~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~20_combout\,
	cout0 => \inst1|Add11~22\,
	cout1 => \inst1|Add11~22COUT1_177\);

-- Location: LC_X16_Y4_N2
\inst1|Add11~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~15_combout\ = (\inst1|TSUM[10][27]~combout\ $ (((!\inst1|Add11~32\ & \inst1|Add11~22\) # (\inst1|Add11~32\ & \inst1|Add11~22COUT1_177\))))
-- \inst1|Add11~17\ = CARRY(((!\inst1|Add11~22\) # (!\inst1|TSUM[10][27]~combout\)))
-- \inst1|Add11~17COUT1_178\ = CARRY(((!\inst1|Add11~22COUT1_177\) # (!\inst1|TSUM[10][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][27]~combout\,
	cin => \inst1|Add11~32\,
	cin0 => \inst1|Add11~22\,
	cin1 => \inst1|Add11~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~15_combout\,
	cout0 => \inst1|Add11~17\,
	cout1 => \inst1|Add11~17COUT1_178\);

-- Location: LC_X16_Y4_N3
\inst1|Add11~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~10_combout\ = (\inst1|TSUM[10][28]~combout\ $ ((!(!\inst1|Add11~32\ & \inst1|Add11~17\) # (\inst1|Add11~32\ & \inst1|Add11~17COUT1_178\))))
-- \inst1|Add11~12\ = CARRY(((\inst1|TSUM[10][28]~combout\ & !\inst1|Add11~17\)))
-- \inst1|Add11~12COUT1_179\ = CARRY(((\inst1|TSUM[10][28]~combout\ & !\inst1|Add11~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][28]~combout\,
	cin => \inst1|Add11~32\,
	cin0 => \inst1|Add11~17\,
	cin1 => \inst1|Add11~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~10_combout\,
	cout0 => \inst1|Add11~12\,
	cout1 => \inst1|Add11~12COUT1_179\);

-- Location: LC_X16_Y4_N4
\inst1|Add11~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~5_combout\ = (\inst1|TSUM[10][29]~combout\ $ (((!\inst1|Add11~32\ & \inst1|Add11~12\) # (\inst1|Add11~32\ & \inst1|Add11~12COUT1_179\))))
-- \inst1|Add11~7\ = CARRY(((!\inst1|Add11~12COUT1_179\) # (!\inst1|TSUM[10][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[10][29]~combout\,
	cin => \inst1|Add11~32\,
	cin0 => \inst1|Add11~12\,
	cin1 => \inst1|Add11~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~5_combout\,
	cout => \inst1|Add11~7\);

-- Location: LC_X16_Y4_N5
\inst1|Add11~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add11~0_combout\ = ((\inst1|Add11~7\ $ (!\inst1|TSUM[10][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[10][30]~combout\,
	cin => \inst1|Add11~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add11~0_combout\);

-- Location: LC_X16_Y4_N6
\inst1|TSUM[11][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][30]~combout\ = LCELL((((\inst1|Add11~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][30]~combout\);

-- Location: LC_X16_Y6_N6
\inst1|TSUM[11][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][29]~combout\ = LCELL((((\inst1|Add11~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][29]~combout\);

-- Location: LC_X15_Y5_N9
\inst1|TSUM[11][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][24]~combout\ = LCELL((((\inst1|Add11~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][24]~combout\);

-- Location: LC_X15_Y5_N5
\inst1|TSUM[11][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][19]~combout\ = LCELL((((\inst1|Add11~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][19]~combout\);

-- Location: LC_X14_Y5_N8
\inst1|TSUM[11][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][14]~combout\ = LCELL((((\inst1|Add11~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][14]~combout\);

-- Location: LC_X12_Y7_N0
\inst1|PSUM[13][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][0]~regout\ = DFFEAS(((!\inst1|PSUM[13][0]~regout\)), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][0]~59\ = CARRY(((\inst1|PSUM[13][0]~regout\)))
-- \inst1|PSUM[13][0]~59COUT1_337\ = CARRY(((\inst1|PSUM[13][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	datab => \inst1|PSUM[13][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][0]~regout\,
	cout0 => \inst1|PSUM[13][0]~59\,
	cout1 => \inst1|PSUM[13][0]~59COUT1_337\);

-- Location: LC_X12_Y7_N1
\inst1|PSUM[13][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][1]~regout\ = DFFEAS((\inst1|PSUM[13][1]~regout\ $ ((\inst1|PSUM[13][0]~59\))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][1]~57\ = CARRY(((!\inst1|PSUM[13][0]~59\) # (!\inst1|PSUM[13][1]~regout\)))
-- \inst1|PSUM[13][1]~57COUT1_338\ = CARRY(((!\inst1|PSUM[13][0]~59COUT1_337\) # (!\inst1|PSUM[13][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	datab => \inst1|PSUM[13][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[13][0]~59\,
	cin1 => \inst1|PSUM[13][0]~59COUT1_337\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][1]~regout\,
	cout0 => \inst1|PSUM[13][1]~57\,
	cout1 => \inst1|PSUM[13][1]~57COUT1_338\);

-- Location: LC_X12_Y7_N2
\inst1|PSUM[13][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][2]~regout\ = DFFEAS((\inst1|PSUM[13][2]~regout\ $ ((!\inst1|PSUM[13][1]~57\))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][2]~55\ = CARRY(((\inst1|PSUM[13][2]~regout\ & !\inst1|PSUM[13][1]~57\)))
-- \inst1|PSUM[13][2]~55COUT1_339\ = CARRY(((\inst1|PSUM[13][2]~regout\ & !\inst1|PSUM[13][1]~57COUT1_338\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	datab => \inst1|PSUM[13][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[13][1]~57\,
	cin1 => \inst1|PSUM[13][1]~57COUT1_338\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][2]~regout\,
	cout0 => \inst1|PSUM[13][2]~55\,
	cout1 => \inst1|PSUM[13][2]~55COUT1_339\);

-- Location: LC_X12_Y7_N3
\inst1|PSUM[13][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][3]~regout\ = DFFEAS(\inst1|PSUM[13][3]~regout\ $ ((((\inst1|PSUM[13][2]~55\)))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][3]~53\ = CARRY(((!\inst1|PSUM[13][2]~55\)) # (!\inst1|PSUM[13][3]~regout\))
-- \inst1|PSUM[13][3]~53COUT1_340\ = CARRY(((!\inst1|PSUM[13][2]~55COUT1_339\)) # (!\inst1|PSUM[13][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	dataa => \inst1|PSUM[13][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[13][2]~55\,
	cin1 => \inst1|PSUM[13][2]~55COUT1_339\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][3]~regout\,
	cout0 => \inst1|PSUM[13][3]~53\,
	cout1 => \inst1|PSUM[13][3]~53COUT1_340\);

-- Location: LC_X12_Y7_N4
\inst1|PSUM[13][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][4]~regout\ = DFFEAS(\inst1|PSUM[13][4]~regout\ $ ((((!\inst1|PSUM[13][3]~53\)))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][4]~51\ = CARRY((\inst1|PSUM[13][4]~regout\ & ((!\inst1|PSUM[13][3]~53COUT1_340\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	dataa => \inst1|PSUM[13][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[13][3]~53\,
	cin1 => \inst1|PSUM[13][3]~53COUT1_340\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][4]~regout\,
	cout => \inst1|PSUM[13][4]~51\);

-- Location: LC_X12_Y7_N5
\inst1|PSUM[13][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][5]~regout\ = DFFEAS(\inst1|PSUM[13][5]~regout\ $ ((((\inst1|PSUM[13][4]~51\)))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][5]~49\ = CARRY(((!\inst1|PSUM[13][4]~51\)) # (!\inst1|PSUM[13][5]~regout\))
-- \inst1|PSUM[13][5]~49COUT1_341\ = CARRY(((!\inst1|PSUM[13][4]~51\)) # (!\inst1|PSUM[13][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	dataa => \inst1|PSUM[13][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[13][4]~51\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][5]~regout\,
	cout0 => \inst1|PSUM[13][5]~49\,
	cout1 => \inst1|PSUM[13][5]~49COUT1_341\);

-- Location: LC_X12_Y7_N6
\inst1|PSUM[13][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][6]~regout\ = DFFEAS(\inst1|PSUM[13][6]~regout\ $ ((((!(!\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][5]~49\) # (\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][5]~49COUT1_341\))))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][6]~47\ = CARRY((\inst1|PSUM[13][6]~regout\ & ((!\inst1|PSUM[13][5]~49\))))
-- \inst1|PSUM[13][6]~47COUT1_342\ = CARRY((\inst1|PSUM[13][6]~regout\ & ((!\inst1|PSUM[13][5]~49COUT1_341\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	dataa => \inst1|PSUM[13][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[13][4]~51\,
	cin0 => \inst1|PSUM[13][5]~49\,
	cin1 => \inst1|PSUM[13][5]~49COUT1_341\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][6]~regout\,
	cout0 => \inst1|PSUM[13][6]~47\,
	cout1 => \inst1|PSUM[13][6]~47COUT1_342\);

-- Location: LC_X12_Y7_N7
\inst1|PSUM[13][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][7]~regout\ = DFFEAS((\inst1|PSUM[13][7]~regout\ $ (((!\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][6]~47\) # (\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][6]~47COUT1_342\)))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][7]~45\ = CARRY(((!\inst1|PSUM[13][6]~47\) # (!\inst1|PSUM[13][7]~regout\)))
-- \inst1|PSUM[13][7]~45COUT1_343\ = CARRY(((!\inst1|PSUM[13][6]~47COUT1_342\) # (!\inst1|PSUM[13][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	datab => \inst1|PSUM[13][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[13][4]~51\,
	cin0 => \inst1|PSUM[13][6]~47\,
	cin1 => \inst1|PSUM[13][6]~47COUT1_342\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][7]~regout\,
	cout0 => \inst1|PSUM[13][7]~45\,
	cout1 => \inst1|PSUM[13][7]~45COUT1_343\);

-- Location: LC_X12_Y7_N8
\inst1|PSUM[13][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][8]~regout\ = DFFEAS(\inst1|PSUM[13][8]~regout\ $ ((((!(!\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][7]~45\) # (\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][7]~45COUT1_343\))))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[13][8]~43\ = CARRY((\inst1|PSUM[13][8]~regout\ & ((!\inst1|PSUM[13][7]~45\))))
-- \inst1|PSUM[13][8]~43COUT1_344\ = CARRY((\inst1|PSUM[13][8]~regout\ & ((!\inst1|PSUM[13][7]~45COUT1_343\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	dataa => \inst1|PSUM[13][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[13][4]~51\,
	cin0 => \inst1|PSUM[13][7]~45\,
	cin1 => \inst1|PSUM[13][7]~45COUT1_343\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][8]~regout\,
	cout0 => \inst1|PSUM[13][8]~43\,
	cout1 => \inst1|PSUM[13][8]~43COUT1_344\);

-- Location: LC_X12_Y7_N9
\inst1|PSUM[13][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[13][9]~regout\ = DFFEAS((((!\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][8]~43\) # (\inst1|PSUM[13][4]~51\ & \inst1|PSUM[13][8]~43COUT1_344\) $ (\inst1|PSUM[13][9]~regout\))), \inst6|STEMP\(13), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(13),
	datad => \inst1|PSUM[13][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[13][4]~51\,
	cin0 => \inst1|PSUM[13][8]~43\,
	cin1 => \inst1|PSUM[13][8]~43COUT1_344\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[13][9]~regout\);

-- Location: LC_X14_Y5_N9
\inst1|TSUM[11][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][9]~combout\ = LCELL((((\inst1|Add11~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][9]~combout\);

-- Location: LC_X13_Y5_N8
\inst1|TSUM[11][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][4]~combout\ = LCELL((((\inst1|Add11~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][4]~combout\);

-- Location: LC_X13_Y6_N4
\inst1|TSUM[11][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][3]~combout\ = LCELL((((\inst1|Add11~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][3]~combout\);

-- Location: LC_X13_Y5_N1
\inst1|TSUM[11][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][2]~combout\ = LCELL((((\inst1|Add11~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][2]~combout\);

-- Location: LC_X13_Y6_N2
\inst1|TSUM[11][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][1]~combout\ = LCELL((((\inst1|Add11~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][1]~combout\);

-- Location: LC_X13_Y5_N3
\inst1|TSUM[11][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][0]~combout\ = LCELL((((\inst1|Add11~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][0]~combout\);

-- Location: LC_X13_Y6_N5
\inst1|Add12~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~150_combout\ = \inst1|TSUM[11][0]~combout\ $ ((\inst1|PSUM[13][0]~regout\))
-- \inst1|Add12~152\ = CARRY((\inst1|TSUM[11][0]~combout\ & (\inst1|PSUM[13][0]~regout\)))
-- \inst1|Add12~152COUT1_156\ = CARRY((\inst1|TSUM[11][0]~combout\ & (\inst1|PSUM[13][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][0]~combout\,
	datab => \inst1|PSUM[13][0]~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~150_combout\,
	cout0 => \inst1|Add12~152\,
	cout1 => \inst1|Add12~152COUT1_156\);

-- Location: LC_X13_Y6_N6
\inst1|Add12~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~145_combout\ = \inst1|PSUM[13][1]~regout\ $ (\inst1|TSUM[11][1]~combout\ $ ((\inst1|Add12~152\)))
-- \inst1|Add12~147\ = CARRY((\inst1|PSUM[13][1]~regout\ & (!\inst1|TSUM[11][1]~combout\ & !\inst1|Add12~152\)) # (!\inst1|PSUM[13][1]~regout\ & ((!\inst1|Add12~152\) # (!\inst1|TSUM[11][1]~combout\))))
-- \inst1|Add12~147COUT1_157\ = CARRY((\inst1|PSUM[13][1]~regout\ & (!\inst1|TSUM[11][1]~combout\ & !\inst1|Add12~152COUT1_156\)) # (!\inst1|PSUM[13][1]~regout\ & ((!\inst1|Add12~152COUT1_156\) # (!\inst1|TSUM[11][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[13][1]~regout\,
	datab => \inst1|TSUM[11][1]~combout\,
	cin0 => \inst1|Add12~152\,
	cin1 => \inst1|Add12~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~145_combout\,
	cout0 => \inst1|Add12~147\,
	cout1 => \inst1|Add12~147COUT1_157\);

-- Location: LC_X13_Y6_N7
\inst1|Add12~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~140_combout\ = \inst1|TSUM[11][2]~combout\ $ (\inst1|PSUM[13][2]~regout\ $ ((!\inst1|Add12~147\)))
-- \inst1|Add12~142\ = CARRY((\inst1|TSUM[11][2]~combout\ & ((\inst1|PSUM[13][2]~regout\) # (!\inst1|Add12~147\))) # (!\inst1|TSUM[11][2]~combout\ & (\inst1|PSUM[13][2]~regout\ & !\inst1|Add12~147\)))
-- \inst1|Add12~142COUT1_158\ = CARRY((\inst1|TSUM[11][2]~combout\ & ((\inst1|PSUM[13][2]~regout\) # (!\inst1|Add12~147COUT1_157\))) # (!\inst1|TSUM[11][2]~combout\ & (\inst1|PSUM[13][2]~regout\ & !\inst1|Add12~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][2]~combout\,
	datab => \inst1|PSUM[13][2]~regout\,
	cin0 => \inst1|Add12~147\,
	cin1 => \inst1|Add12~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~140_combout\,
	cout0 => \inst1|Add12~142\,
	cout1 => \inst1|Add12~142COUT1_158\);

-- Location: LC_X13_Y6_N8
\inst1|Add12~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~135_combout\ = \inst1|TSUM[11][3]~combout\ $ (\inst1|PSUM[13][3]~regout\ $ ((\inst1|Add12~142\)))
-- \inst1|Add12~137\ = CARRY((\inst1|TSUM[11][3]~combout\ & (!\inst1|PSUM[13][3]~regout\ & !\inst1|Add12~142\)) # (!\inst1|TSUM[11][3]~combout\ & ((!\inst1|Add12~142\) # (!\inst1|PSUM[13][3]~regout\))))
-- \inst1|Add12~137COUT1_159\ = CARRY((\inst1|TSUM[11][3]~combout\ & (!\inst1|PSUM[13][3]~regout\ & !\inst1|Add12~142COUT1_158\)) # (!\inst1|TSUM[11][3]~combout\ & ((!\inst1|Add12~142COUT1_158\) # (!\inst1|PSUM[13][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][3]~combout\,
	datab => \inst1|PSUM[13][3]~regout\,
	cin0 => \inst1|Add12~142\,
	cin1 => \inst1|Add12~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~135_combout\,
	cout0 => \inst1|Add12~137\,
	cout1 => \inst1|Add12~137COUT1_159\);

-- Location: LC_X13_Y6_N9
\inst1|Add12~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~130_combout\ = \inst1|TSUM[11][4]~combout\ $ (\inst1|PSUM[13][4]~regout\ $ ((!\inst1|Add12~137\)))
-- \inst1|Add12~132\ = CARRY((\inst1|TSUM[11][4]~combout\ & ((\inst1|PSUM[13][4]~regout\) # (!\inst1|Add12~137COUT1_159\))) # (!\inst1|TSUM[11][4]~combout\ & (\inst1|PSUM[13][4]~regout\ & !\inst1|Add12~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][4]~combout\,
	datab => \inst1|PSUM[13][4]~regout\,
	cin0 => \inst1|Add12~137\,
	cin1 => \inst1|Add12~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~130_combout\,
	cout => \inst1|Add12~132\);

-- Location: LC_X14_Y5_N5
\inst1|TSUM[11][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][8]~combout\ = LCELL((((\inst1|Add11~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][8]~combout\);

-- Location: LC_X14_Y5_N3
\inst1|TSUM[11][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][7]~combout\ = LCELL((((\inst1|Add11~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][7]~combout\);

-- Location: LC_X14_Y5_N7
\inst1|TSUM[11][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][6]~combout\ = LCELL((((\inst1|Add11~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][6]~combout\);

-- Location: LC_X14_Y5_N0
\inst1|TSUM[11][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][5]~combout\ = LCELL((((\inst1|Add11~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][5]~combout\);

-- Location: LC_X14_Y6_N0
\inst1|Add12~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~125_combout\ = \inst1|PSUM[13][5]~regout\ $ (\inst1|TSUM[11][5]~combout\ $ ((\inst1|Add12~132\)))
-- \inst1|Add12~127\ = CARRY((\inst1|PSUM[13][5]~regout\ & (!\inst1|TSUM[11][5]~combout\ & !\inst1|Add12~132\)) # (!\inst1|PSUM[13][5]~regout\ & ((!\inst1|Add12~132\) # (!\inst1|TSUM[11][5]~combout\))))
-- \inst1|Add12~127COUT1_160\ = CARRY((\inst1|PSUM[13][5]~regout\ & (!\inst1|TSUM[11][5]~combout\ & !\inst1|Add12~132\)) # (!\inst1|PSUM[13][5]~regout\ & ((!\inst1|Add12~132\) # (!\inst1|TSUM[11][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[13][5]~regout\,
	datab => \inst1|TSUM[11][5]~combout\,
	cin => \inst1|Add12~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~125_combout\,
	cout0 => \inst1|Add12~127\,
	cout1 => \inst1|Add12~127COUT1_160\);

-- Location: LC_X14_Y6_N1
\inst1|Add12~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~120_combout\ = \inst1|TSUM[11][6]~combout\ $ (\inst1|PSUM[13][6]~regout\ $ ((!(!\inst1|Add12~132\ & \inst1|Add12~127\) # (\inst1|Add12~132\ & \inst1|Add12~127COUT1_160\))))
-- \inst1|Add12~122\ = CARRY((\inst1|TSUM[11][6]~combout\ & ((\inst1|PSUM[13][6]~regout\) # (!\inst1|Add12~127\))) # (!\inst1|TSUM[11][6]~combout\ & (\inst1|PSUM[13][6]~regout\ & !\inst1|Add12~127\)))
-- \inst1|Add12~122COUT1_161\ = CARRY((\inst1|TSUM[11][6]~combout\ & ((\inst1|PSUM[13][6]~regout\) # (!\inst1|Add12~127COUT1_160\))) # (!\inst1|TSUM[11][6]~combout\ & (\inst1|PSUM[13][6]~regout\ & !\inst1|Add12~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][6]~combout\,
	datab => \inst1|PSUM[13][6]~regout\,
	cin => \inst1|Add12~132\,
	cin0 => \inst1|Add12~127\,
	cin1 => \inst1|Add12~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~120_combout\,
	cout0 => \inst1|Add12~122\,
	cout1 => \inst1|Add12~122COUT1_161\);

-- Location: LC_X14_Y6_N2
\inst1|Add12~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~115_combout\ = \inst1|PSUM[13][7]~regout\ $ (\inst1|TSUM[11][7]~combout\ $ (((!\inst1|Add12~132\ & \inst1|Add12~122\) # (\inst1|Add12~132\ & \inst1|Add12~122COUT1_161\))))
-- \inst1|Add12~117\ = CARRY((\inst1|PSUM[13][7]~regout\ & (!\inst1|TSUM[11][7]~combout\ & !\inst1|Add12~122\)) # (!\inst1|PSUM[13][7]~regout\ & ((!\inst1|Add12~122\) # (!\inst1|TSUM[11][7]~combout\))))
-- \inst1|Add12~117COUT1_162\ = CARRY((\inst1|PSUM[13][7]~regout\ & (!\inst1|TSUM[11][7]~combout\ & !\inst1|Add12~122COUT1_161\)) # (!\inst1|PSUM[13][7]~regout\ & ((!\inst1|Add12~122COUT1_161\) # (!\inst1|TSUM[11][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[13][7]~regout\,
	datab => \inst1|TSUM[11][7]~combout\,
	cin => \inst1|Add12~132\,
	cin0 => \inst1|Add12~122\,
	cin1 => \inst1|Add12~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~115_combout\,
	cout0 => \inst1|Add12~117\,
	cout1 => \inst1|Add12~117COUT1_162\);

-- Location: LC_X14_Y6_N3
\inst1|Add12~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~110_combout\ = \inst1|PSUM[13][8]~regout\ $ (\inst1|TSUM[11][8]~combout\ $ ((!(!\inst1|Add12~132\ & \inst1|Add12~117\) # (\inst1|Add12~132\ & \inst1|Add12~117COUT1_162\))))
-- \inst1|Add12~112\ = CARRY((\inst1|PSUM[13][8]~regout\ & ((\inst1|TSUM[11][8]~combout\) # (!\inst1|Add12~117\))) # (!\inst1|PSUM[13][8]~regout\ & (\inst1|TSUM[11][8]~combout\ & !\inst1|Add12~117\)))
-- \inst1|Add12~112COUT1_163\ = CARRY((\inst1|PSUM[13][8]~regout\ & ((\inst1|TSUM[11][8]~combout\) # (!\inst1|Add12~117COUT1_162\))) # (!\inst1|PSUM[13][8]~regout\ & (\inst1|TSUM[11][8]~combout\ & !\inst1|Add12~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[13][8]~regout\,
	datab => \inst1|TSUM[11][8]~combout\,
	cin => \inst1|Add12~132\,
	cin0 => \inst1|Add12~117\,
	cin1 => \inst1|Add12~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~110_combout\,
	cout0 => \inst1|Add12~112\,
	cout1 => \inst1|Add12~112COUT1_163\);

-- Location: LC_X14_Y6_N4
\inst1|Add12~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~105_combout\ = \inst1|PSUM[13][9]~regout\ $ (\inst1|TSUM[11][9]~combout\ $ (((!\inst1|Add12~132\ & \inst1|Add12~112\) # (\inst1|Add12~132\ & \inst1|Add12~112COUT1_163\))))
-- \inst1|Add12~107\ = CARRY((\inst1|PSUM[13][9]~regout\ & (!\inst1|TSUM[11][9]~combout\ & !\inst1|Add12~112COUT1_163\)) # (!\inst1|PSUM[13][9]~regout\ & ((!\inst1|Add12~112COUT1_163\) # (!\inst1|TSUM[11][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[13][9]~regout\,
	datab => \inst1|TSUM[11][9]~combout\,
	cin => \inst1|Add12~132\,
	cin0 => \inst1|Add12~112\,
	cin1 => \inst1|Add12~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~105_combout\,
	cout => \inst1|Add12~107\);

-- Location: LC_X14_Y5_N2
\inst1|TSUM[11][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][13]~combout\ = LCELL((((\inst1|Add11~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][13]~combout\);

-- Location: LC_X14_Y5_N1
\inst1|TSUM[11][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][12]~combout\ = LCELL((((\inst1|Add11~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][12]~combout\);

-- Location: LC_X14_Y5_N4
\inst1|TSUM[11][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][11]~combout\ = LCELL((((\inst1|Add11~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][11]~combout\);

-- Location: LC_X14_Y5_N6
\inst1|TSUM[11][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][10]~combout\ = LCELL((((\inst1|Add11~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][10]~combout\);

-- Location: LC_X14_Y6_N5
\inst1|Add12~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~100_combout\ = \inst1|TSUM[11][10]~combout\ $ ((((!\inst1|Add12~107\))))
-- \inst1|Add12~102\ = CARRY((\inst1|TSUM[11][10]~combout\ & ((!\inst1|Add12~107\))))
-- \inst1|Add12~102COUT1_164\ = CARRY((\inst1|TSUM[11][10]~combout\ & ((!\inst1|Add12~107\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][10]~combout\,
	cin => \inst1|Add12~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~100_combout\,
	cout0 => \inst1|Add12~102\,
	cout1 => \inst1|Add12~102COUT1_164\);

-- Location: LC_X14_Y6_N6
\inst1|Add12~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~95_combout\ = \inst1|TSUM[11][11]~combout\ $ (((((!\inst1|Add12~107\ & \inst1|Add12~102\) # (\inst1|Add12~107\ & \inst1|Add12~102COUT1_164\)))))
-- \inst1|Add12~97\ = CARRY(((!\inst1|Add12~102\)) # (!\inst1|TSUM[11][11]~combout\))
-- \inst1|Add12~97COUT1_165\ = CARRY(((!\inst1|Add12~102COUT1_164\)) # (!\inst1|TSUM[11][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][11]~combout\,
	cin => \inst1|Add12~107\,
	cin0 => \inst1|Add12~102\,
	cin1 => \inst1|Add12~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~95_combout\,
	cout0 => \inst1|Add12~97\,
	cout1 => \inst1|Add12~97COUT1_165\);

-- Location: LC_X14_Y6_N7
\inst1|Add12~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~90_combout\ = (\inst1|TSUM[11][12]~combout\ $ ((!(!\inst1|Add12~107\ & \inst1|Add12~97\) # (\inst1|Add12~107\ & \inst1|Add12~97COUT1_165\))))
-- \inst1|Add12~92\ = CARRY(((\inst1|TSUM[11][12]~combout\ & !\inst1|Add12~97\)))
-- \inst1|Add12~92COUT1_166\ = CARRY(((\inst1|TSUM[11][12]~combout\ & !\inst1|Add12~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][12]~combout\,
	cin => \inst1|Add12~107\,
	cin0 => \inst1|Add12~97\,
	cin1 => \inst1|Add12~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~90_combout\,
	cout0 => \inst1|Add12~92\,
	cout1 => \inst1|Add12~92COUT1_166\);

-- Location: LC_X14_Y6_N8
\inst1|Add12~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~85_combout\ = (\inst1|TSUM[11][13]~combout\ $ (((!\inst1|Add12~107\ & \inst1|Add12~92\) # (\inst1|Add12~107\ & \inst1|Add12~92COUT1_166\))))
-- \inst1|Add12~87\ = CARRY(((!\inst1|Add12~92\) # (!\inst1|TSUM[11][13]~combout\)))
-- \inst1|Add12~87COUT1_167\ = CARRY(((!\inst1|Add12~92COUT1_166\) # (!\inst1|TSUM[11][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][13]~combout\,
	cin => \inst1|Add12~107\,
	cin0 => \inst1|Add12~92\,
	cin1 => \inst1|Add12~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~85_combout\,
	cout0 => \inst1|Add12~87\,
	cout1 => \inst1|Add12~87COUT1_167\);

-- Location: LC_X14_Y6_N9
\inst1|Add12~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~80_combout\ = (\inst1|TSUM[11][14]~combout\ $ ((!(!\inst1|Add12~107\ & \inst1|Add12~87\) # (\inst1|Add12~107\ & \inst1|Add12~87COUT1_167\))))
-- \inst1|Add12~82\ = CARRY(((\inst1|TSUM[11][14]~combout\ & !\inst1|Add12~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][14]~combout\,
	cin => \inst1|Add12~107\,
	cin0 => \inst1|Add12~87\,
	cin1 => \inst1|Add12~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~80_combout\,
	cout => \inst1|Add12~82\);

-- Location: LC_X15_Y5_N8
\inst1|TSUM[11][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][18]~combout\ = LCELL((((\inst1|Add11~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][18]~combout\);

-- Location: LC_X15_Y5_N1
\inst1|TSUM[11][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][17]~combout\ = LCELL((((\inst1|Add11~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][17]~combout\);

-- Location: LC_X15_Y5_N6
\inst1|TSUM[11][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][16]~combout\ = LCELL((((\inst1|Add11~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][16]~combout\);

-- Location: LC_X15_Y5_N0
\inst1|TSUM[11][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][15]~combout\ = LCELL((((\inst1|Add11~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][15]~combout\);

-- Location: LC_X15_Y6_N0
\inst1|Add12~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~75_combout\ = \inst1|TSUM[11][15]~combout\ $ ((((\inst1|Add12~82\))))
-- \inst1|Add12~77\ = CARRY(((!\inst1|Add12~82\)) # (!\inst1|TSUM[11][15]~combout\))
-- \inst1|Add12~77COUT1_168\ = CARRY(((!\inst1|Add12~82\)) # (!\inst1|TSUM[11][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][15]~combout\,
	cin => \inst1|Add12~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~75_combout\,
	cout0 => \inst1|Add12~77\,
	cout1 => \inst1|Add12~77COUT1_168\);

-- Location: LC_X15_Y6_N1
\inst1|Add12~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~70_combout\ = \inst1|TSUM[11][16]~combout\ $ ((((!(!\inst1|Add12~82\ & \inst1|Add12~77\) # (\inst1|Add12~82\ & \inst1|Add12~77COUT1_168\)))))
-- \inst1|Add12~72\ = CARRY((\inst1|TSUM[11][16]~combout\ & ((!\inst1|Add12~77\))))
-- \inst1|Add12~72COUT1_169\ = CARRY((\inst1|TSUM[11][16]~combout\ & ((!\inst1|Add12~77COUT1_168\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][16]~combout\,
	cin => \inst1|Add12~82\,
	cin0 => \inst1|Add12~77\,
	cin1 => \inst1|Add12~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~70_combout\,
	cout0 => \inst1|Add12~72\,
	cout1 => \inst1|Add12~72COUT1_169\);

-- Location: LC_X15_Y6_N2
\inst1|Add12~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~65_combout\ = (\inst1|TSUM[11][17]~combout\ $ (((!\inst1|Add12~82\ & \inst1|Add12~72\) # (\inst1|Add12~82\ & \inst1|Add12~72COUT1_169\))))
-- \inst1|Add12~67\ = CARRY(((!\inst1|Add12~72\) # (!\inst1|TSUM[11][17]~combout\)))
-- \inst1|Add12~67COUT1_170\ = CARRY(((!\inst1|Add12~72COUT1_169\) # (!\inst1|TSUM[11][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][17]~combout\,
	cin => \inst1|Add12~82\,
	cin0 => \inst1|Add12~72\,
	cin1 => \inst1|Add12~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~65_combout\,
	cout0 => \inst1|Add12~67\,
	cout1 => \inst1|Add12~67COUT1_170\);

-- Location: LC_X15_Y6_N3
\inst1|Add12~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~60_combout\ = (\inst1|TSUM[11][18]~combout\ $ ((!(!\inst1|Add12~82\ & \inst1|Add12~67\) # (\inst1|Add12~82\ & \inst1|Add12~67COUT1_170\))))
-- \inst1|Add12~62\ = CARRY(((\inst1|TSUM[11][18]~combout\ & !\inst1|Add12~67\)))
-- \inst1|Add12~62COUT1_171\ = CARRY(((\inst1|TSUM[11][18]~combout\ & !\inst1|Add12~67COUT1_170\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][18]~combout\,
	cin => \inst1|Add12~82\,
	cin0 => \inst1|Add12~67\,
	cin1 => \inst1|Add12~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~60_combout\,
	cout0 => \inst1|Add12~62\,
	cout1 => \inst1|Add12~62COUT1_171\);

-- Location: LC_X15_Y6_N4
\inst1|Add12~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~55_combout\ = (\inst1|TSUM[11][19]~combout\ $ (((!\inst1|Add12~82\ & \inst1|Add12~62\) # (\inst1|Add12~82\ & \inst1|Add12~62COUT1_171\))))
-- \inst1|Add12~57\ = CARRY(((!\inst1|Add12~62COUT1_171\) # (!\inst1|TSUM[11][19]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][19]~combout\,
	cin => \inst1|Add12~82\,
	cin0 => \inst1|Add12~62\,
	cin1 => \inst1|Add12~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~55_combout\,
	cout => \inst1|Add12~57\);

-- Location: LC_X15_Y5_N4
\inst1|TSUM[11][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][23]~combout\ = LCELL((((\inst1|Add11~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][23]~combout\);

-- Location: LC_X15_Y5_N3
\inst1|TSUM[11][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][22]~combout\ = LCELL((((\inst1|Add11~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][22]~combout\);

-- Location: LC_X15_Y5_N2
\inst1|TSUM[11][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][21]~combout\ = LCELL((((\inst1|Add11~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][21]~combout\);

-- Location: LC_X15_Y5_N7
\inst1|TSUM[11][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][20]~combout\ = LCELL((((\inst1|Add11~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][20]~combout\);

-- Location: LC_X15_Y6_N5
\inst1|Add12~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~50_combout\ = \inst1|TSUM[11][20]~combout\ $ ((((!\inst1|Add12~57\))))
-- \inst1|Add12~52\ = CARRY((\inst1|TSUM[11][20]~combout\ & ((!\inst1|Add12~57\))))
-- \inst1|Add12~52COUT1_172\ = CARRY((\inst1|TSUM[11][20]~combout\ & ((!\inst1|Add12~57\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][20]~combout\,
	cin => \inst1|Add12~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~50_combout\,
	cout0 => \inst1|Add12~52\,
	cout1 => \inst1|Add12~52COUT1_172\);

-- Location: LC_X15_Y6_N6
\inst1|Add12~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~45_combout\ = (\inst1|TSUM[11][21]~combout\ $ (((!\inst1|Add12~57\ & \inst1|Add12~52\) # (\inst1|Add12~57\ & \inst1|Add12~52COUT1_172\))))
-- \inst1|Add12~47\ = CARRY(((!\inst1|Add12~52\) # (!\inst1|TSUM[11][21]~combout\)))
-- \inst1|Add12~47COUT1_173\ = CARRY(((!\inst1|Add12~52COUT1_172\) # (!\inst1|TSUM[11][21]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][21]~combout\,
	cin => \inst1|Add12~57\,
	cin0 => \inst1|Add12~52\,
	cin1 => \inst1|Add12~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~45_combout\,
	cout0 => \inst1|Add12~47\,
	cout1 => \inst1|Add12~47COUT1_173\);

-- Location: LC_X15_Y6_N7
\inst1|Add12~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~40_combout\ = (\inst1|TSUM[11][22]~combout\ $ ((!(!\inst1|Add12~57\ & \inst1|Add12~47\) # (\inst1|Add12~57\ & \inst1|Add12~47COUT1_173\))))
-- \inst1|Add12~42\ = CARRY(((\inst1|TSUM[11][22]~combout\ & !\inst1|Add12~47\)))
-- \inst1|Add12~42COUT1_174\ = CARRY(((\inst1|TSUM[11][22]~combout\ & !\inst1|Add12~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][22]~combout\,
	cin => \inst1|Add12~57\,
	cin0 => \inst1|Add12~47\,
	cin1 => \inst1|Add12~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~40_combout\,
	cout0 => \inst1|Add12~42\,
	cout1 => \inst1|Add12~42COUT1_174\);

-- Location: LC_X15_Y6_N8
\inst1|Add12~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~35_combout\ = \inst1|TSUM[11][23]~combout\ $ (((((!\inst1|Add12~57\ & \inst1|Add12~42\) # (\inst1|Add12~57\ & \inst1|Add12~42COUT1_174\)))))
-- \inst1|Add12~37\ = CARRY(((!\inst1|Add12~42\)) # (!\inst1|TSUM[11][23]~combout\))
-- \inst1|Add12~37COUT1_175\ = CARRY(((!\inst1|Add12~42COUT1_174\)) # (!\inst1|TSUM[11][23]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][23]~combout\,
	cin => \inst1|Add12~57\,
	cin0 => \inst1|Add12~42\,
	cin1 => \inst1|Add12~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~35_combout\,
	cout0 => \inst1|Add12~37\,
	cout1 => \inst1|Add12~37COUT1_175\);

-- Location: LC_X15_Y6_N9
\inst1|Add12~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~30_combout\ = (\inst1|TSUM[11][24]~combout\ $ ((!(!\inst1|Add12~57\ & \inst1|Add12~37\) # (\inst1|Add12~57\ & \inst1|Add12~37COUT1_175\))))
-- \inst1|Add12~32\ = CARRY(((\inst1|TSUM[11][24]~combout\ & !\inst1|Add12~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][24]~combout\,
	cin => \inst1|Add12~57\,
	cin0 => \inst1|Add12~37\,
	cin1 => \inst1|Add12~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~30_combout\,
	cout => \inst1|Add12~32\);

-- Location: LC_X16_Y6_N9
\inst1|TSUM[11][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][28]~combout\ = LCELL((((\inst1|Add11~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][28]~combout\);

-- Location: LC_X16_Y5_N5
\inst1|TSUM[11][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][27]~combout\ = LCELL((((\inst1|Add11~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][27]~combout\);

-- Location: LC_X16_Y4_N8
\inst1|TSUM[11][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][26]~combout\ = LCELL((((\inst1|Add11~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add11~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][26]~combout\);

-- Location: LC_X16_Y6_N8
\inst1|TSUM[11][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[11][25]~combout\ = LCELL((((\inst1|Add11~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add11~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[11][25]~combout\);

-- Location: LC_X16_Y6_N0
\inst1|Add12~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~25_combout\ = \inst1|TSUM[11][25]~combout\ $ ((((\inst1|Add12~32\))))
-- \inst1|Add12~27\ = CARRY(((!\inst1|Add12~32\)) # (!\inst1|TSUM[11][25]~combout\))
-- \inst1|Add12~27COUT1_176\ = CARRY(((!\inst1|Add12~32\)) # (!\inst1|TSUM[11][25]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][25]~combout\,
	cin => \inst1|Add12~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~25_combout\,
	cout0 => \inst1|Add12~27\,
	cout1 => \inst1|Add12~27COUT1_176\);

-- Location: LC_X16_Y6_N1
\inst1|Add12~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~20_combout\ = (\inst1|TSUM[11][26]~combout\ $ ((!(!\inst1|Add12~32\ & \inst1|Add12~27\) # (\inst1|Add12~32\ & \inst1|Add12~27COUT1_176\))))
-- \inst1|Add12~22\ = CARRY(((\inst1|TSUM[11][26]~combout\ & !\inst1|Add12~27\)))
-- \inst1|Add12~22COUT1_177\ = CARRY(((\inst1|TSUM[11][26]~combout\ & !\inst1|Add12~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][26]~combout\,
	cin => \inst1|Add12~32\,
	cin0 => \inst1|Add12~27\,
	cin1 => \inst1|Add12~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~20_combout\,
	cout0 => \inst1|Add12~22\,
	cout1 => \inst1|Add12~22COUT1_177\);

-- Location: LC_X16_Y6_N2
\inst1|Add12~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~15_combout\ = (\inst1|TSUM[11][27]~combout\ $ (((!\inst1|Add12~32\ & \inst1|Add12~22\) # (\inst1|Add12~32\ & \inst1|Add12~22COUT1_177\))))
-- \inst1|Add12~17\ = CARRY(((!\inst1|Add12~22\) # (!\inst1|TSUM[11][27]~combout\)))
-- \inst1|Add12~17COUT1_178\ = CARRY(((!\inst1|Add12~22COUT1_177\) # (!\inst1|TSUM[11][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][27]~combout\,
	cin => \inst1|Add12~32\,
	cin0 => \inst1|Add12~22\,
	cin1 => \inst1|Add12~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~15_combout\,
	cout0 => \inst1|Add12~17\,
	cout1 => \inst1|Add12~17COUT1_178\);

-- Location: LC_X16_Y6_N3
\inst1|Add12~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~10_combout\ = (\inst1|TSUM[11][28]~combout\ $ ((!(!\inst1|Add12~32\ & \inst1|Add12~17\) # (\inst1|Add12~32\ & \inst1|Add12~17COUT1_178\))))
-- \inst1|Add12~12\ = CARRY(((\inst1|TSUM[11][28]~combout\ & !\inst1|Add12~17\)))
-- \inst1|Add12~12COUT1_179\ = CARRY(((\inst1|TSUM[11][28]~combout\ & !\inst1|Add12~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[11][28]~combout\,
	cin => \inst1|Add12~32\,
	cin0 => \inst1|Add12~17\,
	cin1 => \inst1|Add12~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~10_combout\,
	cout0 => \inst1|Add12~12\,
	cout1 => \inst1|Add12~12COUT1_179\);

-- Location: LC_X16_Y6_N4
\inst1|Add12~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~5_combout\ = \inst1|TSUM[11][29]~combout\ $ (((((!\inst1|Add12~32\ & \inst1|Add12~12\) # (\inst1|Add12~32\ & \inst1|Add12~12COUT1_179\)))))
-- \inst1|Add12~7\ = CARRY(((!\inst1|Add12~12COUT1_179\)) # (!\inst1|TSUM[11][29]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[11][29]~combout\,
	cin => \inst1|Add12~32\,
	cin0 => \inst1|Add12~12\,
	cin1 => \inst1|Add12~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~5_combout\,
	cout => \inst1|Add12~7\);

-- Location: LC_X16_Y6_N5
\inst1|Add12~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add12~0_combout\ = ((\inst1|Add12~7\ $ (!\inst1|TSUM[11][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[11][30]~combout\,
	cin => \inst1|Add12~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add12~0_combout\);

-- Location: LC_X16_Y8_N8
\inst1|TSUM[12][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][30]~combout\ = LCELL((((\inst1|Add12~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][30]~combout\);

-- Location: LC_X16_Y7_N2
\inst1|TSUM[12][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][29]~combout\ = LCELL((((\inst1|Add12~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][29]~combout\);

-- Location: LC_X15_Y7_N5
\inst1|TSUM[12][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][24]~combout\ = LCELL((((\inst1|Add12~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][24]~combout\);

-- Location: LC_X15_Y7_N8
\inst1|TSUM[12][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][19]~combout\ = LCELL((((\inst1|Add12~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][19]~combout\);

-- Location: LC_X14_Y7_N8
\inst1|TSUM[12][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][14]~combout\ = LCELL((((\inst1|Add12~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][14]~combout\);

-- Location: LC_X12_Y8_N0
\inst1|PSUM[14][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][0]~regout\ = DFFEAS(((!\inst1|PSUM[14][0]~regout\)), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][0]~39\ = CARRY(((\inst1|PSUM[14][0]~regout\)))
-- \inst1|PSUM[14][0]~39COUT1_329\ = CARRY(((\inst1|PSUM[14][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	datab => \inst1|PSUM[14][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][0]~regout\,
	cout0 => \inst1|PSUM[14][0]~39\,
	cout1 => \inst1|PSUM[14][0]~39COUT1_329\);

-- Location: LC_X12_Y8_N1
\inst1|PSUM[14][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][1]~regout\ = DFFEAS((\inst1|PSUM[14][1]~regout\ $ ((\inst1|PSUM[14][0]~39\))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][1]~37\ = CARRY(((!\inst1|PSUM[14][0]~39\) # (!\inst1|PSUM[14][1]~regout\)))
-- \inst1|PSUM[14][1]~37COUT1_330\ = CARRY(((!\inst1|PSUM[14][0]~39COUT1_329\) # (!\inst1|PSUM[14][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	datab => \inst1|PSUM[14][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[14][0]~39\,
	cin1 => \inst1|PSUM[14][0]~39COUT1_329\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][1]~regout\,
	cout0 => \inst1|PSUM[14][1]~37\,
	cout1 => \inst1|PSUM[14][1]~37COUT1_330\);

-- Location: LC_X12_Y8_N2
\inst1|PSUM[14][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][2]~regout\ = DFFEAS((\inst1|PSUM[14][2]~regout\ $ ((!\inst1|PSUM[14][1]~37\))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][2]~35\ = CARRY(((\inst1|PSUM[14][2]~regout\ & !\inst1|PSUM[14][1]~37\)))
-- \inst1|PSUM[14][2]~35COUT1_331\ = CARRY(((\inst1|PSUM[14][2]~regout\ & !\inst1|PSUM[14][1]~37COUT1_330\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	datab => \inst1|PSUM[14][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[14][1]~37\,
	cin1 => \inst1|PSUM[14][1]~37COUT1_330\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][2]~regout\,
	cout0 => \inst1|PSUM[14][2]~35\,
	cout1 => \inst1|PSUM[14][2]~35COUT1_331\);

-- Location: LC_X12_Y8_N3
\inst1|PSUM[14][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][3]~regout\ = DFFEAS(\inst1|PSUM[14][3]~regout\ $ ((((\inst1|PSUM[14][2]~35\)))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][3]~33\ = CARRY(((!\inst1|PSUM[14][2]~35\)) # (!\inst1|PSUM[14][3]~regout\))
-- \inst1|PSUM[14][3]~33COUT1_332\ = CARRY(((!\inst1|PSUM[14][2]~35COUT1_331\)) # (!\inst1|PSUM[14][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	dataa => \inst1|PSUM[14][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[14][2]~35\,
	cin1 => \inst1|PSUM[14][2]~35COUT1_331\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][3]~regout\,
	cout0 => \inst1|PSUM[14][3]~33\,
	cout1 => \inst1|PSUM[14][3]~33COUT1_332\);

-- Location: LC_X12_Y8_N4
\inst1|PSUM[14][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][4]~regout\ = DFFEAS(\inst1|PSUM[14][4]~regout\ $ ((((!\inst1|PSUM[14][3]~33\)))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][4]~31\ = CARRY((\inst1|PSUM[14][4]~regout\ & ((!\inst1|PSUM[14][3]~33COUT1_332\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	dataa => \inst1|PSUM[14][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[14][3]~33\,
	cin1 => \inst1|PSUM[14][3]~33COUT1_332\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][4]~regout\,
	cout => \inst1|PSUM[14][4]~31\);

-- Location: LC_X12_Y8_N5
\inst1|PSUM[14][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][5]~regout\ = DFFEAS(\inst1|PSUM[14][5]~regout\ $ ((((\inst1|PSUM[14][4]~31\)))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][5]~29\ = CARRY(((!\inst1|PSUM[14][4]~31\)) # (!\inst1|PSUM[14][5]~regout\))
-- \inst1|PSUM[14][5]~29COUT1_333\ = CARRY(((!\inst1|PSUM[14][4]~31\)) # (!\inst1|PSUM[14][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	dataa => \inst1|PSUM[14][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[14][4]~31\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][5]~regout\,
	cout0 => \inst1|PSUM[14][5]~29\,
	cout1 => \inst1|PSUM[14][5]~29COUT1_333\);

-- Location: LC_X12_Y8_N6
\inst1|PSUM[14][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][6]~regout\ = DFFEAS(\inst1|PSUM[14][6]~regout\ $ ((((!(!\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][5]~29\) # (\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][5]~29COUT1_333\))))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][6]~27\ = CARRY((\inst1|PSUM[14][6]~regout\ & ((!\inst1|PSUM[14][5]~29\))))
-- \inst1|PSUM[14][6]~27COUT1_334\ = CARRY((\inst1|PSUM[14][6]~regout\ & ((!\inst1|PSUM[14][5]~29COUT1_333\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	dataa => \inst1|PSUM[14][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[14][4]~31\,
	cin0 => \inst1|PSUM[14][5]~29\,
	cin1 => \inst1|PSUM[14][5]~29COUT1_333\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][6]~regout\,
	cout0 => \inst1|PSUM[14][6]~27\,
	cout1 => \inst1|PSUM[14][6]~27COUT1_334\);

-- Location: LC_X12_Y8_N7
\inst1|PSUM[14][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][7]~regout\ = DFFEAS((\inst1|PSUM[14][7]~regout\ $ (((!\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][6]~27\) # (\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][6]~27COUT1_334\)))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][7]~25\ = CARRY(((!\inst1|PSUM[14][6]~27\) # (!\inst1|PSUM[14][7]~regout\)))
-- \inst1|PSUM[14][7]~25COUT1_335\ = CARRY(((!\inst1|PSUM[14][6]~27COUT1_334\) # (!\inst1|PSUM[14][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	datab => \inst1|PSUM[14][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[14][4]~31\,
	cin0 => \inst1|PSUM[14][6]~27\,
	cin1 => \inst1|PSUM[14][6]~27COUT1_334\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][7]~regout\,
	cout0 => \inst1|PSUM[14][7]~25\,
	cout1 => \inst1|PSUM[14][7]~25COUT1_335\);

-- Location: LC_X12_Y8_N8
\inst1|PSUM[14][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][8]~regout\ = DFFEAS(\inst1|PSUM[14][8]~regout\ $ ((((!(!\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][7]~25\) # (\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][7]~25COUT1_335\))))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[14][8]~23\ = CARRY((\inst1|PSUM[14][8]~regout\ & ((!\inst1|PSUM[14][7]~25\))))
-- \inst1|PSUM[14][8]~23COUT1_336\ = CARRY((\inst1|PSUM[14][8]~regout\ & ((!\inst1|PSUM[14][7]~25COUT1_335\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	dataa => \inst1|PSUM[14][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[14][4]~31\,
	cin0 => \inst1|PSUM[14][7]~25\,
	cin1 => \inst1|PSUM[14][7]~25COUT1_335\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][8]~regout\,
	cout0 => \inst1|PSUM[14][8]~23\,
	cout1 => \inst1|PSUM[14][8]~23COUT1_336\);

-- Location: LC_X12_Y8_N9
\inst1|PSUM[14][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[14][9]~regout\ = DFFEAS((((!\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][8]~23\) # (\inst1|PSUM[14][4]~31\ & \inst1|PSUM[14][8]~23COUT1_336\) $ (\inst1|PSUM[14][9]~regout\))), \inst6|STEMP\(14), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(14),
	datad => \inst1|PSUM[14][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[14][4]~31\,
	cin0 => \inst1|PSUM[14][8]~23\,
	cin1 => \inst1|PSUM[14][8]~23COUT1_336\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[14][9]~regout\);

-- Location: LC_X14_Y7_N2
\inst1|TSUM[12][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][9]~combout\ = LCELL((((\inst1|Add12~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][9]~combout\);

-- Location: LC_X13_Y6_N3
\inst1|TSUM[12][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][4]~combout\ = LCELL((((\inst1|Add12~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][4]~combout\);

-- Location: LC_X13_Y8_N1
\inst1|TSUM[12][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][3]~combout\ = LCELL((((\inst1|Add12~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][3]~combout\);

-- Location: LC_X13_Y6_N1
\inst1|TSUM[12][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][2]~combout\ = LCELL((((\inst1|Add12~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][2]~combout\);

-- Location: LC_X13_Y8_N2
\inst1|TSUM[12][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][1]~combout\ = LCELL((((\inst1|Add12~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][1]~combout\);

-- Location: LC_X13_Y6_N0
\inst1|TSUM[12][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][0]~combout\ = LCELL((((\inst1|Add12~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][0]~combout\);

-- Location: LC_X13_Y8_N5
\inst1|Add13~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~150_combout\ = \inst1|PSUM[14][0]~regout\ $ ((\inst1|TSUM[12][0]~combout\))
-- \inst1|Add13~152\ = CARRY((\inst1|PSUM[14][0]~regout\ & (\inst1|TSUM[12][0]~combout\)))
-- \inst1|Add13~152COUT1_156\ = CARRY((\inst1|PSUM[14][0]~regout\ & (\inst1|TSUM[12][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[14][0]~regout\,
	datab => \inst1|TSUM[12][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~150_combout\,
	cout0 => \inst1|Add13~152\,
	cout1 => \inst1|Add13~152COUT1_156\);

-- Location: LC_X13_Y8_N6
\inst1|Add13~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~145_combout\ = \inst1|PSUM[14][1]~regout\ $ (\inst1|TSUM[12][1]~combout\ $ ((\inst1|Add13~152\)))
-- \inst1|Add13~147\ = CARRY((\inst1|PSUM[14][1]~regout\ & (!\inst1|TSUM[12][1]~combout\ & !\inst1|Add13~152\)) # (!\inst1|PSUM[14][1]~regout\ & ((!\inst1|Add13~152\) # (!\inst1|TSUM[12][1]~combout\))))
-- \inst1|Add13~147COUT1_157\ = CARRY((\inst1|PSUM[14][1]~regout\ & (!\inst1|TSUM[12][1]~combout\ & !\inst1|Add13~152COUT1_156\)) # (!\inst1|PSUM[14][1]~regout\ & ((!\inst1|Add13~152COUT1_156\) # (!\inst1|TSUM[12][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[14][1]~regout\,
	datab => \inst1|TSUM[12][1]~combout\,
	cin0 => \inst1|Add13~152\,
	cin1 => \inst1|Add13~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~145_combout\,
	cout0 => \inst1|Add13~147\,
	cout1 => \inst1|Add13~147COUT1_157\);

-- Location: LC_X13_Y8_N7
\inst1|Add13~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~140_combout\ = \inst1|TSUM[12][2]~combout\ $ (\inst1|PSUM[14][2]~regout\ $ ((!\inst1|Add13~147\)))
-- \inst1|Add13~142\ = CARRY((\inst1|TSUM[12][2]~combout\ & ((\inst1|PSUM[14][2]~regout\) # (!\inst1|Add13~147\))) # (!\inst1|TSUM[12][2]~combout\ & (\inst1|PSUM[14][2]~regout\ & !\inst1|Add13~147\)))
-- \inst1|Add13~142COUT1_158\ = CARRY((\inst1|TSUM[12][2]~combout\ & ((\inst1|PSUM[14][2]~regout\) # (!\inst1|Add13~147COUT1_157\))) # (!\inst1|TSUM[12][2]~combout\ & (\inst1|PSUM[14][2]~regout\ & !\inst1|Add13~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][2]~combout\,
	datab => \inst1|PSUM[14][2]~regout\,
	cin0 => \inst1|Add13~147\,
	cin1 => \inst1|Add13~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~140_combout\,
	cout0 => \inst1|Add13~142\,
	cout1 => \inst1|Add13~142COUT1_158\);

-- Location: LC_X13_Y8_N8
\inst1|Add13~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~135_combout\ = \inst1|PSUM[14][3]~regout\ $ (\inst1|TSUM[12][3]~combout\ $ ((\inst1|Add13~142\)))
-- \inst1|Add13~137\ = CARRY((\inst1|PSUM[14][3]~regout\ & (!\inst1|TSUM[12][3]~combout\ & !\inst1|Add13~142\)) # (!\inst1|PSUM[14][3]~regout\ & ((!\inst1|Add13~142\) # (!\inst1|TSUM[12][3]~combout\))))
-- \inst1|Add13~137COUT1_159\ = CARRY((\inst1|PSUM[14][3]~regout\ & (!\inst1|TSUM[12][3]~combout\ & !\inst1|Add13~142COUT1_158\)) # (!\inst1|PSUM[14][3]~regout\ & ((!\inst1|Add13~142COUT1_158\) # (!\inst1|TSUM[12][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[14][3]~regout\,
	datab => \inst1|TSUM[12][3]~combout\,
	cin0 => \inst1|Add13~142\,
	cin1 => \inst1|Add13~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~135_combout\,
	cout0 => \inst1|Add13~137\,
	cout1 => \inst1|Add13~137COUT1_159\);

-- Location: LC_X13_Y8_N9
\inst1|Add13~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~130_combout\ = \inst1|TSUM[12][4]~combout\ $ (\inst1|PSUM[14][4]~regout\ $ ((!\inst1|Add13~137\)))
-- \inst1|Add13~132\ = CARRY((\inst1|TSUM[12][4]~combout\ & ((\inst1|PSUM[14][4]~regout\) # (!\inst1|Add13~137COUT1_159\))) # (!\inst1|TSUM[12][4]~combout\ & (\inst1|PSUM[14][4]~regout\ & !\inst1|Add13~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][4]~combout\,
	datab => \inst1|PSUM[14][4]~regout\,
	cin0 => \inst1|Add13~137\,
	cin1 => \inst1|Add13~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~130_combout\,
	cout => \inst1|Add13~132\);

-- Location: LC_X14_Y7_N0
\inst1|TSUM[12][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][8]~combout\ = LCELL((((\inst1|Add12~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][8]~combout\);

-- Location: LC_X14_Y7_N6
\inst1|TSUM[12][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][7]~combout\ = LCELL((((\inst1|Add12~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][7]~combout\);

-- Location: LC_X14_Y7_N5
\inst1|TSUM[12][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][6]~combout\ = LCELL((((\inst1|Add12~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][6]~combout\);

-- Location: LC_X14_Y7_N3
\inst1|TSUM[12][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][5]~combout\ = LCELL((((\inst1|Add12~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][5]~combout\);

-- Location: LC_X14_Y8_N0
\inst1|Add13~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~125_combout\ = \inst1|PSUM[14][5]~regout\ $ (\inst1|TSUM[12][5]~combout\ $ ((\inst1|Add13~132\)))
-- \inst1|Add13~127\ = CARRY((\inst1|PSUM[14][5]~regout\ & (!\inst1|TSUM[12][5]~combout\ & !\inst1|Add13~132\)) # (!\inst1|PSUM[14][5]~regout\ & ((!\inst1|Add13~132\) # (!\inst1|TSUM[12][5]~combout\))))
-- \inst1|Add13~127COUT1_160\ = CARRY((\inst1|PSUM[14][5]~regout\ & (!\inst1|TSUM[12][5]~combout\ & !\inst1|Add13~132\)) # (!\inst1|PSUM[14][5]~regout\ & ((!\inst1|Add13~132\) # (!\inst1|TSUM[12][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[14][5]~regout\,
	datab => \inst1|TSUM[12][5]~combout\,
	cin => \inst1|Add13~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~125_combout\,
	cout0 => \inst1|Add13~127\,
	cout1 => \inst1|Add13~127COUT1_160\);

-- Location: LC_X14_Y8_N1
\inst1|Add13~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~120_combout\ = \inst1|PSUM[14][6]~regout\ $ (\inst1|TSUM[12][6]~combout\ $ ((!(!\inst1|Add13~132\ & \inst1|Add13~127\) # (\inst1|Add13~132\ & \inst1|Add13~127COUT1_160\))))
-- \inst1|Add13~122\ = CARRY((\inst1|PSUM[14][6]~regout\ & ((\inst1|TSUM[12][6]~combout\) # (!\inst1|Add13~127\))) # (!\inst1|PSUM[14][6]~regout\ & (\inst1|TSUM[12][6]~combout\ & !\inst1|Add13~127\)))
-- \inst1|Add13~122COUT1_161\ = CARRY((\inst1|PSUM[14][6]~regout\ & ((\inst1|TSUM[12][6]~combout\) # (!\inst1|Add13~127COUT1_160\))) # (!\inst1|PSUM[14][6]~regout\ & (\inst1|TSUM[12][6]~combout\ & !\inst1|Add13~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[14][6]~regout\,
	datab => \inst1|TSUM[12][6]~combout\,
	cin => \inst1|Add13~132\,
	cin0 => \inst1|Add13~127\,
	cin1 => \inst1|Add13~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~120_combout\,
	cout0 => \inst1|Add13~122\,
	cout1 => \inst1|Add13~122COUT1_161\);

-- Location: LC_X14_Y8_N2
\inst1|Add13~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~115_combout\ = \inst1|TSUM[12][7]~combout\ $ (\inst1|PSUM[14][7]~regout\ $ (((!\inst1|Add13~132\ & \inst1|Add13~122\) # (\inst1|Add13~132\ & \inst1|Add13~122COUT1_161\))))
-- \inst1|Add13~117\ = CARRY((\inst1|TSUM[12][7]~combout\ & (!\inst1|PSUM[14][7]~regout\ & !\inst1|Add13~122\)) # (!\inst1|TSUM[12][7]~combout\ & ((!\inst1|Add13~122\) # (!\inst1|PSUM[14][7]~regout\))))
-- \inst1|Add13~117COUT1_162\ = CARRY((\inst1|TSUM[12][7]~combout\ & (!\inst1|PSUM[14][7]~regout\ & !\inst1|Add13~122COUT1_161\)) # (!\inst1|TSUM[12][7]~combout\ & ((!\inst1|Add13~122COUT1_161\) # (!\inst1|PSUM[14][7]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][7]~combout\,
	datab => \inst1|PSUM[14][7]~regout\,
	cin => \inst1|Add13~132\,
	cin0 => \inst1|Add13~122\,
	cin1 => \inst1|Add13~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~115_combout\,
	cout0 => \inst1|Add13~117\,
	cout1 => \inst1|Add13~117COUT1_162\);

-- Location: LC_X14_Y8_N3
\inst1|Add13~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~110_combout\ = \inst1|TSUM[12][8]~combout\ $ (\inst1|PSUM[14][8]~regout\ $ ((!(!\inst1|Add13~132\ & \inst1|Add13~117\) # (\inst1|Add13~132\ & \inst1|Add13~117COUT1_162\))))
-- \inst1|Add13~112\ = CARRY((\inst1|TSUM[12][8]~combout\ & ((\inst1|PSUM[14][8]~regout\) # (!\inst1|Add13~117\))) # (!\inst1|TSUM[12][8]~combout\ & (\inst1|PSUM[14][8]~regout\ & !\inst1|Add13~117\)))
-- \inst1|Add13~112COUT1_163\ = CARRY((\inst1|TSUM[12][8]~combout\ & ((\inst1|PSUM[14][8]~regout\) # (!\inst1|Add13~117COUT1_162\))) # (!\inst1|TSUM[12][8]~combout\ & (\inst1|PSUM[14][8]~regout\ & !\inst1|Add13~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][8]~combout\,
	datab => \inst1|PSUM[14][8]~regout\,
	cin => \inst1|Add13~132\,
	cin0 => \inst1|Add13~117\,
	cin1 => \inst1|Add13~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~110_combout\,
	cout0 => \inst1|Add13~112\,
	cout1 => \inst1|Add13~112COUT1_163\);

-- Location: LC_X14_Y8_N4
\inst1|Add13~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~105_combout\ = \inst1|PSUM[14][9]~regout\ $ (\inst1|TSUM[12][9]~combout\ $ (((!\inst1|Add13~132\ & \inst1|Add13~112\) # (\inst1|Add13~132\ & \inst1|Add13~112COUT1_163\))))
-- \inst1|Add13~107\ = CARRY((\inst1|PSUM[14][9]~regout\ & (!\inst1|TSUM[12][9]~combout\ & !\inst1|Add13~112COUT1_163\)) # (!\inst1|PSUM[14][9]~regout\ & ((!\inst1|Add13~112COUT1_163\) # (!\inst1|TSUM[12][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[14][9]~regout\,
	datab => \inst1|TSUM[12][9]~combout\,
	cin => \inst1|Add13~132\,
	cin0 => \inst1|Add13~112\,
	cin1 => \inst1|Add13~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~105_combout\,
	cout => \inst1|Add13~107\);

-- Location: LC_X14_Y7_N1
\inst1|TSUM[12][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][13]~combout\ = LCELL((((\inst1|Add12~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][13]~combout\);

-- Location: LC_X14_Y7_N4
\inst1|TSUM[12][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][12]~combout\ = LCELL((((\inst1|Add12~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][12]~combout\);

-- Location: LC_X14_Y7_N9
\inst1|TSUM[12][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][11]~combout\ = LCELL((((\inst1|Add12~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][11]~combout\);

-- Location: LC_X14_Y7_N7
\inst1|TSUM[12][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][10]~combout\ = LCELL((((\inst1|Add12~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][10]~combout\);

-- Location: LC_X14_Y8_N5
\inst1|Add13~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~100_combout\ = (\inst1|TSUM[12][10]~combout\ $ ((!\inst1|Add13~107\)))
-- \inst1|Add13~102\ = CARRY(((\inst1|TSUM[12][10]~combout\ & !\inst1|Add13~107\)))
-- \inst1|Add13~102COUT1_164\ = CARRY(((\inst1|TSUM[12][10]~combout\ & !\inst1|Add13~107\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][10]~combout\,
	cin => \inst1|Add13~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~100_combout\,
	cout0 => \inst1|Add13~102\,
	cout1 => \inst1|Add13~102COUT1_164\);

-- Location: LC_X14_Y8_N6
\inst1|Add13~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~95_combout\ = (\inst1|TSUM[12][11]~combout\ $ (((!\inst1|Add13~107\ & \inst1|Add13~102\) # (\inst1|Add13~107\ & \inst1|Add13~102COUT1_164\))))
-- \inst1|Add13~97\ = CARRY(((!\inst1|Add13~102\) # (!\inst1|TSUM[12][11]~combout\)))
-- \inst1|Add13~97COUT1_165\ = CARRY(((!\inst1|Add13~102COUT1_164\) # (!\inst1|TSUM[12][11]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][11]~combout\,
	cin => \inst1|Add13~107\,
	cin0 => \inst1|Add13~102\,
	cin1 => \inst1|Add13~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~95_combout\,
	cout0 => \inst1|Add13~97\,
	cout1 => \inst1|Add13~97COUT1_165\);

-- Location: LC_X14_Y8_N7
\inst1|Add13~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~90_combout\ = (\inst1|TSUM[12][12]~combout\ $ ((!(!\inst1|Add13~107\ & \inst1|Add13~97\) # (\inst1|Add13~107\ & \inst1|Add13~97COUT1_165\))))
-- \inst1|Add13~92\ = CARRY(((\inst1|TSUM[12][12]~combout\ & !\inst1|Add13~97\)))
-- \inst1|Add13~92COUT1_166\ = CARRY(((\inst1|TSUM[12][12]~combout\ & !\inst1|Add13~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][12]~combout\,
	cin => \inst1|Add13~107\,
	cin0 => \inst1|Add13~97\,
	cin1 => \inst1|Add13~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~90_combout\,
	cout0 => \inst1|Add13~92\,
	cout1 => \inst1|Add13~92COUT1_166\);

-- Location: LC_X14_Y8_N8
\inst1|Add13~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~85_combout\ = (\inst1|TSUM[12][13]~combout\ $ (((!\inst1|Add13~107\ & \inst1|Add13~92\) # (\inst1|Add13~107\ & \inst1|Add13~92COUT1_166\))))
-- \inst1|Add13~87\ = CARRY(((!\inst1|Add13~92\) # (!\inst1|TSUM[12][13]~combout\)))
-- \inst1|Add13~87COUT1_167\ = CARRY(((!\inst1|Add13~92COUT1_166\) # (!\inst1|TSUM[12][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][13]~combout\,
	cin => \inst1|Add13~107\,
	cin0 => \inst1|Add13~92\,
	cin1 => \inst1|Add13~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~85_combout\,
	cout0 => \inst1|Add13~87\,
	cout1 => \inst1|Add13~87COUT1_167\);

-- Location: LC_X14_Y8_N9
\inst1|Add13~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~80_combout\ = (\inst1|TSUM[12][14]~combout\ $ ((!(!\inst1|Add13~107\ & \inst1|Add13~87\) # (\inst1|Add13~107\ & \inst1|Add13~87COUT1_167\))))
-- \inst1|Add13~82\ = CARRY(((\inst1|TSUM[12][14]~combout\ & !\inst1|Add13~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][14]~combout\,
	cin => \inst1|Add13~107\,
	cin0 => \inst1|Add13~87\,
	cin1 => \inst1|Add13~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~80_combout\,
	cout => \inst1|Add13~82\);

-- Location: LC_X15_Y7_N6
\inst1|TSUM[12][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][18]~combout\ = LCELL((((\inst1|Add12~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][18]~combout\);

-- Location: LC_X15_Y7_N3
\inst1|TSUM[12][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][17]~combout\ = LCELL((((\inst1|Add12~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][17]~combout\);

-- Location: LC_X15_Y7_N1
\inst1|TSUM[12][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][16]~combout\ = LCELL((((\inst1|Add12~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][16]~combout\);

-- Location: LC_X15_Y7_N0
\inst1|TSUM[12][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][15]~combout\ = LCELL((((\inst1|Add12~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][15]~combout\);

-- Location: LC_X15_Y8_N0
\inst1|Add13~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~75_combout\ = \inst1|TSUM[12][15]~combout\ $ ((((\inst1|Add13~82\))))
-- \inst1|Add13~77\ = CARRY(((!\inst1|Add13~82\)) # (!\inst1|TSUM[12][15]~combout\))
-- \inst1|Add13~77COUT1_168\ = CARRY(((!\inst1|Add13~82\)) # (!\inst1|TSUM[12][15]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][15]~combout\,
	cin => \inst1|Add13~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~75_combout\,
	cout0 => \inst1|Add13~77\,
	cout1 => \inst1|Add13~77COUT1_168\);

-- Location: LC_X15_Y8_N1
\inst1|Add13~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~70_combout\ = \inst1|TSUM[12][16]~combout\ $ ((((!(!\inst1|Add13~82\ & \inst1|Add13~77\) # (\inst1|Add13~82\ & \inst1|Add13~77COUT1_168\)))))
-- \inst1|Add13~72\ = CARRY((\inst1|TSUM[12][16]~combout\ & ((!\inst1|Add13~77\))))
-- \inst1|Add13~72COUT1_169\ = CARRY((\inst1|TSUM[12][16]~combout\ & ((!\inst1|Add13~77COUT1_168\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][16]~combout\,
	cin => \inst1|Add13~82\,
	cin0 => \inst1|Add13~77\,
	cin1 => \inst1|Add13~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~70_combout\,
	cout0 => \inst1|Add13~72\,
	cout1 => \inst1|Add13~72COUT1_169\);

-- Location: LC_X15_Y8_N2
\inst1|Add13~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~65_combout\ = (\inst1|TSUM[12][17]~combout\ $ (((!\inst1|Add13~82\ & \inst1|Add13~72\) # (\inst1|Add13~82\ & \inst1|Add13~72COUT1_169\))))
-- \inst1|Add13~67\ = CARRY(((!\inst1|Add13~72\) # (!\inst1|TSUM[12][17]~combout\)))
-- \inst1|Add13~67COUT1_170\ = CARRY(((!\inst1|Add13~72COUT1_169\) # (!\inst1|TSUM[12][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][17]~combout\,
	cin => \inst1|Add13~82\,
	cin0 => \inst1|Add13~72\,
	cin1 => \inst1|Add13~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~65_combout\,
	cout0 => \inst1|Add13~67\,
	cout1 => \inst1|Add13~67COUT1_170\);

-- Location: LC_X15_Y8_N3
\inst1|Add13~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~60_combout\ = \inst1|TSUM[12][18]~combout\ $ ((((!(!\inst1|Add13~82\ & \inst1|Add13~67\) # (\inst1|Add13~82\ & \inst1|Add13~67COUT1_170\)))))
-- \inst1|Add13~62\ = CARRY((\inst1|TSUM[12][18]~combout\ & ((!\inst1|Add13~67\))))
-- \inst1|Add13~62COUT1_171\ = CARRY((\inst1|TSUM[12][18]~combout\ & ((!\inst1|Add13~67COUT1_170\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][18]~combout\,
	cin => \inst1|Add13~82\,
	cin0 => \inst1|Add13~67\,
	cin1 => \inst1|Add13~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~60_combout\,
	cout0 => \inst1|Add13~62\,
	cout1 => \inst1|Add13~62COUT1_171\);

-- Location: LC_X15_Y8_N4
\inst1|Add13~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~55_combout\ = (\inst1|TSUM[12][19]~combout\ $ (((!\inst1|Add13~82\ & \inst1|Add13~62\) # (\inst1|Add13~82\ & \inst1|Add13~62COUT1_171\))))
-- \inst1|Add13~57\ = CARRY(((!\inst1|Add13~62COUT1_171\) # (!\inst1|TSUM[12][19]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][19]~combout\,
	cin => \inst1|Add13~82\,
	cin0 => \inst1|Add13~62\,
	cin1 => \inst1|Add13~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~55_combout\,
	cout => \inst1|Add13~57\);

-- Location: LC_X15_Y7_N2
\inst1|TSUM[12][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][23]~combout\ = LCELL((((\inst1|Add12~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][23]~combout\);

-- Location: LC_X15_Y7_N9
\inst1|TSUM[12][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][22]~combout\ = LCELL((((\inst1|Add12~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][22]~combout\);

-- Location: LC_X15_Y7_N7
\inst1|TSUM[12][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][21]~combout\ = LCELL((((\inst1|Add12~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][21]~combout\);

-- Location: LC_X15_Y7_N4
\inst1|TSUM[12][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][20]~combout\ = LCELL((((\inst1|Add12~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][20]~combout\);

-- Location: LC_X15_Y8_N5
\inst1|Add13~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~50_combout\ = \inst1|TSUM[12][20]~combout\ $ ((((!\inst1|Add13~57\))))
-- \inst1|Add13~52\ = CARRY((\inst1|TSUM[12][20]~combout\ & ((!\inst1|Add13~57\))))
-- \inst1|Add13~52COUT1_172\ = CARRY((\inst1|TSUM[12][20]~combout\ & ((!\inst1|Add13~57\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][20]~combout\,
	cin => \inst1|Add13~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~50_combout\,
	cout0 => \inst1|Add13~52\,
	cout1 => \inst1|Add13~52COUT1_172\);

-- Location: LC_X15_Y8_N6
\inst1|Add13~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~45_combout\ = \inst1|TSUM[12][21]~combout\ $ (((((!\inst1|Add13~57\ & \inst1|Add13~52\) # (\inst1|Add13~57\ & \inst1|Add13~52COUT1_172\)))))
-- \inst1|Add13~47\ = CARRY(((!\inst1|Add13~52\)) # (!\inst1|TSUM[12][21]~combout\))
-- \inst1|Add13~47COUT1_173\ = CARRY(((!\inst1|Add13~52COUT1_172\)) # (!\inst1|TSUM[12][21]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[12][21]~combout\,
	cin => \inst1|Add13~57\,
	cin0 => \inst1|Add13~52\,
	cin1 => \inst1|Add13~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~45_combout\,
	cout0 => \inst1|Add13~47\,
	cout1 => \inst1|Add13~47COUT1_173\);

-- Location: LC_X15_Y8_N7
\inst1|Add13~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~40_combout\ = (\inst1|TSUM[12][22]~combout\ $ ((!(!\inst1|Add13~57\ & \inst1|Add13~47\) # (\inst1|Add13~57\ & \inst1|Add13~47COUT1_173\))))
-- \inst1|Add13~42\ = CARRY(((\inst1|TSUM[12][22]~combout\ & !\inst1|Add13~47\)))
-- \inst1|Add13~42COUT1_174\ = CARRY(((\inst1|TSUM[12][22]~combout\ & !\inst1|Add13~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][22]~combout\,
	cin => \inst1|Add13~57\,
	cin0 => \inst1|Add13~47\,
	cin1 => \inst1|Add13~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~40_combout\,
	cout0 => \inst1|Add13~42\,
	cout1 => \inst1|Add13~42COUT1_174\);

-- Location: LC_X15_Y8_N8
\inst1|Add13~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~35_combout\ = (\inst1|TSUM[12][23]~combout\ $ (((!\inst1|Add13~57\ & \inst1|Add13~42\) # (\inst1|Add13~57\ & \inst1|Add13~42COUT1_174\))))
-- \inst1|Add13~37\ = CARRY(((!\inst1|Add13~42\) # (!\inst1|TSUM[12][23]~combout\)))
-- \inst1|Add13~37COUT1_175\ = CARRY(((!\inst1|Add13~42COUT1_174\) # (!\inst1|TSUM[12][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][23]~combout\,
	cin => \inst1|Add13~57\,
	cin0 => \inst1|Add13~42\,
	cin1 => \inst1|Add13~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~35_combout\,
	cout0 => \inst1|Add13~37\,
	cout1 => \inst1|Add13~37COUT1_175\);

-- Location: LC_X15_Y8_N9
\inst1|Add13~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~30_combout\ = (\inst1|TSUM[12][24]~combout\ $ ((!(!\inst1|Add13~57\ & \inst1|Add13~37\) # (\inst1|Add13~57\ & \inst1|Add13~37COUT1_175\))))
-- \inst1|Add13~32\ = CARRY(((\inst1|TSUM[12][24]~combout\ & !\inst1|Add13~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][24]~combout\,
	cin => \inst1|Add13~57\,
	cin0 => \inst1|Add13~37\,
	cin1 => \inst1|Add13~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~30_combout\,
	cout => \inst1|Add13~32\);

-- Location: LC_X16_Y6_N7
\inst1|TSUM[12][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][28]~combout\ = LCELL((((\inst1|Add12~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][28]~combout\);

-- Location: LC_X11_Y6_N0
\inst1|TSUM[12][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][27]~combout\ = LCELL((((\inst1|Add12~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][27]~combout\);

-- Location: LC_X16_Y8_N9
\inst1|TSUM[12][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][26]~combout\ = LCELL((((\inst1|Add12~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add12~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][26]~combout\);

-- Location: LC_X16_Y7_N6
\inst1|TSUM[12][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[12][25]~combout\ = LCELL((((\inst1|Add12~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add12~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[12][25]~combout\);

-- Location: LC_X16_Y8_N0
\inst1|Add13~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~25_combout\ = (\inst1|TSUM[12][25]~combout\ $ ((\inst1|Add13~32\)))
-- \inst1|Add13~27\ = CARRY(((!\inst1|Add13~32\) # (!\inst1|TSUM[12][25]~combout\)))
-- \inst1|Add13~27COUT1_176\ = CARRY(((!\inst1|Add13~32\) # (!\inst1|TSUM[12][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][25]~combout\,
	cin => \inst1|Add13~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~25_combout\,
	cout0 => \inst1|Add13~27\,
	cout1 => \inst1|Add13~27COUT1_176\);

-- Location: LC_X16_Y8_N1
\inst1|Add13~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~20_combout\ = (\inst1|TSUM[12][26]~combout\ $ ((!(!\inst1|Add13~32\ & \inst1|Add13~27\) # (\inst1|Add13~32\ & \inst1|Add13~27COUT1_176\))))
-- \inst1|Add13~22\ = CARRY(((\inst1|TSUM[12][26]~combout\ & !\inst1|Add13~27\)))
-- \inst1|Add13~22COUT1_177\ = CARRY(((\inst1|TSUM[12][26]~combout\ & !\inst1|Add13~27COUT1_176\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][26]~combout\,
	cin => \inst1|Add13~32\,
	cin0 => \inst1|Add13~27\,
	cin1 => \inst1|Add13~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~20_combout\,
	cout0 => \inst1|Add13~22\,
	cout1 => \inst1|Add13~22COUT1_177\);

-- Location: LC_X16_Y8_N2
\inst1|Add13~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~15_combout\ = (\inst1|TSUM[12][27]~combout\ $ (((!\inst1|Add13~32\ & \inst1|Add13~22\) # (\inst1|Add13~32\ & \inst1|Add13~22COUT1_177\))))
-- \inst1|Add13~17\ = CARRY(((!\inst1|Add13~22\) # (!\inst1|TSUM[12][27]~combout\)))
-- \inst1|Add13~17COUT1_178\ = CARRY(((!\inst1|Add13~22COUT1_177\) # (!\inst1|TSUM[12][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][27]~combout\,
	cin => \inst1|Add13~32\,
	cin0 => \inst1|Add13~22\,
	cin1 => \inst1|Add13~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~15_combout\,
	cout0 => \inst1|Add13~17\,
	cout1 => \inst1|Add13~17COUT1_178\);

-- Location: LC_X16_Y8_N3
\inst1|Add13~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~10_combout\ = (\inst1|TSUM[12][28]~combout\ $ ((!(!\inst1|Add13~32\ & \inst1|Add13~17\) # (\inst1|Add13~32\ & \inst1|Add13~17COUT1_178\))))
-- \inst1|Add13~12\ = CARRY(((\inst1|TSUM[12][28]~combout\ & !\inst1|Add13~17\)))
-- \inst1|Add13~12COUT1_179\ = CARRY(((\inst1|TSUM[12][28]~combout\ & !\inst1|Add13~17COUT1_178\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][28]~combout\,
	cin => \inst1|Add13~32\,
	cin0 => \inst1|Add13~17\,
	cin1 => \inst1|Add13~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~10_combout\,
	cout0 => \inst1|Add13~12\,
	cout1 => \inst1|Add13~12COUT1_179\);

-- Location: LC_X16_Y8_N4
\inst1|Add13~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~5_combout\ = (\inst1|TSUM[12][29]~combout\ $ (((!\inst1|Add13~32\ & \inst1|Add13~12\) # (\inst1|Add13~32\ & \inst1|Add13~12COUT1_179\))))
-- \inst1|Add13~7\ = CARRY(((!\inst1|Add13~12COUT1_179\) # (!\inst1|TSUM[12][29]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[12][29]~combout\,
	cin => \inst1|Add13~32\,
	cin0 => \inst1|Add13~12\,
	cin1 => \inst1|Add13~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~5_combout\,
	cout => \inst1|Add13~7\);

-- Location: LC_X16_Y8_N5
\inst1|Add13~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add13~0_combout\ = ((\inst1|Add13~7\ $ (!\inst1|TSUM[12][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[12][30]~combout\,
	cin => \inst1|Add13~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add13~0_combout\);

-- Location: LC_X16_Y8_N6
\inst1|TSUM[13][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][30]~combout\ = LCELL((((\inst1|Add13~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][30]~combout\);

-- Location: LC_X15_Y9_N6
\inst1|TSUM[13][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][29]~combout\ = LCELL((((\inst1|Add13~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][29]~combout\);

-- Location: LC_X15_Y10_N6
\inst1|TSUM[13][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][24]~combout\ = LCELL((((\inst1|Add13~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][24]~combout\);

-- Location: LC_X15_Y10_N7
\inst1|TSUM[13][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][19]~combout\ = LCELL((((\inst1|Add13~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][19]~combout\);

-- Location: LC_X13_Y10_N6
\inst1|TSUM[13][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][14]~combout\ = LCELL((((\inst1|Add13~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][14]~combout\);

-- Location: LC_X10_Y8_N0
\inst1|PSUM[15][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][0]~regout\ = DFFEAS(((!\inst1|PSUM[15][0]~regout\)), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][0]~19\ = CARRY(((\inst1|PSUM[15][0]~regout\)))
-- \inst1|PSUM[15][0]~19COUT1_321\ = CARRY(((\inst1|PSUM[15][0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33cc",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	datab => \inst1|PSUM[15][0]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][0]~regout\,
	cout0 => \inst1|PSUM[15][0]~19\,
	cout1 => \inst1|PSUM[15][0]~19COUT1_321\);

-- Location: LC_X10_Y8_N1
\inst1|PSUM[15][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][1]~regout\ = DFFEAS((\inst1|PSUM[15][1]~regout\ $ ((\inst1|PSUM[15][0]~19\))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][1]~17\ = CARRY(((!\inst1|PSUM[15][0]~19\) # (!\inst1|PSUM[15][1]~regout\)))
-- \inst1|PSUM[15][1]~17COUT1_322\ = CARRY(((!\inst1|PSUM[15][0]~19COUT1_321\) # (!\inst1|PSUM[15][1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	datab => \inst1|PSUM[15][1]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[15][0]~19\,
	cin1 => \inst1|PSUM[15][0]~19COUT1_321\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][1]~regout\,
	cout0 => \inst1|PSUM[15][1]~17\,
	cout1 => \inst1|PSUM[15][1]~17COUT1_322\);

-- Location: LC_X10_Y8_N2
\inst1|PSUM[15][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][2]~regout\ = DFFEAS((\inst1|PSUM[15][2]~regout\ $ ((!\inst1|PSUM[15][1]~17\))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][2]~15\ = CARRY(((\inst1|PSUM[15][2]~regout\ & !\inst1|PSUM[15][1]~17\)))
-- \inst1|PSUM[15][2]~15COUT1_323\ = CARRY(((\inst1|PSUM[15][2]~regout\ & !\inst1|PSUM[15][1]~17COUT1_322\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	datab => \inst1|PSUM[15][2]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[15][1]~17\,
	cin1 => \inst1|PSUM[15][1]~17COUT1_322\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][2]~regout\,
	cout0 => \inst1|PSUM[15][2]~15\,
	cout1 => \inst1|PSUM[15][2]~15COUT1_323\);

-- Location: LC_X10_Y8_N3
\inst1|PSUM[15][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][3]~regout\ = DFFEAS(\inst1|PSUM[15][3]~regout\ $ ((((\inst1|PSUM[15][2]~15\)))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][3]~13\ = CARRY(((!\inst1|PSUM[15][2]~15\)) # (!\inst1|PSUM[15][3]~regout\))
-- \inst1|PSUM[15][3]~13COUT1_324\ = CARRY(((!\inst1|PSUM[15][2]~15COUT1_323\)) # (!\inst1|PSUM[15][3]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	dataa => \inst1|PSUM[15][3]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[15][2]~15\,
	cin1 => \inst1|PSUM[15][2]~15COUT1_323\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][3]~regout\,
	cout0 => \inst1|PSUM[15][3]~13\,
	cout1 => \inst1|PSUM[15][3]~13COUT1_324\);

-- Location: LC_X10_Y8_N4
\inst1|PSUM[15][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][4]~regout\ = DFFEAS(\inst1|PSUM[15][4]~regout\ $ ((((!\inst1|PSUM[15][3]~13\)))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][4]~11\ = CARRY((\inst1|PSUM[15][4]~regout\ & ((!\inst1|PSUM[15][3]~13COUT1_324\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	dataa => \inst1|PSUM[15][4]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin0 => \inst1|PSUM[15][3]~13\,
	cin1 => \inst1|PSUM[15][3]~13COUT1_324\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][4]~regout\,
	cout => \inst1|PSUM[15][4]~11\);

-- Location: LC_X10_Y8_N5
\inst1|PSUM[15][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][5]~regout\ = DFFEAS(\inst1|PSUM[15][5]~regout\ $ ((((\inst1|PSUM[15][4]~11\)))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][5]~9\ = CARRY(((!\inst1|PSUM[15][4]~11\)) # (!\inst1|PSUM[15][5]~regout\))
-- \inst1|PSUM[15][5]~9COUT1_325\ = CARRY(((!\inst1|PSUM[15][4]~11\)) # (!\inst1|PSUM[15][5]~regout\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	dataa => \inst1|PSUM[15][5]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[15][4]~11\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][5]~regout\,
	cout0 => \inst1|PSUM[15][5]~9\,
	cout1 => \inst1|PSUM[15][5]~9COUT1_325\);

-- Location: LC_X10_Y8_N6
\inst1|PSUM[15][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][6]~regout\ = DFFEAS(\inst1|PSUM[15][6]~regout\ $ ((((!(!\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][5]~9\) # (\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][5]~9COUT1_325\))))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][6]~7\ = CARRY((\inst1|PSUM[15][6]~regout\ & ((!\inst1|PSUM[15][5]~9\))))
-- \inst1|PSUM[15][6]~7COUT1_326\ = CARRY((\inst1|PSUM[15][6]~regout\ & ((!\inst1|PSUM[15][5]~9COUT1_325\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	dataa => \inst1|PSUM[15][6]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[15][4]~11\,
	cin0 => \inst1|PSUM[15][5]~9\,
	cin1 => \inst1|PSUM[15][5]~9COUT1_325\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][6]~regout\,
	cout0 => \inst1|PSUM[15][6]~7\,
	cout1 => \inst1|PSUM[15][6]~7COUT1_326\);

-- Location: LC_X10_Y8_N7
\inst1|PSUM[15][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][7]~regout\ = DFFEAS((\inst1|PSUM[15][7]~regout\ $ (((!\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][6]~7\) # (\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][6]~7COUT1_326\)))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][7]~5\ = CARRY(((!\inst1|PSUM[15][6]~7\) # (!\inst1|PSUM[15][7]~regout\)))
-- \inst1|PSUM[15][7]~5COUT1_327\ = CARRY(((!\inst1|PSUM[15][6]~7COUT1_326\) # (!\inst1|PSUM[15][7]~regout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	datab => \inst1|PSUM[15][7]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[15][4]~11\,
	cin0 => \inst1|PSUM[15][6]~7\,
	cin1 => \inst1|PSUM[15][6]~7COUT1_326\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][7]~regout\,
	cout0 => \inst1|PSUM[15][7]~5\,
	cout1 => \inst1|PSUM[15][7]~5COUT1_327\);

-- Location: LC_X10_Y8_N8
\inst1|PSUM[15][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][8]~regout\ = DFFEAS(\inst1|PSUM[15][8]~regout\ $ ((((!(!\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][7]~5\) # (\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][7]~5COUT1_327\))))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )
-- \inst1|PSUM[15][8]~3\ = CARRY((\inst1|PSUM[15][8]~regout\ & ((!\inst1|PSUM[15][7]~5\))))
-- \inst1|PSUM[15][8]~3COUT1_328\ = CARRY((\inst1|PSUM[15][8]~regout\ & ((!\inst1|PSUM[15][7]~5COUT1_327\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	dataa => \inst1|PSUM[15][8]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[15][4]~11\,
	cin0 => \inst1|PSUM[15][7]~5\,
	cin1 => \inst1|PSUM[15][7]~5COUT1_327\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][8]~regout\,
	cout0 => \inst1|PSUM[15][8]~3\,
	cout1 => \inst1|PSUM[15][8]~3COUT1_328\);

-- Location: LC_X10_Y8_N9
\inst1|PSUM[15][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|PSUM[15][9]~regout\ = DFFEAS((((!\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][8]~3\) # (\inst1|PSUM[15][4]~11\ & \inst1|PSUM[15][8]~3COUT1_328\) $ (\inst1|PSUM[15][9]~regout\))), \inst6|STEMP\(15), VCC, , , , , \inst7|DIVD~regout\, )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "0ff0",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "on")
-- pragma translate_on
PORT MAP (
	clk => \inst6|STEMP\(15),
	datad => \inst1|PSUM[15][9]~regout\,
	aclr => GND,
	sclr => \inst7|DIVD~regout\,
	cin => \inst1|PSUM[15][4]~11\,
	cin0 => \inst1|PSUM[15][8]~3\,
	cin1 => \inst1|PSUM[15][8]~3COUT1_328\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \inst1|PSUM[15][9]~regout\);

-- Location: LC_X13_Y10_N5
\inst1|TSUM[13][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][9]~combout\ = LCELL((((\inst1|Add13~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][9]~combout\);

-- Location: LC_X13_Y8_N3
\inst1|TSUM[13][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][4]~combout\ = LCELL((((\inst1|Add13~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][4]~combout\);

-- Location: LC_X13_Y8_N4
\inst1|TSUM[13][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][3]~combout\ = LCELL((((\inst1|Add13~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][3]~combout\);

-- Location: LC_X12_Y9_N2
\inst1|TSUM[13][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][2]~combout\ = LCELL((((\inst1|Add13~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][2]~combout\);

-- Location: LC_X12_Y9_N4
\inst1|TSUM[13][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][1]~combout\ = LCELL((((\inst1|Add13~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][1]~combout\);

-- Location: LC_X12_Y9_N0
\inst1|TSUM[13][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][0]~combout\ = LCELL((((\inst1|Add13~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][0]~combout\);

-- Location: LC_X12_Y9_N5
\inst1|Add14~150\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~150_combout\ = \inst1|PSUM[15][0]~regout\ $ ((\inst1|TSUM[13][0]~combout\))
-- \inst1|Add14~152\ = CARRY((\inst1|PSUM[15][0]~regout\ & (\inst1|TSUM[13][0]~combout\)))
-- \inst1|Add14~152COUT1_156\ = CARRY((\inst1|PSUM[15][0]~regout\ & (\inst1|TSUM[13][0]~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[15][0]~regout\,
	datab => \inst1|TSUM[13][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~150_combout\,
	cout0 => \inst1|Add14~152\,
	cout1 => \inst1|Add14~152COUT1_156\);

-- Location: LC_X12_Y9_N6
\inst1|Add14~145\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~145_combout\ = \inst1|TSUM[13][1]~combout\ $ (\inst1|PSUM[15][1]~regout\ $ ((\inst1|Add14~152\)))
-- \inst1|Add14~147\ = CARRY((\inst1|TSUM[13][1]~combout\ & (!\inst1|PSUM[15][1]~regout\ & !\inst1|Add14~152\)) # (!\inst1|TSUM[13][1]~combout\ & ((!\inst1|Add14~152\) # (!\inst1|PSUM[15][1]~regout\))))
-- \inst1|Add14~147COUT1_157\ = CARRY((\inst1|TSUM[13][1]~combout\ & (!\inst1|PSUM[15][1]~regout\ & !\inst1|Add14~152COUT1_156\)) # (!\inst1|TSUM[13][1]~combout\ & ((!\inst1|Add14~152COUT1_156\) # (!\inst1|PSUM[15][1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][1]~combout\,
	datab => \inst1|PSUM[15][1]~regout\,
	cin0 => \inst1|Add14~152\,
	cin1 => \inst1|Add14~152COUT1_156\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~145_combout\,
	cout0 => \inst1|Add14~147\,
	cout1 => \inst1|Add14~147COUT1_157\);

-- Location: LC_X12_Y9_N7
\inst1|Add14~140\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~140_combout\ = \inst1|PSUM[15][2]~regout\ $ (\inst1|TSUM[13][2]~combout\ $ ((!\inst1|Add14~147\)))
-- \inst1|Add14~142\ = CARRY((\inst1|PSUM[15][2]~regout\ & ((\inst1|TSUM[13][2]~combout\) # (!\inst1|Add14~147\))) # (!\inst1|PSUM[15][2]~regout\ & (\inst1|TSUM[13][2]~combout\ & !\inst1|Add14~147\)))
-- \inst1|Add14~142COUT1_158\ = CARRY((\inst1|PSUM[15][2]~regout\ & ((\inst1|TSUM[13][2]~combout\) # (!\inst1|Add14~147COUT1_157\))) # (!\inst1|PSUM[15][2]~regout\ & (\inst1|TSUM[13][2]~combout\ & !\inst1|Add14~147COUT1_157\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[15][2]~regout\,
	datab => \inst1|TSUM[13][2]~combout\,
	cin0 => \inst1|Add14~147\,
	cin1 => \inst1|Add14~147COUT1_157\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~140_combout\,
	cout0 => \inst1|Add14~142\,
	cout1 => \inst1|Add14~142COUT1_158\);

-- Location: LC_X12_Y9_N8
\inst1|Add14~135\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~135_combout\ = \inst1|TSUM[13][3]~combout\ $ (\inst1|PSUM[15][3]~regout\ $ ((\inst1|Add14~142\)))
-- \inst1|Add14~137\ = CARRY((\inst1|TSUM[13][3]~combout\ & (!\inst1|PSUM[15][3]~regout\ & !\inst1|Add14~142\)) # (!\inst1|TSUM[13][3]~combout\ & ((!\inst1|Add14~142\) # (!\inst1|PSUM[15][3]~regout\))))
-- \inst1|Add14~137COUT1_159\ = CARRY((\inst1|TSUM[13][3]~combout\ & (!\inst1|PSUM[15][3]~regout\ & !\inst1|Add14~142COUT1_158\)) # (!\inst1|TSUM[13][3]~combout\ & ((!\inst1|Add14~142COUT1_158\) # (!\inst1|PSUM[15][3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][3]~combout\,
	datab => \inst1|PSUM[15][3]~regout\,
	cin0 => \inst1|Add14~142\,
	cin1 => \inst1|Add14~142COUT1_158\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~135_combout\,
	cout0 => \inst1|Add14~137\,
	cout1 => \inst1|Add14~137COUT1_159\);

-- Location: LC_X12_Y9_N9
\inst1|Add14~130\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~130_combout\ = \inst1|PSUM[15][4]~regout\ $ (\inst1|TSUM[13][4]~combout\ $ ((!\inst1|Add14~137\)))
-- \inst1|Add14~132\ = CARRY((\inst1|PSUM[15][4]~regout\ & ((\inst1|TSUM[13][4]~combout\) # (!\inst1|Add14~137COUT1_159\))) # (!\inst1|PSUM[15][4]~regout\ & (\inst1|TSUM[13][4]~combout\ & !\inst1|Add14~137COUT1_159\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[15][4]~regout\,
	datab => \inst1|TSUM[13][4]~combout\,
	cin0 => \inst1|Add14~137\,
	cin1 => \inst1|Add14~137COUT1_159\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~130_combout\,
	cout => \inst1|Add14~132\);

-- Location: LC_X13_Y10_N8
\inst1|TSUM[13][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][8]~combout\ = LCELL((((\inst1|Add13~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][8]~combout\);

-- Location: LC_X13_Y10_N7
\inst1|TSUM[13][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][7]~combout\ = LCELL((((\inst1|Add13~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][7]~combout\);

-- Location: LC_X13_Y10_N1
\inst1|TSUM[13][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][6]~combout\ = LCELL((((\inst1|Add13~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][6]~combout\);

-- Location: LC_X13_Y10_N3
\inst1|TSUM[13][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][5]~combout\ = LCELL((((\inst1|Add13~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][5]~combout\);

-- Location: LC_X13_Y9_N0
\inst1|Add14~125\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~125_combout\ = \inst1|PSUM[15][5]~regout\ $ (\inst1|TSUM[13][5]~combout\ $ ((\inst1|Add14~132\)))
-- \inst1|Add14~127\ = CARRY((\inst1|PSUM[15][5]~regout\ & (!\inst1|TSUM[13][5]~combout\ & !\inst1|Add14~132\)) # (!\inst1|PSUM[15][5]~regout\ & ((!\inst1|Add14~132\) # (!\inst1|TSUM[13][5]~combout\))))
-- \inst1|Add14~127COUT1_160\ = CARRY((\inst1|PSUM[15][5]~regout\ & (!\inst1|TSUM[13][5]~combout\ & !\inst1|Add14~132\)) # (!\inst1|PSUM[15][5]~regout\ & ((!\inst1|Add14~132\) # (!\inst1|TSUM[13][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[15][5]~regout\,
	datab => \inst1|TSUM[13][5]~combout\,
	cin => \inst1|Add14~132\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~125_combout\,
	cout0 => \inst1|Add14~127\,
	cout1 => \inst1|Add14~127COUT1_160\);

-- Location: LC_X13_Y9_N1
\inst1|Add14~120\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~120_combout\ = \inst1|TSUM[13][6]~combout\ $ (\inst1|PSUM[15][6]~regout\ $ ((!(!\inst1|Add14~132\ & \inst1|Add14~127\) # (\inst1|Add14~132\ & \inst1|Add14~127COUT1_160\))))
-- \inst1|Add14~122\ = CARRY((\inst1|TSUM[13][6]~combout\ & ((\inst1|PSUM[15][6]~regout\) # (!\inst1|Add14~127\))) # (!\inst1|TSUM[13][6]~combout\ & (\inst1|PSUM[15][6]~regout\ & !\inst1|Add14~127\)))
-- \inst1|Add14~122COUT1_161\ = CARRY((\inst1|TSUM[13][6]~combout\ & ((\inst1|PSUM[15][6]~regout\) # (!\inst1|Add14~127COUT1_160\))) # (!\inst1|TSUM[13][6]~combout\ & (\inst1|PSUM[15][6]~regout\ & !\inst1|Add14~127COUT1_160\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][6]~combout\,
	datab => \inst1|PSUM[15][6]~regout\,
	cin => \inst1|Add14~132\,
	cin0 => \inst1|Add14~127\,
	cin1 => \inst1|Add14~127COUT1_160\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~120_combout\,
	cout0 => \inst1|Add14~122\,
	cout1 => \inst1|Add14~122COUT1_161\);

-- Location: LC_X13_Y9_N2
\inst1|Add14~115\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~115_combout\ = \inst1|TSUM[13][7]~combout\ $ (\inst1|PSUM[15][7]~regout\ $ (((!\inst1|Add14~132\ & \inst1|Add14~122\) # (\inst1|Add14~132\ & \inst1|Add14~122COUT1_161\))))
-- \inst1|Add14~117\ = CARRY((\inst1|TSUM[13][7]~combout\ & (!\inst1|PSUM[15][7]~regout\ & !\inst1|Add14~122\)) # (!\inst1|TSUM[13][7]~combout\ & ((!\inst1|Add14~122\) # (!\inst1|PSUM[15][7]~regout\))))
-- \inst1|Add14~117COUT1_162\ = CARRY((\inst1|TSUM[13][7]~combout\ & (!\inst1|PSUM[15][7]~regout\ & !\inst1|Add14~122COUT1_161\)) # (!\inst1|TSUM[13][7]~combout\ & ((!\inst1|Add14~122COUT1_161\) # (!\inst1|PSUM[15][7]~regout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][7]~combout\,
	datab => \inst1|PSUM[15][7]~regout\,
	cin => \inst1|Add14~132\,
	cin0 => \inst1|Add14~122\,
	cin1 => \inst1|Add14~122COUT1_161\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~115_combout\,
	cout0 => \inst1|Add14~117\,
	cout1 => \inst1|Add14~117COUT1_162\);

-- Location: LC_X13_Y9_N3
\inst1|Add14~110\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~110_combout\ = \inst1|PSUM[15][8]~regout\ $ (\inst1|TSUM[13][8]~combout\ $ ((!(!\inst1|Add14~132\ & \inst1|Add14~117\) # (\inst1|Add14~132\ & \inst1|Add14~117COUT1_162\))))
-- \inst1|Add14~112\ = CARRY((\inst1|PSUM[15][8]~regout\ & ((\inst1|TSUM[13][8]~combout\) # (!\inst1|Add14~117\))) # (!\inst1|PSUM[15][8]~regout\ & (\inst1|TSUM[13][8]~combout\ & !\inst1|Add14~117\)))
-- \inst1|Add14~112COUT1_163\ = CARRY((\inst1|PSUM[15][8]~regout\ & ((\inst1|TSUM[13][8]~combout\) # (!\inst1|Add14~117COUT1_162\))) # (!\inst1|PSUM[15][8]~regout\ & (\inst1|TSUM[13][8]~combout\ & !\inst1|Add14~117COUT1_162\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[15][8]~regout\,
	datab => \inst1|TSUM[13][8]~combout\,
	cin => \inst1|Add14~132\,
	cin0 => \inst1|Add14~117\,
	cin1 => \inst1|Add14~117COUT1_162\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~110_combout\,
	cout0 => \inst1|Add14~112\,
	cout1 => \inst1|Add14~112COUT1_163\);

-- Location: LC_X13_Y9_N4
\inst1|Add14~105\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~105_combout\ = \inst1|PSUM[15][9]~regout\ $ (\inst1|TSUM[13][9]~combout\ $ (((!\inst1|Add14~132\ & \inst1|Add14~112\) # (\inst1|Add14~132\ & \inst1|Add14~112COUT1_163\))))
-- \inst1|Add14~107\ = CARRY((\inst1|PSUM[15][9]~regout\ & (!\inst1|TSUM[13][9]~combout\ & !\inst1|Add14~112COUT1_163\)) # (!\inst1|PSUM[15][9]~regout\ & ((!\inst1|Add14~112COUT1_163\) # (!\inst1|TSUM[13][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|PSUM[15][9]~regout\,
	datab => \inst1|TSUM[13][9]~combout\,
	cin => \inst1|Add14~132\,
	cin0 => \inst1|Add14~112\,
	cin1 => \inst1|Add14~112COUT1_163\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~105_combout\,
	cout => \inst1|Add14~107\);

-- Location: LC_X13_Y10_N9
\inst1|TSUM[13][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][13]~combout\ = LCELL((((\inst1|Add13~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][13]~combout\);

-- Location: LC_X13_Y8_N0
\inst1|TSUM[13][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][12]~combout\ = LCELL((((\inst1|Add13~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][12]~combout\);

-- Location: LC_X13_Y10_N2
\inst1|TSUM[13][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][11]~combout\ = LCELL((((\inst1|Add13~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][11]~combout\);

-- Location: LC_X13_Y10_N0
\inst1|TSUM[13][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][10]~combout\ = LCELL((((\inst1|Add13~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][10]~combout\);

-- Location: LC_X13_Y9_N5
\inst1|Add14~100\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~100_combout\ = \inst1|TSUM[13][10]~combout\ $ ((((!\inst1|Add14~107\))))
-- \inst1|Add14~102\ = CARRY((\inst1|TSUM[13][10]~combout\ & ((!\inst1|Add14~107\))))
-- \inst1|Add14~102COUT1_164\ = CARRY((\inst1|TSUM[13][10]~combout\ & ((!\inst1|Add14~107\))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][10]~combout\,
	cin => \inst1|Add14~107\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~100_combout\,
	cout0 => \inst1|Add14~102\,
	cout1 => \inst1|Add14~102COUT1_164\);

-- Location: LC_X13_Y9_N6
\inst1|Add14~95\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~95_combout\ = \inst1|TSUM[13][11]~combout\ $ (((((!\inst1|Add14~107\ & \inst1|Add14~102\) # (\inst1|Add14~107\ & \inst1|Add14~102COUT1_164\)))))
-- \inst1|Add14~97\ = CARRY(((!\inst1|Add14~102\)) # (!\inst1|TSUM[13][11]~combout\))
-- \inst1|Add14~97COUT1_165\ = CARRY(((!\inst1|Add14~102COUT1_164\)) # (!\inst1|TSUM[13][11]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][11]~combout\,
	cin => \inst1|Add14~107\,
	cin0 => \inst1|Add14~102\,
	cin1 => \inst1|Add14~102COUT1_164\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~95_combout\,
	cout0 => \inst1|Add14~97\,
	cout1 => \inst1|Add14~97COUT1_165\);

-- Location: LC_X13_Y9_N7
\inst1|Add14~90\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~90_combout\ = (\inst1|TSUM[13][12]~combout\ $ ((!(!\inst1|Add14~107\ & \inst1|Add14~97\) # (\inst1|Add14~107\ & \inst1|Add14~97COUT1_165\))))
-- \inst1|Add14~92\ = CARRY(((\inst1|TSUM[13][12]~combout\ & !\inst1|Add14~97\)))
-- \inst1|Add14~92COUT1_166\ = CARRY(((\inst1|TSUM[13][12]~combout\ & !\inst1|Add14~97COUT1_165\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][12]~combout\,
	cin => \inst1|Add14~107\,
	cin0 => \inst1|Add14~97\,
	cin1 => \inst1|Add14~97COUT1_165\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~90_combout\,
	cout0 => \inst1|Add14~92\,
	cout1 => \inst1|Add14~92COUT1_166\);

-- Location: LC_X13_Y9_N8
\inst1|Add14~85\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~85_combout\ = (\inst1|TSUM[13][13]~combout\ $ (((!\inst1|Add14~107\ & \inst1|Add14~92\) # (\inst1|Add14~107\ & \inst1|Add14~92COUT1_166\))))
-- \inst1|Add14~87\ = CARRY(((!\inst1|Add14~92\) # (!\inst1|TSUM[13][13]~combout\)))
-- \inst1|Add14~87COUT1_167\ = CARRY(((!\inst1|Add14~92COUT1_166\) # (!\inst1|TSUM[13][13]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][13]~combout\,
	cin => \inst1|Add14~107\,
	cin0 => \inst1|Add14~92\,
	cin1 => \inst1|Add14~92COUT1_166\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~85_combout\,
	cout0 => \inst1|Add14~87\,
	cout1 => \inst1|Add14~87COUT1_167\);

-- Location: LC_X13_Y9_N9
\inst1|Add14~80\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~80_combout\ = (\inst1|TSUM[13][14]~combout\ $ ((!(!\inst1|Add14~107\ & \inst1|Add14~87\) # (\inst1|Add14~107\ & \inst1|Add14~87COUT1_167\))))
-- \inst1|Add14~82\ = CARRY(((\inst1|TSUM[13][14]~combout\ & !\inst1|Add14~87COUT1_167\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][14]~combout\,
	cin => \inst1|Add14~107\,
	cin0 => \inst1|Add14~87\,
	cin1 => \inst1|Add14~87COUT1_167\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~80_combout\,
	cout => \inst1|Add14~82\);

-- Location: LC_X15_Y10_N0
\inst1|TSUM[13][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][18]~combout\ = LCELL((((\inst1|Add13~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][18]~combout\);

-- Location: LC_X15_Y10_N5
\inst1|TSUM[13][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][17]~combout\ = LCELL((((\inst1|Add13~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][17]~combout\);

-- Location: LC_X15_Y10_N4
\inst1|TSUM[13][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][16]~combout\ = LCELL((((\inst1|Add13~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][16]~combout\);

-- Location: LC_X15_Y10_N3
\inst1|TSUM[13][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][15]~combout\ = LCELL((((\inst1|Add13~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][15]~combout\);

-- Location: LC_X14_Y9_N0
\inst1|Add14~75\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~75_combout\ = (\inst1|TSUM[13][15]~combout\ $ ((\inst1|Add14~82\)))
-- \inst1|Add14~77\ = CARRY(((!\inst1|Add14~82\) # (!\inst1|TSUM[13][15]~combout\)))
-- \inst1|Add14~77COUT1_168\ = CARRY(((!\inst1|Add14~82\) # (!\inst1|TSUM[13][15]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][15]~combout\,
	cin => \inst1|Add14~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~75_combout\,
	cout0 => \inst1|Add14~77\,
	cout1 => \inst1|Add14~77COUT1_168\);

-- Location: LC_X14_Y9_N1
\inst1|Add14~70\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~70_combout\ = (\inst1|TSUM[13][16]~combout\ $ ((!(!\inst1|Add14~82\ & \inst1|Add14~77\) # (\inst1|Add14~82\ & \inst1|Add14~77COUT1_168\))))
-- \inst1|Add14~72\ = CARRY(((\inst1|TSUM[13][16]~combout\ & !\inst1|Add14~77\)))
-- \inst1|Add14~72COUT1_169\ = CARRY(((\inst1|TSUM[13][16]~combout\ & !\inst1|Add14~77COUT1_168\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][16]~combout\,
	cin => \inst1|Add14~82\,
	cin0 => \inst1|Add14~77\,
	cin1 => \inst1|Add14~77COUT1_168\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~70_combout\,
	cout0 => \inst1|Add14~72\,
	cout1 => \inst1|Add14~72COUT1_169\);

-- Location: LC_X14_Y9_N2
\inst1|Add14~65\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~65_combout\ = (\inst1|TSUM[13][17]~combout\ $ (((!\inst1|Add14~82\ & \inst1|Add14~72\) # (\inst1|Add14~82\ & \inst1|Add14~72COUT1_169\))))
-- \inst1|Add14~67\ = CARRY(((!\inst1|Add14~72\) # (!\inst1|TSUM[13][17]~combout\)))
-- \inst1|Add14~67COUT1_170\ = CARRY(((!\inst1|Add14~72COUT1_169\) # (!\inst1|TSUM[13][17]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][17]~combout\,
	cin => \inst1|Add14~82\,
	cin0 => \inst1|Add14~72\,
	cin1 => \inst1|Add14~72COUT1_169\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~65_combout\,
	cout0 => \inst1|Add14~67\,
	cout1 => \inst1|Add14~67COUT1_170\);

-- Location: LC_X14_Y9_N3
\inst1|Add14~60\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~60_combout\ = \inst1|TSUM[13][18]~combout\ $ ((((!(!\inst1|Add14~82\ & \inst1|Add14~67\) # (\inst1|Add14~82\ & \inst1|Add14~67COUT1_170\)))))
-- \inst1|Add14~62\ = CARRY((\inst1|TSUM[13][18]~combout\ & ((!\inst1|Add14~67\))))
-- \inst1|Add14~62COUT1_171\ = CARRY((\inst1|TSUM[13][18]~combout\ & ((!\inst1|Add14~67COUT1_170\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][18]~combout\,
	cin => \inst1|Add14~82\,
	cin0 => \inst1|Add14~67\,
	cin1 => \inst1|Add14~67COUT1_170\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~60_combout\,
	cout0 => \inst1|Add14~62\,
	cout1 => \inst1|Add14~62COUT1_171\);

-- Location: LC_X14_Y9_N4
\inst1|Add14~55\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~55_combout\ = \inst1|TSUM[13][19]~combout\ $ (((((!\inst1|Add14~82\ & \inst1|Add14~62\) # (\inst1|Add14~82\ & \inst1|Add14~62COUT1_171\)))))
-- \inst1|Add14~57\ = CARRY(((!\inst1|Add14~62COUT1_171\)) # (!\inst1|TSUM[13][19]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][19]~combout\,
	cin => \inst1|Add14~82\,
	cin0 => \inst1|Add14~62\,
	cin1 => \inst1|Add14~62COUT1_171\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~55_combout\,
	cout => \inst1|Add14~57\);

-- Location: LC_X15_Y10_N9
\inst1|TSUM[13][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][23]~combout\ = LCELL((((\inst1|Add13~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][23]~combout\);

-- Location: LC_X15_Y10_N1
\inst1|TSUM[13][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][22]~combout\ = LCELL((((\inst1|Add13~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][22]~combout\);

-- Location: LC_X15_Y10_N2
\inst1|TSUM[13][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][21]~combout\ = LCELL((((\inst1|Add13~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][21]~combout\);

-- Location: LC_X15_Y10_N8
\inst1|TSUM[13][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][20]~combout\ = LCELL((((\inst1|Add13~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][20]~combout\);

-- Location: LC_X14_Y9_N5
\inst1|Add14~50\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~50_combout\ = (\inst1|TSUM[13][20]~combout\ $ ((!\inst1|Add14~57\)))
-- \inst1|Add14~52\ = CARRY(((\inst1|TSUM[13][20]~combout\ & !\inst1|Add14~57\)))
-- \inst1|Add14~52COUT1_172\ = CARRY(((\inst1|TSUM[13][20]~combout\ & !\inst1|Add14~57\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][20]~combout\,
	cin => \inst1|Add14~57\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~50_combout\,
	cout0 => \inst1|Add14~52\,
	cout1 => \inst1|Add14~52COUT1_172\);

-- Location: LC_X14_Y9_N6
\inst1|Add14~45\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~45_combout\ = \inst1|TSUM[13][21]~combout\ $ (((((!\inst1|Add14~57\ & \inst1|Add14~52\) # (\inst1|Add14~57\ & \inst1|Add14~52COUT1_172\)))))
-- \inst1|Add14~47\ = CARRY(((!\inst1|Add14~52\)) # (!\inst1|TSUM[13][21]~combout\))
-- \inst1|Add14~47COUT1_173\ = CARRY(((!\inst1|Add14~52COUT1_172\)) # (!\inst1|TSUM[13][21]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][21]~combout\,
	cin => \inst1|Add14~57\,
	cin0 => \inst1|Add14~52\,
	cin1 => \inst1|Add14~52COUT1_172\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~45_combout\,
	cout0 => \inst1|Add14~47\,
	cout1 => \inst1|Add14~47COUT1_173\);

-- Location: LC_X14_Y9_N7
\inst1|Add14~40\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~40_combout\ = (\inst1|TSUM[13][22]~combout\ $ ((!(!\inst1|Add14~57\ & \inst1|Add14~47\) # (\inst1|Add14~57\ & \inst1|Add14~47COUT1_173\))))
-- \inst1|Add14~42\ = CARRY(((\inst1|TSUM[13][22]~combout\ & !\inst1|Add14~47\)))
-- \inst1|Add14~42COUT1_174\ = CARRY(((\inst1|TSUM[13][22]~combout\ & !\inst1|Add14~47COUT1_173\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][22]~combout\,
	cin => \inst1|Add14~57\,
	cin0 => \inst1|Add14~47\,
	cin1 => \inst1|Add14~47COUT1_173\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~40_combout\,
	cout0 => \inst1|Add14~42\,
	cout1 => \inst1|Add14~42COUT1_174\);

-- Location: LC_X14_Y9_N8
\inst1|Add14~35\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~35_combout\ = (\inst1|TSUM[13][23]~combout\ $ (((!\inst1|Add14~57\ & \inst1|Add14~42\) # (\inst1|Add14~57\ & \inst1|Add14~42COUT1_174\))))
-- \inst1|Add14~37\ = CARRY(((!\inst1|Add14~42\) # (!\inst1|TSUM[13][23]~combout\)))
-- \inst1|Add14~37COUT1_175\ = CARRY(((!\inst1|Add14~42COUT1_174\) # (!\inst1|TSUM[13][23]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][23]~combout\,
	cin => \inst1|Add14~57\,
	cin0 => \inst1|Add14~42\,
	cin1 => \inst1|Add14~42COUT1_174\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~35_combout\,
	cout0 => \inst1|Add14~37\,
	cout1 => \inst1|Add14~37COUT1_175\);

-- Location: LC_X14_Y9_N9
\inst1|Add14~30\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~30_combout\ = (\inst1|TSUM[13][24]~combout\ $ ((!(!\inst1|Add14~57\ & \inst1|Add14~37\) # (\inst1|Add14~57\ & \inst1|Add14~37COUT1_175\))))
-- \inst1|Add14~32\ = CARRY(((\inst1|TSUM[13][24]~combout\ & !\inst1|Add14~37COUT1_175\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c30c",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][24]~combout\,
	cin => \inst1|Add14~57\,
	cin0 => \inst1|Add14~37\,
	cin1 => \inst1|Add14~37COUT1_175\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~30_combout\,
	cout => \inst1|Add14~32\);

-- Location: LC_X15_Y9_N8
\inst1|TSUM[13][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][28]~combout\ = LCELL((((\inst1|Add13~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][28]~combout\);

-- Location: LC_X15_Y9_N9
\inst1|TSUM[13][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][27]~combout\ = LCELL((((\inst1|Add13~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add13~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][27]~combout\);

-- Location: LC_X16_Y8_N7
\inst1|TSUM[13][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][26]~combout\ = LCELL((((\inst1|Add13~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][26]~combout\);

-- Location: LC_X15_Y9_N7
\inst1|TSUM[13][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[13][25]~combout\ = LCELL((((\inst1|Add13~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add13~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[13][25]~combout\);

-- Location: LC_X15_Y9_N0
\inst1|Add14~25\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~25_combout\ = (\inst1|TSUM[13][25]~combout\ $ ((\inst1|Add14~32\)))
-- \inst1|Add14~27\ = CARRY(((!\inst1|Add14~32\) # (!\inst1|TSUM[13][25]~combout\)))
-- \inst1|Add14~27COUT1_176\ = CARRY(((!\inst1|Add14~32\) # (!\inst1|TSUM[13][25]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][25]~combout\,
	cin => \inst1|Add14~32\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~25_combout\,
	cout0 => \inst1|Add14~27\,
	cout1 => \inst1|Add14~27COUT1_176\);

-- Location: LC_X15_Y9_N1
\inst1|Add14~20\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~20_combout\ = \inst1|TSUM[13][26]~combout\ $ ((((!(!\inst1|Add14~32\ & \inst1|Add14~27\) # (\inst1|Add14~32\ & \inst1|Add14~27COUT1_176\)))))
-- \inst1|Add14~22\ = CARRY((\inst1|TSUM[13][26]~combout\ & ((!\inst1|Add14~27\))))
-- \inst1|Add14~22COUT1_177\ = CARRY((\inst1|TSUM[13][26]~combout\ & ((!\inst1|Add14~27COUT1_176\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][26]~combout\,
	cin => \inst1|Add14~32\,
	cin0 => \inst1|Add14~27\,
	cin1 => \inst1|Add14~27COUT1_176\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~20_combout\,
	cout0 => \inst1|Add14~22\,
	cout1 => \inst1|Add14~22COUT1_177\);

-- Location: LC_X15_Y9_N2
\inst1|Add14~15\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~15_combout\ = (\inst1|TSUM[13][27]~combout\ $ (((!\inst1|Add14~32\ & \inst1|Add14~22\) # (\inst1|Add14~32\ & \inst1|Add14~22COUT1_177\))))
-- \inst1|Add14~17\ = CARRY(((!\inst1|Add14~22\) # (!\inst1|TSUM[13][27]~combout\)))
-- \inst1|Add14~17COUT1_178\ = CARRY(((!\inst1|Add14~22COUT1_177\) # (!\inst1|TSUM[13][27]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "3c3f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst1|TSUM[13][27]~combout\,
	cin => \inst1|Add14~32\,
	cin0 => \inst1|Add14~22\,
	cin1 => \inst1|Add14~22COUT1_177\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~15_combout\,
	cout0 => \inst1|Add14~17\,
	cout1 => \inst1|Add14~17COUT1_178\);

-- Location: LC_X15_Y9_N3
\inst1|Add14~10\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~10_combout\ = \inst1|TSUM[13][28]~combout\ $ ((((!(!\inst1|Add14~32\ & \inst1|Add14~17\) # (\inst1|Add14~32\ & \inst1|Add14~17COUT1_178\)))))
-- \inst1|Add14~12\ = CARRY((\inst1|TSUM[13][28]~combout\ & ((!\inst1|Add14~17\))))
-- \inst1|Add14~12COUT1_179\ = CARRY((\inst1|TSUM[13][28]~combout\ & ((!\inst1|Add14~17COUT1_178\))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "a50a",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][28]~combout\,
	cin => \inst1|Add14~32\,
	cin0 => \inst1|Add14~17\,
	cin1 => \inst1|Add14~17COUT1_178\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~10_combout\,
	cout0 => \inst1|Add14~12\,
	cout1 => \inst1|Add14~12COUT1_179\);

-- Location: LC_X15_Y9_N4
\inst1|Add14~5\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~5_combout\ = \inst1|TSUM[13][29]~combout\ $ (((((!\inst1|Add14~32\ & \inst1|Add14~12\) # (\inst1|Add14~32\ & \inst1|Add14~12COUT1_179\)))))
-- \inst1|Add14~7\ = CARRY(((!\inst1|Add14~12COUT1_179\)) # (!\inst1|TSUM[13][29]~combout\))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "5a5f",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|TSUM[13][29]~combout\,
	cin => \inst1|Add14~32\,
	cin0 => \inst1|Add14~12\,
	cin1 => \inst1|Add14~12COUT1_179\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~5_combout\,
	cout => \inst1|Add14~7\);

-- Location: LC_X15_Y9_N5
\inst1|Add14~0\ : maxii_lcell
-- Equation(s):
-- \inst1|Add14~0_combout\ = ((\inst1|Add14~7\ $ (!\inst1|TSUM[13][30]~combout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "f00f",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[13][30]~combout\,
	cin => \inst1|Add14~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|Add14~0_combout\);

-- Location: LC_X16_Y7_N4
\inst1|TSUM[14][30]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][30]~combout\ = LCELL((((\inst1|Add14~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][30]~combout\);

-- Location: LC_X16_Y7_N5
\inst1|SUM[30]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(30) = LCELL((((\inst1|TSUM[14][30]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][30]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(30));

-- Location: LC_X16_Y5_N7
\inst1|TSUM[14][29]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][29]~combout\ = LCELL((((\inst1|Add14~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add14~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][29]~combout\);

-- Location: LC_X16_Y5_N8
\inst1|SUM[29]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(29) = LCELL((((\inst1|TSUM[14][29]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][29]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(29));

-- Location: LC_X16_Y10_N5
\inst1|TSUM[14][28]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][28]~combout\ = LCELL((((\inst1|Add14~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][28]~combout\);

-- Location: LC_X16_Y10_N1
\inst1|SUM[28]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(28) = LCELL((((\inst1|TSUM[14][28]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|TSUM[14][28]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(28));

-- Location: LC_X16_Y10_N4
\inst1|TSUM[14][27]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][27]~combout\ = LCELL((((\inst1|Add14~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][27]~combout\);

-- Location: LC_X16_Y10_N2
\inst1|SUM[27]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(27) = LCELL((((\inst1|TSUM[14][27]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|TSUM[14][27]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(27));

-- Location: LC_X16_Y5_N9
\inst1|TSUM[14][26]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][26]~combout\ = LCELL((((\inst1|Add14~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][26]~combout\);

-- Location: LC_X16_Y5_N4
\inst1|SUM[26]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(26) = LCELL((((\inst1|TSUM[14][26]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][26]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(26));

-- Location: LC_X16_Y5_N2
\inst1|TSUM[14][25]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][25]~combout\ = LCELL((((\inst1|Add14~25_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][25]~combout\);

-- Location: LC_X16_Y5_N3
\inst1|SUM[25]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(25) = LCELL((((\inst1|TSUM[14][25]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][25]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(25));

-- Location: LC_X12_Y10_N1
\inst1|TSUM[14][24]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][24]~combout\ = LCELL((((\inst1|Add14~30_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add14~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][24]~combout\);

-- Location: LC_X12_Y10_N5
\inst1|SUM[24]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(24) = LCELL((((\inst1|TSUM[14][24]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][24]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(24));

-- Location: LC_X12_Y10_N8
\inst1|TSUM[14][23]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][23]~combout\ = LCELL((((\inst1|Add14~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][23]~combout\);

-- Location: LC_X12_Y10_N6
\inst1|SUM[23]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(23) = LCELL((((\inst1|TSUM[14][23]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][23]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(23));

-- Location: LC_X12_Y10_N7
\inst1|TSUM[14][22]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][22]~combout\ = LCELL((((\inst1|Add14~40_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][22]~combout\);

-- Location: LC_X12_Y10_N0
\inst1|SUM[22]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(22) = LCELL((((\inst1|TSUM[14][22]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|TSUM[14][22]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(22));

-- Location: LC_X12_Y10_N3
\inst1|TSUM[14][21]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][21]~combout\ = LCELL((((\inst1|Add14~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~45_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][21]~combout\);

-- Location: LC_X12_Y10_N4
\inst1|SUM[21]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(21) = LCELL((((\inst1|TSUM[14][21]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][21]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(21));

-- Location: LC_X14_Y10_N1
\inst1|TSUM[14][20]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][20]~combout\ = LCELL((((\inst1|Add14~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][20]~combout\);

-- Location: LC_X14_Y10_N4
\inst1|SUM[20]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(20) = LCELL((((\inst1|TSUM[14][20]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][20]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(20));

-- Location: LC_X14_Y10_N3
\inst1|TSUM[14][19]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][19]~combout\ = LCELL((((\inst1|Add14~55_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~55_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][19]~combout\);

-- Location: LC_X14_Y10_N2
\inst1|SUM[19]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(19) = LCELL((((\inst1|TSUM[14][19]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][19]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(19));

-- Location: LC_X14_Y10_N5
\inst1|TSUM[14][18]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][18]~combout\ = LCELL((((\inst1|Add14~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][18]~combout\);

-- Location: LC_X14_Y10_N6
\inst1|SUM[18]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(18) = LCELL((((\inst1|TSUM[14][18]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][18]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(18));

-- Location: LC_X14_Y10_N9
\inst1|TSUM[14][17]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][17]~combout\ = LCELL((((\inst1|Add14~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~65_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][17]~combout\);

-- Location: LC_X14_Y10_N0
\inst1|SUM[17]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(17) = LCELL((((\inst1|TSUM[14][17]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][17]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(17));

-- Location: LC_X12_Y10_N9
\inst1|TSUM[14][16]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][16]~combout\ = LCELL((((\inst1|Add14~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][16]~combout\);

-- Location: LC_X12_Y10_N2
\inst1|SUM[16]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(16) = LCELL((((\inst1|TSUM[14][16]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][16]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(16));

-- Location: LC_X14_Y10_N8
\inst1|TSUM[14][15]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][15]~combout\ = LCELL((((\inst1|Add14~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~75_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][15]~combout\);

-- Location: LC_X14_Y10_N7
\inst1|SUM[15]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(15) = LCELL((((\inst1|TSUM[14][15]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][15]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(15));

-- Location: LC_X13_Y7_N9
\inst1|TSUM[14][14]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][14]~combout\ = LCELL((((\inst1|Add14~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][14]~combout\);

-- Location: LC_X13_Y7_N5
\inst1|SUM[14]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(14) = LCELL((((\inst1|TSUM[14][14]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][14]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(14));

-- Location: LC_X13_Y7_N0
\inst1|TSUM[14][13]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][13]~combout\ = LCELL((((\inst1|Add14~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~85_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][13]~combout\);

-- Location: LC_X13_Y7_N1
\inst1|SUM[13]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(13) = LCELL((((\inst1|TSUM[14][13]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][13]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(13));

-- Location: LC_X13_Y7_N8
\inst1|TSUM[14][12]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][12]~combout\ = LCELL((((\inst1|Add14~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][12]~combout\);

-- Location: LC_X13_Y7_N7
\inst1|SUM[12]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(12) = LCELL((((\inst1|TSUM[14][12]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][12]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(12));

-- Location: LC_X13_Y7_N4
\inst1|TSUM[14][11]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][11]~combout\ = LCELL((((\inst1|Add14~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~95_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][11]~combout\);

-- Location: LC_X13_Y7_N3
\inst1|SUM[11]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(11) = LCELL((((\inst1|TSUM[14][11]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|TSUM[14][11]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(11));

-- Location: LC_X16_Y9_N4
\inst1|TSUM[14][10]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][10]~combout\ = LCELL((((\inst1|Add14~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~100_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][10]~combout\);

-- Location: LC_X16_Y9_N3
\inst1|SUM[10]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(10) = LCELL((((\inst1|TSUM[14][10]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|TSUM[14][10]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(10));

-- Location: LC_X16_Y9_N2
\inst1|TSUM[14][9]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][9]~combout\ = LCELL((((\inst1|Add14~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~105_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][9]~combout\);

-- Location: LC_X16_Y9_N9
\inst1|SUM[9]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(9) = LCELL((((\inst1|TSUM[14][9]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][9]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(9));

-- Location: LC_X16_Y9_N6
\inst1|TSUM[14][8]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][8]~combout\ = LCELL((((\inst1|Add14~110_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~110_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][8]~combout\);

-- Location: LC_X16_Y9_N7
\inst1|SUM[8]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(8) = LCELL((((\inst1|TSUM[14][8]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][8]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(8));

-- Location: LC_X16_Y9_N8
\inst1|TSUM[14][7]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][7]~combout\ = LCELL((((\inst1|Add14~115_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~115_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][7]~combout\);

-- Location: LC_X16_Y9_N1
\inst1|SUM[7]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(7) = LCELL((((\inst1|TSUM[14][7]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][7]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(7));

-- Location: LC_X16_Y9_N5
\inst1|TSUM[14][6]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][6]~combout\ = LCELL((((\inst1|Add14~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~120_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][6]~combout\);

-- Location: LC_X16_Y9_N0
\inst1|SUM[6]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(6) = LCELL((((\inst1|TSUM[14][6]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|TSUM[14][6]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(6));

-- Location: LC_X13_Y7_N2
\inst1|TSUM[14][5]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][5]~combout\ = LCELL((((\inst1|Add14~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add14~125_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][5]~combout\);

-- Location: LC_X13_Y7_N6
\inst1|SUM[5]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(5) = LCELL((((\inst1|TSUM[14][5]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][5]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(5));

-- Location: LC_X11_Y10_N3
\inst1|TSUM[14][4]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][4]~combout\ = LCELL((((\inst1|Add14~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~130_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][4]~combout\);

-- Location: LC_X11_Y10_N6
\inst1|SUM[4]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(4) = LCELL((((\inst1|TSUM[14][4]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][4]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(4));

-- Location: LC_X12_Y9_N1
\inst1|TSUM[14][3]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][3]~combout\ = LCELL((((\inst1|Add14~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~135_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][3]~combout\);

-- Location: LC_X12_Y9_N3
\inst1|SUM[3]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(3) = LCELL((((\inst1|TSUM[14][3]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][3]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(3));

-- Location: LC_X11_Y10_N9
\inst1|TSUM[14][2]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][2]~combout\ = LCELL((((\inst1|Add14~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add14~140_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][2]~combout\);

-- Location: LC_X11_Y10_N2
\inst1|SUM[2]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(2) = LCELL((((\inst1|TSUM[14][2]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][2]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(2));

-- Location: LC_X11_Y10_N8
\inst1|TSUM[14][1]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][1]~combout\ = LCELL((((\inst1|Add14~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~145_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][1]~combout\);

-- Location: LC_X11_Y10_N1
\inst1|SUM[1]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(1) = LCELL((((\inst1|TSUM[14][1]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|TSUM[14][1]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(1));

-- Location: LC_X11_Y10_N7
\inst1|TSUM[14][0]\ : maxii_lcell
-- Equation(s):
-- \inst1|TSUM[14][0]~combout\ = LCELL((((\inst1|Add14~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff00",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datad => \inst1|Add14~150_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|TSUM[14][0]~combout\);

-- Location: LC_X11_Y10_N5
\inst1|SUM[0]\ : maxii_lcell
-- Equation(s):
-- \inst1|SUM\(0) = LCELL((((\inst1|TSUM[14][0]~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "f0f0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \inst1|TSUM[14][0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \inst1|SUM\(0));

-- Location: PIN_98,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHSSIGN~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst21|TMPNEG~regout\,
	oe => VCC,
	padio => ww_PHSSIGN);

-- Location: PIN_58,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHSSIG~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(15),
	oe => VCC,
	padio => ww_PHSSIG);

-- Location: PIN_95,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[31]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst21|TMPNEG~regout\,
	oe => VCC,
	padio => ww_PHASE(31));

-- Location: PIN_96,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[30]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(30),
	oe => VCC,
	padio => ww_PHASE(30));

-- Location: PIN_86,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[29]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(29),
	oe => VCC,
	padio => ww_PHASE(29));

-- Location: PIN_110,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[28]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(28),
	oe => VCC,
	padio => ww_PHASE(28));

-- Location: PIN_109,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[27]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(27),
	oe => VCC,
	padio => ww_PHASE(27));

-- Location: PIN_88,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[26]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(26),
	oe => VCC,
	padio => ww_PHASE(26));

-- Location: PIN_89,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[25]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(25),
	oe => VCC,
	padio => ww_PHASE(25));

-- Location: PIN_125,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[24]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(24),
	oe => VCC,
	padio => ww_PHASE(24));

-- Location: PIN_129,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[23]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(23),
	oe => VCC,
	padio => ww_PHASE(23));

-- Location: PIN_122,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[22]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(22),
	oe => VCC,
	padio => ww_PHASE(22));

-- Location: PIN_123,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[21]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(21),
	oe => VCC,
	padio => ww_PHASE(21));

-- Location: PIN_124,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[20]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(20),
	oe => VCC,
	padio => ww_PHASE(20));

-- Location: PIN_112,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[19]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(19),
	oe => VCC,
	padio => ww_PHASE(19));

-- Location: PIN_108,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[18]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(18),
	oe => VCC,
	padio => ww_PHASE(18));

-- Location: PIN_113,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[17]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(17),
	oe => VCC,
	padio => ww_PHASE(17));

-- Location: PIN_114,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[16]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(16),
	oe => VCC,
	padio => ww_PHASE(16));

-- Location: PIN_111,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[15]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(15),
	oe => VCC,
	padio => ww_PHASE(15));

-- Location: PIN_97,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[14]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(14),
	oe => VCC,
	padio => ww_PHASE(14));

-- Location: PIN_101,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[13]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(13),
	oe => VCC,
	padio => ww_PHASE(13));

-- Location: PIN_93,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[12]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(12),
	oe => VCC,
	padio => ww_PHASE(12));

-- Location: PIN_102,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[11]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(11),
	oe => VCC,
	padio => ww_PHASE(11));

-- Location: PIN_107,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[10]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(10),
	oe => VCC,
	padio => ww_PHASE(10));

-- Location: PIN_106,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[9]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(9),
	oe => VCC,
	padio => ww_PHASE(9));

-- Location: PIN_104,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[8]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(8),
	oe => VCC,
	padio => ww_PHASE(8));

-- Location: PIN_103,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[7]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(7),
	oe => VCC,
	padio => ww_PHASE(7));

-- Location: PIN_105,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[6]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(6),
	oe => VCC,
	padio => ww_PHASE(6));

-- Location: PIN_94,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[5]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(5),
	oe => VCC,
	padio => ww_PHASE(5));

-- Location: PIN_121,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[4]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(4),
	oe => VCC,
	padio => ww_PHASE(4));

-- Location: PIN_127,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[3]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(3),
	oe => VCC,
	padio => ww_PHASE(3));

-- Location: PIN_117,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[2]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(2),
	oe => VCC,
	padio => ww_PHASE(2));

-- Location: PIN_118,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[1]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(1),
	oe => VCC,
	padio => ww_PHASE(1));

-- Location: PIN_120,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PHASE[0]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst1|SUM\(0),
	oe => VCC,
	padio => ww_PHASE(0));

-- Location: PIN_59,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[15]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(15),
	oe => VCC,
	padio => ww_PRECOUNT(15));

-- Location: PIN_119,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[14]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(14),
	oe => VCC,
	padio => ww_PRECOUNT(14));

-- Location: PIN_62,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[13]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(13),
	oe => VCC,
	padio => ww_PRECOUNT(13));

-- Location: PIN_61,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[12]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(12),
	oe => VCC,
	padio => ww_PRECOUNT(12));

-- Location: PIN_63,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[11]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(11),
	oe => VCC,
	padio => ww_PRECOUNT(11));

-- Location: PIN_55,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[10]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(10),
	oe => VCC,
	padio => ww_PRECOUNT(10));

-- Location: PIN_50,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[9]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(9),
	oe => VCC,
	padio => ww_PRECOUNT(9));

-- Location: PIN_48,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[8]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(8),
	oe => VCC,
	padio => ww_PRECOUNT(8));

-- Location: PIN_51,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[7]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(7),
	oe => VCC,
	padio => ww_PRECOUNT(7));

-- Location: PIN_138,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[6]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(6),
	oe => VCC,
	padio => ww_PRECOUNT(6));

-- Location: PIN_16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[5]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(5),
	oe => VCC,
	padio => ww_PRECOUNT(5));

-- Location: PIN_137,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[4]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(4),
	oe => VCC,
	padio => ww_PRECOUNT(4));

-- Location: PIN_12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[3]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(3),
	oe => VCC,
	padio => ww_PRECOUNT(3));

-- Location: PIN_45,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[2]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(2),
	oe => VCC,
	padio => ww_PRECOUNT(2));

-- Location: PIN_53,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[1]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(1),
	oe => VCC,
	padio => ww_PRECOUNT(1));

-- Location: PIN_60,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PRECOUNT[0]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \inst6|STEMP\(0),
	oe => VCC,
	padio => ww_PRECOUNT(0));
END structure;


