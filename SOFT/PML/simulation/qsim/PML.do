onerror {exit -code 1}
vlib work
vcom -work work PML.vho
vcom -work work WAVSIM.vwf.vht
vsim  -c -t 1ps -sdfmax PML_vhd_vec_tst/i1=PML_vhd.sdo -L maxii -L altera -L altera_mf -L 220model -L sgate -L altera_lnsim work.PML_vhd_vec_tst
vcd file -direction PML.msim.vcd
vcd add -internal PML_vhd_vec_tst/*
vcd add -internal PML_vhd_vec_tst/i1/*
proc simTimestamp {} {
    echo "Simulation time: $::now ps"
    if { [string equal running [runStatus]] } {
        after 2500 simTimestamp
    }
}
after 2500 simTimestamp
run -all
quit -f














































