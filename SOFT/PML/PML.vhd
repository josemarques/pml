Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

use ieee.numeric_std.all;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;

--Phase measurement logic element
 ENTITY PML IS
 generic(N: integer :=32);--Number of bits of the detector
 PORT (FRQ0: IN std_logic;
 FRQ1: IN std_logic;
 PHASE: OUT std_logic_vector(N-1 downto 0)
 );
 END ENTITY;

ARCHITECTURE PROP_DELAY_TIMER OF PML IS

signal CTEMP : std_logic_vector(N-1 downto 0);--Control timer
signal STEMP : std_logic_vector(N-1 downto 0);--Signal timer
signal PHSPLS: std_logic;--Phase pulse

signal PHASEREG : std_logic_vector(N-1 downto 0);--Phase register

ATTRIBUTE keep: BOOLEAN;

ATTRIBUTE keep OF CTEMP, STEMP, PHSPLS: SIGNAL IS TRUE;
 BEGIN
 PHSPLS<=FRQ0 AND FRQ1;

--NOTDELAYC
CTEMP(N-1)<= NOT PHSPLS;
NOTDELAYC : for i in N-2 downto 0 generate
    CTEMP(i) <= NOT CTEMP(i+1);
end generate;    

--NOTDELAYS
STEMP(N-1)<= NOT FRQ0; 
NOTDELAYS : for i in N-2 downto 0 generate
    STEMP(i) <= NOT STEMP(i+1);
end generate; 

--PHASE<=std_logic_vector(unsigned(CTEMP)-unsigned(STEMP));
PHASE<=STEMP;
 END ARCHITECTURE;