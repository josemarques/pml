Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

use ieee.numeric_std.all;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;

--Propagation delay measurement logic
 ENTITY PDML IS
 generic(N: integer :=32);--Number of bits of the detector
 PORT (PULS: IN std_logic;
RESET: IN std_logic;
 PROP: OUT std_logic_vector(N-1 downto 0)
 );
 END ENTITY;

ARCHITECTURE PROP_DELAY_TIMER OF PDML IS
signal PHSS: std_logic;--Phase pulse
signal STEMP : std_logic_vector(N-1 downto 0);--Signal timer

ATTRIBUTE keep: BOOLEAN;

ATTRIBUTE keep OF STEMP: SIGNAL IS TRUE;
 BEGIN

 PHSS<=PULS AND NOT RESET;
--NOTDELAYS
STEMP(N-1)<= NOT PHSS ; 
NOTDELAYS : for i in N-2 downto 0 generate
    STEMP(i) <= NOT STEMP(i+1);
end generate; 

PROP<=STEMP;

END ARCHITECTURE;