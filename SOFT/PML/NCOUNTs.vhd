--Dated 05/August/2019
--https://www.engineersgarage.com/n-bit-gray-counter-using-vhdl/
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY NCOUNTs IS
  GENERIC (N: integer := 32);
  PORT (FRQSIG, RESET: IN std_logic;
        FRQCNT: OUT std_logic_vector (N-1 DOWNTO 0));
END NCOUNTs;

ARCHITECTURE BINARYCNT OF NCOUNTs IS
  SIGNAL COUNT: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'0');
BEGIN
	FRQCNT<=COUNT;
 PROCESS (FRQSIG)
  BEGIN
    IF (FRQSIG = '1' AND FRQSIG'EVENT) THEN
		IF (RESET = '1') THEN
			COUNT <= (OTHERS =>'0');
		ELSE
			COUNT  <= std_logic_vector(unsigned(COUNT)+1);
		END IF;
		
      
        
    END IF;
  END PROCESS;
END BINARYCNT;